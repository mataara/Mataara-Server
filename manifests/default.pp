# Setup internal constants for where things will be.

# APP should be a machine-readable slug, not a display title.  It should
# probably stay "archimedes" for now, but may change to "mataara" later.
$APP = "archimedes"

$share_dir = "/usr/share/${APP}"
$static_dir = "/usr/share/${APP}/static/"
$venv_dir = "/vagrant/venv"
$frontend_dir = "/vagrant/${APP}/frontend"

include apt

# Setup repository for latest LTS version of nodejs
apt::key { 'nodesource':
    ensure     => present,
    id		=> '9FD3B784BC1C6FC31A8A0A1C1655A0AB68576280',
    source	=> 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key',
}
->
apt::source { 'nodejs':
  ensure	=> present,
  location	=> 'https://deb.nodesource.com/node_12.x',
  release	=> $::lsbdistcodename,
  require	=> Apt::Key['nodesource'],
}
->
# Ensure that nodejs is installed from the repo, which will automatically install npm
package {['nodejs']:
  ensure => latest,
  require => Exec['apt_update'],
}
->
exec { 'js modules':
  # Install JS modules for ReactJS so that they are there for the npm run.
  cwd => $frontend_dir,
  user => 'vagrant',
  command => "/usr/bin/npm install",
}

# Create key directories
file { [$share_dir, $static_dir]:
  ensure => directory,
  owner  => 'vagrant',
  group  => 'vagrant',
}

# Note, we can't set the ownership on the venv directory because it's mapped by Vagrant
file { [$venv_dir]:
  ensure => directory,
}

## POSTGRES CONFIG
package {['postgresql']:
  ensure => latest,
  notify => [Service['postgresql'], Exec['create-postgres-user'], Exec['reload-devserver']],
  require => Exec['apt_update']
}
->
service {'postgresql':
  ensure => running
}
->
exec {'create-postgres-user':
  require => [Package['postgresql'], Service['postgresql']],
  refreshonly => true,
  command => "/bin/su - postgres -c 'createuser vagrant -d'",
}

## Ubuntu packages needed to support SAML
package {['xmlsec1']:
  ensure => latest,
  require => Exec['apt_update']
}

## Ubuntu packages needed to install Python packages with native extensions
## or to build binary wheels of the extensions, or run the application itself
# python3-dev is required for lots of packages
# libldap2-dev, libsasl2-dev is required to build pyldap package
# libpq-dev is required to build the psycopg2 wheel
# libssl-dev, libffi-dev for cryptography package
# gcc is needed to build some packages (in Xenial we need to explicitly install it)
# python3-openssl is needed for pip (Ubuntu Xenial onwards)
package {['gcc', 'python3-setuptools', 'shared-mime-info', 'libpq-dev', 'python3-dev', 'libldap2-dev', 'libsasl2-dev', 'libssl-dev', 'libffi-dev', 'python3-openssl']:
  ensure => latest,
  require => Exec['apt_update']
}

## Packages we need to develop and run the application in vagrant
# libxml2 and libxslt1.1 are required by nginx
package {['python3-all', 'supervisor', 'libxml2', 'libxslt1.1']:
  ensure => latest,
  require => Exec['apt_update']
}
->
## Python Package Index Dependencies
exec {'pip-install-python-libraries':
  command => "/vagrant/bin/prep-vagrant-venv.sh",
  # Takes a long time to install all the dependencies so need more time than the default puppet timeout of 300
  timeout => 1800,
  require => [File[$share_dir], File[$venv_dir]],
  user => 'vagrant'
}

file {'/home/vagrant/.bash_aliases':
  content => "
## Yes, we should be editing the path in .bashrc not the aliases file.
## This approach is easier for a quick fix.
export PATH=${venv_dir}/bin:\${PATH}
",
  owner  => vagrant,
  group  => vagrant,
}

file { '/etc/localtime':
  ensure => 'link',
  target => '/usr/share/zoneinfo/Pacific/Auckland',
}

## Ensure vagrant has permission to modify the supervisor socket

group { 'supervisor':
  ensure => present
}
->
user { 'vagrant':
  groups => ['supervisor', 'adm']
}

file { '/etc/supervisor/supervisord.conf':
  source => '/vagrant/manifests/files/supervisord.conf',
  owner   => 'root',
  require => Package['supervisor'],
  notify  => Service['supervisor'],
}

file {"/etc/supervisor/conf.d/${APP}.conf":
  content => "
[program:gunicorn-${APP}]
command=${venv_dir}/bin/gunicorn --log-level=DEBUG ${APP}.wsgi --bind=0.0.0.0:4005 --reload --workers 2
user=vagrant
numprocs=1
killasgroup=true
stdout_logfile=/var/log/supervisor/gunicorn-${APP}-stdout.log
stderr_logfile=/var/log/supervisor/gunicorn-${APP}-stderr.log

[program:celery-${APP}]
command=${venv_dir}/bin/celery --app=${APP}.celery --no-color worker --loglevel=DEBUG  --autoscale=4,2
user=vagrant
stopsignal=INT
numprocs=1
killasgroup=true
environment=AUTOSCALE_KEEPALIVE=\"120\"
stdout_logfile=/var/log/supervisor/celery-${APP}-stdout.log
stderr_logfile=/var/log/supervisor/celery-${APP}-stderr.log

[program:beat-${APP}]
command=${venv_dir}/bin/celery --app=${APP}.celery --no-color beat --pidfile=
user=vagrant
numprocs=1
killasgroup=true
stdout_logfile=/var/log/supervisor/beat-${APP}-stdout.log
stderr_logfile=/var/log/supervisor/beat-${APP}-stderr.log
",
  owner => root,
  require => File['/etc/supervisor/supervisord.conf'],
  notify  => Service['supervisor'],
}

service {'supervisor':
  ensure  => running,
  require => [
              Group['supervisor'],
              Package['supervisor'],
              Service['redis-server'],
              Service['postgresql'],
              File["/etc/supervisor/conf.d/${APP}.conf"],
              File["/etc/${APP}/${APP}.ini"],
              File['/etc/supervisor/supervisord.conf'],
              # Ensure necessary Python dependencies are installed for supervisor
              # to be able to start the applications services
              Exec['pip-install-python-libraries']
              ],
}

## APPLICATION CONFIGURATION FILE
## (stored in the 'conf' directory so it can be edited from the host)
file {"/vagrant/conf/${APP}.ini.vagrant":
  content => template("${APP}/${APP}.ini.vagrant.erb")
}
->
file {"/etc/${APP}":
  ensure => directory,
  mode   => '0755',
  owner  => vagrant,
  group  => vagrant,
}
->
file {"/etc/${APP}/${APP}.ini":
  ensure  => link,
  target  => "/vagrant/conf/${APP}.ini.vagrant",
}

file {"/usr/bin/${APP}":
  ensure  => link,
  target  => "${venv_dir}/bin/${APP}",
}

## Redis
package { 'redis-server':
  ensure  => latest,
  require => Exec['apt_update']
}
->
service { 'redis-server':
  ensure => running
}

## Use Nginx to proxy requests to Gunicorn so we can test X-Request-Id
package { 'nginx':
  ensure  => latest,
  require => Exec['apt_update']
}
->
file { '/etc/nginx/sites-available/default':
  ensure  => file,
  owner   => root,
  group   => root,
  notify  => Service['nginx'],
  content => "
upstream gunicorn {
    server localhost:4005;
}

server {
    listen 8008;
    client_max_body_size 20M;
    location / {
        proxy_pass http://gunicorn;
        proxy_set_header X-Request-Id \$pid-\$msec-\$remote_addr-\$request_length;
        proxy_set_header Host \$host:8008;
    }

    location /static/ {
        alias ${static_dir};
    }
}
"
}
->
file { '/etc/nginx/sites-enabled/default':
  ensure => link,
  target => '/etc/nginx/sites-available/default',
  notify => Service['nginx']
}
->
service { 'nginx':
  ensure  => running,
  require => Package['nginx']
}

# Create public/private keypair
exec { 'create_key_dir':
  command => '/bin/mkdir -p /vagrant/reportkey',
  creates => '/vagrant/reportkey'
}
->
exec { 'create_private_key':
  command => '/usr/bin/openssl genrsa -out /vagrant/reportkey/private.pem',
  creates => '/vagrant/reportkey/private.pem'
}
->
exec { 'create_public_key':
  command => '/usr/bin/openssl rsa -in /vagrant/reportkey/private.pem -outform PEM -pubout -out /vagrant/reportkey/public.pem',
  creates => '/vagrant/reportkey/public.pem'
}
->
exec { 'create_public_cert':
  command => '/usr/bin/printf "\n\n\n\n\n\n\n" | openssl req -new -x509 -key /vagrant/reportkey/private.pem -out /vagrant/reportkey/public.cer',
  creates => '/vagrant/reportkey/public.cer'
}

## DATABASE RESET, after application and supervisor have been fully installed, but not yet running
## Note, it runs 'supervisorctl start all' after finishing the application configuration
exec {'reload-devserver':
  refreshonly => true,
  user        => 'vagrant',
  command     => '/vagrant/bin/reload-devserver.sh',
  require     => [
                  File[$share_dir, $static_dir],        # Can't collectstatic if the directory does not exist
                  Exec['pip-install-python-libraries'], # ensure necessary Python dependencies are installed
                  Service['postgresql'],                # ensure database service is running
                  Exec['create-postgres-user'],         # ensure Postgres user and database exist
                  Service['supervisor'],                # ensure supervisor service is running
                  ],
}
