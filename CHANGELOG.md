# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Template
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [Unreleases]
### Added
### Changed


## 0.1.20190628.2
### Added
- Added support for anonymous API Keys via djangorestframework-api-key.
- Added drupalsitereport console command


## 0.1.20190624.2
### Added
- Upgraded to Django 1.11.
- Django admin can now filter sites by environment or decommissioned status.
### Changed


## 0.1.20190520.1

### Added
- CLI syncreleases command has "--now" flag for immediate use, otherwise spawns a background task.

### Changed
- Supervisor config in Puppet now uses "[program:{gunicorn,celery,beat}-APP]" format.
- Supervisor config also explicitly names logfiles as "/var/log/supervisor/name-APP-{stdout,stderr}.log"
- UserProfile model now has a generic usersettings JSONField, for storing frontend preferences.
- Moving to an explicit requirement for Postgresql, so we can do JSONFields without breaking tests
- Splitting out syncreleases task into individual Celery tasks for each project.


## 0.1.20190416.1

A major upgrade of Celery here, getting us a lot closer to upping the Django version.

Virtual environments in development VMs will need to be recreated, since the upgrade path is messy.


### Changed
- Moved from django-celery package to celery<4.3.0 and django-celery-beat.
- django_mailbox updated in the process of recreating venv; may need an extra createmigrations/migrate.

## 0.1.20190226.1
### Added
- In Django Project admin, the ability to sync releases for individual projects; timestamps and filtering

### Changed

Again quite a lot of things. We have not been practising good change logging. So, a summary of the last year:

- SAML and LDAP integration into IDPs
- CI pipelines for code quality and regression testing
- Better error state logging for increased reliability in client and server
- Standardisation of development/deployment tasks with Fabric

- Groundwork for user-specific profile settings and a notification system
- Pulling data from Drupal.org APIs for comprehensive advisory/release coverage, rather than RSS-based updates
- Refactoring Mataara server APIs for better communication with external systems
- SilverStripe server module begun, with some system refactoring to better support non-Drupal platforms better
- Major refactoring of vulnerability matching to use Drupal.org release history API instead of Advisories, for more consistent/timely information
- Beginning work on Site Groups, to support collections of sites and integration with external information sources

- ReactJS-based Material UI frontend implemented
- Users can subscribe to sites and have them highlighted on the dashboard

### Removed
- Completed retirement and removal of old AngularJS code


## 0.1.20180830.1
### Added
- Drupal advisory parsing via API
- Users can now subscribe to particular sites of interest

### Changed
- Quite a lot of things. The ReactJS frontend has been well and truly revamped.

### Deprecated
- Old Drupal Advisory RSS feeds


## 0.1.20180517
### Added
- Fabric commands for showing Django logs in development and reload the server
- Fabric commands serving the ReactJS frontend in development
- Added a login page
- Updates for parsing of Drupal advisories
### Changed
- Fabric runs ReactJS tests
- Changed base template so that it does not use Angular
### Deprecated
- Fabric run command as it is not needed.
- Documentation has been updated
- Vagrant provisioning network setting configurable
- Vagrant puppet manifests to fix dependancies
### Removed
- Tests for the old AngularJS views
- URLS that went to the old AngularJS views.
### Fixed
- Gitlab CI environment was failing tests

## 0.1.20180326
### Added
- Added CI to run some tests and build wheels automatically
### Changed
- Updated some Python modules

## 0.1.20180321
### Added
- Optimised wheel building to include creation of JavaScript bundles
### Changed
- JavaScript bundle compilation for production to match development
### Removed
- Bower JavaScript files and djangobower related code
- JavaScript compiled bundles

## 0.1.20180315
### Added
- Add ability for users to (un)subscribe to sites

## 0.1.20171115
### Fixed
- Some bug fixes to the API as used by the ReactJS pages

## 0.1.20171016
### Added
- added bundles dir
- added webpack-stats.json

## 0.1.20170725
### Added
- Added SAML authentication support
- Added initial SilverStripe support

## 0.1.20170612
### Removed
- Legacy Jenkins integration support

## 0.1.20170519
### Added
- Special handling function for Drupal security advisories that are badly formed

## 0.1.20170516
### Added
- Handle Drupal security advisories that affect all versions of a module

## 0.1.20170503
### Added
- Command-line option to configure an existing user as an superuser. Especially useful in development
### Changed
- Updates to development and build process to support to use of a build agent on deployment server
- Detect advisory parsing failure earlier and set failed flag
- Make vagrant managed venv directly accessible from host
- Upgrade built-in bootstrap version of pip used in vagrant
### Removed
- Fabric tasks related to publishing the wheelhouse ready for deployments

## 0.1-20170405
### Changed
- Finished upgrading all dependencies to latest versions

## 0.1-20170331
### Added
- Added new fabric task and command for initialising advisory sources

### Changed
- Bugfix in pagination in views

## 0.1-20170330
### Changed
- Updated Django API modules

## 0.1-20170328
### Added
- Add support for pip-tools within development environment and start upgrading dependencies
- Add user profile model

## 0.1-20170320
### Added
- Add support for configuration file checking within the Vagrantfile

### Changed
- Switch from Catalyst specific LDAP module to django_auth_LDAP
