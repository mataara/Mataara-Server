

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Sanity check.
Vagrant.require_version ">= 2.0.0"

# Check YAML config settings
PUPPET_HIERA_DIR = File.join(File.dirname(__FILE__), "conf/hiera")
PUPPET_HIERA_FILE = File.join(PUPPET_HIERA_DIR, "config.yaml")
MIN_HIERA_CONFIG_VERSION = 3

unless File.exist?(PUPPET_HIERA_FILE)
  abort "No Puppet settings file found for your VM!\n\n"\
    "Please copy conf/hiera/config.yaml.example to conf/hiera/config.yaml\n"\
    "and check the values, then run 'vagrant provision'."
end

vagrant_settings = YAML.load_file(PUPPET_HIERA_FILE)
version_check = vagrant_settings['version'].to_i

if not version_check >= MIN_HIERA_CONFIG_VERSION
  abort "Error - old puppet, yaml, setting file found for your VM!\n\n"\
    "Found version " + version_check.to_s + " Need a minimum of " + MIN_HIERA_CONFIG_VERSION.to_s + "\n"\
    "Please check config/hiera/config.yaml.example for anything new that needs\n"\
    "adding into your config.yaml and then update the vagrant version, or just replace the file.\n"\
    "Then run 'vagrant provision'"
end

# Check for server hostname override in config.
if vagrant_settings['vagrant']['server_hostname']
  server_hostname = vagrant_settings['vagrant']['server_hostname']
else
  server_hostname = "mataara-server.local"
end


# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a base box to build from.
  config.vm.box = "ubuntu/xenial64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080
  # config.vm.network "forwarded_port", guest: 8008, host: 8008
  # Forward ssh to separate ports on the host box - run mult. vms
  # config.vm.network "forwarded_port", guest: 22, host: 2208
  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "forwarded_port", guest: 22, host: vagrant_settings['vagrant']['ssh_port'], id: 'ssh'
  config.vm.network "private_network", ip: vagrant_settings['vagrant']['ip_address']

  # Host name
  config.vm.hostname = server_hostname

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"
  # config.vm.network "public_network", ip: "192.168.0.17"


  # If true, then any SSH connections made will enable agent forwarding.
  # Default value: false
  # config.ssh.forward_agent = true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  config.vm.synced_folder ".", "/vagrant"

  # Sync our hiera configuration to /var/lib/hiera (as per our hiera_config.yaml)
  config.vm.synced_folder PUPPET_HIERA_DIR, "/var/lib/hiera"


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Don't boot with headless mode:
    # vb.gui = true
    #
    # Increase standard memory:
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end

  $script = <<SCRIPT
# Install puppet (if necessary)
dpkg -l | grep -q puppet || apt-get update && apt-get install -y puppet

mkdir -p /etc/puppet/modules
# Install PuppetLabs-APT module to support installing nodejs repo
puppet module list | grep -q puppetlabs-apt || puppet module install puppetlabs/apt --version 2.4.0
if [ $? -gt 0 ] ; then
  echo "ERROR - Installation of puppet module failed. Puppet provisioning will fail. Please check the error message and resolve.";
fi
SCRIPT

  config.vm.provision "shell" do |shell|
    shell.inline = $script
  end

  config.vm.provision "puppet" do |puppet|
    puppet.hiera_config_path  = "hiera_config.yaml"
    puppet.module_path = "puppet/modules"
    puppet.manifests_path = "manifests"
    puppet.manifest_file  = "default.pp"
  end

  # In case supervisor has tried to run before the shared dir with the Python
  # virtual environment has arrived, explicitly start it.
  config.vm.provision "shell", inline: "supervisorctl start all", run: "always"
  end
