# -*- coding: utf-8 -*-

import re

from django.conf import settings
from django.contrib.auth.decorators import login_required

# For transition in upgrade to settings.MIDDLEWARE
try:
    from django.utils.deprecation import MiddlewareMixin
except ImportError:
    MiddlewareMixin = object


class RequireLoginMiddleware(MiddlewareMixin):
    """
    Middleware component that wraps the login_required decorator around matching URL patterns.

    To use, add the class to MIDDLEWARE_CLASSES and define LOGIN_REQUIRED_URLS
    and LOGIN_REQUIRED_URLS_EXCEPTIONS in your settings.py. For example::

        LOGIN_REQUIRED_URLS = (
            r'/topsecret/(.*)$',
        )
        LOGIN_REQUIRED_URLS_EXCEPTIONS = (
            r'/topsecret/login(.*)$',
            r'/topsecret/logout(.*)$',
        )


    LOGIN_REQUIRED_URLS is where you define URL patterns; each pattern must
    be a valid regex.

    LOGIN_REQUIRED_URLS_EXCEPTIONS is, conversely, where you explicitly
    define any exceptions (like login and logout URLs).

    Reference:

      `Best way to make Django's login_required the default
      <http://stackoverflow.com/questions/2164069/best-way-to-make-djangos-login-required-the-default>`__
      at Stack Overflow

    """
    def __init__(self, get_response):
        self.get_response = get_response
        self.required = tuple(re.compile(url) for url in settings.LOGIN_REQUIRED_URLS)
        self.exceptions = tuple(re.compile(url) for url in settings.LOGIN_REQUIRED_URLS_EXCEPTIONS)

    def process_view(self, request, view_func, view_args, view_kwargs):
        # No need to process URLs if user already logged in
        if request.user.is_authenticated:
            return None

        # An exception match should immediately pass through.
        for url in self.exceptions:
            if url.match(request.path):
                return None

        # Requests matching a restricted URL pattern are returned wrapped
        # with the login_required decorator.
        for url in self.required:
            if url.match(request.path):
                return login_required(view_func)(request, *view_args, **view_kwargs)

        # Explicitly pass through for all non-login-required requests.
        return None
