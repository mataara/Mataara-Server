from celery.utils.log import get_task_logger
from django.core.management import call_command

from archimedes.celery import app
from .models import Advisory
from .parser import AdvisoryParser

logger = get_task_logger(__name__)


@app.task
def import_advisories():
    """Import advisories from all RSS sources."""
    call_command('importadvisories')  # pragma: no cover


@app.task
def parse_advisories():
    """Parse all available advisories."""
    call_command('parseadvisories')  # pragma: no cover


@app.task
def parse_advisory(advisory_pk, force=False):
    """If an advisory has been parsed, output a message on the logger and return,\
    otherwise, give the advisory to AdvisoryParser.parse() """
    # Fetch the advisory from the database
    advisory = Advisory.objects.get(pk=advisory_pk)

    # Don't re-parse advisories unless forced to.
    if advisory.date_parsed and not force:
        logger.warning('Advisory #%d already parsed.' % advisory_pk)
        return

    logger.info('Parsing advisory #%s...' % advisory_pk)
    parser = AdvisoryParser()
    parse_status = parser.parse(advisory, forcedownload=False)
    # If parsing unsuccessful, set a failed status flag.
    if parse_status is False:
        logger.error('Parsing advisory #%s failed.' % advisory_pk)
        advisory.failed = True
        advisory.save()
    else:
        logger.info("Advisory #%s parsed." % advisory_pk)

    return parse_status
