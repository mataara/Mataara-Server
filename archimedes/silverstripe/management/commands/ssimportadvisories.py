from django.core.management.base import BaseCommand, CommandError

from ...models import AdvisorySource
from ...utils import import_advisories


class Command(BaseCommand):
    help = 'Import security advisories'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('sources', nargs='*', type=str, help='advisory source slug(s) to import from')

        # Named (optional) arguments
        parser.add_argument(
            '--file',
            action='append',
            dest='files',
            default=[],
            help="XML file(s) to import from, instead of the source's configured URL"
        )

    def handle(self, *args, **options):
        # Validate advisory sources
        sources = AdvisorySource.objects.filter(slug__in=options['sources'])
        if len(sources) != len(options['sources']):
            for s in sources:
                if s.slug in options['sources']:
                    options['sources'].remove(s.slug)
            raise CommandError('Could not find advisory source(s): %s' % ', '.join(options['sources']))

        # If no sources specified, import all
        if len(sources) == 0:
            sources = AdvisorySource.objects.all()

        # If filenames have been specified, match them to sources
        if options['files'] and len(options['files']) != len(sources):
            raise CommandError(
                'Mismatch - the number of XML files specified (%d) does not match the number of sources (%d)'
                % (len(options['files']), len(sources))
            )

        # Import advisories
        for i, s in enumerate(sources):

            # If filename specified, read XML
            if options['files']:
                try:
                    with open(options['files'][i], 'r') as f:
                        xml = f.read()
                except IOError as e:
                    raise CommandError(e)
            else:
                xml = None

            self.stdout.write('Importing advisories from the "%s" source' % s.title)
            import_advisories(s, xml)
