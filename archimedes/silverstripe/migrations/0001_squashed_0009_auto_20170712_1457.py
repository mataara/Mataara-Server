# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    #replaces = [('silverstripe', '0001_initial'), ('silverstripe', '0002_advisorysource_risk_ssadvisory'), ('silverstripe', '0003_auto_20170711_1312'), ('silverstripe', '0004_auto_20170711_1338'), ('silverstripe', '0005_auto_20170711_1608'), ('silverstripe', '0006_auto_20170711_1713'), ('silverstripe', '0007_silverstripesite_core'), ('silverstripe', '0008_auto_20170712_1341'), ('silverstripe', '0009_auto_20170712_1457')]

    dependencies = [
        ('sites', '0012_site_decommissioned'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SilverStripeSite',
            fields=[
                ('site_ptr', models.OneToOneField(to='sites.Site', primary_key=True, auto_created=True, parent_link=True, serialize=False, on_delete=models.CASCADE)),
                ('users', models.PositiveIntegerField(null=True, blank=True, help_text='Number of users on site')),
            ],
            options={
                'abstract': False,
            },
            bases=('sites.site',),
        ),
        migrations.CreateModel(
            name='AdvisorySource',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('title', models.CharField(unique=True, max_length=200, help_text='Enter the name for this advisory source.')),
                ('slug', models.SlugField(unique=True, editable=False)),
                ('url', models.URLField(unique=True, help_text='Enter URL for the advisory source. Only http(s):// or ftp(s):// are supported.')),
            ],
        ),
        migrations.CreateModel(
            name='Advisory',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('guid', models.CharField(unique=True, max_length=200)),
                ('title', models.CharField(max_length=200)),
                ('url', models.URLField()),
                ('date_posted', models.DateTimeField()),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('date_parsed', models.DateTimeField(null=True, blank=True)),
                ('ignored', models.BooleanField(default=False)),
                ('failed', models.BooleanField(default=False)),
                ('status', models.CharField(choices=[('downloaded', 'Downloaded'), ('parsed', 'Needs Review'), ('reviewed', 'Reviewed'), ('ignored', 'Ignored'), ('failed', 'Parsing Failed')], default='downloaded', max_length=50)),
                ('description', models.TextField()),
                ('advisory_id', models.CharField(max_length=200, blank=True)),
                ('affected_sites', models.ManyToManyField(to='silverstripe.SilverStripeSite')),
                ('reviewer', models.ForeignKey(null=True, related_name='ss_advisory', to=settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE)),
                ('source', models.ForeignKey(default=None, to='silverstripe.AdvisorySource', on_delete=models.CASCADE)),
                ('raw_summary', models.TextField(default='')),
                ('release_date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'ordering': ['-date_posted'],
                'verbose_name_plural': 'Advisories',
            },
        ),
        #  migrations.RunSQL(
            #  sql='SET CONSTRAINTS ALL IMMEDIATE',
            #  reverse_sql='',
        #  ),
        migrations.AddField(
            model_name='advisory',
            name='risk',
            field=models.CharField(choices=[('low', 'Low'), ('moderate', 'Moderate'), ('important', 'Important'), ('critical', 'Critical')], default='', max_length=50),
            preserve_default=False,
        ),
        #  migrations.RunSQL(
            #  sql='',
            #  reverse_sql='SET CONSTRAINTS ALL IMMEDIATE',
        #  ),
        migrations.AddField(
            model_name='silverstripesite',
            name='core',
            field=models.CharField(default=0, max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='advisory',
            name='versions_affected',
            field=models.CharField(null=True, max_length=50, blank=True),
        ),
        migrations.AddField(
            model_name='advisory',
            name='versions_fixed',
            field=models.CharField(null=True, max_length=50, blank=True),
        ),
        migrations.RemoveField(
            model_name='advisory',
            name='date_updated',
        ),
        migrations.RemoveField(
            model_name='advisory',
            name='release_date',
        ),
        migrations.AlterField(
            model_name='advisory',
            name='date_posted',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
