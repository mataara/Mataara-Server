from django.contrib import admin
from django.core.management import call_command
from polymorphic.admin import PolymorphicChildModelAdmin

from . import models


@admin.register(models.Advisory)
class AdvisoryAdmin(admin.ModelAdmin):
    search_fields = ('title', 'advisory_id', 'project__slug')
    readonly_fields = ('status',)
    list_display = ('title', 'status', 'url', 'risk')
    actions = ['parse_advisories']

    def parse_advisories(self, request, queryset):
        from archimedes.silverstripe.tasks import parse_advisory

        for obj in queryset:
            parse_advisory.delay(obj.pk, force=True)

        self.message_user(request, "Submitted advisory(ies) for processing")

    parse_advisories.short_description = "Schedule advisories for parsing"


@admin.register(models.AdvisorySource)
class AdvisorySourceAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'url')
    actions = ['import_advisories']

    def import_advisories(self, request, queryset):
        for obj in queryset:
            call_command('ssimportadvisories', obj.slug)

        self.message_user(request, 'Importing advisories from the "%s" source' % obj.slug)

    import_advisories.short_description = "Import advisories"


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    search_fields = ('name', 'slug', 'type', )
    list_display = ('slug', 'name', 'url', 'type')


@admin.register(models.Release)
class ReleaseAdmin(admin.ModelAdmin):
    search_fields = ('version', 'project__slug')
    list_display = ('__str__', 'project', 'version', 'url')


@admin.register(models.SilverStripeSite)
class SilverStripeSiteAdmin(PolymorphicChildModelAdmin):
    base_model = models.SilverStripeSite
    list_display = ('__str__', 'environment', 'url', 'hostname', 'root', 'title')
