from rest_framework import serializers

from archimedes.sites.api.serializers import SiteSerializer

from .. import models


class SilverStripeSiteSerializer(SiteSerializer):
    # This is the default serializer for models.IntegerField, so it is
    # not actually necessary.
    users = serializers.IntegerField(read_only=True)

    # TODO: Add field-specific custom serializers.

    class Meta:
        model = models.SilverStripeSite
        # TODO: Add fields, comma-separated.
        fields = SiteSerializer.Meta.fields + (
            'users',
        )


class AdvisorySourceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.AdvisorySource
        fields = (
            'title',
            'slug',
            'url'
        )


class AdvisorySerializer(serializers.HyperlinkedModelSerializer):
    source = serializers.StringRelatedField()
    pk = serializers.IntegerField(read_only=True)
    # Expose affected sites property for site counts.
    #  affected_sites = SiteIndexSerializer(many=True, read_only=True)

    class Meta:
        model = models.Advisory
        fields = (
            'api_url',
            'pk',
            'guid',
            'title',
            'url',
            'date_posted',
            'date_parsed',
            'advisory_id',
            'risk',
            'source',
            'status',
            'affected_sites',
            'description',
        )
