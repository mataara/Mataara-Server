<dl class="dl-horizontal"><dt>Severity:</dt>
		<dd>Low (<a href="https://docs.silverstripe.org/en/contributing/release_process/#security-releases">?</a>)</dd>
	
	
		<dt class="identifier">Identifier:</dt><dd>SS-2017-004</dd>
	
	
		<dt class="versions-affected">Versions Affected:</dt><dd>3.4.5 and below, 3.5.0 to 3.5.3</dd>
	
	
		<dt class="versions-fixed">Versions Fixed:</dt><dd>3.4.6, 3.5.4, 3.6.0</dd>
	
	
		<dt class="release-date">Release Date:</dt><dd>2017-05-31</dd>
	
</dl><p><span>Authenticated user with page edit permission can craft HTML, which when rendered in a page history comparison can execute client scripts.</span></p>
<p><span>Credit to </span>Anti Räis for reporting this issue.</p>
