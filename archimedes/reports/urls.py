

from django.conf.urls import url

from .views import endpoint

app_name = 'reports-incoming'

urlpatterns = [
    url(r'^endpoint', endpoint, name='endpoint'),
    url(r'^legacy', endpoint, name='legacy_endpoint')
]
