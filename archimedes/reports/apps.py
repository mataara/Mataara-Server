from django.apps import AppConfig


class ReportsConfig(AppConfig):
    name = "archimedes.reports"

    def ready(self):
        # Register mail signal handlers
        import archimedes.reports.mail  # noqa : F401 - Ensures config is ready before registration.
