from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_api_key.permissions import HasAPIKey

from .. import models
from . import serializers


class ReportViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Reports to be viewed or edited.
    """
    permission_classes = [HasAPIKey | IsAuthenticated]
    queryset = models.Report.objects.all()
    serializer_class = serializers.ReportSerializer
    search_fields = ('name', 'site_key')
    filter_fields = ('type', 'site_key', 'processed_site')
