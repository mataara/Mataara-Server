import re
from base64 import b64decode
from random import uniform
from datetime import datetime

from raven.contrib.django.raven_compat.models import client as raven_client
from celery.utils.log import get_task_logger
from db_mutex.db_mutex import db_mutex
from db_mutex.exceptions import DBMutexError, DBMutexTimeoutError
from django.conf import settings
from django.core.management import call_command
from django_mailbox.models import Mailbox, Message

from archimedes.celery import app
from .exceptions import (ReportAlreadyReceivedError, ReportDecodeError,
                         ReportDecryptError, ReportsException)
from .models import Report

# Is Raven enabled? Assume it is if there is a RAVEN_CONFIG setting.
has_raven = hasattr(settings, 'RAVEN_CONFIG')

logger = get_task_logger(__name__)


@app.task
def getmail():
    """Runs the django-mailbox getmail management command"""
    # Suppress extra tasks if getmail has already been called within the last polling
    # interval. This helps to reduce the number of extra polls if the task queues got
    # behind and needed to catch up.
    #
    # TODO: In the long term we should probably separate out the mailbox polling into
    # its own single-item task queue so it doesn't have to wait for other processing
    # backlogs to clear, but that's a fire to fight another day.
    mbox = Mailbox.objects.get(name='archimedes')
    now = datetime.now(tz=mbox.last_polling.tzinfo)  # pytz.UTC
    seconds_since_poll = (now - mbox.last_polling).seconds

    if seconds_since_poll >= settings.MAILBOX_POLLING_INTERVAL:
        call_command('getmail')  # pragma: no cover


@app.task(default_retry_delay=settings.REPORT_PROCESSING_RETRY_DELAY,
          max_retries=settings.REPORT_PROCESSING_MAX_RETRIES)
def process(report_pk):
    """Processes each item in an incoming report, into the relevant site object"""

    # Fetch the report from the database
    try:
        report = Report.objects.get(pk=report_pk)
    except Report.DoesNotExist:
        # Warning: unsure why this happens just yet, but surfacing in logging for visibility.
        msg = 'Task attempted to process Report ID #{}, which could not be found in the database.'.format(report_pk)
        if has_raven:
            ident = raven_client.get_ident(raven_client.captureMessage(msg, level='warning'))
            logger.warning("Sentry ident: %s" % ident)
        logger.warning(msg)
        return

    # Don't reprocess reports
    if report.processed:
        logger.info('Report #%d already processed', report_pk)
        return

    # Only process one site key at once!
    # (to prevent a race-condtion where multiple sites are created with the same key)
    lock_id = 'lock-sitekey-{0}'.format(report.site_key)
    try:
        with db_mutex(lock_id):
            from archimedes.reports.utils import process_report
            process_report(report)

    except DBMutexError:
        logger.info('Scheduling retry for processing report #%d (could not acquire lock)', report_pk)

        # Retry with jitter
        raise process.retry(countdown=settings.REPORT_PROCESSING_RETRY_DELAY + int(uniform(0, 10)))  # nosec

    except DBMutexTimeoutError:
        logger.warning('Processing report #%d succeeded, but the lock timed out before it completed', report_pk)

    # Other exceptions - e.g. if process_report fails for some reason
    except Exception as exc:
        raise process.retry(exc=exc)


@app.task
def process_email(message_pk):

    try:
        # Fetch the email message from the database
        message = Message.objects.get(pk=message_pk)
    except Message.DoesNotExist:
        logger.warning('Message #%d does not exist.', message_pk)
        return False

    # Find the encoded attachments
    ekey = None
    encdata = None
    legacy = False
    for a in message.attachments.all():
        if not ekey and a.get_filename() == 'ekey.enc':
            ekey = a.document.read()
        if not encdata and a.get_filename() == 'data.enc':
            encdata = a.document.read()
        if not encdata and a.get_filename() == 'data.xml':
            if a.document.size == 0:
                err_msg = 'Legacy Message #{} (from: {}) has zero-length data.xml attachment'.format(
                    message.pk,
                    message.from_header
                )
                raise ReportsException(err_msg)
            encdata = a.document.read()
            legacy = True
            logger.info('Message #%d is in legacy format', message_pk)

    # If we found legacy data, grab the ekey from the message body
    if legacy:
        try:
            # Read the raw email message from disk
            # - this prevents django_mailbox from trying to b64decode the ekey/encdata,
            #   and from throwing decoding errors
            message.eml.open()
            raw = message.eml.read()
            message.eml.close()

            if type(raw) == bytes:
                # Coerce into string
                raw = raw.decode()

            # logger.debug(raw)
            # It's yucky, but it works because it only matches the exact Archimedes email format.
            ekey_b64 = re.split('\n\n', raw)[3].split('\n-------')[0]
            # Strip linefeed characters
            ekey_b64 = ekey_b64.translate(''.maketrans('', '', '\r\n'))

            ekey = b64decode(ekey_b64).replace(b'EKEY: ', b'')

        except IndexError:
            logger.warning('EKEY not found in legacy message #%d.', message_pk)
            return False

    # Stop if attachments not found
    if not ekey or not encdata and not legacy:
        logger.warning('Attachments not found in message #%d.', message_pk)
        message.delete()
        return False

    # Decode and save the report
    try:
        from archimedes.reports.utils import mail_decode_and_save
        mail_decode_and_save(ekey, encdata, legacy=legacy)
        logger.info('Message #%d decoded and saved', message_pk)
    except (ReportDecodeError, ReportDecryptError) as exc:
        # Leave the message in the system
        logger.error('Unable to decode/decrypt the report in message #%d', message_pk)
        logger.error(exc)
        return False
    except ReportAlreadyReceivedError:
        # Silence, the message will be deleted
        logger.warning('Message #%d contains a report we have already received', message_pk)
        pass

    # Delete the message
    message.delete()
    logger.info('Message #%d deleted', message_pk)
    return True
