# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0003_auto_20151126_1143'),
        ('reports', '0003_report_processed'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='processed_site',
            field=models.ForeignKey(blank=True, to='sites.Site', null=True, on_delete=models.CASCADE),
        ),
    ]
