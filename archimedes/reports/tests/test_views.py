import json
from base64 import b64encode

from django.core.urlresolvers import reverse
from django.test import TestCase
from mock import patch


class EndpointViewTestCase(TestCase):

    def mock_init():
        pass

    def is_json(self, string):
        try:
            json.loads(string)
        except ValueError:
            return False
        return True

    def test_endpoint_requires_post(self):
        """Return an HTTP 405 error when endpoint receives a GET request."""
        response = self.client.get(reverse('reports-incoming:endpoint'))
        self.assertEqual(response.status_code, 405,
                         "Endpoint GET does not return 405 (Method Not Allowed) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('detail', decoded, "JSON does not contain a 'detail' message key.")
        self.assertEqual(decoded['detail'], 'Method "GET" not allowed.',
                         "Detail does not match expected error message.")

    def test_endpoint_requires_post_data(self):
        """Return an HTTP 400 error when endpoint receives an empty POST request."""
        response = self.client.post(reverse('reports-incoming:endpoint'))
        self.assertEqual(response.status_code, 400,
                         "Endpoint empty POST does not return 400 (Bad Request) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('success', decoded, "JSON does not contain an 'success' key.")
        self.assertEqual(decoded['success'], False, "Success flag is not false on error.")
        self.assertIn('error', decoded, "JSON does not contain an 'error' key.")
        self.assertEqual(decoded['error'], 'Missing required value(s): ek enc', "Error message mismatch.")

    def test_endpoint_post_nonb64(self):
        """Return an HTTP 400 error given a POST request with non-base64 encoded json data."""
        response = self.client.post(reverse('reports-incoming:endpoint'), {'ek': 'somekey', 'enc': 'somedata'})
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        self.assertContains(response, 'Incorrect padding', status_code=400)

    @patch('raven.contrib.django.raven_compat.models.client.captureException')
    @patch('raven.contrib.django.raven_compat.models.client.get_ident')
    def test_endpoint_post_error(self, raven_capture, raven_ident):
        """Return an HTTP 500 error given b64encoded json data and key, if decryption fails."""
        raven_capture.return_value = 'deaddeaddeaddeaddeaddeaddeaddead'
        raven_ident.return_value = 'deaddeaddeaddeaddeaddeaddeaddead'
        with self.assertLogs('archimedes.reports.views') as cm:
            response = self.client.post(
                reverse('reports-incoming:endpoint'),
                {'ek': b64encode(b'somekey').decode(), 'enc': b64encode(b'somedata').decode()}
            )
            self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
            self.assertContains(response, 'Unable to decrypt', status_code=500)
            # Assert logs are as expected.
            self.assertTrue('INFO:archimedes.reports.views:Report received via HTTP: endpoint' in cm.output)
            self.assertTrue("ERROR:archimedes.reports.views:Couldn't decrypt the report: Unable to decrypt"
                            " the envelope key: Ciphertext length must be equal to key size." in cm.output)


class LegacyEndpointViewTestCase(TestCase):

    def mock_init(self):
        pass

    def is_json(self, string):
        try:
            json.loads(string)
        except ValueError:
            return False
        return True

    def test_legacy_endpoint_requires_post(self):
        """Return an HTTP 405 error when endpoint receives a GET request."""
        response = self.client.get(reverse('reports-incoming:legacy_endpoint'))
        self.assertEqual(response.status_code, 405,
                         "Endpoint GET does not return 405 (Method Not Allowed) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('detail', decoded, "JSON does not contain a 'detail' message key.")
        self.assertEqual(decoded['detail'], 'Method "GET" not allowed.',
                         "Detail does not match expected error message.")

    def test_legacy_endpoint_requires_post_data(self):
        """Return an HTTP 400 error when endpoint receives an empty POST request."""
        response = self.client.post(reverse('reports-incoming:legacy_endpoint'))
        self.assertEqual(response.status_code, 400,
                         "Endpoint empty POST does not return 400 (Bad Request) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('success', decoded, "JSON does not contain an 'success' key.")
        self.assertEqual(decoded['success'], False, "Success flag is not false on error.")
        self.assertIn('error', decoded, "JSON does not contain an 'error' key.")
        self.assertEqual(decoded['error'], 'Missing required value(s): key data', "Error message mismatch.")

    def test_legacy_endpoint_post_nonb64(self):
        """Return an HTTP 400 error given a POST request with non-base64 encoded json data."""
        response = self.client.post(
            reverse('reports-incoming:legacy_endpoint'),
            {'key': 'somekey', 'data': 'somedata'}
        )
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        self.assertContains(response, 'Incorrect padding', status_code=400)

    @patch('raven.contrib.django.raven_compat.models.client.captureException')
    @patch('raven.contrib.django.raven_compat.models.client.get_ident')
    def test_legacy_endpoint_post_error(self, raven_capture, raven_ident):
        """Return an HTTP 500 error given b64encoded json data and key, if decryption fails."""
        raven_capture.return_value = 'deaddeaddeaddeaddeaddeaddeaddead'
        raven_ident.return_value = 'deaddeaddeaddeaddeaddeaddeaddead'
        with self.assertLogs('archimedes.reports.views') as cm:
            response = self.client.post(
                reverse('reports-incoming:legacy_endpoint'),
                {'key': b64encode(b'somekey').decode(), 'data': b64encode(b'somedata').decode()}
            )
            self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
            self.assertContains(response, 'Unable to decrypt', status_code=500)
            # Assert logs are as expected.
            self.assertTrue('INFO:archimedes.reports.views:Report received via HTTP: legacy_endpoint' in cm.output)
            self.assertTrue("ERROR:archimedes.reports.views:Couldn't decrypt the report: Unable to decrypt"
                            " the envelope key: Ciphertext length must be equal to key size." in cm.output)
