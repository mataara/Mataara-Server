
from datetime import datetime

import json
import pytz
from django.test import TestCase

from ..models import Report


def get_sample_report_json():
    # TODO: Split this out into its own file for readability.
    sample_report_json = """
    {
        "ReportType": "drupalLegacy",
        "ReportVersion": "1.0.0.",
        "Environment": {
            "Label": "Development",
            "Name": "development"
        },
        "Users": "1459",
        "SiteRoot": "/var/www/odt-d7/app",
        "SiteKey": "7def4c1bb28e00d7dabef091290cfc11",
        "ServerName": "http://servername.local/",
        "Themes": [
            {
                "Description": "A simple one-column, tableless, fluid width administration theme.",
                "Version": "7.52",
                "Theme": "seven",
                "Name": "Seven",
                "Project": "drupal",
                "Url": ""
            }
        ],
        "DirectoryHash": "809456fad962ce228f1e46eaecfe03d8",
        "SiteTitle": "Sample Site",
        "Legacy": true,
        "ReportGenerated": 1480372329,
        "Databases": {
            "default": {
                "Database": "vagrant",
                "Host": "localhost",
                "Driver": "unknown"
            }
        },
        "DrupalVersion": "7.52",
        "SiteSlogan": "No description has been set.",
        "Modules": [
            {
                "Module": "environment",
                "Description": "Sets the environment variable for a site instance",
                "Version": "7.x-1.0",
                "Package": "",
                "Name": "Environment",
                "Project": "environment",
                "Url": ""
            },
            {
                "Module": "user",
                "Description": "Manages the user registration and login system.",
                "Version": "7.52",
                "Package": "Core",
                "Name": "User",
                "Project": "drupal",
                "Url": ""
            }
        ],
        "Nodes": {
            "Revisions": "510072",
            "Nodes": "495670"
        },
        "ReportExpiry": 1480372359,
        "ServerHostname": "servername.local"
    }
    """
    return sample_report_json


def get_mismatch_report_json():
    mismatch_report_json = get_sample_report_json().replace(
        '"ReportType": "drupalLegacy"',
        '"ReportType": "drupal7"'
    ).replace(
        '"Label": "Development"',
        '"Label": "Staging"'
    ).replace(
        '"Name": "development"',
        '"Name": "staging"'
    ).replace(
        '"ServerHostname": "servername.local"',
        '"ServerHostname": "servername.notlocal"'
    )
    return mismatch_report_json


class ReportTestCase(TestCase):

    def test_str(self):
        """Given a :Report the string representation is a formatted string containing the date and a type"""
        stub_report = Report()
        self.assertEqual(str(stub_report), '[] Report from an unknown server generated at an unspecified time')

        stamp = datetime.utcfromtimestamp(1325376000).replace(tzinfo=pytz.utc)
        report = Report(date_generated=stamp, type='sometype')
        self.assertEqual(str(report), '[sometype] Report from an unknown server generated at 2012-01-01 00:00:00 UTC')

    def test_dict(self):
        """Given a :Report, load the json data at report.json"""
        report = Report(
            json='''[
              "firstItem",
              {
                "str": "frog",
                "bool": true,
                "int": 42
              }
            ]'''
        )
        self.assertEqual(len(report.dict), 2)
        self.assertEqual(report.dict[1]['str'], "frog")
        self.assertEqual(report.dict[1]['bool'], True)
        self.assertEqual(report.dict[1]['int'], 42)

    def test_compare_json(self):
        """
        Check for mismatches in default Drupal report fields.
        """
        report = Report(json=get_sample_report_json())
        mismatch = Report(json=get_mismatch_report_json())
        mismatches = report.compare_json(
            json.loads(mismatch.json),
            field_paths=['ReportType', ['Environment', 'Name'], ['Environment', 'Label'], 'ServerHostname']
        )
        self.assertEqual(len(mismatches), 4, "Four mismatches expected")
        self.assertEqual(mismatches['ReportType']['change_from'], "drupalLegacy", "ReportType: from drupalLegacy")
        self.assertEqual(mismatches['ReportType']['change_to'], "drupal7", "ReportType: to drupal7")
        self.assertEqual(mismatches['Environment.Label']['change_to'], "Staging", "'Staging' label")
        self.assertEqual(mismatches['Environment.Name']['change_to'], "staging", "'staging' name")
        self.assertEqual(mismatches['ServerHostname']['change_to'], "servername.notlocal", "Server name")
        # Test on invalid comparators.
        with self.assertRaises(TypeError):
            report.compare_json(None)
        with self.assertRaises(TypeError):
            report.compare_json(5)
        with self.assertRaises(ValueError):
            report.compare_json("not-json")
