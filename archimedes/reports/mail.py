

from django.dispatch import receiver
from django_mailbox.signals import message_received

from .tasks import process_email


@receiver(message_received)
def endpoint(sender, message, **args):
    print('Received message #%d - scheduling processing' % message.pk)
    process_email.delay(message.pk)
