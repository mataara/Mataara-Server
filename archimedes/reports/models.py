import json
from datetime import datetime

from django.db import models


class Report(models.Model):
    """
    A report, populated by data recieved from a website.
    """
    site_key = models.CharField(max_length=50)
    date_received = models.DateTimeField(auto_now_add=True)
    date_generated = models.DateTimeField()
    date_expires = models.DateTimeField()
    type = models.CharField(max_length=50)
    endpoint = models.CharField(max_length=50)
    json = models.TextField()
    processed = models.BooleanField(default=False)
    processed_site = models.ForeignKey('sites.Site', null=True, blank=True, on_delete=models.SET_NULL)
    version = models.CharField(max_length=20, blank=True)

    class Meta:
        ordering = ['-date_generated']

    def __str__(self):
        if not self.json or 'ServerHostname' not in self.dict:
            hostname = 'an unknown server'
        else:
            hostname = self.dict['ServerHostname']

        if self.date_generated:
            generated_at = datetime.strftime(self.date_generated, '%Y-%m-%d %X %Z')
        else:
            generated_at = 'an unspecified time'

        return '[{}] Report from {} generated at {}'.format(
            self.type,
            hostname,
            generated_at
        )

    @property
    def dict(self):
        return json.loads(self.json)

    def compare_json(self, report_json, field_paths=None):
        """
        Check for mismatches on a subset of JSON field paths within the :Report.
        Given a JSON report, returns a dict of changed fields (path keys
        dot-concatenated) with changed_from and changed_to subkeys.

        The report_json parameter can take either a parseable text string of
        JSON, or a pre-parsed dict.

        If no field_paths argument is specified, a default_field_paths set may be used.
        If no paths have changed, an empty dict is returned.
        """
        # TODO: At some point in the future, see if JSONPath is a good option.

        # Coerce JSON into dict, if it is not already one.
        if type(report_json) is str:
            try:
                report_json = json.loads(report_json)
            except ValueError as exc:
                raise ValueError(
                    "Could not parse report_json string value for comparison"
                ).with_traceback(exc.__traceback__)
        elif type(report_json) is not dict:
            raise TypeError(
                "Cannot parse report_json from an unsupported type {}; use a string or dict".format(
                    type(report_json).__name__
                )
            )

        mismatches = {}
        default_field_paths = ['ReportType']
        if field_paths is None:
            field_paths = default_field_paths

        for path in field_paths:
            # Coerce into array form.
            if type(path) is str:
                path = [path]

            # Copy path array to use as mismatch dict key.
            path_key = '.'.join(path)
            path_copy = path[:]

            # Step through path elements
            search_from = self.dict
            search_to = report_json
            while len(path) > 0:
                key = path.pop(0)
                # TODO: KeyError exception handling when search paths are not present in either report.
                search_from = search_from[key]
                search_to = search_to[key]

            # If mismatch is found, add to the result dict.
            if str(search_from) != str(search_to):
                mismatches[path_key] = {
                    'change_from': search_from,
                    'change_to': search_to,
                    'field_path': path_copy
                }

        return mismatches
