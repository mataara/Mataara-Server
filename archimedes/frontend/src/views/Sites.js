import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import debounce from 'lodash/debounce';
import { withStyles } from "@material-ui/core/styles";
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

import InfiniteTable from '../components/helpers/InfiniteTable';
import CustomTabs from '../components/helpers/CustomTabs';
import CustomTab from '../components/helpers/CustomTab';
import CustomChip from '../components/helpers/CustomChip';
import { getDate, removeDecommissioned } from '../components/helpers/formatData';
import { fetchSites, sitesToggleSubscription } from '../actions/sitesActions';
import { setPageTitle } from '../actions/frameActions';
import { renderMozillaObservatory } from '../components/helpers/formatData';

export class Sites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {},
      sortOptions: {
        sortBy: 'title',
        sortDir: 'asc'
      },
      activeTab: 'all',
      showDecommissioned: false,
    };

    this.loadMore = this.loadMore.bind(this);
    this.doFetch = this.doFetch.bind(this);
    this.debouncedFetch = debounce(this.doFetch.bind(this), 500);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.setSubscribed = this.setSubscribed.bind(this);
  }

  componentDidMount() {
    this.doFetch(1);
    this.props.dispatch(setPageTitle('Sites'));
  }

  loadMore() {
    if (!this.props.pending) this.doFetch(this.props.page + 1);
  }

  doFetch(page) {
    const { filters, sortOptions } = this.state;
    this.props.dispatch(fetchSites(filters, page, sortOptions));
  }

  handleTabChange(event, value) {
    this.setState({
      activeTab: value,
      filters: value === 'all' ? {} : { type: value },
    }, () => this.doFetch(1));
  }

  handleSearch(event) {
    // Add search query to existing filters (= core/contrib) if any
    this.setState({
      filters: {
        ...this.state.filters,
        search: event.target.value,
      }
    }, () => this.debouncedFetch(1));
  }

  handleSort(sortBy, sortDir) {
    this.setState({
      sortOptions: {
        sortBy,
        sortDir
      }
    }, () => this.doFetch(1));
  }

  setSubscribed(site) {
    this.props.dispatch(sitesToggleSubscription(site.pk));
  }

  render() {
    const { hasMore, classes } = this.props;
    const { filters, sortOptions, activeTab, showDecommissioned } = this.state;
    const searchText = filters.search || '';

    const sites = showDecommissioned
      ? this.props.sites
      : removeDecommissioned(this.props.sites);

    const explainMozillaObservatory = 'Observatory by Mozilla is a project designed to help developers, system administrators, and security professionals configure their sites safely and securely.';

    const tableHeader = [
      { title: '', sortable: false, padding: 'none' },
      { title: 'Site', sortable: true, sortKey: 'title', style: { minWidth: 300, paddingLeft: 0 } },
      { title: 'Vulnerabilities', sortable: false },
      { title: 'Type / Ver.', sortable: false },
      { title: 'Environment', sortable: true, sortKey: 'environment' },
      { title: 'Activity', sortable: false, style: { minWidth: 200 } },
      {
        title: (
          <Tooltip title={explainMozillaObservatory} classes={{ tooltip: classes.customWidth }}>
            <span>Mozilla Observatory</span>
          </Tooltip>
        ),
        sortable: false
      },
    ];

    const tableData = sites.map(site => {
      return [
        <Checkbox
          checked={site.subscribed}
          onChange={this.setSubscribed.bind(this, site)}
          icon={<StarBorderIcon />}
          checkedIcon={<StarIcon className={classes.subscribed} />}
        />,
        <Link to={`sites/${site.pk}`}><strong>{site.title}</strong> (on {site.hostname})</Link>,
        site.vulnerability_count,
        `${site.type} ${site.core}`,
        site.environment,
        site.decommissioned
          ? <CustomChip label="Decommissioned" />
          : getDate(site.date_last_received),
        renderMozillaObservatory(site.url),
      ];
    });

    return (
      <React.Fragment>
        <Helmet title="Sites | Mataara" />

        <CustomTabs
          value={activeTab}
          onChange={this.handleTabChange}
        >
          <CustomTab label="All" value="all" />
          <CustomTab label="Drupal" value="drupal" />
          <CustomTab label="Silverstripe" value="silverstripe" disabled />
          <CustomTab label="Moodle" value="moodle" disabled />
          <CustomTab label="Django" value="django" disabled />
          <CustomTab label="Mezzanine" value="mezzanine" disabled />
          <CustomTab label="Other" value="other" disabled />
        </CustomTabs>

        <Grid container justify="space-between">
          <Input
            value={searchText}
            onChange={this.handleSearch}
            placeholder="Search Sites ..."
            autoFocus
            className={classes.search}
          />

          <FormControlLabel
            control={
              <Switch
                checked={this.state.showDecommissioned}
                onChange={() => this.setState({ showDecommissioned: !this.state.showDecommissioned })}
                value="showDecommissioned"
                color="primary"
              />
            }
            label="Show decommissioned sites"
          />
        </Grid>

        <InfiniteTable
          loadMore={this.loadMore}
          hasMore={hasMore}
          tableHeader={tableHeader}
          tableData={tableData}
          sortOptions={sortOptions}
          onSortChange={this.handleSort}
        />
      </React.Fragment>
    );
  }
}

Sites.propTypes = {
  sites: PropTypes.arrayOf(PropTypes.shape({
    subscribed: PropTypes.bool,
    pk: PropTypes.number,
    title: PropTypes.string,
    hostname: PropTypes.string,
    vulnerability_count: PropTypes.number,
    type: PropTypes.string,
    core: PropTypes.string,
    environment: PropTypes.string,
    decommissioned: PropTypes.bool,
    date_last_received: PropTypes.string,
    url: PropTypes.string,
  })).isRequired,
  page: PropTypes.number.isRequired,
  hasMore: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  search: {
    marginBottom: '1rem',
  },
  subscribed: {
    color: theme.palette.yellow,
  },
  customWidth: {
    maxWidth: 300,
  },
});

const mapStateToProps = state => ({
  sites: state.sites.list,
  page: state.sites.page,
  hasMore: !!state.sites.payload.next,
  pending: state.sites.pending,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(Sites);
