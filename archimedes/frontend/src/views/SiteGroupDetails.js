import React, { Component } from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Breadcrumb from "../components/helpers/Breadcrumb";
import BreadcrumbItem from "../components/helpers/BreadcrumbItem";
import SiteGroupInfo from "../components/siteGroupDetails/SiteGroupInfo";
import Contacts from "../components/siteGroupDetails/Contacts";
import SiteList from "../components/siteGroupDetails/SiteList";

import {
  fetchSiteGroupDetails,
  updateSiteGroupDetails,
} from "../actions/siteDetailsActions";
import { setPageTitle } from "../actions/frameActions";

export class SiteGroupDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {},
      sortOptions: {
        sortBy: "title",
        sortDir: "asc",
      },
      activeTab: "all",
      showDecommissioned: false,
    };

    this.handleSort = this.handleSort.bind(this);
    this.doFetch = this.doFetch.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  componentDidMount() {
    const { dispatch, siteGroupDetails } = this.props;
    const { sitegroupId } = this.props.match.params;
    dispatch(fetchSiteGroupDetails(sitegroupId, this.state.sortOptions));
    dispatch(setPageTitle(siteGroupDetails.name)); // Needed for when site details are already fetched
  }

  // Needed for updating page title when site details are fetched
  componentDidUpdate(prevProps) {
    const { dispatch, siteGroupDetails } = this.props;
    if (siteGroupDetails.name !== prevProps.siteGroupDetails.name) {
      dispatch(setPageTitle(siteGroupDetails.name));
    }
  }

  doFetch() {
    const { sitegroupId } = this.props.match.params;
    this.props.dispatch(
      fetchSiteGroupDetails(sitegroupId, this.state.sortOptions)
    );
  }

  handleUpdate(data) {
    const { sitegroupId } = this.props.match.params;
    this.props.dispatch(
      updateSiteGroupDetails(sitegroupId, this.state.sortOptions, data)
    );
  }

  handleSort(sortBy, sortDir) {
    this.setState(
      {
        sortOptions: {
          sortBy,
          sortDir,
        },
      },
      () => this.doFetch()
    );
  }

  render() {
    const { siteGroupDetails } = this.props;

    return (
      <div>
        <Helmet
          title={`${siteGroupDetails.name} | Site Group Details | Mataara`}
        />

        <Breadcrumb>
          <BreadcrumbItem>
            <Link to="/sitegroups">Site Groups</Link>
          </BreadcrumbItem>
          <BreadcrumbItem active>{siteGroupDetails.name}</BreadcrumbItem>
        </Breadcrumb>

        <SiteGroupInfo
          siteGroupDetails={siteGroupDetails}
          handleUpdate={this.handleUpdate}
        />
        <Contacts siteGroupDetails={siteGroupDetails} />
        <SiteList
          siteGroupDetails={siteGroupDetails}
          sortOptions={this.state.sortOptions}
          onSortChange={this.handleSort}
        />
      </div>
    );
  }
}

SiteGroupDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      sitegroupId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  siteGroupDetails: PropTypes.shape({
    string: PropTypes.string,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  siteGroupDetails: state.siteGroupDetails,
});

export default connect(mapStateToProps)(SiteGroupDetails);
