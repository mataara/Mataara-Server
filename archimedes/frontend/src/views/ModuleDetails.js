import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

import Breadcrumb from '../components/helpers/Breadcrumb';
import BreadcrumbItem from '../components/helpers/BreadcrumbItem';
import ModuleInfo from '../components/moduleDetails/ModuleInfo';
import SitesUsing from '../components/moduleDetails/SitesUsing';
import { fetchModuleDetails, fetchModuleReleases } from '../actions/moduleDetailsActions';
import { setPageTitle } from '../actions/frameActions';

export class ModuleDetails extends Component {
  componentDidMount() {
    const { dispatch, moduleDetails } = this.props;
    const { moduleId } = this.props.match.params;
    dispatch(fetchModuleDetails(moduleId));
    dispatch(fetchModuleReleases(moduleId));
    dispatch(setPageTitle(moduleDetails.name)); // Needed for when site details are already fetched
  }

  // Needed for updating page title when site details are fetched
  componentDidUpdate(prevProps) {
    const { dispatch, moduleDetails } = this.props;
    if (moduleDetails.name !== prevProps.moduleDetails.name) {
      dispatch(setPageTitle(moduleDetails.name));
    }
  }

  render() {
    const { moduleDetails } = this.props;
    return (
      <div>
        <Helmet title={`${moduleDetails.name} | ${moduleDetails.type_name} Details | Mataara`} />

        <Breadcrumb>
          <BreadcrumbItem><Link to='/modules'>Modules & Themes</Link></BreadcrumbItem>
          <BreadcrumbItem active>{moduleDetails.name}</BreadcrumbItem>
        </Breadcrumb>

        <Grid container spacing={3}>
          <Grid item xm={12} md={6}>
            <ModuleInfo moduleDetails={moduleDetails} />
          </Grid>

          <Grid item xm={12} md={6}>
            <SitesUsing moduleDetails={moduleDetails} />
          </Grid>

        </Grid>
      </div>
    );
  }
}

ModuleDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      moduleId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  moduleDetails: PropTypes.shape({
    name: PropTypes.string,
    type_name: PropTypes.string,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  moduleDetails: state.moduleDetails,
});

export default connect(mapStateToProps)(ModuleDetails);
