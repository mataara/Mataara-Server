import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import debounce from 'lodash/debounce';
import { withStyles } from "@material-ui/core/styles";
import Input from '@material-ui/core/Input';
import Grid from '@material-ui/core/Grid';

import InfiniteTable from '../components/helpers/InfiniteTable';
import { fetchSiteGroups, sitesToggleSubscription } from '../actions/sitesActions';
import { setPageTitle } from '../actions/frameActions';

export class SiteGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {},
      sortOptions: {
        sortBy: 'title',
        sortDir: 'asc'
      },
      activeTab: 'all',
      showDecommissioned: false,
    };

    this.loadMore = this.loadMore.bind(this);
    this.doFetch = this.doFetch.bind(this);
    this.debouncedFetch = debounce(this.doFetch.bind(this), 500);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.setSubscribed = this.setSubscribed.bind(this);
  }

  componentDidMount() {
    this.doFetch(1);
    this.props.dispatch(setPageTitle('Site Groups'));
  }

  loadMore() {
    if (!this.props.pending) this.doFetch(this.props.page + 1);
  }

  doFetch(page) {
    const { filters, sortOptions } = this.state;
    this.props.dispatch(fetchSiteGroups(filters, page, sortOptions));
  }

  handleSearch(event) {
    // Add search query to existing filters (= core/contrib) if any
    this.setState({
      filters: {
        ...this.state.filters,
        search: event.target.value,
      }
    }, () => this.debouncedFetch(1));
  }

  handleSort(sortBy, sortDir) {
    this.setState({
      sortOptions: {
        sortBy,
        sortDir
      }
    }, () => this.doFetch(1));
  }

  setSubscribed(site) {
    this.props.dispatch(sitesToggleSubscription(site.pk));
  }

  render() {
    const { hasMore, classes } = this.props;
    const { filters, sortOptions } = this.state;
    const searchText = filters.search || '';

    const sitegroups = this.props.sitegroups

    const tableHeader = [
      { title: 'Site Group', sortable: true, sortKey: 'name', style: { minWidth: 300, paddingLeft: 20 } },
      { title: 'Description', sortable: false },
      { title: 'Number of Sites', sortable: false  },
    ];

    const tableData = sitegroups.map(sitegroup => {
      return [
        <Link to={`sitegroups/${sitegroup.pk}`}><strong>{sitegroup.name}</strong></Link>,
        sitegroup.description,
        sitegroup.sites.length
      ];
    });

    return (
      <React.Fragment>
        <Helmet title="Site Groups | Mataara" />

        <Grid container justify="space-between">
          <Input
            value={searchText}
            onChange={this.handleSearch}
            placeholder="Search Site Groups ..."
            autoFocus
            className={classes.search}
          />
        </Grid>

        <InfiniteTable
          loadMore={this.loadMore}
          hasMore={hasMore}
          tableHeader={tableHeader}
          tableData={tableData}
          sortOptions={sortOptions}
          onSortChange={this.handleSort}
        />
      </React.Fragment>
    );
  }
}

SiteGroups.propTypes = {
  sitegroups: PropTypes.arrayOf(PropTypes.shape({
    pk: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    notes: PropTypes.string,
    api_url: PropTypes.string,
  })).isRequired,
  page: PropTypes.number.isRequired,
  hasMore: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  search: {
    marginBottom: '1rem',
  },
  subscribed: {
    color: theme.palette.yellow,
  },
  customWidth: {
    maxWidth: 300,
  },
});

const mapStateToProps = state => ({
  sitegroups: state.sitegroups.list,
  page: state.sites.page,
  hasMore: !!state.sitegroups.payload.next,
  pending: state.sitegroups.pending,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(SiteGroups);
