import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import debounce from 'lodash/debounce';
import Moment from 'moment';
import { withStyles } from "@material-ui/core/styles";
import Input from '@material-ui/core/Input';

import InfiniteTable from '../components/helpers/InfiniteTable';
import CustomTabs from '../components/helpers/CustomTabs';
import CustomTab from '../components/helpers/CustomTab';
import { fetchAdvisories } from '../actions/advisoriesActions';
import { setPageTitle } from '../actions/frameActions';
import { removeDecommissioned } from '../components/helpers/formatData';

export class Advisories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {},
      sortOptions: {
        sortBy: 'date_posted',
        sortDir: 'desc'
      },
      activeTab: 'all',
    };

    this.loadMore = this.loadMore.bind(this);
    this.doFetch = this.doFetch.bind(this);
    this.debouncedFetch = debounce(this.doFetch.bind(this), 500);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  componentDidMount() {
    this.doFetch(1);
    this.props.dispatch(setPageTitle('Advisories'));
  }

  loadMore() {
    if (!this.props.pending) this.doFetch(this.props.page + 1);
  }

  doFetch(page) {
    const { filters, sortOptions } = this.state;
    this.props.dispatch(fetchAdvisories(filters, page, sortOptions));
  }

  handleTabChange(event, value) {
    this.setState({
      activeTab: value,
      filters: value === 'all' ? {} : { 'source__slug': value },
    }, () => this.doFetch(1));
  }

  handleSearch(event) {
    // Add search query to existing filters (= core/contrib) if any
    this.setState({
      filters: {
        ...this.state.filters,
        search: event.target.value,
      }
    }, () => this.debouncedFetch(1));
  }

  handleSort(sortBy, sortDir) {
    this.setState({
      sortOptions: {
        sortBy,
        sortDir
      }
    }, () => this.doFetch(1));
  }

  render() {
    const { advisories, hasMore, classes } = this.props;
    const { filters, sortOptions, activeTab } = this.state;
    const searchText = filters.search || '';

    const tableHeader = [
      { title: 'Title', sortable: true, sortKey: 'title', style: { minWidth: 300 } },
      { title: 'Affected (incl. decommissioned)', sortable: true, sortKey: 'affected_sites_count', style: { maxWidth: 150, textAlign: 'center' } },
      { title: 'Source', sortable: true, sortKey: 'source' },
      { title: 'Date', sortable: true, sortKey: 'date_posted', style: { minWidth: 140 } },
      { title: 'Risk', sortable: true, sortKey: 'risk_score' },
      { title: 'Status', sortable: true, sortKey: 'status' },
    ];

    const tableData = advisories.map(row => [
      <Link to={`drupal/advisories/${row.pk}`}>{row.title}</Link>,
      row.affected_sites_count,
      row.source,
      Moment(row.date_posted).format('DD MMMM YYYY'),
      // 25 is the standard maximum risk score for Drupal advisories
      <progress value={row.risk_score || 0} max={row.risk_score_total || 25} />,
      row.status_display,
    ]);

    return (
      <React.Fragment>
        <Helmet title="Advisories | Mataara" />

        <CustomTabs
          value={activeTab}
          onChange={this.handleTabChange}
        >
          <CustomTab label="All" value="all" />
          <CustomTab label="Drupal Core" value="drupal-core" />
          <CustomTab label="Drupal Contrib" value="drupal-contrib" />
        </CustomTabs>

        <Input
          value={searchText}
          onChange={this.handleSearch}
          placeholder="Search Advisories ..."
          autoFocus
          className={classes.search}
        />

        <InfiniteTable
          loadMore={this.loadMore}
          hasMore={hasMore}
          tableHeader={tableHeader}
          tableData={tableData}
          sortOptions={sortOptions}
          onSortChange={this.handleSort}
        />
      </React.Fragment>
    );
  }
}

Advisories.propTypes = {
  advisories: PropTypes.arrayOf(PropTypes.shape({
    pk: PropTypes.number,
    title: PropTypes.string,
    affected_sites: PropTypes.arrayOf(PropTypes.shape({
      decommissioned: PropTypes.bool.isRequired,
    })),
    source: PropTypes.string,
    date_posted: PropTypes.string,
    risk_score: PropTypes.number,
    risk_score_total: PropTypes.number,
    status_display: PropTypes.string,
  })).isRequired,
  page: PropTypes.number.isRequired,
  hasMore: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  search: {
    marginBottom: '1rem',
  },
});

const mapStateToProps = state => ({
  advisories: state.advisories.list,
  page: state.advisories.page,
  hasMore: !!state.advisories.payload.next,
  pending: state.advisories.pending,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(Advisories);
