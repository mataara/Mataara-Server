import React from 'react';
import { shallow } from 'enzyme';

import { AdvisoryDetails } from '../AdvisoryDetails';
import { Helmet } from 'react-helmet';
import Breadcrumb from '../../components/helpers/Breadcrumb';
import BreadcrumbItem from '../../components/helpers/BreadcrumbItem';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import Metadata from '../../components/advisoryDetails/Metadata';
import ParsedData from '../../components/advisoryDetails/ParsedData';
import fetchAdvisoryDetails from '../../actions/advisoryDetailsActions';
import * as frameActions from '../../actions/frameActions';
jest.mock('../../actions/advisoryDetailsActions', () => jest.fn(fn => fn));

describe('AdvisoryDetails', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<AdvisoryDetails {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      match: {
        params: {
          advisoryId: '100',
        },
      },
      advisoryDetails: {
        title: 'test title',
      },
      dispatch: jest.fn(),
    };
    wrapper = undefined;
  });

  it('renders a `div`', () => {
    expect(setup().find('div').length).toBe(1);
  });

  describe('rendered `div`', () => {
    it('contains `Helmet`', () => {
      const div = setup().find('div');
      expect(div.find(Helmet).length).toBe(1);
    });

    it('contains `Breadcrumb`', () => {
      const div = setup().find('div');
      expect(div.find(Breadcrumb).length).toBe(1);
    });

    it('contains `Grid`', () => {
      const div = setup().find('div');
      expect(div.find(Grid).length).toBe(3);
    });
  });

  describe('rendered `Helmet`', () => {
    it('is passed `${advisoryDetails.title} | Advisory Details | Mataara` as `title` prop', () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop('title')).toBe('test title | Advisory Details | Mataara');
    });
  });

  describe('rendered `Breadcrumb`', () => {
    it('contains `BreadcrumbItem` of `Link` to `/advisories`', () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(0);
      expect(item.find(Link).length).toBe(1);
      expect(item.find(Link).prop('to')).toBe('/advisories');
      expect(item.find(Link).contains('Advisories')).toBe(true);
    });

    it('contains `BreadcrumbItem` of `title` text', () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(1);
      expect(item.contains('test title')).toBe(true);
    });
  });

  describe('rendered `Grid`', () => {
    it('contains `Grid` of `Metadata`', () => {
      const grid = setup().find(Grid).at(1);
      expect(grid.find(Metadata).length).toBe(1);
    });

    it('contains `Grid` of `ParsedData`', () => {
      const grid = setup().find(Grid).at(2);
      expect(grid.find(ParsedData).length).toBe(1);
    });
  });

  describe('rendered `Metadata`', () => {
    it('is passed `props.advisoryDetails` as `advisoryDetails` prop', () => {
      const metadata = setup().find(Metadata);
      expect(metadata.prop('advisoryDetails')).toBe(props.advisoryDetails);
    });
  });

  describe('rendered `ParsedData`', () => {
    it('is passed `props.advisoryDetails` as `advisoryDetails` prop', () => {
      const parseddata = setup().find(ParsedData);
      expect(parseddata.prop('advisoryDetails')).toBe(props.advisoryDetails);
    });
  });

  describe('`componentDidMount` method', () => {
    it('dispatches `fetchAdvisoryDetails` with param `props.match.params.advisoryId`', () => {
      const instance = setup().instance();
      fetchAdvisoryDetails.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2);
      expect(fetchAdvisoryDetails).toHaveBeenCalledTimes(1);
      expect(fetchAdvisoryDetails).toHaveBeenCalledWith('100');
    });

    it('dispatches `setPageTitle` with param `advisoryDetails.title`', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('test title');
    });
  });

  describe('`componentDidUpdate` method', () => {
    it('dispatches `setPageTitle` with param `advisoryDetails.title` if title is changed', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = {
        ...props,
        advisoryDetails: { title: 'previous title' },
      };
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('test title');
    });

    it('does not dispatch `setPageTitle` if title is not changed', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = props;
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).not.toHaveBeenCalled();
      expect(frameActions.setPageTitle).not.toHaveBeenCalled();
    });
  });
});
