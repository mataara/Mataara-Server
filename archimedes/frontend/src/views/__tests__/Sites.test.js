import React from 'react';
import { shallow } from 'enzyme';

import { Sites } from '../Sites';
import { Helmet } from 'react-helmet';
import CustomTabs from '../../components/helpers/CustomTabs';
import CustomTab from '../../components/helpers/CustomTab';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InfiniteTable from '../../components/helpers/InfiniteTable';
import { Link } from 'react-router-dom';
import CustomChip from '../../components/helpers/CustomChip';
import * as sitesActions from '../../actions/sitesActions';
import * as frameActions from '../../actions/frameActions';
import debounce from 'lodash/debounce';
jest.mock('lodash/debounce', () => jest.fn(fn => fn));

describe('Sites', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Sites {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      sites: [
        {
          subscribed: true,
          pk: 1,
          title: 'title 1',
          hostname: 'hostname 1',
          vulnerability_count: 2,
          type: 'Drupal',
          core: '6.38',
          environment: 'Production',
          decommissioned: false,
          date_last_received: '2018-08-02T13:03:54.268884Z',
          url: 'http://accent.ac.nz',
        },
        {
          subscribed: false,
          pk: 2,
          title: 'title 2',
          hostname: 'hostname 2',
          vulnerability_count: 5,
          type: 'Drupal',
          core: '7.59',
          environment: 'Staging',
          decommissioned: true,
          date_last_received: '2018-08-02T10:43:53.990573Z',
          url: 'http://shop.akoaotearoa.ac.nz/',
        },
      ],
      page: 1,
      hasMore: true,
      pending: false,
      dispatch: jest.fn(),
      classes: {
        search: 'search',
        subscribed: 'subscribed',
        customWidth: 'customWidth',
      },
    };
    wrapper = undefined;
  });

  it('renders a `Helmet`', () => {
    expect(setup().find(Helmet).length).toBe(1);
  });

  it('renders `CustomTabs`', () => {
    expect(setup().find(CustomTabs).length).toBe(1);
  });

  it('renders `Grid`', () => {
    expect(setup().find(Grid).length).toBe(1);
  });

  it('renders `InfiniteTable`', () => {
    expect(setup().find(InfiniteTable).length).toBe(1);
  });

  describe('rendered `Helmet`', () => {
    it('is passed `Sites | Mataara` as `title` prop', () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop('title')).toBe('Sites | Mataara');
    });
  });

  describe('rendered `CustomTabs`', () => {
    it('is passed `state.activeTab` as `value` prop', () => {
      const tabs = setup().find(CustomTabs);
      expect(tabs.prop('value')).toBe(setup().state('activeTab'));
    });

    it('is passed `this.handleTabChange` fn as `onChange` prop', () => {
      const instance = setup().instance();
      const tabs = setup().find(CustomTabs);
      expect(tabs.prop('onChange')).toBe(instance.handleTabChange);
    });

    it('contains `All` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(0);
      expect(tab.prop('label')).toBe('All');
      expect(tab.prop('value')).toBe('all');
    });

    it('contains `Drupal` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(1);
      expect(tab.prop('label')).toBe('Drupal');
      expect(tab.prop('value')).toBe('drupal');
    });

    it('contains `Silverstripe` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(2);
      expect(tab.prop('label')).toBe('Silverstripe');
      expect(tab.prop('value')).toBe('silverstripe');
    });

    it('contains `Moodle` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(3);
      expect(tab.prop('label')).toBe('Moodle');
      expect(tab.prop('value')).toBe('moodle');
    });

    it('contains `Django` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(4);
      expect(tab.prop('label')).toBe('Django');
      expect(tab.prop('value')).toBe('django');
    });

    it('contains `Mezzanine` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(5);
      expect(tab.prop('label')).toBe('Mezzanine');
      expect(tab.prop('value')).toBe('mezzanine');
    });

    it('contains `Other` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(6);
      expect(tab.prop('label')).toBe('Other');
      expect(tab.prop('value')).toBe('other');
    });
  });

  describe('rendered `Grid`', () => {
    it('contains `Input`', () => {
      const grid = setup().find(Grid);
      expect(grid.find(Input).length).toBe(1);
    });

    it('contains `FormControlLabel`', () => {
      const grid = setup().find(Grid);
      expect(grid.find(FormControlLabel).length).toBe(1);
    });
  });

  describe('rendered `Input`', () => {
    it('is passed `state.filters.search` as `value` prop when the state has value', () => {
      setup().setState({ filters: { search: 'test search' } });
      const input = setup().find(Input);
      expect(input.prop('value')).toBe(setup().state('filters').search);
    });

    it('is passed empty strings as `value` prop when the state has no value', () => {
      const input = setup().find(Input);
      expect(input.prop('value')).toBe('');
    });

    it('is passed `this.handleSearch` fn as `onChange` prop', () => {
      const instance = setup().instance();
      const input = setup().find(Input);
      expect(input.prop('onChange')).toBe(instance.handleSearch);
    });

    it('has `search` class', () => {
      const input = setup().find(Input);
      expect(input.prop('className').includes('search')).toBe(true);
    });
  });

  describe('rendered `FormControlLabel`', () => {
    it('is passed `Switch` react element as `control` props', () => {
      const form = setup().find(FormControlLabel);
      const control = shallow(form.prop('control'));
      expect(control.debug().includes('Switch')).toBe(true);
    });

    describe('passed `Switch` react element', () => {
      it('is passed `state.showDecommissioned` as `checked` prop', () => {
        const form = setup().find(FormControlLabel);
        const control = shallow(form.prop('control'));
        expect(control.prop('checked')).toBe(setup().state('showDecommissioned'));
        });

      it('is passed a function to toggle `state.showDecommissioned` as `onChange` prop', () => {
        const form = setup().find(FormControlLabel);
        const control = shallow(form.prop('control'));

        expect(setup().state('showDecommissioned')).toBe(false);
        control.prop('onChange')();
        expect(setup().state('showDecommissioned')).toBe(true);
      });
    });
  });

  describe('rendered `InfiniteTable`', () => {
    it('is passed `this.loadMore` fn as `loadMore` prop', () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop('loadMore')).toBe(instance.loadMore);
    });

    it('is passed `props.hasMore` as `hasMore` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop('hasMore')).toBe(props.hasMore);
    });

    it('is passed an array as `tableHeader` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop('tableHeader'))).toBe(true);
    });

    it('is passed an array as `tableData` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop('tableData'))).toBe(true);
    });

    it('is passed `state.sortOptions` as `sortOptions` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop('sortOptions')).toBe(setup().state('sortOptions'));
    });

    it('is passed `this.handleSort` fn as `onSortChange` prop', () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop('onSortChange')).toBe(instance.handleSort);
    });

    describe('passed `tableHeader`', () => {
      it('has `` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[0].title).toBe('');
        expect(header[0].sortable).toBe(false);
      });

      it('has `Site` and it is sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[1].title).toBe('Site');
        expect(header[1].sortable).toBe(true);
      });

      it('has `Vulnerabilities` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[2].title).toBe('Vulnerabilities');
        expect(header[2].sortable).toBe(false);
      });

      it('has `Type / Ver.` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[3].title).toBe('Type / Ver.');
        expect(header[3].sortable).toBe(false);
      });

      it('has `Environment` and it is sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[4].title).toBe('Environment');
        expect(header[4].sortable).toBe(true);
      });

      it('has `Activity` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[5].title).toBe('Activity');
        expect(header[5].sortable).toBe(false);
      });

      it('has `Mozilla Observatory` `Tooltip` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        const title = shallow(header[6].title);
        expect(title.debug().includes('Mozilla Observatory')).toBe(true);
        expect(header[6].sortable).toBe(false);
      });
    });

    describe('passed `tableData`', () => {
      it('includes decommissioned sites when `state.showDecommissioned` is true', () => {
        setup().setState({ showDecommissioned: true });
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[1]).toBeDefined();
      });

      it('does not include decommissioned sites when `state.showDecommissioned` is false', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[1]).toBeUndefined();
      });

      it('has `Checkbox`', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        const checkbox = shallow(data[0][0]);
        expect(checkbox.debug().includes('Checkbox')).toBe(true);
      });

      describe('`Checkbox`', () => {
        it('is passed `site.subscribed` as `checked` prop', () => {
          const data = setup().find(InfiniteTable).prop('tableData');
          const checkbox = shallow(data[0][0]);
          expect(checkbox.prop('checked')).toBe(props.sites[0].subscribed);
        });

        it('is passed `this.setSubscribed` fn as `onChange` prop', () => {
          // Bound function is not the same object with the original function.
          // Therefore cannot be compared to instance.setSubscribed.
          // So, test by mocking its prototype and calling onChange.
          const original = Sites.prototype.setSubscribed;
          const mock = jest.fn();
          Sites.prototype.setSubscribed = mock;

          const data = setup().find(InfiniteTable).prop('tableData');
          mock.mockClear();
          const checkbox = shallow(data[0][0]);

          expect(mock).not.toHaveBeenCalled();
          checkbox.prop('onChange')();
          expect(mock).toHaveBeenCalledTimes(1);

          Sites.prototype.setSubscribed = original;
        });
      });

      it('has `Link` to site details page', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][1]).toEqual(
          <Link to='sites/1'><strong>{props.sites[0].title}</strong> (on {props.sites[0].hostname})</Link>
        );
      });

      it('has `vulnerability_count` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][2]).toBe(props.sites[0].vulnerability_count);
      });

      it('has `site.type` + `site.core` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][3]).toBe('Drupal 6.38');
      });

      it('has `environment` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][4]).toBe(props.sites[0].environment);
      });

      it('has `CustomChip` if the site is decommissioned', () => {
        setup().setState({ showDecommissioned: true });
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[1][5]).toEqual(
          <CustomChip label="Decommissioned" />
        );
      });

      it('has formatted `date_last_received` strings if the site is not decommissioned', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][5]).toBe('03 Aug 2018, 01:03 am');
      });

      it('has a link to Mozilla Observatory', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        const link = shallow(data[0][6]);
        expect(link.find('a').length).toBe(1);
        expect(link.find('a').prop('href')).toBe('https://observatory.mozilla.org/analyze.html?host=accent.ac.nz');
      });
    });
  });

  describe('`componentDidMount` method', () => {
    it('calls `this.doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.componentDidMount();

      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });

    it('dispatches `setPageTitle` with param `Sites`', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      frameActions.setPageTitle.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2); // inside doFetch + setPageTitle
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('Sites');
    });
  });

  describe('`loadMore` method', () => {
    it('calls `this.doFetch` with param `props.page + 1` if `props.pending` is false', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.loadMore();

      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(2);
    });

    it('does not call `this.doFetch` if `props.pending` is true', () => {
      props.pending = true;
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.loadMore();

      expect(instance.doFetch).not.toHaveBeenCalled();
    });
  });
  
  describe('`doFetch` method', () => {
    it('dispatches `fetchSites` with params `state.filters`, given `page`, `state.sortOptions`', () => {
      sitesActions.fetchSites = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      sitesActions.fetchSites.mockClear();
      const filters = setup().state('filters');
      const page = 1;
      const sortOptions = setup().state('sortOptions');

      instance.doFetch(page);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(sitesActions.fetchSites).toHaveBeenCalledTimes(1);
      expect(sitesActions.fetchSites).toHaveBeenCalledWith(filters, page, sortOptions);
    });
  });

  describe('`debouncedFetch` method', () => {
    it('calls `doFetch` with 500ms debounce', () => {
      debounce.mockClear();
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.debouncedFetch(3);

      expect(debounce).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).not.toHaveBeenCalled();
      setTimeout(() => {
        expect(instance.doFetch).toHaveBeenCalledWith(3);
      }, 600);
    });
  });

  describe('`handleTabChange` method', () => {
    it('sets `state.activeTab` with given `value`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'drupal');
      expect(setup().state('activeTab')).toBe('drupal');
    });

    it('sets `state.filters` with {} when `value` is `all`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'all');
      expect(setup().state('filters')).toEqual({});
    });

    it('sets `state.filters` with { type: value } when `value` is not `all`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'drupal');
      expect(setup().state('filters')).toEqual({ type: 'drupal' });
    });

    it('calls `doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.handleTabChange(null, 'drupal');
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`handleSearch` method', () => {
    it('sets `state.filters.search` with given `event.target.value`', () => {
      const instance = setup().instance();
      const event = { target: { value: 'search' } };
      instance.handleSearch(event);
      expect(setup().state('filters').search).toBe('search');
    });

    it('calls `debouncedFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'debouncedFetch');
      const event = { target: { value: 'search' } };
      instance.handleSearch(event);
      expect(instance.debouncedFetch).toHaveBeenCalledTimes(1);
      expect(instance.debouncedFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`handleSort` method', () => {
    it('sets state with given `sortBy` and `sortDir`', () => {
      const instance = setup().instance();
      expect(setup().state('sortOptions').sortBy).toBe('title');
      expect(setup().state('sortOptions').sortDir).toBe('asc');
      instance.handleSort('environment', 'desc');
      expect(setup().state('sortOptions').sortBy).toBe('environment');
      expect(setup().state('sortOptions').sortDir).toBe('desc');
    });

    it('calls `doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.handleSort('environment', 'desc');
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`setSubscribed` method', () => {
    it('dispatches `sitesToggleSubscription` with given `site.pk`', () => {
      sitesActions.sitesToggleSubscription = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      sitesActions.sitesToggleSubscription.mockClear();
      const site = { pk: 200 };

      instance.setSubscribed(site);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(sitesActions.sitesToggleSubscription).toHaveBeenCalledTimes(1);
      expect(sitesActions.sitesToggleSubscription).toHaveBeenCalledWith(site.pk);
    });
  });
});
