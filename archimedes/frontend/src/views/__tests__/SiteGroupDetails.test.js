import React from "react";
import { shallow } from "enzyme";

import { SiteGroupDetails } from "../SiteGroupDetails";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import Breadcrumb from "../../components/helpers/Breadcrumb";
import BreadcrumbItem from "../../components/helpers/BreadcrumbItem";
import SiteGroupInfo from "../../components/siteGroupDetails/SiteGroupInfo";
import Contacts from "../../components/siteGroupDetails/Contacts";
import SiteList from "../../components/siteGroupDetails/SiteList";
import * as siteDetailsActions from "../../actions/siteDetailsActions";
import * as frameActions from "../../actions/frameActions";

describe("SiteGroupDetails", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SiteGroupDetails {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      match: {
        params: {
          sitegroupId: "1"
        }
      },
      siteGroupDetails: {
        pk: 1,
        name: "Test",
        description: "A test group",
        notes:
          "This is a test group.\r\n\r\nThis area is used to link to other resources.\r\n\r\nWe might at some stage make this field support Markdown or something, but straight text is fine for now.\r\n\r\n### Heading\r\n* Link 1\r\n* Link 2\r\n* Link 3",
        sites: [
          {
            pk: 156,
            string: "Test Site 1",
            decommissioned: false
          },
          {
            pk: 114,
            string: "Test Site 2",
            decommissioned: true
          }
        ],
        keys: [
          {
            value: "",
            enabled: true,
            description: ""
          }
        ],
        contacts: [
          {
            name: " Test User 1",
            email: "user1@example.com",
            link: "",
            project_role: "Developer",
            phone: "222-2222",
            external: "no"
          },
          {
            name: " Test User 2",
            email: "user2@example.com",
            link: "",
            project_role: "Developer",
            phone: "222-2222",
            external: "no"
          }
        ]
      },
      dispatch: jest.fn()
    };
    wrapper = undefined;
  });

  it("renders a `div`", () => {
    expect(setup().find("div").length).toBe(1);
  });

  describe("rendered `div`", () => {
    it("contains `Helmet`", () => {
      const div = setup().find("div");
      expect(div.find(Helmet).length).toBe(1);
    });

    it("contains `Breadcrumb`", () => {
      const div = setup().find("div");
      expect(div.find(Breadcrumb).length).toBe(1);
    });

    it("contains `SiteGroupInfo`", () => {
      const div = setup().find("div");
      expect(div.find(SiteGroupInfo).length).toBe(1);
    });

    it("contains `Contacts`", () => {
      const div = setup().find("div");
      expect(div.find(Contacts).length).toBe(1);
    });

    it("contains `SiteList`", () => {
      const div = setup().find("div");
      expect(div.find(SiteList).length).toBe(1);
    });
  });

  describe("rendered `Helmet`", () => {
    it("is passed `${siteGroupDetails.name} | Site Group Details | Mataara` as `title` prop", () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop("title")).toBe("Test | Site Group Details | Mataara");
    });
  });

  describe("rendered `Breadcrumb`", () => {
    it("contains `BreadcrumbItem` of `Link` to `/sitegroups`", () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(0);
      expect(item.find(Link).length).toBe(1);
      expect(item.find(Link).prop("to")).toBe("/sitegroups");
      expect(item.find(Link).contains("Site Groups")).toBe(true);
    });

    it("contains `BreadcrumbItem` of `string` text", () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(1);
      expect(item.contains("Test")).toBe(true);
    });
  });

  describe("rendered `SiteGroupInfo`", () => {
    it("is passed `props.siteGroupDetails` as `siteGroupDetails` prop", () => {
      const sitegroupinfo = setup().find(SiteGroupInfo);
      expect(sitegroupinfo.prop("siteGroupDetails")).toBe(
        props.siteGroupDetails
      );
    });
  });

  describe("rendered `Contacts`", () => {
    it("is passed `props.siteGroupDetails` as `siteGroupDetails` prop", () => {
      const contacts = setup().find(Contacts);
      expect(contacts.prop("siteGroupDetails")).toBe(props.siteGroupDetails);
    });
  });

  describe("rendered `SiteList`", () => {
    it("is passed `props.siteGroupDetails` as `siteGroupDetails` prop", () => {
      const sitelist = setup().find(SiteList);
      expect(sitelist.prop("siteGroupDetails")).toBe(props.siteGroupDetails);
    });

    it("is passed `this.state.sortOptions` as `sortOptions` prop", () => {
      const siteGroupDetails = setup();
      const sitelist = siteGroupDetails.find(SiteList);
      expect(sitelist.prop("sortOptions")).toBe(
        siteGroupDetails.state("sortOptions")
      );
    });

    it("is passed `this.handleSort` function as `onSortChange` prop", () => {
      const siteGroupDetails = setup();
      const instance = siteGroupDetails.instance();
      const sitelist = siteGroupDetails.find(SiteList);
      expect(sitelist.prop("onSortChange")).toBe(instance.handleSort);
    });
  });

  describe("`componentDidMount` method", () => {
    it("dispatches `fetchSiteGroupDetails` with param `props.match.params.sitegroupId`", () => {
      siteDetailsActions.fetchSiteGroupDetails = jest.fn();
      const instance = setup().instance();
      siteDetailsActions.fetchSiteGroupDetails.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2);
      expect(siteDetailsActions.fetchSiteGroupDetails).toHaveBeenCalledTimes(1);
      expect(siteDetailsActions.fetchSiteGroupDetails).toHaveBeenCalledWith(
        "1",
        { sortBy: "title", sortDir: "asc" }
      );
    });

    it("dispatches `setPageTitle` with param `siteGroupDetails.name`", () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith("Test");
    });
  });

  describe("`componentDidUpdate` method", () => {
    it("dispatches `setPageTitle` with param `siteGroupDetails.name` if string is changed", () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = {
        ...props,
        siteGroupDetails: { string: "previous string" }
      };
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith("Test");
    });

    it("does not dispatch `setPageTitle` if string is not changed", () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = props;
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).not.toHaveBeenCalled();
      expect(frameActions.setPageTitle).not.toHaveBeenCalled();
    });
  });
});
