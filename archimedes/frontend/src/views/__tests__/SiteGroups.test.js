import React from "react";
import { shallow } from "enzyme";

import { SiteGroups } from "../SiteGroups";
import { Helmet } from "react-helmet";
import { Grid, Input } from "@material-ui/core";
import InfiniteTable from "../../components/helpers/InfiniteTable";
import { Link } from "react-router-dom";
import * as sitesActions from "../../actions/sitesActions";
import * as frameActions from "../../actions/frameActions";
import debounce from "lodash/debounce";
jest.mock("lodash/debounce", () => jest.fn(fn => fn));

describe("SitesGroups", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SiteGroups {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      sitegroups: [
        {
          pk: 1,
          name: "Test",
          description: "A test group",
          notes:
            "This is a test group.\r\n\r\nThis area is used to link to other resources.\r\n\r\nWe might at some stage make this field support Markdown or something, but straight text is fine for now.\r\n\r\n### Heading\r\n* Link 1\r\n* Link 2\r\n* Link 3",
          sites: [
            {
              pk: 156,
              string: "Test Site 1",
              decommissioned: false
            },
            {
              pk: 114,
              string: "Test Site 2",
              decommissioned: false
            }
          ],
          keys: [
            {
              value: "",
              enabled: true,
              description: ""
            }
          ]
        }
      ],
      page: 1,
      hasMore: false,
      pending: false,
      dispatch: jest.fn(),
      classes: {
        search: "search",
        subscribed: "subscribed",
        customWidth: "customWidth"
      }
    };
    wrapper = undefined;
  });

  it("renders a `Helmet`", () => {
    expect(setup().find(Helmet).length).toBe(1);
  });

  it("renders `Grid`", () => {
    expect(setup().find(Grid).length).toBe(1);
  });

  it("renders `InfiniteTable`", () => {
    expect(setup().find(InfiniteTable).length).toBe(1);
  });

  describe("rendered `Helmet`", () => {
    it("is passed `Site Groups | Mataara` as `title` prop", () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop("title")).toBe("Site Groups | Mataara");
    });
  });

  describe("rendered `Grid`", () => {
    it("contains `Input`", () => {
      const grid = setup().find(Grid);
      expect(grid.find(Input).length).toBe(1);
    });
  });

  describe("rendered `Input`", () => {
    it("is passed `state.filters.search` as `value` prop when the state has value", () => {
      setup().setState({ filters: { search: "test search" } });
      const input = setup().find(Input);
      expect(input.prop("value")).toBe(setup().state("filters").search);
    });

    it("is passed empty strings as `value` prop when the state has no value", () => {
      const input = setup().find(Input);
      expect(input.prop("value")).toBe("");
    });

    it("is passed `this.handleSearch` fn as `onChange` prop", () => {
      const instance = setup().instance();
      const input = setup().find(Input);
      expect(input.prop("onChange")).toBe(instance.handleSearch);
    });

    it("has `search` class", () => {
      const input = setup().find(Input);
      expect(input.prop("className").includes("search")).toBe(true);
    });
  });

  describe("rendered `InfiniteTable`", () => {
    it("is passed `this.loadMore` fn as `loadMore` prop", () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop("loadMore")).toBe(instance.loadMore);
    });

    it("is passed `props.hasMore` as `hasMore` prop", () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop("hasMore")).toBe(props.hasMore);
    });

    it("is passed an array as `tableHeader` prop", () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop("tableHeader"))).toBe(true);
    });

    it("is passed an array as `tableData` prop", () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop("tableData"))).toBe(true);
    });

    it("is passed `state.sortOptions` as `sortOptions` prop", () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop("sortOptions")).toBe(setup().state("sortOptions"));
    });

    it("is passed `this.handleSort` fn as `onSortChange` prop", () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop("onSortChange")).toBe(instance.handleSort);
    });

    describe("passed `tableHeader`", () => {
      it("has `Site Group` and it is sortable", () => {
        const header = setup()
          .find(InfiniteTable)
          .prop("tableHeader");
        expect(header[0].title).toBe("Site Group");
        expect(header[0].sortable).toBe(true);
      });

      it("has `Description` and it is not sortable", () => {
        const header = setup()
          .find(InfiniteTable)
          .prop("tableHeader");
        expect(header[1].title).toBe("Description");
        expect(header[1].sortable).toBe(false);
      });

      it("has `Number of Sites` and it is not sortable", () => {
        const header = setup()
          .find(InfiniteTable)
          .prop("tableHeader");
        expect(header[2].title).toBe("Number of Sites");
        expect(header[2].sortable).toBe(false);
      });
    });

    describe("passed `tableData`", () => {
      it("has `Link` to site group details page", () => {
        const data = setup()
          .find(InfiniteTable)
          .prop("tableData");
        expect(data[0][0]).toEqual(
          <Link to="sitegroups/1">
            <strong>{props.sitegroups[0].name}</strong>
          </Link>
        );
      });

      it("has `description` string", () => {
        const data = setup()
          .find(InfiniteTable)
          .prop("tableData");
        expect(data[0][1]).toBe("A test group");
      });

      it("has `number of sites` value", () => {
        const data = setup()
          .find(InfiniteTable)
          .prop("tableData");
        expect(data[0][2]).toBe(props.sitegroups[0].sites.length);
      });
    });
  });

  describe("`componentDidMount` method", () => {
    it("calls `this.doFetch` with param `1`", () => {
      const instance = setup().instance();
      jest.spyOn(instance, "doFetch");

      instance.componentDidMount();

      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });

    it("dispatches `setPageTitle` with param `Site Groups`", () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      frameActions.setPageTitle.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2); // inside doFetch + setPageTitle
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith("Site Groups");
    });
  });

  describe("`loadMore` method", () => {
    it("calls `this.doFetch` with param `props.page + 1` if `props.pending` is false", () => {
      const instance = setup().instance();
      jest.spyOn(instance, "doFetch");

      instance.loadMore();

      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(2);
    });

    it("does not call `this.doFetch` if `props.pending` is true", () => {
      props.pending = true;
      const instance = setup().instance();
      jest.spyOn(instance, "doFetch");

      instance.loadMore();

      expect(instance.doFetch).not.toHaveBeenCalled();
    });
  });

  describe("`doFetch` method", () => {
    it("dispatches `fetchSiteGroups` with params `state.filters`, given `page`, `state.sortOptions`", () => {
      sitesActions.fetchSiteGroups = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      sitesActions.fetchSiteGroups.mockClear();
      const filters = setup().state("filters");
      const page = 1;
      const sortOptions = setup().state("sortOptions");

      instance.doFetch(page);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(sitesActions.fetchSiteGroups).toHaveBeenCalledTimes(1);
      expect(sitesActions.fetchSiteGroups).toHaveBeenCalledWith(
        filters,
        page,
        sortOptions
      );
    });
  });

  describe("`debouncedFetch` method", () => {
    it("calls `doFetch` with 500ms debounce", () => {
      debounce.mockClear();
      const instance = setup().instance();
      jest.spyOn(instance, "doFetch");

      instance.debouncedFetch(3);

      expect(debounce).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).not.toHaveBeenCalled();
      setTimeout(() => {
        expect(instance.doFetch).toHaveBeenCalledWith(3);
      }, 600);
    });
  });

  describe("`handleSearch` method", () => {
    it("sets `state.filters.search` with given `event.target.value`", () => {
      const instance = setup().instance();
      const event = { target: { value: "search" } };
      instance.handleSearch(event);
      expect(setup().state("filters").search).toBe("search");
    });

    it("calls `debouncedFetch` with param `1`", () => {
      const instance = setup().instance();
      jest.spyOn(instance, "debouncedFetch");
      const event = { target: { value: "search" } };
      instance.handleSearch(event);
      expect(instance.debouncedFetch).toHaveBeenCalledTimes(1);
      expect(instance.debouncedFetch).toHaveBeenCalledWith(1);
    });
  });

  describe("`handleSort` method", () => {
    it("sets state with given `sortBy` and `sortDir`", () => {
      const instance = setup().instance();
      expect(setup().state("sortOptions").sortBy).toBe("title");
      expect(setup().state("sortOptions").sortDir).toBe("asc");
      instance.handleSort("environment", "desc");
      expect(setup().state("sortOptions").sortBy).toBe("environment");
      expect(setup().state("sortOptions").sortDir).toBe("desc");
    });

    it("calls `doFetch` with param `1`", () => {
      const instance = setup().instance();
      jest.spyOn(instance, "doFetch");
      instance.handleSort("environment", "desc");
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });
});
