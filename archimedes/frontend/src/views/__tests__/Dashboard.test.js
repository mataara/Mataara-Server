import React from "react";
import { shallow } from "enzyme";

import { Dashboard } from "../Dashboard";
import { Helmet } from "react-helmet";
import { setPageTitle } from "../../actions/frameActions";
import SubscribedSites from "../../components/Dashboard/SubscribedSites";
import LatestDrupalAdvisories from "../../components/Dashboard/LatestDrupalAdvisories";
import DrupalSitesByMajorVersion from "../../components/Dashboard/DrupalSitesByMajorVersion";
import DrupalSitesByVersion from "../../components/Dashboard/DrupalSitesByVersion";

describe("Dashboard", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Dashboard {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      dispatch: jest.fn(),
      classes: {
        search: "search"
      }
    };
    wrapper = undefined;
  });

  it("renders a `Helmet`", () => {
    expect(setup().find(Helmet).length).toBe(1);
  });

  it("renders a `SubscribedSites`", () => {
    expect(setup().find(SubscribedSites).length).toBe(1);
  });

  it("renders a `LatestDrupalAdvisories`", () => {
    expect(setup().find(LatestDrupalAdvisories).length).toBe(1);
  });

  it("renders a `DrupalSitesByMajorVersion`", () => {
    expect(setup().find(DrupalSitesByMajorVersion).length).toBe(1);
  });

  it("renders a `DrupalSitesByVersion`", () => {
    expect(setup().find(DrupalSitesByVersion).length).toBe(1);
  });

  describe("rendered `Helmet`", () => {
    it("is passed `Dashboard | Mataara` as `title` prop", () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop("title")).toBe("Dashboard | Mataara");
    });
  });

  describe("`componentDidMount` method", () => {
    it("dispatches `setPageTitle` with param `Dashboard`", () => {
      const instance = setup().instance();
      instance.componentDidMount();
      expect(props.dispatch).toHaveBeenCalledWith(setPageTitle("Dashboard"));
    });
  });
});
