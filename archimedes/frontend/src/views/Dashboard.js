import React, { Component } from "react";
import { PropTypes } from "prop-types";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";

import { setPageTitle } from "../actions/frameActions";
import SubscribedSites from "../components/Dashboard/SubscribedSites";
import LatestDrupalAdvisories from "../components/Dashboard/LatestDrupalAdvisories";
import DrupalSitesByMajorVersion from "../components/Dashboard/DrupalSitesByMajorVersion";
import DrupalSitesByVersion from "../components/Dashboard/DrupalSitesByVersion";
import { fetchDrupalStats } from "../actions/dashboardActions";

export class Dashboard extends Component {
  componentDidMount() {
    this.props.dispatch(setPageTitle("Dashboard"));
    this.props.dispatch(fetchDrupalStats(1));
  }

  render() {
    const { drupalStats } = this.props;
    const advisories =
      (drupalStats && drupalStats.latest_drupal_advisories) || [];
    const coreVersionStats =
      (drupalStats && drupalStats.core_version_data) || [];
    const drupalVersionLabels =
      (drupalStats && drupalStats.drupal_version_labels) || [];
    const drupalVersionData =
      (drupalStats && drupalStats.drupal_version_data) || [];

    return (
      <React.Fragment>
        <Helmet title="Dashboard | Mataara" />

        <Grid container spacing={3}>
          <Grid item xs={12} lg={6}>
            <SubscribedSites />
          </Grid>

          <Grid item xs={12} lg={6}>
            <LatestDrupalAdvisories advisories={advisories} />
          </Grid>
        </Grid>

        <Grid container spacing={3}>
          <Grid item xs={12} lg={4}>
            <DrupalSitesByMajorVersion coreVersionStats={coreVersionStats} />
          </Grid>

          <Grid item xs={12} lg={8}>
            <DrupalSitesByVersion
              drupalVersionLabels={drupalVersionLabels}
              drupalVersionData={drupalVersionData}
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  drupalStats: state.dashboard.drupalStats,
  subscribedSites: state.dashboard.subscribedSites
});

export default connect(mapStateToProps)(Dashboard);
