import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import debounce from 'lodash/debounce';
import { withStyles } from "@material-ui/core/styles";
import Input from '@material-ui/core/Input';

import InfiniteTable from '../components/helpers/InfiniteTable';
import CustomTabs from '../components/helpers/CustomTabs';
import CustomTab from '../components/helpers/CustomTab';
import { fetchModules } from '../actions/modulesActions';
import { setPageTitle } from '../actions/frameActions';
import { removeDecommissioned } from '../components/helpers/formatData';

export class Modules extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {},
      sortOptions: {
        sortBy: 'name',
        sortDir: 'asc'
      },
      activeTab: 'all',
    };

    this.loadMore = this.loadMore.bind(this);
    this.doFetch = this.doFetch.bind(this);
    this.debouncedFetch = debounce(this.doFetch.bind(this), 500);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  componentDidMount() {
    this.doFetch(1);
    this.props.dispatch(setPageTitle('Modules & Themes'));
  }

  loadMore() {
    if (!this.props.pending) this.doFetch(this.props.page + 1);
  }

  doFetch(page) {
    const { filters, sortOptions } = this.state;
    this.props.dispatch(fetchModules(filters, page, sortOptions));
  }

  handleTabChange(event, value) {
    this.setState({
      activeTab: value,
      filters: value === 'all' ? {} : { type: value },
    }, () => this.doFetch(1));
  }

  handleSearch(event) {
    // Add search query to existing filters (= core/contrib) if any
    this.setState({
      filters: {
        ...this.state.filters,
        search: event.target.value,
      }
    }, () => this.debouncedFetch(1));
  }

  handleSort(sortBy, sortDir) {
    this.setState({
      sortOptions: {
        sortBy,
        sortDir
      }
    }, () => this.doFetch(1));
  }

  render() {
    const { modules, hasMore, classes } = this.props;
    const { filters, sortOptions, activeTab } = this.state;
    const searchText = filters.search || '';

    const tableHeader = [
      { title: 'Name', sortable: true, sortKey: 'name', style: { minWidth: 200 } },
      { title: 'Slug', sortable: false },
      { title: 'Sites Using Vulnerable Release', sortable: false },
      { title: 'Sites Using', sortable: true, sortKey: 'sites_using__count' },
      { title: 'Maintenance Status', sortable: false },
      { title: 'Development Status', sortable: false },
      { title: 'URL', sortable: false },
    ];

    const tableData = modules.map(module => {
      return [
        <Link to={`drupal/modules/${module.slug}`}><strong>{module.name || '[No name]'}</strong></Link>,
        module.slug,
        removeDecommissioned(module.sites_vulnerable).length,
        removeDecommissioned(module.sites_using).length,
        module.maintenance_status,
        module.development_status,
        module.url && <a href={module.url}>{module.url}</a>,
      ];
    });

    return (
      <React.Fragment>
        <Helmet title="Modules & Themes | Mataara" />

        <CustomTabs
          value={activeTab}
          onChange={this.handleTabChange}
        >
          <CustomTab label="All" value="all" />
          <CustomTab label="Modules" value="project_module" />
          <CustomTab label="Themes" value="project_theme" />
        </CustomTabs>

        <Input
          value={searchText}
          onChange={this.handleSearch}
          placeholder="Search Modules/Themes ..."
          autoFocus
          className={classes.search}
        />

        <InfiniteTable
          loadMore={this.loadMore}
          hasMore={hasMore}
          tableHeader={tableHeader}
          tableData={tableData}
          sortOptions={sortOptions}
          onSortChange={this.handleSort}
        />
      </React.Fragment>
    );
  }
}

Modules.propTypes = {
  modules: PropTypes.arrayOf(PropTypes.shape({
    pk: PropTypes.number,
    name: PropTypes.string,
    slug: PropTypes.string,
    sites_vulnerable: PropTypes.arrayOf(PropTypes.shape({
      decommissioned: PropTypes.bool.isRequired,
    })),
    sites_using: PropTypes.arrayOf(PropTypes.shape({
      decommissioned: PropTypes.bool.isRequired,
    })),
    maintenance_status: PropTypes.string,
    development_status: PropTypes.string,
    url: PropTypes.string,
  })).isRequired,
  page: PropTypes.number.isRequired,
  hasMore: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  search: {
    marginBottom: '1rem',
  },
});

const mapStateToProps = state => ({
  modules: state.modules.list,
  page: state.modules.page,
  hasMore: !!state.modules.payload.next,
  pending: state.modules.pending,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(Modules);
