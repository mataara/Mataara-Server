import React from 'react';
import { shallow } from 'enzyme';

import { DrupalInfo } from '../DrupalInfo';
import InfoTable from '../../helpers/InfoTable';
import CustomChip from '../../helpers/CustomChip';
import { Link } from 'react-router-dom';

describe('DrupalInfo', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<DrupalInfo {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteDetails: {
        core: '7.54',
        core_version: '7.54',
        slogan: 'test slogan',
        nodes: 10,
        revisions: 11,
        users: 12,
        vulnerable_core: [
          { project: "http://mataara-server.local:8000/api/drupal/projects/1/", slug: "drupal:insecure_version"},
        ],
        vulnerable_modules: [
          { project: 'http://mataara-server.local:8000/api/drupal/projects/4/', slug: 'slug:4' },
          { project: 'http://mataara-server.local:8000/api/drupal/projects/5/', slug: 'slug:5' },
        ],
        vulnerable_themes: [
          { project: 'http://mataara-server.local:8000/api/drupal/projects/14/', slug: 'slug:14' },
          { project: 'http://mataara-server.local:8000/api/drupal/projects/15/', slug: 'slug:15' },
        ],
        modules: ['module 1', 'module 2'],
        themes: ['theme 1', 'theme 2'],
      },
      classes: {
        vulnerable: 'vulnerable',
      },
    };
    wrapper = undefined;
  });

  it('renders an `InfoTable`', () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe('data passed to `InfoTable`', () => {
    it('contains `Core` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Core');
      expect(item.value).toBe(props.siteDetails.core);
    });

    it('contains `Core version` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Core version');
      expect(item.value).toBe(props.siteDetails.core_version);
    });

    describe('`Vulnerable core?` when no vulnerabilities', () => {
      it('renders a `CustomChip` with color=success', () => {
        props.siteDetails.vulnerable_core = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Vulnerable core?');
        expect(item.value).toEqual(
          <CustomChip label='No core vulnerablilities' color='success' />
        );
      });
    });

    describe('`Vulnerable core?` when core is vulnerable', () => {
      it('renders `CustomChip`s for vulnerable core(s)', () => {
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Vulnerable core?');
        const value = shallow(item.value);
        expect(value.find(CustomChip).length).toBe(1);
        expect(value.contains('Core drupal:insecure_version is vulnerable'));

      });
    });

    it('contains `Site Slogan` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Site Slogan');
      expect(item.value).toBe(props.siteDetails.slogan);
    });

    it('contains `Content Nodes` and its value is the passed number', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Content Nodes');
      expect(item.value).toBe(props.siteDetails.nodes);
    });

    it('contains `Revision` and its value is the passed number', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Revision');
      expect(item.value).toBe(props.siteDetails.revisions);
    });

    it('contains `Users` and its value is the passed number', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Users');
      expect(item.value).toBe(props.siteDetails.users);
    });

    describe('`Vulnerable modules` when no vulnerable modules', () => {
      it('renders a `CustomChip` with color=success', () => {
        props.siteDetails.vulnerable_modules = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Vulnerable modules');
        expect(item.value).toEqual(
          <CustomChip label='No vulnerable modules' color='success' />
        );
      });
    });

    describe('`Vulnerable modules` when there are vulnerable modules', () => {
      it('renders `Link`s to the vulnerable modules', () => {
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Vulnerable modules');
        const value = shallow(item.value);
        expect(value.find(Link).length).toBe(2);
        expect(value.contains('slug:4')).toBe(true);
        expect(value.contains('slug:5')).toBe(true);
      });
    });

    describe('`Vulnerable themes` when no vulnerable themes', () => {
      it('renders a `CustomChip` with color=success', () => {
        props.siteDetails.vulnerable_themes = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Vulnerable themes');
        expect(item.value).toEqual(
          <CustomChip label='No vulnerable modules' color='success' />
        );
      });
    });

    describe('`Vulnerable themes` when there are vulnerable themes', () => {
      it('renders `Link`s to the vulnerable themes', () => {
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Vulnerable themes');
        const value = shallow(item.value);
        expect(value.find(Link).length).toBe(2);
        expect(value.contains('slug:14')).toBe(true);
        expect(value.contains('slug:15')).toBe(true);
      });
    });

    describe('`Modules` when no modules', () => {
      it('renders null', () => {
        props.siteDetails.modules = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Modules');
        expect(item.value).toBeNull();
      });
    });

    describe('`Modules` when there are modules', () => {
      let item;
      beforeEach(() => {
        const data = setup().find(InfoTable).props().data;
        item = data.find(item => item.key === 'Modules');
      });

      it('renders `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.text()).toBe('<Expansion />');
      });

      it('passes the number of modules as `summary` to `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.props().summary).toBe(2);
      });

      it('renders list of modules', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.find('li').length).toBe(2);
        expect(list.contains('module 1')).toBe(true);
        expect(list.contains('module 2')).toBe(true);
      });
    });

    describe('`Themes` when no themes', () => {
      it('renders null', () => {
        props.siteDetails.themes = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Themes');
        expect(item.value).toBeNull();
      });
    });

    describe('`Themes` when there are themes', () => {
      let item;
      beforeEach(() => {
        const data = setup().find(InfoTable).props().data;
        item = data.find(item => item.key === 'Themes');
      });

      it('renders `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.text()).toBe('<Expansion />');
      });

      it('passes the number of themes as `summary` to `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.props().summary).toBe(2);
      });

      it('renders list of themes', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.find('li').length).toBe(2);
        expect(list.contains('theme 1')).toBe(true);
        expect(list.contains('theme 2')).toBe(true);
      });
    });

  });
});
