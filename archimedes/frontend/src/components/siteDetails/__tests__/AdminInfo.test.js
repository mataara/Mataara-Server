import React from "react";
import { shallow } from "enzyme";

import SortableTable from "../../helpers/SortableTable";
import Expansion from "../../helpers/Expansion";
import { AdminInfo } from "../AdminInfo";
import CustomChip from "../../helpers/CustomChip";

describe("AdminInfo", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<AdminInfo {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteDetails: {
        report__adminrole: {
          AdminRole: {
            RoleName: "Site Administator",
            RoleUsers: {
              "45": {
                Name: "testAdmin1",
                LastLogin: "1453172806",
                Mail: "test1@test.com",
                Status: "0"
              },
              "12": {
                Name: "testAdmin2",
                LastLogin: "1489720341",
                Mail: "test2@test.com",
                Status: "1"
              }
            }
          },
          RoleID: "3"
        }
      },
      classes: {
        vulnerable: "vulnerable"
      }
    };
    wrapper = undefined;
  });

  it("renders `No Info` message when admin role is missing", () => {
    props.siteDetails = {};
    expect(setup().text()).toMatch("No Admin Information Available");
  });

  it("renders an `Expansion`", () => {
    expect(setup().find(Expansion).length).toBe(1);
  });

  describe("rendered `Expansion`", () => {
    let expansion;
    beforeEach(() => {
      expansion = setup().find(Expansion);
    });

    it("is passed `summary` prop which is a jsx that contains role name", () => {
      const summary = shallow(expansion.prop("summary"));
      expect(summary.text()).toMatch(/Site Administator/);
    });

    it("is passed `details` prop which is a jsx that contains `SortableTable`", () => {
      const details = shallow(expansion.prop("details"));
      expect(details.find(SortableTable).length).toBe(1);
    });

    describe("rendered `SortableTable`", () => {
      let details, sortableTable;
      beforeEach(() => {
        details = shallow(expansion.prop("details"));
        sortableTable = details.find(SortableTable);
      });

      it("is passed an array as `tableHeader` prop", () => {
        expect(Array.isArray(sortableTable.prop("tableHeader"))).toBe(true);
      });

      it("is passed an array as `tableData` prop", () => {
        expect(Array.isArray(sortableTable.prop("tableData"))).toBe(true);
      });

      describe("passed `tableHeader`", () => {
        let header;
        beforeEach(() => {
          header = sortableTable.prop("tableHeader");
        });

        it("has `Id` and it is not sortable", () => {
          expect(header[0].title).toBe("Id");
          expect(header[0].sortable).toBe(false);
        });

        it("has `Name` and it is not sortable", () => {
          expect(header[1].title).toBe("Name");
          expect(header[1].sortable).toBe(false);
        });

        it("has `Email` header and it is not sortable", () => {
          expect(header[2].title).toBe("Email");
          expect(header[2].sortable).toBe(false);
        });

        it("has `Status` header and it is not sortable", () => {
          expect(header[3].title).toBe("Status");
          expect(header[3].sortable).toBe(false);
        });

        it("has `Last Login` header and it is not sortable", () => {
          expect(header[4].title).toBe("Last Login");
          expect(header[4].sortable).toBe(false);
        });
      });

      describe("passed `tableData`", () => {
        let data;
        beforeEach(() => {
          data = sortableTable.prop("tableData");
        });

        it("has `id` string", () => {
          expect(data[1][0]).toEqual("45");
          expect(data[0][0]).toEqual("12");
        });

        it("has `name` string", () => {
          expect(data[1][1]).toEqual("testAdmin1");
          expect(data[0][1]).toEqual("testAdmin2");
        });

        it("has `email` anchor", () => {
          const data12 = shallow(data[1][2]);
          expect(data12.find("a").length).toBe(1);
          expect(data12.text()).toMatch(/test1@test.com/);
        });

        it("has `status` CustomChip", () => {
          expect(data[1][3]).toEqual(<CustomChip label="Not Active" />);
          expect(data[0][3]).toEqual(
            <CustomChip label="Active" color="success" />
          );
        });

        it("has `last login` string", () => {
          expect(data[1][4]).toEqual("19 Jan 2016, 04:06 pm");
          expect(data[0][4]).toEqual("17 Mar 2017, 04:12 pm");
        });
      });
    });
  });
});
