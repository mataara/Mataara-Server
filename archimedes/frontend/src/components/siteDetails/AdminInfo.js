import React from "react";
import Moment from "moment";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import SortableTable from "../helpers/SortableTable";
import CustomChip from "../helpers/CustomChip";
import Expansion from "../helpers/Expansion";
import { DATE_FORMAT } from "../../constants/date";

export const AdminInfo = ({ siteDetails, classes }) => {
  if (
    !siteDetails ||
    !siteDetails.report__adminrole ||
    !siteDetails.report__adminrole.AdminRole
  ) {
    return (
      <div className={classes.wrapper}>
        <div className={classes.noInfo}>No Admin Information Available</div>
      </div>
    );
  }

  const adminRole = siteDetails.report__adminrole.AdminRole;

  const tableHeader = [
    { title: "Id", sortable: false },
    { title: "Name", sortable: false },
    { title: "Email", sortable: false },
    { title: "Status", sortable: false },
    { title: "Last Login", sortable: false }
  ];

  const tableData = [];
  for (let [id, user] of Object.entries(adminRole.RoleUsers)) {
    tableData.push([
      id,
      user.Name,
      <a href={`mailto:${user.Mail}`}>{user.Mail}</a>,
      user.Status === "1" ? (
        <CustomChip label="Active" color="success" />
      ) : (
        <CustomChip label="Not Active" />
      ),
      Moment.unix(user.LastLogin).format(DATE_FORMAT)
    ]);
  }

  return (
    <div className={classes.wrapper}>
      <Expansion
        summary={
          <div className={classes.summary}>
            Admin Role: {adminRole.RoleName}
          </div>
        }
        details={
          <div className={classes.table}>
            <SortableTable tableHeader={tableHeader} tableData={tableData} />
          </div>
        }
      />
    </div>
  );
};

AdminInfo.propTypes = {
  siteDetails: PropTypes.shape({
    report__adminrole: PropTypes.shape({
      AdminRole: PropTypes.shape({
        RoleName: PropTypes.string.isRequired,
        RoleUsers: PropTypes.object.isRequired
      })
    })
  })
};

const styles = theme => ({
  wrapper: {
    border: `1px solid ${theme.palette.border}`,
    borderRadius: 4,
    paddingRight: 16,
  },
  summary: {
    paddingLeft: 16,
    fontSize: "0.875rem",
    fontWeight: 700
  },
  table: {
    marginRight: -16,
  },
  noInfo: {
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 16,
    fontSize: "0.875rem",
    fontWeight: 700
  }
});

export default withStyles(styles)(AdminInfo);
