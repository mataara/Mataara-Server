import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';

import InfoTable from '../helpers/InfoTable';
import Expansion from '../helpers/Expansion';
import CustomChip from '../helpers/CustomChip';
import { getSlug, renderList } from '../helpers/formatData';

const alphabeticalSort = (a, b) => {
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
};

const renderExpansion = (data) => {
  if (!Array.isArray(data) || data.length === 0) return null;
  const details = renderList(data.sort(alphabeticalSort));
  return <Expansion summary={data.length} details={details} />;
};

export const DrupalInfo = ({ siteDetails, classes }) => {

  // Inner function to use classes

  // Retired: since we can't be sure about Advisories, use renderIsCoreVulnerable
  //
  // const renderCoreVulnerabilities = (coreVulnerabilities) => {
  //   if (!Array.isArray(coreVulnerabilities) || coreVulnerabilities.length === 0) {
  //     return <CustomChip label='No vulnerablilities' color='success' />;
  //   }
  //   const links = coreVulnerabilities.map(advisory => (
  //     <Link to={`/drupal/advisories/${advisory.pk}`} key={advisory.pk} className={classes.vulnerable}>
  //       {advisory.title}
  //     </Link>
  //   ));
  //   return renderList(links);
  // };

  const renderIsCoreVulnerable = (vulnerableCore) => {
    if (!Array.isArray(vulnerableCore) || vulnerableCore.length === 0) {
      return <CustomChip label='No core vulnerablilities' color='success' />;
    }
    const links = vulnerableCore.map(release => (
      <CustomChip label={'Core ' + release.slug + ' is vulnerable'} color='error'/>
    ));
    return renderList(links);
  };

  const renderVulnerable = (vulnerableModules) => {
    if (!Array.isArray(vulnerableModules) || vulnerableModules.length === 0) {
      return <CustomChip label='No vulnerable modules' color='success' />;
    }
    const links = vulnerableModules.map(module => {
      const slug = getSlug(module.project);
      return (
        <Link to={`/drupal/modules/${slug}`} key={slug} className={classes.vulnerable}>
          {module.slug}
        </Link>
      );
    });
    return renderList(links);
  };

  const data = [
    { key: 'Core', value: siteDetails.core },
    { key: 'Core version', value: siteDetails.core_version },
    // { key: 'Core vulnerabilities', value: renderCoreVulnerabilities(siteDetails.core_vulnerabilities) },
    { key: 'Vulnerable core?', value: renderIsCoreVulnerable(siteDetails.vulnerable_core)},
    { key: 'Site Slogan', value: siteDetails.slogan },
    { key: 'Content Nodes', value: siteDetails.nodes },
    { key: 'Revision', value: siteDetails.revisions },
    { key: 'Users', value: siteDetails.users },
    { key: 'Vulnerable modules', value: renderVulnerable(siteDetails.vulnerable_modules) },
    { key: 'Vulnerable themes', value: renderVulnerable(siteDetails.vulnerable_themes) },
    { key: 'Modules', value: renderExpansion(siteDetails.modules) },
    { key: 'Themes', value: renderExpansion(siteDetails.themes) },
  ];

  return (
    <InfoTable title='Drupal info' data={data} />
  );
};

DrupalInfo.propTypes = {
  siteDetails: PropTypes.shape({
    core: PropTypes.string,
    core_version: PropTypes.string,
    core_vulnerabilities: PropTypes.arrayOf(PropTypes.shape({
      pk: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
    })),
    slogan: PropTypes.string,
    nodes: PropTypes.number,
    revisions: PropTypes.number,
    users: PropTypes.number,
    vulnerable_core: PropTypes.arrayOf(PropTypes.shape({
      slug: PropTypes.string.isRequired,
    })),
    vulnerable_modules: PropTypes.arrayOf(PropTypes.shape({
      project: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
    })),
    vulnerable_themes: PropTypes.arrayOf(PropTypes.shape({
      project: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
    })),
    modules: PropTypes.arrayOf(PropTypes.string.isRequired),
    themes: PropTypes.arrayOf(PropTypes.string.isRequired),
  }).isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  vulnerable: {
    color: theme.palette.error.main,
    fontWeight: 500,
  },
});

export default withStyles(styles)(DrupalInfo);
