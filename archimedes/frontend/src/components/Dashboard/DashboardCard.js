import React from "react";
import { PropTypes } from "prop-types";
import { Card, CardHeader, CardContent } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

export const DashboardCard = ({ title, noPadding, children, classes }) => (
  <Card className={classes.card}>
    <CardHeader
      title={title}
      titleTypographyProps={{
        classes: {
          h5: classes.cardHeaderTypography
        }
      }}
      className={classes.cardHeader}
    />
    <CardContent className={noPadding ? classes.noPadding : ""}>
      {children}
    </CardContent>
  </Card>
);

DashboardCard.propTypes = {
  title: PropTypes.string.isRequired,
  noPadding: PropTypes.bool,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired
};

const styles = theme => ({
  card: {
    border: `1px solid ${theme.palette.border}`,
    boxShadow: "none"
  },
  cardHeader: {
    backgroundColor: theme.head.backgroundColor,
    borderBottom: `1px solid ${theme.palette.border}`
  },
  cardHeaderTypography: {
    fontSize: "1rem",
    fontWeight: 500
  },
  noPadding: {
    padding: "0 !important"
  }
});

export default withStyles(styles)(DashboardCard);
