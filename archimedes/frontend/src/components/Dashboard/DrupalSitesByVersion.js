import React from "react";
import { PropTypes } from "prop-types";
import C3Chart from "react-c3js";
import "c3/c3.css";
import { withStyles } from "@material-ui/core/styles";

import DashboardCard from "./DashboardCard";

const DrupalSitesByVersion = ({
  drupalVersionLabels,
  drupalVersionData,
  theme
}) => {
  const data = {
    type: "bar",
    x: "x",
    columns: [
      ["x", ...drupalVersionLabels],
      ["Site count", ...drupalVersionData]
    ]
  };
  const bar = { width: { ratio: 0.5 } };
  const legend = { show: false };
  const axis = { x: { type: "category" } };
  const color = { pattern: [theme.palette.primary.main] };

  return (
    <DashboardCard title="Active Drupal sites by version">
      <C3Chart
        data={data}
        bar={bar}
        legend={legend}
        axis={axis}
        color={color}
      />
    </DashboardCard>
  );
};

DrupalSitesByVersion.propTypes = {
  drupalVersionLabels: PropTypes.array.isRequired,
  drupalVersionData: PropTypes.array.isRequired,
  theme: PropTypes.object.isRequired
};

const styles = theme => ({});

export default withStyles(styles, { withTheme: true })(DrupalSitesByVersion);
