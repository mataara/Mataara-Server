import React from "react";
import { PropTypes } from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Table, TableHead, TableBody, TableRow, TableCell } from "@material-ui/core";
import { grey } from '@material-ui/core/colors';

export const DashboardTable = ({ tableHeader, tableData, classes }) => (
  <Table size="small">
    <TableHead>
      <TableRow className={classes.headRow}>
        {tableHeader.map((head, i) => (
          <TableCell
            key={i}
            padding={head.padding}
            style={head.style}
            align={head.numeric ? "right" : "inherit" }
            className={classes.head}
          >
            {head.title}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>

    <TableBody>
      {tableData.map((row, i) => (
        <TableRow key={i} className={classes.bodyRow}>
          {row.map((cell, i) => (
            <TableCell
              key={i}
              padding={tableHeader[i].padding}
              style={tableHeader[i].style}
              align={tableHeader[i].numeric ? "right" : "inherit" }
            >
              {cell !== null && cell !== undefined && cell !== ""
                ? cell
                : "---"}
            </TableCell>
          ))}
        </TableRow>
      ))}
    </TableBody>
  </Table>
);

DashboardTable.propTypes = {
  tableHeader: PropTypes.array.isRequired,
  tableData: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired
};

const styles = theme => ({
  head: {
    color: grey[600],
    fontSize: "0.75rem",
    fontWeight: 500,
    lineHeight: 1,
  },
  headRow: {
    height: 40
  },
  bodyRow: {
    height: 36
  }
});

export default withStyles(styles)(DashboardTable);
