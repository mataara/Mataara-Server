import React from "react";
import { PropTypes } from "prop-types";
import { Link } from "react-router-dom";

import DashboardCard from "./DashboardCard";
import DashboardTable from "./DashboardTable";

const LatestDrupalAdvisories = ({ advisories }) => {
  const tableHeader = [
    { title: "Advisory" },
    { title: "Potentially insecure", numeric: true }
  ];

  const tableData = advisories.map(advisory => [
    <Link to={`drupal/advisories/${advisory.pk}`}>{advisory.title}</Link>,
    advisory.affected
  ]);

  return (
    <DashboardCard title="Latest Drupal security advisories" noPadding>
      {tableData.length > 0 && (
        <DashboardTable tableHeader={tableHeader} tableData={tableData} />
      )}
    </DashboardCard>
  );
};

LatestDrupalAdvisories.propTypes = {
  advisories: PropTypes.array
};

export default LatestDrupalAdvisories;
