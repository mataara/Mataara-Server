import React from "react";
import { PropTypes } from "prop-types";
import C3Chart from "react-c3js";
import "c3/c3.css";
import { withStyles } from "@material-ui/core/styles";

import DashboardCard from "./DashboardCard";

const DrupalSitesByMajorVersion = ({ coreVersionStats, theme }) => {
  const data = {
    type: "pie",
    columns: coreVersionStats
      .sort((a, b) => a[0] - b[0]) // sort by major version number
      .map(item => [`Drupal ${item[0]}`, item[1]]),
    order: null
  };
  const color = {
    pattern: [
      theme.palette.primary.dark,
      theme.palette.primary.main,
      theme.palette.primary.light,
      theme.palette.secondary.dark,
      theme.palette.secondary.main,
      theme.palette.secondary.light
    ]
  };
  const axis = { x: { type: "category" } };

  return (
    <DashboardCard title="Active Drupal sites by major version">
      <C3Chart data={data} axis={axis} color={color} />
    </DashboardCard>
  );
};

DrupalSitesByMajorVersion.propTypes = {
  coreVersionStats: PropTypes.array.isRequired
};

const styles = theme => ({});

// export default DrupalSitesByMajorVersion;
export default withStyles(styles, { withTheme: true })(
  DrupalSitesByMajorVersion
);
