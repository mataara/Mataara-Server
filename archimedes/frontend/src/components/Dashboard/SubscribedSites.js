import React, { Component } from "react";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { Checkbox, Tooltip } from "@material-ui/core";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import StarIcon from "@material-ui/icons/Star";

import { renderMozillaObservatory } from "../helpers/formatData";
import { fetchSubscribedSites } from "../../actions/dashboardActions";
import { sitesToggleSubscription } from "../../actions/sitesActions";
import DashboardCard from "./DashboardCard";
import DashboardTable from "./DashboardTable";

const explainMozillaObservatory =
  "Observatory by Mozilla is a project designed to help developers, system administrators, and security professionals configure their sites safely and securely.";

export class SubscribedSites extends Component {
  constructor(props) {
    super(props);

    this.toggleSubscription = this.toggleSubscription.bind(this);
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchSubscribedSites());
  }

  toggleSubscription(pk) {
    this.props.dispatch(sitesToggleSubscription(pk));
  }

  render() {
    const { subscribedSites, classes } = this.props;
    const tableHeader = [
      {
        title: <div style={{ width: 35 }} />,
        padding: "none"
      },
      {
        title: "Site",
        style: { paddingLeft: 0, paddingRight: 0 }
      },
      {
        title: "Vulnerabilities",
        numeric: true,
        style: { paddingLeft: 0 }
      },
      {
        title: (
          <Tooltip
            title={explainMozillaObservatory}
            classes={{ tooltip: classes.customWidth }}
          >
            <span>Mozilla Observatory</span>
          </Tooltip>
        ),
        numeric: true
      }
    ];

    const tableData = subscribedSites.map(site => [
      <Checkbox
        checked={site.subscribed}
        onChange={() => this.toggleSubscription(site.pk)}
        icon={<StarBorderIcon />}
        checkedIcon={<StarIcon className={classes.subscribed} />}
      />,
      <Link to={`sites/${site.pk}`}>
        <strong>{site.title}</strong> (on {site.hostname})
      </Link>,
      site.vulnerability_count,
      renderMozillaObservatory(site.url)
    ]);

    return (
      <DashboardCard title="Subscribed Sites" noPadding>
        {tableData.length > 0 && (
          <DashboardTable tableHeader={tableHeader} tableData={tableData} />
        )}

        {tableData.length === 0 && (
          <div className={classes.empty}>
            <StarBorderIcon className={classes.emptyStar} />
            <p>Star a site to see it listed here</p>
          </div>
        )}
      </DashboardCard>
    );
  }
}

SubscribedSites.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  subscribedSites: state.dashboard.subscribedSites
});

const styles = theme => ({
  customWidth: {
    maxWidth: 300
  },
  subscribed: {
    color: theme.palette.yellow
  },
  empty: {
    height: 300,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    color: theme.palette.text.disabled
  },
  emptyStar: {
    color: theme.palette.border,
    fontSize: "4rem"
  }
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(SubscribedSites);
