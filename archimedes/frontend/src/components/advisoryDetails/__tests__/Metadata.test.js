import React from 'react';
import { shallow } from 'enzyme';

import Metadata from '../Metadata';
import InfoTable from '../../helpers/InfoTable';
import CustomChip from '../../helpers/CustomChip';
import Chip from '@material-ui/core/Chip';
import { Link } from 'react-router-dom';

describe('Metadata', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Metadata {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      advisoryDetails: {
        guid: 'test guid',
        title: 'test title',
        url: 'https://example.com',
        source: 'Drupal Core',
        date_posted: '2018-08-15T12:32:53Z',
        date_updated: '2018-10-23T20:20:55.953089Z',
        date_parsed: '2018-08-29T03:57:06.664785Z',
        status_display: 'Needs Review',
        affected_sites: [
          { pk: 12, string: 'test site 12' },
          { pk: 34, string: 'test site 34' },
        ],
      },
    };
    wrapper = undefined;
  });

  it('renders an `InfoTable`', () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe('data passed to `InfoTable`', () => {
    let data;
    beforeEach(() => {
      data = setup().find(InfoTable).props().data;
    });

    it('contains `GUID` and its value is the passed string', () => {
      const item = data.find(item => item.key === 'GUID');
      expect(item.value).toBe(props.advisoryDetails.guid);
    });

    it('contains `Title` and its value is the passed string', () => {
      const item = data.find(item => item.key === 'Title');
      expect(item.value).toBe(props.advisoryDetails.title);
    });

    it('contains `URL` and its value is a link to the passed url', () => {
      const item = data.find(item => item.key === 'URL');
      const value = shallow(item.value);
      expect(value.find('a').length).toBe(1);
      expect(value.find('a').props().href).toBe(props.advisoryDetails.url);
    });

    it('contains `Source` and its value is the passed string', () => {
      const item = data.find(item => item.key === 'Source');
      expect(item.value).toBe(props.advisoryDetails.source);
    });

    it('contains `Date posted` and its value is the formatted date', () => {
      const item = data.find(item => item.key === 'Date posted');
      expect(item.value).toBe('16 Aug 2018, 12:32 am');
    });

    it('contains `Date updated` and its value is the formatted date', () => {
      const item = data.find(item => item.key === 'Date updated');
      expect(item.value).toBe('24 Oct 2018, 09:20 am');
    });

    it('contains `Date parsed` and its value is the formatted date', () => {
      const item = data.find(item => item.key === 'Date parsed');
      expect(item.value).toBe('29 Aug 2018, 03:57 pm');
    });

    it('contains `Status` and its value is `CustomChip`', () => {
      const item = data.find(item => item.key === 'Status');
      const value = shallow(item.value).dive();
      expect(value.find(Chip).length).toBe(1);
    });

    describe('`Status`s rendered `CustomChip`', () => {
      it('is passed `success` as `color` prop when `status` is `Needs Review`', () => {
        const item = data.find(item => item.key === 'Status');
        expect(item.value).toEqual(
          <CustomChip label='Needs Review' color='success' />
        );
      });

      it('is passed `error` as `color` prop when `status` is `Parsing Failed`', () => {
        props.advisoryDetails.status_display = 'Parsing Failed';
        wrapper = undefined;

        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Status');
        expect(item.value).toEqual(
          <CustomChip label='Parsing Failed' color='error' />
        );
      });
    });

    it('contains `Affected sites` and its value is a node', () => {
      const item = data.find(item => item.key === 'Affected sites');
      const value = shallow(item.value);
      expect(value.exists()).toBe(true);
    });

    describe('`Affected sites` when no sites affected', () => {
      it('is a `CustomChip` with color=success', () => {
        props.advisoryDetails.affected_sites = [];
        wrapper = undefined;

        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Affected sites');
        expect(item.value).toEqual(
          <CustomChip label='No sites affected' color='success' />
        );
      });
    });

    describe('`Affected sites` when there are affected sites', () => {
      let item;
      let value;
      beforeEach(() => {
        item = data.find(item => item.key === 'Affected sites');
        value = shallow(item.value);
      });

      it('renders an error `CustomChip`', () => {
        expect(value.find(CustomChip).length).toBe(1);
        expect(value.find(CustomChip).props().color).toBe('error');
      });

      it('renders links to affected sites', () => {
        const links = value.find(Link);
        expect(links.length).toBe(2);
        expect(links.at(0).props().to).toBe('/sites/12');
        expect(links.at(0).contains('test site 12')).toBe(true);
        expect(links.at(1).props().to).toBe('/sites/34');
        expect(links.at(1).contains('test site 34')).toBe(true);
      });
    });
  });
});
