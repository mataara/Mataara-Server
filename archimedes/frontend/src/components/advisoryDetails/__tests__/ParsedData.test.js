import React from 'react';
import { shallow } from 'enzyme';

import { ParsedData } from '../ParsedData';
import InfoTable from '../../helpers/InfoTable';
import { Link } from 'react-router-dom';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';

describe('ParsedData', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<ParsedData {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      advisoryDetails: {
        advisory_id: 'test-id',
        cores: ['7.x', '8.x'],
        project: 'https://mataara.catalyst.net.nz/api/drupal/projects/2089/',
        releases: [
          { slug: 'slug1', date: '2018-10-16T17:43:00Z' },
          { slug: 'slug2', date: '2016-02-26T09:28:59Z' },
        ],
        risk: 'Moderately critical',
        risk_score: 10,
        risk_score_total: 25,
        vulnerabilities: ['vulnerability 1', 'vulnerability 2', 'vulnerability 3'],
        cves: ['CVE-2018-14773', 'cve 2', 'CVE-2018-12345', 'CVE-2018-67890'],
      },
      classes: {
        innerTableRow: 'innerTableRow',
      },
    };
    wrapper = undefined;
  });

  it('renders an `InfoTable`', () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe('data passed to `InfoTable`', () => {
    let data;
    beforeEach(() => {
      data = setup().find(InfoTable).props().data;
    });

    it('contains `Advisory ID` and its value is the passed string', () => {
      const item = data.find(item => item.key === 'Advisory ID');
      expect(item.value).toBe(props.advisoryDetails.advisory_id);
    });

    it('contains `Cores` and its value is a list of the passed string', () => {
      const item = data.find(item => item.key === 'Cores');
      const value = shallow(item.value);
      expect(value.find('li').length).toBe(2);
      expect(value.contains('7.x')).toBe(true);
      expect(value.contains('8.x')).toBe(true);
    });
    
    it('contains `Project` and its value is a link to the project detail page', () => {
      const item = data.find(item => item.key === 'Project');
      expect(item.value).toEqual(
        <Link to={'/drupal/modules/2089'}>Project detail</Link>
      );
    });

    it('contains `Affected releases` and its value is a node', () => {
      const item = data.find(item => item.key === 'Affected releases');
      const value = shallow(item.value);
      expect(value.exists()).toBe(true);
    });

    describe('`Affected releases` when no releases affected', () => {
      it('is null', () => {
        props.advisoryDetails.releases = [];
        wrapper = undefined;
        const data = setup().find(InfoTable).props().data;

        const item = data.find(item => item.key === 'Affected releases');
        expect(item.value).toBeNull();
      });
    });

    describe('`Affected releases` when there are affected releases', () => {
      let item;
      beforeEach(() => {
        item = data.find(item => item.key === 'Affected releases');
      });
      it('renders `Expansion`', () => {
        const value = shallow(item.value);
        // console.log(value.text())
        // console.log(value.debug())
        expect(value.text()).toBe('<Expansion />');
      });

      it('passes the number of releases as `summary` to `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.props().summary).toBe(2);
      });

      it('passes a `Table` as `details` to `Expansion`', () => {
        const value = shallow(item.value);
        const table = shallow(value.props().details);
        expect(table.debug().includes('Table')).toBe(true);
      });

      it('has matching number of rows to the number of releases', () => {
        const value = shallow(item.value);
        const table = shallow(value.props().details);
        const body = table.find(TableBody);
        expect(body.find(TableRow).length).toBe(2);
      });

      it('renders `slug` of the releases', () => {
        const value = shallow(item.value);
        const table = shallow(value.props().details);
        const body = table.find(TableBody);
        expect(body.contains('slug1')).toBe(true);
        expect(body.contains('slug2')).toBe(true);
      });

      it('renders `date` of the releases', () => {
        const value = shallow(item.value);
        const table = shallow(value.props().details);
        const body = table.find(TableBody);
        expect(body.contains('17 Oct 2018, 06:43 am')).toBe(true);
        expect(body.contains('26 Feb 2016, 10:28 pm')).toBe(true);
      });
    });

    it('contains `Risk` and its value is the passed string', () => {
      const item = data.find(item => item.key === 'Risk');
      expect(item.value).toBe(props.advisoryDetails.risk);
    });

    describe('`Risk score`', () => {
      it('has string value of `risk_score / risk_score_total`', () => {
        const item = data.find(item => item.key === 'Risk score');
        expect(item.value).toBe('10 / 25');
      });

      it('is null when `risk_score` is not defined', () => {
        props.advisoryDetails.risk_score = null;
        wrapper = undefined;
        const data = setup().find(InfoTable).props().data;

        const item = data.find(item => item.key === 'Risk score');
        expect(item.value).toBeNull();
      });

      it('is null when `risk_score_total` is not defined', () => {
        props.advisoryDetails.risk_score_total = null;
        wrapper = undefined;
        const data = setup().find(InfoTable).props().data;

        const item = data.find(item => item.key === 'Risk score');
        expect(item.value).toBeNull();
      });
    });

    it('contains `Vulnerabilities` and its value is a list of the passed string', () => {
      const item = data.find(item => item.key === 'Vulnerabilities');
      const value = shallow(item.value);
      expect(value.find('li').length).toBe(3);
      expect(value.contains('vulnerability 1')).toBe(true);
      expect(value.contains('vulnerability 2')).toBe(true);
      expect(value.contains('vulnerability 3')).toBe(true);
    });
    
    it('contains `CVEs` and its value is a list', () => {
      const item = data.find(item => item.key === 'CVEs');
      const value = shallow(item.value);
      expect(value.find('li').length).toBe(4);
    });

    describe('`CVEs`', () => {
      it('shows a link to CVE when the strings is CVE ID', () => {
        const item = data.find(item => item.key === 'CVEs');
        const value = shallow(item.value);
        expect(value.find('a').length).toBe(3);
        const link = value.find('a').first();
        expect(link.prop('href')).toBe('https://cve.mitre.org/cgi-bin/cvename.cgi?name=2018-14773');
        expect(link.contains('CVE-2018-14773')).toBe(true);
      });

      it('shows the strings when the strings is not CVE ID', () => {
        const item = data.find(item => item.key === 'CVEs');
        const value = shallow(item.value);
        expect(value.contains('cve 2')).toBe(true);
      });
    });
  });
});
