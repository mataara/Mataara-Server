import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { withStyles } from "@material-ui/core/styles";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';
import InfiniteScroll from 'react-infinite-scroller';

import SortableHeader from './SortableHeader';

export class InfiniteTable extends Component {
  render() {
    const {
      loadMore, hasMore,
      tableHeader, tableData,
      sortOptions, onSortChange,
      classes
    } = this.props;
    const defaultPadding = 'default';
    return (
      <InfiniteScroll
        initialLoad={false}
        loadMore={loadMore}
        hasMore={hasMore}
        loader={<div key='loader' className={classes.loader}><CircularProgress size={60} /></div>}
      >
        <div className={classes.tableWrapper}>
          <Table size="small" className={classes.table}>

            <TableHead>
              <TableRow>
                {tableHeader.map(head => {
                  const padding = head.padding || defaultPadding;
                  return (
                    <TableCell key={head.title} padding={padding} className={classes.head} style={head.style}>
                      {head.sortable &&
                        <SortableHeader
                          title={head.title}
                          sortKey={head.sortKey}
                          currentOptions={sortOptions}
                          onSortChange={onSortChange}
                        />
                      }
                      {!head.sortable &&
                        head.title
                      }
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableHead>

            <TableBody>
              {tableData.map((row, i) => (
                <TableRow key={i}>
                  {row.map((cell, i) => {
                    const head = tableHeader[i];
                    const padding = head.padding || defaultPadding;
                    return (
                      <TableCell key={i} padding={padding} style={head.style}>
                        {(cell !== null && cell !== undefined && cell !== '')
                          ? cell
                          : '---'
                        }
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>

          </Table>
        </div>
      </InfiniteScroll>
    );
  }
}

InfiniteTable.propTypes = {
  loadMore: PropTypes.func.isRequired,
  hasMore: PropTypes.bool.isRequired,
  tableHeader: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node.isRequired,
      sortable: PropTypes.bool.isRequired,
      sortKey: PropTypes.string,
      padding: PropTypes.string,
      style: PropTypes.object,
    }).isRequired
  ).isRequired,
  tableData: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.node,
    )
  ).isRequired,
  sortOptions: PropTypes.shape({
    sortBy: PropTypes.string.isRequired,
    sortDir: PropTypes.string.isRequired,
  }).isRequired,
  onSortChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  head: {
    backgroundColor: theme.head.backgroundColor,
    color: theme.head.color,
    fontSize: '.75rem',
    lineHeight: 1,
  },
  tableWrapper: {
    overflowX: 'scroll',
  },
  table: {
    border: `1px solid ${theme.palette.border}`,
  },
  loader: {
    width: '100%', 
    marginTop: '1rem', 
    textAlign: 'center',
  },
});

export default withStyles(styles)(InfiniteTable);
