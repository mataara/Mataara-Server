import React from 'react';
import moment from 'moment';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import { DATE_FORMAT } from '../../constants/date';

export const getDate = date => {
  if (!date) return null;
  return moment(date).format(DATE_FORMAT);
};

export const renderUrl = url => {
  if (!url) return null;
  return <a href={url} target="_blank" rel="noopener noreferrer">{url}</a>;
};

export const renderList = array => {
  if (!array || array.length === 0) return null;
  return (
    <ul>
      {array.map((item, i) => (
        <li key={i}>{item}</li>
      ))}
    </ul>
  );
};

export const getPk = apiUrl => {
  if (!apiUrl) return null;
  const match = apiUrl.match(/(\d+)\/$/);
  return match ? match[1] : null;
};

export const getSlug = apiUrl => {
  if (!apiUrl) return null;
  const match = apiUrl.match(/(\w+)\/$/);
  return match ? match[1] : null;
};

export const removeDecommissioned = sites => {
  if (!Array.isArray(sites) || sites.length === 0) return [];
  return sites.filter(site => !site.decommissioned);
};

export const renderMozillaObservatory = url => {
  if (!url) return null;
  const parser = document.createElement('a');
  parser.href = url;
  const siteHost = parser.host;
  return (
    <a href={'https://observatory.mozilla.org/analyze.html?host=' + siteHost} target="_blank" rel="noopener noreferrer">
      <OpenInNewIcon />
    </a>
  );
};
