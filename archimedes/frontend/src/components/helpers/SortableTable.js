import React, { Component } from "react";
import { PropTypes } from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Table, TableBody, TableHead, TableCell, TableRow } from "@material-ui/core";

import SortableHeader from "./SortableHeader";

export class SortableTable extends Component {
  render() {
    const {
      title,
      tableHeader,
      tableData,
      sortOptions,
      onSortChange,
      classes
    } = this.props;
    const defaultPadding = "default";
    return (
      <div className={classes.tableWrapper}>
        <Table size="small" className={classes.table}>
          <TableHead>
            {title && (
              <TableRow>
                <TableCell
                  className={classes.title}
                  colSpan={tableHeader.length}
                  size="medium"
                >
                  {title}
                </TableCell>
              </TableRow>
            )}
            <TableRow>
              {tableHeader.map(head => {
                const padding = head.padding || defaultPadding;
                return (
                  <TableCell
                    key={head.title}
                    padding={padding}
                    className={classes.head}
                    style={head.style}
                  >
                    {head.sortable ? (
                      <SortableHeader
                        title={head.title}
                        sortKey={head.sortKey}
                        currentOptions={sortOptions}
                        onSortChange={onSortChange}
                      />
                    ) : (
                      head.title
                    )}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>

          <TableBody>
            {tableData.map((row, i) => (
              <TableRow key={i}>
                {row.map((cell, i) => {
                  const head = tableHeader[i];
                  const padding = head.padding || defaultPadding;
                  return (
                    <TableCell key={i} padding={padding} style={head.style}>
                      {cell !== null && cell !== undefined && cell !== ""
                        ? cell
                        : "---"}
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );
  }
}

SortableTable.propTypes = {
  tableHeader: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.node.isRequired,
      sortable: PropTypes.bool,
      sortKey: PropTypes.string,
      padding: PropTypes.string,
      style: PropTypes.object
    }).isRequired
  ).isRequired,
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.node)).isRequired,
  sortOptions: PropTypes.shape({
    sortBy: PropTypes.string.isRequired,
    sortDir: PropTypes.string.isRequired
  }),
  onSortChange: PropTypes.func,
  classes: PropTypes.object
};

const styles = theme => ({
  title: {
    backgroundColor: theme.head.backgroundColor,
    color: theme.head.color,
    fontSize: "1rem",
    fontWeight: 500
  },
  head: {
    backgroundColor: theme.head.backgroundColor,
    color: theme.head.color,
    fontSize: '.75rem',
    lineHeight: 1,
  },
  tableWrapper: {
    overflowX: "scroll",
    borderRadius: 4
  },
  table: {
    border: `1px solid ${theme.palette.border}`,
    whiteSpace: "nowrap",
  },
  loader: {
    width: "100%",
    marginTop: "1rem",
    textAlign: "center"
  }
});

export default withStyles(styles)(SortableTable);
