import React from 'react';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import classNames from 'classnames';
import { withStyles } from "@material-ui/core/styles";

export const CustomChip = ({ label, color, classes }) => (
  <Chip
    label={label}
    className={classNames(classes.customChip, {
      [classes.success]: color === 'success',
      [classes.error]: color === 'error',
    })}
  />
);

CustomChip.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  customChip: {
    backgroundColor: theme.palette.grey[500],
    color: theme.palette.common.white,
    fontWeight: 700,
  },
  success: {
    backgroundColor: theme.palette.secondary.main,
  },
  error: {
    backgroundColor: theme.palette.error.main,
  },
});

export default withStyles(styles)(CustomChip);
