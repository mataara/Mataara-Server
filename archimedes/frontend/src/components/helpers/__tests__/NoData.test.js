import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { NoData } from '../NoData';

describe('NoData', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<NoData {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      classes: { noData: 'noData' },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<NoData {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders a `span`', () => {
    expect(setup().find('span').length).toBe(1);
  });

  describe('rendered `span`', () => {
    it('contains `No data` string', () => {
      expect(setup().find('span').contains('No data')).toBe(true);
    });

    it('has `noData` class passed as props', () => {
      expect(setup().find('span').props().className).toBe(props.classes.noData);
    });
  });
});
