import React from "react";
import { mount } from "enzyme";
// import renderer from 'react-test-renderer';

import { SortableTable } from "../SortableTable";
import { Table, TableBody, TableHead, TableCell, TableRow } from "@material-ui/core";

describe("SortableTable", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = mount(
        <div>
          <SortableTable {...props} />
        </div>
      );
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      tableHeader: [
        { title: "a", sortable: true, sortKey: "a", style: { minWidth: 300 } },
        { title: "b", sortable: false },
        { title: "c", sortable: false, padding: "none" }
      ],
      tableData: [
        ["d", "e", null],
        ["g", "h", undefined],
        ["j", "k", ""],
        ["m", "n", 0]
      ],
      sortOptions: {
        sortBy: "title",
        sortDir: "asc"
      },
      onSortChange: function() {},
      classes: {
        head: "head",
        tableWrapper: "tableWrapper",
        table: "table"
      }
    };
    wrapper = undefined;
  });

  it("renders a `Table`", () => {
    expect(setup().find(Table).length).toBe(1);
  });

  it("renders a `TableHead`", () => {
    expect(setup().find(TableHead).length).toBe(1);
  });

  describe("rendered `TableHead`", () => {
    let tableHead;
    beforeEach(() => {
      tableHead = setup().find(TableHead);
    });

    it("contains corresponding number of `TableCell`s to tableHeader prop", () => {
      expect(tableHead.find(TableCell).length).toBe(3);
    });

    describe("when `head.padding` is set", () => {
      it("passes `head.padding` to `TableCell` as `padding` prop", () => {
        const tableCell = tableHead.find(TableCell).last();
        expect(tableCell.props().padding).toEqual("none");
      });
    });

    describe("when `head.padding` is not set", () => {
      it("passes `default` to `TableCell` as `padding` prop", () => {
        const tableCell = tableHead.find(TableCell).at(1);
        expect(tableCell.props().padding).toEqual("default");
      });
    });

    describe("when `head.style` is set", () => {
      it("passes `head.style` to `TableCell` as `style` prop", () => {
        const tableCell = tableHead.find(TableCell).first();
        expect(tableCell.props().style).toEqual(props.tableHeader[0].style);
      });
    });
  });

  it("renders a `TableBody`", () => {
    expect(setup().find(TableBody).length).toBe(1);
  });

  describe("rendered `TableBody`", () => {
    let tableBody;
    beforeEach(() => {
      tableBody = setup().find(TableBody);
    });

    it("contains corresponding number of `TableRows` to tableData prop", () => {
      expect(tableBody.find(TableRow).length).toBe(4);
    });

    describe("rendered `TableRow`", () => {
      it("contains corresponding number of `TableCell` to tableData prop", () => {
        const tableRow = tableBody.find(TableRow).first();
        expect(tableRow.find(TableCell).length).toBe(3);
      });
    });

    describe("rendered `TableCell`", () => {
      it("renders contents of tableData", () => {
        const tableRow = tableBody.find(TableRow).first();
        const tableCell = tableRow.find(TableCell).first();
        expect(tableCell.contains("d")).toBeTruthy();
      });
    });

    describe("when tableData content is null, undefined, or empty string", () => {
      it("renders `---`", () => {
        const tableCellNull = tableBody
          .find(TableRow)
          .at(0)
          .find(TableCell)
          .last();
        expect(tableCellNull.contains("---")).toBeTruthy();

        const tableCellUndef = tableBody
          .find(TableRow)
          .at(1)
          .find(TableCell)
          .last();
        expect(tableCellUndef.contains("---")).toBeTruthy();

        const tableCellEmptyStr = tableBody
          .find(TableRow)
          .at(2)
          .find(TableCell)
          .last();
        expect(tableCellEmptyStr.contains("---")).toBeTruthy();
      });
    });

    describe("when tableData content is 0", () => {
      it("renders 0", () => {
        const tableCellZero = tableBody
          .find(TableRow)
          .at(3)
          .find(TableCell)
          .last();
        expect(tableCellZero.text()).toEqual("0");
      });
    });

    describe("when `head.padding` is set", () => {
      it("passes `head.padding` to `TableCell` as `padding` prop", () => {
        const tableRow = tableBody.find(TableRow).first();
        const tableCell = tableRow.find(TableCell).last();
        expect(tableCell.props().padding).toEqual("none");
      });
    });

    describe("when `head.padding` is not set", () => {
      it("passes `default` to `TableCell` as `padding` prop", () => {
        const tableRow = tableBody.find(TableRow).first();
        const tableCell = tableRow.find(TableCell).at(1);
        expect(tableCell.props().padding).toEqual("default");
      });
    });

    describe("when `head.style` is set", () => {
      it("passes `head.style` to `TableCell` as `style` prop", () => {
        const tableCell = tableBody.find(TableCell).first();
        expect(tableCell.props().style).toEqual(props.tableHeader[0].style);
      });
    });
  });
});
