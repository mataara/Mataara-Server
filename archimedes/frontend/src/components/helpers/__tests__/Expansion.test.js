import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { Expansion } from '../Expansion';
import { Accordion, AccordionSummary, AccordionDetails } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

describe('Expansion', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Expansion {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      summary: 'summary',
      details: 'details',
      classes: {
        panel: 'panel',
        summary: 'summary',
        details: 'details',
      },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<Expansion {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders `Accordion`', () => {
    expect(setup().find(Accordion).length).toBe(1);
  });

  it('renders `AccordionSummary`', () => {
    expect(setup().find(AccordionSummary).length).toBe(1);
  });

  describe('rendered `AccordionSummary`', () => {
    it('contains `summary` node', () => {
      const accordionSummary = setup().find(AccordionSummary);
      expect(accordionSummary.contains(props.summary)).toBeTruthy();
    });

    it('is passed `ExpandMoreIcon` as `expandIcon` prop', () => {
      const accordionSummary = setup().find(AccordionSummary);
      expect(accordionSummary.props().expandIcon).toEqual(<ExpandMoreIcon />);
    });
  });

  it('renders `AccordionDetails`', () => {
    expect(setup().find(AccordionDetails).length).toBe(1);
  });

  describe('rendered `AccordionDetails`', () => {
    it('contains `details` node', () => {
      const accordionDetails = setup().find(AccordionDetails);
      expect(accordionDetails.contains(props.details)).toBeTruthy();
    });
  });
});
