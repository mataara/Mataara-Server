import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { InfoTable } from '../InfoTable';
import NoData from '../NoData';

describe('InfoTable', () => {
  let props;
  let wrapper;
  const infoTable = () => {
    if (!wrapper) {
      wrapper = shallow(<InfoTable {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      title: 'Test InfoTable',
      data: [
        { key: 'Name', value: 'Test Site' },
        { key: 'Hostname', value: 'test-site-1' },
        { key: 'URL', value: 'http://test-site-1.com/' },
        { key: 'Slogan', value: '' },
      ],
      classes: { tableWrapper: '', head: '', key: '' },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<InfoTable {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = infoTable().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = infoTable().find("div");
      // When using .find, enzyme arranges the nodes in order such
      // that the outermost node is first in the list. So we can
      // use .first() to get the outermost div.
      const wrappingDiv = divs.first();

      // Enzyme omits the outermost node when using the .children()
      // method on lockScreen(). This is annoying, but we can use it
      // to verify that wrappingDiv contains everything else this
      // component renders.
      expect(wrappingDiv.children()).toEqual(infoTable().children());
    });
  });

  it('always renders a `Table`', () => {
    expect(infoTable().find(Table).length).toBe(1);
  });

  describe('rendered `Table`', () => {
    it('does not receive any props except for children', () => {
      const table = infoTable().find(Table);
      // Passing children to the Table so there is 1
      expect(Object.keys(table.props()).length).toBe(1);
    });
  });

  it('always renders a `TableHead`', () => {
    expect(infoTable().find(TableHead).length).toBe(1);
  });

  describe('rendered `TableHead`', () => {
    it('does not receive any props except for children', () => {
      const head = infoTable().find(TableHead);
      // Passing children to the Table so there is 1
      expect(Object.keys(head.props()).length).toBe(1);
    });

    it('renders props.title', () => {
      const head = infoTable().find(TableHead);
      const titleCell = head.find(TableCell).first();
      expect(titleCell.props().children).toBe(props.title);
    });
  });

  it('always renders a `TableBody`', () => {
    expect(infoTable().find(TableBody).length).toBe(1);
  });

  describe('rendered `TableBody`', () => {
    it('does not receive any props except for children', () => {
      const body = infoTable().find(TableBody);
      // Passing children to the Table so there is 1
      expect(Object.keys(body.props()).length).toBe(1);
    });

    it('renders matching number of rows to props.data', () => {
      const body = infoTable().find(TableBody);
      const rows = body.first().find(TableRow);
      expect(rows.length).toBe(props.data.length);
    });
  });

  describe('rendered `TableRow`s', () => {
    it('renders key/value from props.data', () => {
      const body = infoTable().find(TableBody);
      const rows = body.first().find(TableRow);
      const topRowCells = rows.first().find(TableCell);
      expect(topRowCells.first().props().children).toBe(props.data[0].key);
      expect(topRowCells.last().props().children).toBe(props.data[0].value);
    });

    it('renders `NoData` if value is an empty string', () => {
      const body = infoTable().find(TableBody);
      const rows = body.first().find(TableRow);
      const empty = rows.last().find(TableCell).last();
      expect(empty.find(NoData).length).toBe(1);
    });
  });
});
