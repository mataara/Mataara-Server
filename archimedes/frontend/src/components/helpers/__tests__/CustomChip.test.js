import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { CustomChip } from '../CustomChip';
import Chip from '@material-ui/core/Chip';

describe('CustomChip', () => {
  let props;
  let wrapper;
  const customChip = () => {
    if (!wrapper) {
      wrapper = shallow(<CustomChip {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      label: 'test',
      classes: {
        customChip: 'customChip',
        success: 'success',
        error: 'error',
      },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<CustomChip {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders `Chip`', () => {
    expect(customChip().find(Chip).length).toBe(1);
  });

  it('passes `label` prop to `Chip` as `label`', () => {
    const chip = customChip().find(Chip);
    expect(chip.props().label).toBe('test');
  });

  describe('when `color` is defined', () => {
    it('adds success class to `Chip` when `color` is "success"', () => {
      props = {
        ...props,
        color: 'success',
      };

      const chip = customChip().find(Chip);
      expect(chip.props().className.includes('success')).toBeTruthy();
    });

    it('adds error class to `Chip` when `color` is "error"', () => {
      props = {
        ...props,
        color: 'error',
      };

      const chip = customChip().find(Chip);
      expect(chip.props().className.includes('error')).toBeTruthy();
    });

    it('does not add success nor error class to `Chip` when `color` is not "success" nor "error"', () => {
      props = {
        ...props,
        color: 'something else',
      };

      const chip = customChip().find(Chip);
      expect(chip.props().className.includes('success')).toBeFalsy();
      expect(chip.props().className.includes('error')).toBeFalsy();
    });
  });

  describe('when `color` is not defined', () => {
    it('does not add success nor error class to `Chip`', () => {
      const chip = customChip().find(Chip);
      expect(chip.props().className.includes('success')).toBeFalsy();
      expect(chip.props().className.includes('error')).toBeFalsy();
    });
  });
});
