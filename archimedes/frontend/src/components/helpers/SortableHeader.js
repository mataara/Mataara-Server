import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import ButtonBase from '@material-ui/core/ButtonBase';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { withStyles } from "@material-ui/core/styles";

export class SortableHeader extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    const { currentOptions, onSortChange, sortKey } = this.props;
    if (currentOptions.sortBy !== sortKey) {
      onSortChange(sortKey, 'asc');
      return;
    }
    const newOrder = currentOptions.sortDir === 'asc' ? 'desc' : 'asc';
    onSortChange(sortKey, newOrder);
  }

  render() {
    const { title, sortKey, currentOptions, classes } = this.props;
    const active = currentOptions.sortBy === sortKey;
    return (
      <ButtonBase disableRipple className={classes.text} onClick={this.handleClick}>
        { title }
        { active && currentOptions.sortDir === 'asc' && <ArrowUpwardIcon className={classes.icon} /> }
        { active && currentOptions.sortDir === 'desc' && <ArrowDownwardIcon className={classes.icon} /> }
        { !active && (
          <React.Fragment>
            <ArrowUpwardIcon className={classes.inactiveIcon} />
            <ArrowDownwardIcon className={classes.inactiveIcon} />
          </React.Fragment>
        ) }
      </ButtonBase>
    );
  }
}

SortableHeader.propTypes = {
  title: PropTypes.string.isRequired,
  sortKey: PropTypes.string.isRequired,
  currentOptions: PropTypes.shape({
    sortBy: PropTypes.string.isRequired,
    sortDir: PropTypes.string.isRequired,
  }).isRequired,
  onSortChange: PropTypes.func.isRequired
};

const styles = theme => ({
  text: {
    fontFamily: "'Roboto', sans-serif",
    fontSize: '.75rem',
    fontWeight: 500,
  },
  icon: {
    fontSize: '1rem',
  },
  inactiveIcon: {
    fontSize: '1rem',
    color: theme.palette.grey[500],
  },
});

export default withStyles(styles)(SortableHeader);
