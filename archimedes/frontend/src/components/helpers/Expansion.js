import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Accordion, AccordionSummary, AccordionDetails } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

export const Expansion = props => {
  const { summary, details, classes } = props;
  return (
    <Accordion className={classes.panel}>
      <AccordionSummary
        className={classes.summary}
        expandIcon={<ExpandMoreIcon />}
      >
        {summary}
      </AccordionSummary>
      <AccordionDetails className={classes.details}>
        {details}
      </AccordionDetails>
    </Accordion>
  );
};

Expansion.propTypes = {
  summary: PropTypes.node.isRequired,
  details: PropTypes.node.isRequired,
  classes: PropTypes.shape({
    panel: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    details: PropTypes.string.isRequired
  }).isRequired
};

const styles = theme => ({
  panel: {
    boxShadow: "none",
  },
  summary: {
    paddingLeft: 0
  },
  details: {
    padding: 0,
    display: "block"
  }
});

export default withStyles(styles)(Expansion);
