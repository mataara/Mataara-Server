import React from 'react';
import { withStyles } from "@material-ui/core/styles";
import Tabs from '@material-ui/core/Tabs';

export const CustomTabs = props => (
  <Tabs
    {...props}
    indicatorColor="primary"
    textColor="primary"
    variant="scrollable"
  >
    {props.children}
  </Tabs>
);

const styles = theme => ({
  root: {
    marginBottom: '1rem',
  },
});

export default withStyles(styles)(CustomTabs);
