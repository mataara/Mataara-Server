import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from "@material-ui/core/styles";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import NoData from './NoData';

export const InfoTable = ({ title, data, classes }) => {
  return (
    <div className={classes.tableWrapper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell className={classes.head} colSpan='2'>
              {title}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(row => (
            <TableRow key={row.key}>
              <TableCell className={classes.key}>{row.key}</TableCell>
              <TableCell>
                {(row.value !== null && row.value !== undefined && row.value !== '')
                  ? row.value
                  : <NoData />
                }
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
};

InfoTable.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  tableWrapper: {
    border: `1px solid ${theme.palette.border}`,
    borderBottom: 0,
    borderRadius: 4,
    overflow: 'hidden',
  },
  head: {
    backgroundColor: theme.head.backgroundColor,
    color: theme.head.color,
    fontSize: '1rem',
    fontWeight: 500,
  },
  key: {
    fontWeight: 700,
  },
});

export default withStyles(styles)(InfoTable);
