import React from "react";
import PropTypes from "prop-types";

import InfoTable from "../helpers/InfoTable";
import NotesSection from "./NotesSection";

const SiteGroupInfo = ({ siteGroupDetails, handleUpdate }) => {
  const data = [
    { key: "Name", value: siteGroupDetails.name },
    { key: "Description", value: siteGroupDetails.description },
    {
      key: "Notes",
      value: (
        <NotesSection
          siteGroupDetails={siteGroupDetails}
          handleUpdate={handleUpdate}
        />
      ),
    },
  ];

  return <InfoTable title="Site Group info" data={data} />;
};

SiteGroupInfo.propTypes = {
  siteGroupDetails: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    notes: PropTypes.string,
  }).isRequired,
  handleUpdate: PropTypes.func.isRequired,
};

export default SiteGroupInfo;
