import React from "react";
// import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import SortableTable from "../helpers/SortableTable";
import Expansion from "../helpers/Expansion";

/**
 * ----------------- SAMPLE DATA -----------------
 */
const contacts = [
  {
    name: " Test User 1",
    email: "user1@example.com",
    link: "",
    project_role: "Developer",
    phone: "222-2222",
    external: "no"
  },
  {
    name: " Test User 2",
    email: "user2@example.com",
    link: "",
    project_role: "Developer",
    phone: "222-2222",
    external: "no"
  },
  {
    name: " Test User 3",
    email: "user3@example.com",
    link: "",
    project_role: "Developer",
    phone: "222-2222",
    external: "no"
  }
];

export const Contacts = ({ siteGroupDetails, classes }) => {
  // Using sample data
  siteGroupDetails.contacts = siteGroupDetails.contacts || contacts;

  if (!siteGroupDetails || !siteGroupDetails.contacts) {
    return (
      <div className={classes.wrapper}>
        <div className={classes.noInfo}>No Contacts Registered</div>
      </div>
    );
  }

  const tableHeader = [
    {
      title: "Name",
      sortable: false
    },
    {
      title: "Email",
      sortable: false
    },
    {
      title: "Project Role",
      sortable: false
    },
    {
      title: "Phone",
      sortable: false
    },
    {
      title: "External",
      sortable: false
    }
  ];

  const tableData = siteGroupDetails.contacts.map(contact => [
    <a href={contact.link} target="_blank" rel="noopener noreferrer">
      {contact.name}
    </a>,
    <a href={`mailto:${contact.email}`}>{contact.email}</a>,
    contact.project_role,
    contact.phone,
    contact.external
  ]);

  return (
    <div className={classes.wrapper}>
      <Expansion
        summary={<div className={classes.summary}>Contacts</div>}
        details={
          <div className={classes.table}>
            <SortableTable tableHeader={tableHeader} tableData={tableData} />
          </div>
        }
      />
    </div>
  );
};

// Contacts.propTypes = {
//   siteGroupDetails: PropTypes.shape({
//     report__adminrole: PropTypes.shape({
//       AdminRole: PropTypes.shape({
//         RoleName: PropTypes.string.isRequired,
//         RoleUsers: PropTypes.object.isRequired
//       })
//     })
//   })
// };

const styles = theme => ({
  wrapper: {
    border: `1px solid ${theme.palette.border}`,
    borderRadius: 4,
    marginBottom: 24
  },
  summary: {
    paddingLeft: 16,
    fontSize: "0.875rem",
    fontWeight: 700
  },
  table: {
    // marginRight: -24
  },
  noInfo: {
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 16,
    fontSize: "0.875rem",
    fontWeight: 700
  }
});

export default withStyles(styles)(Contacts);
