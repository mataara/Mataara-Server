import React from "react";
import PropTypes from "prop-types";
import Markdown from "markdown-to-jsx";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

class NotesSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      newNotes: "",
    };
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate() {
    const { siteGroupDetails, handleUpdate } = this.props;
    handleUpdate({
      name: siteGroupDetails.name,
      description: siteGroupDetails.description,
      notes: this.state.newNotes,
    });
    this.setState({ editMode: false });
  }

  render() {
    const { siteGroupDetails } = this.props;
    return !siteGroupDetails.notes ? null : (
      <React.Fragment>
        {this.state.editMode ? (
          <React.Fragment>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <div style={{ marginBottom: 14 }}>
                  <strong>Edit</strong>
                </div>
                <textarea
                  style={{
                    minWidth: "100%",
                    maxWidth: "100%",
                    minHeight: "300px",
                    height: "calc(100% - 60px)",
                  }}
                  value={this.state.newNotes}
                  onChange={(e) => this.setState({ newNotes: e.target.value })}
                />
                <small>
                  <a
                    href="https://www.markdownguide.org/cheat-sheet/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Markdown
                  </a>{" "}
                  is supported
                </small>
              </Grid>
              <Grid item xs={12} md={6}>
                <div style={{ marginBottom: 14 }}>
                  <strong>Preview</strong>
                </div>
                <div
                  style={{
                    border: "1px solid #ddd",
                    padding: "0.5rem",
                  }}
                >
                  <Markdown className="markdown" options={{ forceBlock: true }}>
                    {this.state.newNotes}
                  </Markdown>
                </div>
              </Grid>
            </Grid>
            <div
              style={{
                marginTop: "1rem",
              }}
            >
              <Button
                color="primary"
                variant="outlined"
                onClick={this.handleUpdate}
                disabled={this.state.newNotes === siteGroupDetails.notes}
                style={{
                  marginRight: "1rem",
                }}
              >
                Update
              </Button>
              <Button
                variant="outlined"
                onClick={() => this.setState({ editMode: false })}
              >
                Cancel
              </Button>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Markdown className="markdown" options={{ forceBlock: true }}>
              {siteGroupDetails.notes}
            </Markdown>
            <Button
              color="primary"
              variant="outlined"
              style={{
                marginTop: "1rem",
              }}
              onClick={() =>
                this.setState({
                  editMode: true,
                  newNotes: siteGroupDetails.notes,
                })
              }
            >
              Edit Notes
            </Button>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

NotesSection.propTypes = {
  siteGroupDetails: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    notes: PropTypes.string,
  }).isRequired,
  handleUpdate: PropTypes.func.isRequired,
};

export default NotesSection;
