import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import SortableTable from "../helpers/SortableTable";
import CustomChip from "../helpers/CustomChip";

const SiteList = ({ siteGroupDetails, sortOptions, onSortChange, classes }) => {
  const tableHeader = [
    {
      title: "Name",
      sortable: true,
      sortKey: "title",
      style: { minWidth: 300, paddingLeft: 20 }
    },
    { title: "Status", sortable: false },
    {
      title: "Vulnerabilities",
      sortable: true,
      sortKey: "vunerability_count",
      style: { minWidth: 300, paddingLeft: 20 }
    },
    {
      title: "Environment",
      sortable: true,
      sortKey: "environment",
      style: { minWidth: 300, paddingLeft: 20 }
    }
  ];

  const tableData = siteGroupDetails.sites
    ? siteGroupDetails.sites.map(site => {
        return [
          <Link to={`/sites/${site.pk}`}>
            <strong>{site.string}</strong>
          </Link>,
          site.decommissioned ? <CustomChip label="Decommissioned" /> : "",
          site.vulnerability_count,
          site.environment
        ];
      })
    : [];

  return (
    <SortableTable
      title="Site list"
      tableHeader={tableHeader}
      tableData={tableData}
      sortOptions={sortOptions}
      onSortChange={onSortChange}
    />
  );
};

SiteList.propTypes = {
  siteGroupDetails: PropTypes.shape({
    sites: PropTypes.arrayOf(
      PropTypes.shape({
        string: PropTypes.string.isRequired,
        decommissioned: PropTypes.bool,
        vulnerability_count: PropTypes.number,
        environment: PropTypes.string
      })
    )
  }).isRequired
};

export default SiteList;
