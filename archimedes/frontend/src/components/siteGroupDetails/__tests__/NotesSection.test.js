import React from "react";
import { shallow } from "enzyme";
import NotesSection from "../NotesSection";
import Markdown from "markdown-to-jsx";
import Button from "@material-ui/core/Button";

describe("NotesSection", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<NotesSection {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteGroupDetails: {
        pk: 1,
        name: "Test",
        description: "A test group",
        notes:
          "This is a test group.\r\n\r\nThis area is used to link to other resources.\r\n\r\nWe might at some stage make this field support Markdown or something, but straight text is fine for now.\r\n\r\n### Heading\r\n* Link 1\r\n* Link 2\r\n* Link 3",
        sites: [
          {
            pk: 156,
            string: "Test Site 1",
            decommissioned: false,
          },
          {
            pk: 114,
            string: "Test Site 2",
            decommissioned: false,
          },
        ],
        keys: [
          {
            value: "",
            enabled: true,
            description: "",
          },
        ],
      },
      handleUpdate: jest.fn(),
    };
    wrapper = undefined;
  });

  describe("Falsy value for notes", () => {
    beforeEach(() => {
      props.siteGroupDetails.notes = undefined;
    });
    it("renders null", () => {
      expect(setup().type()).toBeNull();
    });
  });

  describe("Display mode", () => {
    it("renders `Markdown`", () => {
      expect(setup().find(Markdown).length).toBe(1);
    });
    it("renders edit button", () => {
      expect(setup().find(Button).length).toBe(1);
    });
    it("enters edit mode when edit button is clicked", () => {
      const edit = setup().find(Button);
      edit.simulate("click");
      expect(setup().state("editMode")).toBe(true);
    });
  });

  describe("Edit mode", () => {
    beforeEach(() => {
      setup().setState({
        editMode: true,
      });
    });
    it("renders textarea for editing", () => {
      expect(setup().find("textarea").length).toBe(1);
    });
    it("renders preview `Markdown`", () => {
      expect(setup().find(Markdown).length).toBe(1);
    });
    it("renders update and cancel buttons", () => {
      expect(setup().find(Button).length).toBe(2);
    });
    it("runs `handleUpdate` when update button is clicked", () => {
      setup().find(Button).first().simulate("click");
      expect(props.handleUpdate).toHaveBeenCalled();
    });
    it("goes back to display mode when cancel button is clicked", () => {
      setup().find(Button).last().simulate("click");
      expect(setup().state("editMode")).toBe(false);
    });
  });
});
