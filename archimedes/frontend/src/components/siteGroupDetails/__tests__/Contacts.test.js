import React from "react";
import { shallow } from "enzyme";

import { Contacts } from "../Contacts";
import SortableTable from "../../helpers/SortableTable";
import Expansion from "../../helpers/Expansion";

describe("Contacts", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Contacts {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteGroupDetails: {
        contacts: [
          {
            name: "Test User 1",
            email: "user1@example.com",
            link: "",
            project_role: "Owner",
            phone: "111-1111",
            external: "yes"
          },
          {
            name: "Test User 2",
            email: "user2@example.com",
            link: "",
            project_role: "Developer",
            phone: "222-2222",
            external: "no"
          }
        ]
      },
      classes: {
        vulnerable: "vulnerable"
      }
    };
    wrapper = undefined;
  });

  it("renders an `Expansion`", () => {
    expect(setup().find(Expansion).length).toBe(1);
  });

  describe("rendered `Expansion`", () => {
    let expansion;
    beforeEach(() => {
      expansion = setup().find(Expansion);
    });

    it("is passed `summary` prop which is a jsx that contains `Contacts` string", () => {
      const summary = shallow(expansion.prop("summary"));
      expect(summary.text()).toMatch(/Contacts/);
    });

    it("is passed `details` prop which is a jsx that contains `SortableTable`", () => {
      const details = shallow(expansion.prop("details"));
      expect(details.find(SortableTable).length).toBe(1);
    });

    describe("rendered `SortableTable`", () => {
      let details, sortableTable;
      beforeEach(() => {
        details = shallow(expansion.prop("details"));
        sortableTable = details.find(SortableTable);
      });

      it("is passed an array as `tableHeader` prop", () => {
        expect(Array.isArray(sortableTable.prop("tableHeader"))).toBe(true);
      });

      it("is passed an array as `tableData` prop", () => {
        expect(Array.isArray(sortableTable.prop("tableData"))).toBe(true);
      });

      describe("passed `tableHeader`", () => {
        let header;
        beforeEach(() => {
          header = sortableTable.prop("tableHeader");
        });

        it("has `Name` and it is not sortable", () => {
          expect(header[0].title).toBe("Name");
          expect(header[0].sortable).toBe(false);
        });

        it("has `Email` and it is not sortable", () => {
          expect(header[1].title).toBe("Email");
          expect(header[1].sortable).toBe(false);
        });

        it("has `Project Role` header and it is not sortable", () => {
          expect(header[2].title).toBe("Project Role");
          expect(header[2].sortable).toBe(false);
        });

        it("has `Phone` header and it is not sortable", () => {
          expect(header[3].title).toBe("Phone");
          expect(header[3].sortable).toBe(false);
        });

        it("has `External` header and it is not sortable", () => {
          expect(header[4].title).toBe("External");
          expect(header[4].sortable).toBe(false);
        });
      });

      describe("passed `tableData`", () => {
        let data;
        beforeEach(() => {
          data = sortableTable.prop("tableData");
        });

        it("has `name` anchor", () => {
          const data00 = shallow(data[0][0]);
          expect(data00.find("a").length).toBe(1);
          expect(data00.text()).toMatch("Test User 1");
        });

        it("has `email` anchor", () => {
          const data01 = shallow(data[0][1]);
          expect(data01.find("a").length).toBe(1);
          expect(data01.text()).toMatch(/user1@example.com/);
        });

        it("has `project_role` string", () => {
          expect(data[0][2]).toEqual("Owner");
          expect(data[1][2]).toEqual("Developer");
        });

        it("has `phone` string", () => {
          expect(data[0][3]).toEqual("111-1111");
          expect(data[1][3]).toEqual("222-2222");
        });

        it("has `external` string", () => {
          expect(data[0][4]).toEqual("yes");
          expect(data[1][4]).toEqual("no");
        });
      });
    });
  });
});
