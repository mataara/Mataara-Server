import React from "react";
import { shallow } from "enzyme";

import SiteGroupInfo from "../SiteGroupInfo";
import InfoTable from "../../helpers/InfoTable";
import NotesSection from "../NotesSection";

describe("SiteGroupInfo", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SiteGroupInfo {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteGroupDetails: {
        pk: 1,
        name: "Test",
        description: "A test group",
        notes:
          "This is a test group.\r\n\r\nThis area is used to link to other resources.\r\n\r\nWe might at some stage make this field support Markdown or something, but straight text is fine for now.\r\n\r\n### Heading\r\n* Link 1\r\n* Link 2\r\n* Link 3",
        sites: [
          {
            pk: 156,
            string: "Test Site 1",
            decommissioned: false
          },
          {
            pk: 114,
            string: "Test Site 2",
            decommissioned: false
          }
        ],
        keys: [
          {
            value: "",
            enabled: true,
            description: ""
          }
        ]
      },
      handleUpdate: () => {},
    };
    wrapper = undefined;
  });

  it("renders an `InfoTable`", () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe("data passed to `InfoTable`", () => {
    it("contains `Name` and its value is the passed name", () => {
      const data = setup()
        .find(InfoTable)
        .props().data;
      const item = data.find(item => item.key === "Name");
      expect(item.value).toBe(props.siteGroupDetails.name);
    });

    it("contains `Description` and its value is the passed description", () => {
      const data = setup()
        .find(InfoTable)
        .props().data;
      const item = data.find(item => item.key === "Description");
      expect(item.value).toBe(props.siteGroupDetails.description);
    });

    it("contains `NotesSection` for notes", () => {
      const data = setup()
        .find(InfoTable)
        .props().data;
      const item = data.find(item => item.key === "Notes");
      const value = shallow(item.value);
      expect(value.instance()).toBeInstanceOf(NotesSection);
    });
  });
});
