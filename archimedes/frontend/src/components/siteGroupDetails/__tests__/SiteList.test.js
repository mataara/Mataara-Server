import React from "react";
import { shallow } from "enzyme";
import { Link } from "react-router-dom";

import SiteList from "../SiteList";
import SortableTable from "../../helpers/SortableTable";
import CustomChip from "../../helpers/CustomChip";

describe("SiteList", () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SiteList {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteGroupDetails: {
        pk: 1,
        name: "Test",
        description: "A test group",
        notes:
          "This is a test group.\r\n\r\nThis area is used to link to other resources.\r\n\r\nWe might at some stage make this field support Markdown or something, but straight text is fine for now.\r\n\r\n### Heading\r\n* Link 1\r\n* Link 2\r\n* Link 3",
        sites: [
          {
            pk: 156,
            string: "Test Site 1",
            decommissioned: false,
            vulnerability_count: 10,
            environment: "Development"
          },
          {
            pk: 114,
            string: "Test Site 2",
            decommissioned: true,
            vulnerability_count: 15,
            environment: "Production"
          }
        ],
        keys: [
          {
            value: "",
            enabled: true,
            description: ""
          }
        ]
      }
    };
    wrapper = undefined;
  });

  it("renders a `SortableTable`", () => {
    expect(setup().find(SortableTable).length).toBe(1);
  });

  describe("rendered `SortableTable`", () => {
    let sortableTable;
    beforeEach(() => {
      sortableTable = setup().find(SortableTable);
    });

    it("is passed an array as `tableHeader` prop", () => {
      expect(Array.isArray(sortableTable.prop("tableHeader"))).toBe(true);
    });

    it("is passed an array as `tableData` prop", () => {
      expect(Array.isArray(sortableTable.prop("tableData"))).toBe(true);
    });

    describe("passed `tableHeader`", () => {
      let header;
      beforeEach(() => {
        header = sortableTable.prop("tableHeader");
      });

      it("has `Name` and it is sortable", () => {
        expect(header[0].title).toBe("Name");
        expect(header[0].sortable).toBe(true);
      });

      it("has `Status` and it is not sortable", () => {
        expect(header[1].title).toBe("Status");
        expect(header[1].sortable).toBe(false);
      });

      it("has `Vulnerabilities` header and it is sortable", () => {
        expect(header[2].title).toBe("Vulnerabilities");
        expect(header[2].sortable).toBe(true);
      });

      it("has `Environment` header and it is sortable", () => {
        expect(header[3].title).toBe("Environment");
        expect(header[3].sortable).toBe(true);
      });
    });

    describe("passed `tableData`", () => {
      let data;
      beforeEach(() => {
        data = sortableTable.prop("tableData");
      });

      it("has `name` anchor", () => {
        expect(data[0][0]).toEqual(
          <Link to="/sites/156">
            <strong>{props.siteGroupDetails.sites[0].string}</strong>
          </Link>
        );
      });

      it("renders empty string as `status` for not decommissioned site", () => {
        expect(data[0][1]).toEqual("");
      });

      it("renders decommissioned CustomChip as `status` for decommissioned site", () => {
        expect(data[1][1]).toEqual(<CustomChip label="Decommissioned" />);
      });

      it("has `vunerability_count` number", () => {
        expect(data[0][2]).toEqual(10);
        expect(data[1][2]).toEqual(15);
      });

      it("has `environment` string", () => {
        expect(data[0][3]).toEqual("Development");
        expect(data[1][3]).toEqual("Production");
      });
    });
  });
});
