import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles } from "@material-ui/core/styles";
import { connect } from 'react-redux';
import { compose } from 'redux';

export const TopBar = props => {
  const { open, handleDrawerOpen, pageTitle, classes } = props;
  return (
    <AppBar className={classNames(classes.appBar, {
      [classes.appBarShift]: open,
    })}>
      <Toolbar className={!open ? classes.disableLeftGutter : ''}>
        <IconButton
          color="inherit"
          aria-label="Open drawer"
          onClick={handleDrawerOpen}
          className={classNames(classes.menuButton, open && classes.hide)}
        >
          <MenuIcon />
        </IconButton>

        <Typography variant="h6" color="inherit" noWrap className={classes.flex}>
          {pageTitle}
        </Typography>

        <a href="/logout" className={classes.appBarLink}>
          <Button color="inherit">Sign Out</Button>
        </a>
      </Toolbar>
    </AppBar>
  );
};

TopBar.propTypes = {
  open: PropTypes.bool.isRequired,
  handleDrawerOpen: PropTypes.func.isRequired,
  pageTitle: PropTypes.string,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  flex: {
    flex: 1,
  },

  appBar: {
    position: 'fixed',
    marginLeft: theme.drawer.width,
    boxShadow: 'none',

    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  appBarShift: {
    width: `calc(100% - ${theme.drawer.width}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },

  disableLeftGutter: {
    paddingLeft: 0,
  },

  appBarLink: {
    color: theme.palette.primary.contrastText,

    '&:hover': {
      textDecoration: 'none',
    },
  },

  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },

  hide: {
    display: 'none',
  },
});

const mapStateToProps = state => ({
  pageTitle: state.frame.pageTitle,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(TopBar);
