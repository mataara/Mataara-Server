import React from 'react';
import { shallow } from 'enzyme';

import ModuleInfo from '../ModuleInfo';
import InfoTable from '../../helpers/InfoTable';

describe('ModuleInfo', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<ModuleInfo {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      moduleDetails: {
        name: 'test module',
        slug: 'test_module',
        url: 'https://www.drupal.org/project/navigation404',
        description: 'test description',
        date_updated: '2018-08-02T07:05:29.594791Z',
        custom: true,
        type: 'project_module',
        type_name: 'Module',
        maintenance_status: 'Actively maintained',
        development_status: 'Under active development',
        results: [
          { slug: 'test_module:8.x-1.x-dev' },
          { slug: 'test_module:7.x-1.x-dev' },
          { slug: 'test_module:7.x-1.x' },
        ],
      },
    };
    wrapper = undefined;
  });

  it('renders an `InfoTable`', () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe('data passed to `InfoTable`', () => {
    it('contains `Name` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Name');
      expect(item.value).toBe(props.moduleDetails.name);
    });

    it('contains `Slug` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Slug');
      expect(item.value).toBe(props.moduleDetails.slug);
    });

    it('contains `URL` and its value is a link to the passed url', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'URL');
      const value = shallow(item.value);
      expect(value.find('a').length).toBe(1);
      expect(value.find('a').props().href).toBe(props.moduleDetails.url);
    });

    it('contains `Description` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Description');
      expect(item.value).toBe(props.moduleDetails.description);
    });

    it('contains `Date updated` and its value is the formatted date', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Date updated');
      expect(item.value).toBe('02 Aug 2018, 07:05 pm');
    });

    it('contains `Custom` and its value is `True` string when the value is `true`', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Custom');
      expect(item.value).toBe('True');
    });

    it('contains `Custom` and its value is `False` string when the value is `false`', () => {
      props.moduleDetails.custom = false;
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Custom');
      expect(item.value).toBe('False');
    });

    it('contains `Type` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Type');
      expect(item.value).toBe(props.moduleDetails.type);
    });

    it('contains `Type name` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Type name');
      expect(item.value).toBe(props.moduleDetails.type_name);
    });

    it('contains `Maintenance status` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Maintenance status');
      expect(item.value).toBe(props.moduleDetails.maintenance_status);
    });

    it('contains `Development status` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Development status');
      expect(item.value).toBe(props.moduleDetails.development_status);
    });

    it('contains `Releases` and its value is a node', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Releases');
      const value = shallow(item.value);
      expect(value.exists()).toBe(true);
    });

    describe('`Releases` when no releases affected', () => {
      it('is null', () => {
        props.moduleDetails.results = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Releases');
        expect(item.value).toBeNull();
      });
    });

    describe('`Releases` when there are Releases', () => {
      let item;
      beforeEach(() => {
        const data = setup().find(InfoTable).props().data;
        item = data.find(item => item.key === 'Releases');
      });

      it('renders `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.text()).toBe('<Expansion />');
      });

      it('passes the number of releases as `summary` to `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.props().summary).toBe(3);
      });

      it('passes a list as `details` to `Expansion`', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.find('ul').length).toBe(1);
      });

      it('has matching number of `li`s to the number of releases', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.find('li').length).toBe(3);
      });

      it('renders `slug` of the releases', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.contains('test_module:8.x-1.x-dev')).toBe(true);
        expect(list.contains('test_module:7.x-1.x-dev')).toBe(true);
        expect(list.contains('test_module:7.x-1.x')).toBe(true);
      });
    });

  });
});
