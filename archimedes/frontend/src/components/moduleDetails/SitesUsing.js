import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";

import InfoTable from '../helpers/InfoTable';
import Expansion from '../helpers/Expansion';
import CustomChip from '../helpers/CustomChip';
import { renderList, removeDecommissioned } from '../helpers/formatData';

const sortSite = (a, b) => {
  a = a.string;
  b = b.string;
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
};

const renderSites = sites => {
  sites = removeDecommissioned(sites);
  if (!Array.isArray(sites) || sites.length === 0) return null;
  const siteLinks = sites.sort(sortSite).map(site => (
    <Link to={`/sites/${site.pk}`} key={site.pk}>{site.string}</Link>
  ));
  const details = renderList(siteLinks);
  return <Expansion summary={sites.length} details={details} />;
};

export const SitesUsing = ({ moduleDetails, classes }) => {

  const renderVulnerable = sites => {
    sites = removeDecommissioned(sites);
    if (!Array.isArray(sites) || sites.length === 0) {
      return <CustomChip label="No sites using a vulnerable release" color="success" />;
    }
    const summary = (
      <CustomChip
        label={`${sites.length} ${sites.length === 1 ? 'site' : 'sites'} using a vulnerable release`}
        color="error"
      />
    );
    const siteLinks = sites.sort(sortSite).map(site => (
      <Link to={`/sites/${site.pk}`} key={site.pk} className={classes.vulnerable}>{site.string}</Link>
    ));
    return renderList([summary].concat(siteLinks));
  };

  const data = [
    {
      key: 'Sites using a vulnerable release',
      value: renderVulnerable(moduleDetails.sites_vulnerable)
    },
    {
      key: 'Sites using this module',
      value: renderSites(moduleDetails.sites_using)
    },
  ];

  return (
    <InfoTable title="Sites using" data={data} />
  );
};

SitesUsing.propTypes = {
  moduleDetails: PropTypes.shape({
    sites_vulnerable: PropTypes.arrayOf(PropTypes.shape({
      pk: PropTypes.number.isRequired,
      string: PropTypes.string.isRequired,
      decommissioned: PropTypes.bool.isRequired,
    })),
    sites_using: PropTypes.arrayOf(PropTypes.shape({
      pk: PropTypes.number.isRequired,
      string: PropTypes.string.isRequired,
      decommissioned: PropTypes.bool.isRequired,
    })),
  }).isRequired,
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  vulnerable: {
    color: theme.palette.error.main,
    fontWeight: 500,
  },
});

export default withStyles(styles)(SitesUsing);
