import * as api from '../api';
import {
  MODULES_FETCHING,
  MODULES_FETCHED,
  MODULES_FAILED
} from '../constants/ActionTypes';

// eslint-disable-next-line
export function fetchModules(filters, page, sorting) {
  const request = () => ({ type: MODULES_FETCHING, page });
  const success = payload => ({ type: MODULES_FETCHED, payload, filters, sorting, page });
  const failure = payload => ({ type: MODULES_FAILED, payload });

  return (dispatch) => {
    dispatch(request());

    return api.fetchModules(filters, page, sorting)
    .then(payload => dispatch(success(payload)))
    .catch(err => dispatch(failure(err)));
  };
}
