import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../modulesActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('modulesActions', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  it('creates MODULES_FETCHING and MODULES_FETCHED on success', () => {
    // Input for the function
    const filters = {};
    const page = 1;
    const sorting = {
      sortBy: 'date_posted',
      sortDir: 'desc'
    };

    // Mock response
    const init = { status: 200, statusText: 'success' };
    const body = { mockedApiResponse: 'completed' };
    fetch.mockResponseOnce(JSON.stringify(body), init);

    // Expectations
    const expectedActions = [
      { type: types.MODULES_FETCHING, page },
      { type: types.MODULES_FETCHED, payload: body, filters, sorting, page },
    ];

    const store = mockStore({});
    return store.dispatch(actions.fetchModules(filters, page, sorting))
    .then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('creates MODULES_FETCHING and MODULES_FAILED on failure', () => {
    // Input for the function
    const filters = {};
    const page = 1;
    const sorting = {
      sortBy: 'date_posted',
      sortDir: 'desc'
    };

    // Mock response
    const error = { status: 500, statusText: 'failed' };
    fetch.mockRejectOnce(error);

    // Expectations
    const expectedActions = [
      { type: types.MODULES_FETCHING, page },
      { type: types.MODULES_FAILED, payload: error },
    ];

    const store = mockStore({});
    return store.dispatch(actions.fetchModules(filters, page, sorting))
    .then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
