import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../siteDetailsActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('siteDetailsActions', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  describe('fetchSiteDetails function', () => {
    it('creates SITES_DETAILS_FETCHING and SITES_DETAILS_FETCHED on success', () => {
      // Input for the function
      const siteId = 1;

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.SITES_DETAILS_FETCHING },
        { type: types.SITES_DETAILS_FETCHED, payload: body },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSiteDetails(siteId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates SITES_DETAILS_FETCHING and SITES_DETAILS_FAILED on failure', () => {
      // Input for the function
      const siteId = 1;

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.SITES_DETAILS_FETCHING },
        { type: types.SITES_DETAILS_FAILED, ...error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSiteDetails(siteId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchReportDetails function', () => {
    it('creates REPORTS_DETAILS_FETCHING and REPORTS_DETAILS_FETCHED on success', () => {
      // Input for the function
      const siteId = 1;

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.REPORTS_DETAILS_FETCHING },
        { type: types.REPORTS_DETAILS_FETCHED, payload: body },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchReportDetails(siteId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates REPORTS_DETAILS_FETCHING and REPORTS_DETAILS_FAILED on failure', () => {
      // Input for the function
      const siteId = 1;

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.REPORTS_DETAILS_FETCHING },
        { type: types.REPORTS_DETAILS_FAILED, ...error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchReportDetails(siteId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchSiteDetails function', () => {
    it('creates SITE_GROUPS_DETAILS_FETCHING and SITE_GROUPS_DETAILS_FETCHED on success', () => {
      // Input for the function
      const siteId = 1;

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.SITE_GROUPS_DETAILS_FETCHING },
        { type: types.SITE_GROUPS_DETAILS_FETCHED, payload: body },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSiteGroupDetails(siteId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates SITE_GROUPS_DETAILS_FETCHING and SITE_GROUPS_DETAILS_FAILED on failure', () => {
      // Input for the function
      const siteId = 1;

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.SITE_GROUPS_DETAILS_FETCHING },
        { type: types.SITE_GROUPS_DETAILS_FAILED, ...error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSiteGroupDetails(siteId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
