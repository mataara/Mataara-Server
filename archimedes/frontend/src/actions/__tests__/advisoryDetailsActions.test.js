import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchAdvisoryDetails from '../advisoryDetailsActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('advisoryDetailsActions', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  it('creates ADVISORIES_DETAILS_FETCHING and ADVISORIES_DETAILS_FETCHED on success', () => {
    // Input for the function
    const advisoryId = 1;

    // Mock response
    const init = { status: 200, statusText: 'success' };
    const body = { mockedApiResponse: 'completed' };
    fetch.mockResponseOnce(JSON.stringify(body), init);

    // Expectations
    const expectedActions = [
      { type: types.ADVISORIES_DETAILS_FETCHING },
      { type: types.ADVISORIES_DETAILS_FETCHED, payload: body },
    ];

    const store = mockStore({});
    return store.dispatch(fetchAdvisoryDetails(advisoryId))
    .then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('creates ADVISORIES_DETAILS_FETCHING and ADVISORIES_DETAILS_FAILED on failure', () => {
    // Input for the function
    const advisoryId = 1;

    // Mock response
    const error = { status: 500, statusText: 'failed' };
    fetch.mockRejectOnce(error);

    // Expectations
    const expectedActions = [
      { type: types.ADVISORIES_DETAILS_FETCHING },
      { type: types.ADVISORIES_DETAILS_FAILED, ...error },
    ];

    const store = mockStore({});
    return store.dispatch(fetchAdvisoryDetails(advisoryId))
    .then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
