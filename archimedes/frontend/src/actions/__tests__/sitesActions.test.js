import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../sitesActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('sitesActions', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  describe('sitesToggleSubscription function', () => {
    it('creates SITES_TOGGLE_SUBSCRIBING and SITES_TOGGLE_SUBSCRIPTION_SUCCESS on success', () => {
      // Input for the function
      const pk = 1;

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.SITES_TOGGLE_SUBSCRIBING, pk },
        { type: types.SITES_TOGGLE_SUBSCRIPTION_SUCCESS, pk, payload: body },
      ];

      const store = mockStore({});
      return store.dispatch(actions.sitesToggleSubscription(pk))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates SITES_TOGGLE_SUBSCRIBING and SITES_TOGGLE_FAILED on failure', () => {
      // Input for the function
      const pk = 1;

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.SITES_TOGGLE_SUBSCRIBING, pk },
        { type: types.SITES_TOGGLE_FAILED, ...error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.sitesToggleSubscription(pk))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchSites function', () => {
    it('creates SITES_FETCHING and SITES_FETCHED on success', () => {
      // Input for the function
      const filters = {};
      const page = 1;
      const sorting = {
        sortBy: 'date_posted',
        sortDir: 'desc'
      };

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.SITES_FETCHING, page },
        { type: types.SITES_FETCHED, payload: body, filters, sorting, page },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSites(filters, page, sorting))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates SITES_FETCHING and SITES_FAILED on failure', () => {
      // Input for the function
      const filters = {};
      const page = 1;
      const sorting = {
        sortBy: 'date_posted',
        sortDir: 'desc'
      };

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.SITES_FETCHING, page },
        { type: types.SITES_FAILED, payload: error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSites(filters, page, sorting))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchSiteGroups function', () => {
    it('creates SITE_GROUPS_FETCHING and SITE_GROUPS_FETCHED on success', () => {
      // Input for the function
      const filters = {};
      const page = 1;
      const sorting = {
        sortBy: 'date_posted',
        sortDir: 'desc'
      };

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.SITE_GROUPS_FETCHING, page },
        { type: types.SITE_GROUPS_FETCHED, payload: body, filters, sorting, page },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSiteGroups(filters, page, sorting))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates SITE_GROUPS_FETCHING and SITE_GROUPS_FETCHED on failure', () => {
      // Input for the function
      const filters = {};
      const page = 1;
      const sorting = {
        sortBy: 'date_posted',
        sortDir: 'desc'
      };

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.SITE_GROUPS_FETCHING, page },
        { type: types.SITE_GROUPS_FAILED, payload: error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchSiteGroups(filters, page, sorting))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
