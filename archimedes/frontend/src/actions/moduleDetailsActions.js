import * as api from '../api';
import {
  MODULES_DETAILS_FETCHING,
  MODULES_DETAILS_FETCHED,
  MODULES_DETAILS_FAILED,
  MODULES_RELEASES_FETCHING,
  MODULES_RELEASES_FETCHED,
  MODULES_RELEASES_FAILED
} from '../constants/ActionTypes';

function modulesDetailsFetching() {
  return {
    type: MODULES_DETAILS_FETCHING
  };
}

function modulesDetailsFetched(payload) {
  return {
    type: MODULES_DETAILS_FETCHED,
    payload
  };
}

function modulesDetailsFailed(err) {
  return {
    type: MODULES_DETAILS_FAILED,
    ...err
  };
}

function modulesReleasesFetching() {
  return {
    type: MODULES_RELEASES_FETCHING
  };
}

function modulesReleasesFetched(payload) {
  return {
    type: MODULES_RELEASES_FETCHED,
    payload
  };
}

function modulesReleasesFailed(err) {
  return {
    type: MODULES_RELEASES_FAILED,
    ...err
  };
}

export function fetchModuleDetails(moduleId) {
  return (dispatch) => {
    dispatch(modulesDetailsFetching(moduleId));

    return api.fetchModuleDetails(moduleId)
    .then(payload => dispatch(modulesDetailsFetched(payload)))
    .catch(err => dispatch(modulesDetailsFailed(err)));
  };
}

export function fetchModuleReleases(moduleId) {
  return (dispatch) => {
    dispatch(modulesReleasesFetching(moduleId));

    return api.fetchModuleReleases(moduleId)
    .then(payload => dispatch(modulesReleasesFetched(payload)))
    .catch(err => dispatch(modulesReleasesFailed(err)));
  };
}
