import * as api from "../api";
import {
  SITES_DETAILS_FETCHING,
  SITES_DETAILS_FETCHED,
  REPORTS_DETAILS_FETCHING,
  REPORTS_DETAILS_FETCHED,
  SITES_DETAILS_FAILED,
  REPORTS_DETAILS_FAILED,
  SITE_GROUPS_DETAILS_FETCHING,
  SITE_GROUPS_DETAILS_FETCHED,
  SITE_GROUPS_DETAILS_FAILED,
  SITE_GROUPS_DETAILS_UPDATING,
  SITE_GROUPS_DETAILS_UPDATED,
  SITE_GROUPS_DETAILS_UPDATE_FAILED,
} from "../constants/ActionTypes";

function sitesDetailsFetching() {
  return {
    type: SITES_DETAILS_FETCHING
  };
}

function sitesDetailsFetched(payload) {
  return {
    type: SITES_DETAILS_FETCHED,
    payload
  };
}

function sitesDetailsFailed(err) {
  return {
    type: SITES_DETAILS_FAILED,
    ...err
  };
}

function sitegroupsDetailsFetching() {
  return {
    type: SITE_GROUPS_DETAILS_FETCHING
  };
}

function sitegroupsDetailsFetched(payload) {
  return {
    type: SITE_GROUPS_DETAILS_FETCHED,
    payload
  };
}

function sitegroupsDetailsFailed(err) {
  return {
    type: SITE_GROUPS_DETAILS_FAILED,
    ...err
  };
}

function sitegroupsDetailsUpdating() {
  return {
    type: SITE_GROUPS_DETAILS_UPDATING
  };
}

function sitegroupsDetailsUpdated(payload) {
  return {
    type: SITE_GROUPS_DETAILS_UPDATED,
    payload
  };
}

function sitegroupsDetailsUpdateFailed(err) {
  return {
    type: SITE_GROUPS_DETAILS_UPDATE_FAILED,
    ...err
  };
}

function reportsDetailsFetching() {
  return {
    type: REPORTS_DETAILS_FETCHING
  };
}

function reportsDetailsFetched(payload) {
  return {
    type: REPORTS_DETAILS_FETCHED,
    payload
  };
}

function reportsDetailsFailed(err) {
  return {
    type: REPORTS_DETAILS_FAILED,
    ...err
  };
}

export function fetchSiteDetails(siteId) {
  return dispatch => {
    dispatch(sitesDetailsFetching(siteId));

    return api
      .fetchsitesDetails(siteId)
      .then(payload => dispatch(sitesDetailsFetched(payload)))
      .catch(err => dispatch(sitesDetailsFailed(err)));
  };
}

export function fetchSiteGroupDetails(siteId, sortOptions) {
  return dispatch => {
    dispatch(sitegroupsDetailsFetching(siteId));

    return api
      .fetchsitegroupDetails(siteId, sortOptions)
      .then(payload => dispatch(sitegroupsDetailsFetched(payload)))
      .catch(err => dispatch(sitegroupsDetailsFailed(err)));
  };
}

export function updateSiteGroupDetails(siteId, sortOptions, body) {
  return dispatch => {
    dispatch(sitegroupsDetailsUpdating(siteId));

    return api
      .udpateSiteGroupDetails(siteId, sortOptions, body)
      .then(payload => dispatch(sitegroupsDetailsUpdated(payload)))
      .catch(err => dispatch(sitegroupsDetailsUpdateFailed(err)));
  };
}

export function fetchReportDetails(siteId) {
  return dispatch => {
    dispatch(reportsDetailsFetching(siteId));

    return api
      .fetchreportsDetails(siteId)
      .then(payload => dispatch(reportsDetailsFetched(payload)))
      .catch(err => dispatch(reportsDetailsFailed(err)));
  };
}
