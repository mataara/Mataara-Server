import * as api from '../api';
import {
  ADVISORIES_DETAILS_FETCHING,
  ADVISORIES_DETAILS_FETCHED,
  ADVISORIES_DETAILS_FAILED
} from '../constants/ActionTypes';

function advisoriesDetailsFetching() {
  return {
    type: ADVISORIES_DETAILS_FETCHING
  };
}

function advisoriesDetailsFetched(payload) {
  return {
    type: ADVISORIES_DETAILS_FETCHED,
    payload
  };
}

function advisoriesDetailsFailed(err) {
  return {
    type: ADVISORIES_DETAILS_FAILED,
    ...err
  };
}

export default function fetchAdvisoryDetails(advisoryId) {
  return (dispatch) => {
    dispatch(advisoriesDetailsFetching(advisoryId));

    return api.fetchAdvisoryDetails(advisoryId)
    .then(payload => dispatch(advisoriesDetailsFetched(payload)))
    .catch(err => dispatch(advisoriesDetailsFailed(err)));
  };
}
