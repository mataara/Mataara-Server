import { configureStore as configureStoreDev } from "./configureStore.dev";
import { configureStore as configureStoreProd } from "./configureStore.prod";
/**
 * Based on the current environment variable, we need to make sure
 * to exclude any DevTools-related code from the production builds.
 * The code is envify'd - using 'DefinePlugin' in Webpack.
 */

let loadedStore = null;

if (process.env.NODE_ENV === "production") {
  loadedStore = configureStoreProd;
} else {
  loadedStore = configureStoreDev;
}

export const configureStore = loadedStore;
