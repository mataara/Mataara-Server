import { combineReducers } from 'redux';
import sites from './sites';
import sitegroups from './sitegroups';
import modules from './modules';
import dashboard from './dashboard';
import { advisories } from './advisories';
import siteDetails from './siteDetails';
import siteGroupDetails from './sitegroupDetails';
import advisoryDetails from './advisoryDetails';
import moduleDetails from './moduleDetails';
import { frame } from './frameReducer';

const rootReducer = combineReducers({
  sites,
  sitegroups,
  modules,
  dashboard,
  advisories,
  siteDetails,
  siteGroupDetails,
  advisoryDetails,
  moduleDetails,
  frame,
});

export default rootReducer;
