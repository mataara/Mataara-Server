import {
  MODULES_FETCHING,
  MODULES_FETCHED,
} from '../constants/ActionTypes';

const initialState = {
  pending: false,
  filters: {},
  payload: {},
  list: [],
  page: 0,
  total: 9999999,
  sorting: {
    sortBy: 'name',
    sortDir: 'asc'
  }
};

export default function modules(state = initialState, action) {
  switch (action.type) {
    case MODULES_FETCHING: {
      return {
        ...state,
        pending: true,
        list: action.page === 1 ? [] : state.list
      };
    }

    case MODULES_FETCHED: {
      const filters = action.filters || {};
      const sorting = action.sorting || {};

      return {
        ...state,
        filters,
        sorting,
        pending: false,
        payload: action.payload,
        list: action.payload.previous
          ? state.list.concat(action.payload.results)
          : action.payload.results,
        total: action.payload.count,
        page: action.page,
      };
    }

    default:
      return state;

  }
}
