import {
  SITES_FETCHING,
  SITES_FETCHED,
  SITES_TOGGLE_SUBSCRIBING,
  SITES_TOGGLE_SUBSCRIPTION_SUCCESS,
  SITES_TOGGLE_FAILED
} from "../constants/ActionTypes";

const initialState = {
  pending: false,
  filters: {},
  payload: {},
  list: [],
  page: 0,
  total: 9999999,
  sorting: {
    sortBy: "title",
    sortDir: "asc"
  }
};

export default function sites(state = initialState, action) {
  switch (action.type) {
    case SITES_FETCHING:
      return {
        ...state,
        list: action.page === 1 ? [] : state.list,
        pending: true
      };
    case SITES_FETCHED:
      const filters = action.filters || {};
      const sorting = action.sorting || {};

      return {
        ...state,
        filters,
        sorting,
        pending: false,
        payload: action.payload,
        list: action.payload.previous
          ? state.list.concat(action.payload.results)
          : action.payload.results,
        total: action.payload.count,
        page: action.page
      };
    case SITES_TOGGLE_SUBSCRIBING:
      return state;
    case SITES_TOGGLE_SUBSCRIPTION_SUCCESS:
      const newList = state.list.map(site => {
        if (site.pk !== action.pk) return site;
        return { ...site, ...action.payload };
      });
      return {
        ...state,
        list: newList
      };
    case SITES_TOGGLE_FAILED:
    default:
      return state;
  }
}
