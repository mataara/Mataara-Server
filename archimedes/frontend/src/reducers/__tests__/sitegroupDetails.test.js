import sitegroupDetails from '../sitegroupDetails';
import * as types from '../../constants/ActionTypes';

describe('sitegroupDetails reducer', () => {
  const initialState = {
    siteId: 0,
    pending: false
  };

  it('should return the initial state', () => {
    expect(sitegroupDetails(undefined, {})).toEqual(initialState);
  });

  describe('on SITE_GROUPS_DETAILS_FETCHING', () => {
    it('sets pending to true', () => {
      const action = { type: types.SITE_GROUPS_DETAILS_FETCHING };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pending: true,
      };
      expect(sitegroupDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on SITE_GROUPS_DETAILS_FETCHED', () => {
    it('sets pending to false and set payload', () => {
      const payload = { some: 'thing' };
      const action = { type: types.SITE_GROUPS_DETAILS_FETCHED, payload };
      const state = {
        ...initialState,
        pending: true,
      };
      const expectedState = {
        ...state,
        pending: false,
        ...payload,
      };
      expect(sitegroupDetails(state, action)).toEqual(expectedState);
    });
  });
});
