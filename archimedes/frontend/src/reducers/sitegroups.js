import {
  SITE_GROUPS_FETCHING,
  SITE_GROUPS_FETCHED
} from "../constants/ActionTypes";

const initialState = {
  pending: false,
  filters: {},
  payload: {},
  list: [],
  page: 0,
  total: 9999999,
  sorting: {
    sortBy: "title",
    sortDir: "asc"
  }
};

export default function sitegroups(state = initialState, action) {
  switch (action.type) {
    case SITE_GROUPS_FETCHING:
      return {
        ...state,
        list: action.page === 1 ? [] : state.list,
        pending: true
      };
    case SITE_GROUPS_FETCHED:
      const filters = action.filters || {};
      const sorting = action.sorting || {};

      return {
        ...state,
        filters,
        sorting,
        pending: false,
        payload: action.payload,
        list: action.payload.previous
          ? state.list.concat(action.payload.results)
          : action.payload.results,
        total: action.payload.count,
        page: action.page
      };

    default:
      return state;
  }
}
