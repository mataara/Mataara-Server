import {
  SITE_GROUPS_DETAILS_FETCHING,
  SITE_GROUPS_DETAILS_FETCHED,
  SITE_GROUPS_DETAILS_UPDATING,
  SITE_GROUPS_DETAILS_UPDATED,
} from '../constants/ActionTypes';

const initialState = {
  siteId: 0,
  pending: false
};

const siteGroupDetails = (state = initialState, action) => {
  switch (action.type) {
    case SITE_GROUPS_DETAILS_FETCHING:
    case SITE_GROUPS_DETAILS_UPDATING:
      return {
        ...state,
        pending: true
      };

    case SITE_GROUPS_DETAILS_FETCHED:
    case SITE_GROUPS_DETAILS_UPDATED:
      return {
        ...state,
        pending: false,
        ...action.payload
      };

    default:
      return state;
  }
};

export default siteGroupDetails;
