/* global fetch */
import 'whatwg-fetch';
import keys from 'lodash/keys';

const apiRoot = '/api';
const fetchDefaults = {
  credentials: 'same-origin',
  json: true
};


const parseJSON = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response.json();
  }

  const error = new Error(response.statusText);
  return response.json().then((json) => {
    error.serverError = json;
    throw error;
  });
};

const fetchWithDefaults = (path, options = {}) => {
  const fetchOptions = { ...fetchDefaults, ...options };
  return fetch(apiRoot + path, fetchOptions)
  .then(response => (fetchOptions.json ? parseJSON(response) : response))
  .catch((err) => {
    console.log('request failed', err); // eslint-disable-line no-console
    throw err;
  });
};

const buildQueryString = (filters, page, sorting) => {
  const params = [];

  keys(filters).forEach(key =>
    params.push(`${key}=${filters[key]}`)
  );

  if (page) {
    params.push(`page=${page}`);
  }

  if (sorting && sorting.sortBy && sorting.sortDir) {
    if (sorting.sortDir === 'desc') {
      params.push(`ordering=-${sorting.sortBy}`);
    } else {
      params.push(`ordering=${sorting.sortBy}`);
    }
  }

  return `?${params.join('&')}`;
};

const getCookie = (name) => {
  let cookieValue;
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
    cookies.forEach(cookie => {
      const trimmed = cookie.trim();
      if (trimmed.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(trimmed.substring(name.length + 1));
      }
    });
  }
  return cookieValue;
};

const getCsrftoken = () => getCookie('csrftoken');

export const fetchSites = (filters, page, sorting) => {
  const qs = buildQueryString(filters, page, sorting);
  const url = `/sites/${qs}`;
  return fetchWithDefaults(url);
};

export const fetchSiteGroups = (filters, page, sorting) => {
  const qs = buildQueryString(filters, page, sorting);
  const url = `/sitegroups/${qs}`;
  return fetchWithDefaults(url);
};

export const sitesToggleSubscription = (pk) => {
  const url = `/subscription/?toggle=${pk}`;
  return fetchWithDefaults(url);
};

export const fetchSubscribedSites = () => {
  const url = `/subscription/?mine=1`;
  return fetchWithDefaults(url);
};

export const fetchDrupalStats = (filters, page, sorting) => {
  const qs = buildQueryString(filters, page, sorting);
  const url = `/drupal/stats/${qs}`;
  return fetchWithDefaults(url);
};

export const fetchModules = (filters, page, sorting) => {
  const qs = buildQueryString(filters, page, sorting);
  const url = `/drupal/projects/${qs}`;
  return fetchWithDefaults(url);
};

export const fetchAdvisories = (filters, page, sorting) => {
  const qs = buildQueryString(filters, page, sorting);
  const url = `/drupal/advisories/${qs}`;
  return fetchWithDefaults(url);
};

export const fetchsitesDetails = (siteId) => {
  const url = `/drupal/sites/${siteId}`;
  return fetchWithDefaults(url);
};

export const fetchsitegroupDetails = (siteId, sorting) => {
  const qs = buildQueryString({}, null, sorting);
  const url = `/sitegroups/${siteId}/${qs}`;
  return fetchWithDefaults(url);
};

export const udpateSiteGroupDetails = (siteId, sorting, body) => {
  const qs = buildQueryString({}, null, sorting);
  const url = `/sitegroups/${siteId}/${qs}`;
  const csrftoken = getCsrftoken();
  return fetchWithDefaults(url, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrftoken,
    },
    body: JSON.stringify(body)
  });
};

export const fetchAdvisoryDetails = (advisoryId) => {
  const url = `/drupal/advisories/${advisoryId}`;
  return fetchWithDefaults(url);
};

export const fetchModuleDetails = (moduleId) => {
  const url = `/drupal/projects/${moduleId}`;
  return fetchWithDefaults(url);
};

export const fetchModuleReleases = (moduleId) => {
  const url = `/drupal/releases/?project__slug=${moduleId}`;
  return fetchWithDefaults(url);
};

export const fetchreportsDetails = (siteId) => {
  const url = `/reports/?processed_site=${siteId}`;
  return fetchWithDefaults(url);
};
