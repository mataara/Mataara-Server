import React from "react";
import { Provider } from "react-redux";
import { Helmet } from "react-helmet";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";

import Dashboard from "./views/Dashboard";
import Advisories from "./views/Advisories";
import Sites from "./views/Sites";
import Modules from "./views/Modules";
import AdvisoryDetails from "./views/AdvisoryDetails";
import SiteDetails from "./views/SiteDetails";
import ModuleDetails from "./views/ModuleDetails";

import { configureStore } from "./store/configureStore";
import { theme } from "./styles/theme";
import GlobalStyles from "./styles/GlobalStyles";
import ScrollToTop from "./components/helpers/ScrollToTop";
import Frame from "./components/presentation/Frame";
import SiteGroups from "./views/SiteGroups";
import SiteGroupDetails from "./views/SiteGroupDetails";

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <ThemeProvider theme={theme}>
          <React.Fragment>
            <Helmet title="Mataara" />

            <GlobalStyles />
            <ScrollToTop />
            <Frame>
              <Switch>
                <Route exact path="/" component={Dashboard} />
                <Route path="/sites/:siteId" component={SiteDetails} />
                <Route path="/sites" component={Sites} />
                <Route path="/modules" component={Modules} />
                <Route path="/advisories" component={Advisories} />
                <Route path="/sitegroups/:sitegroupId" component={SiteGroupDetails} />
                <Route path="/sitegroups" component={SiteGroups} />
                <Route
                  path="/drupal/advisories/:advisoryId"
                  component={AdvisoryDetails}
                />
                <Route
                  path="/drupal/modules/:moduleId"
                  component={ModuleDetails}
                />
              </Switch>
            </Frame>
          </React.Fragment>
        </ThemeProvider>
      </Router>
    </Provider>
  );
};

export default App;
