const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const BundleTracker = require('webpack-bundle-tracker');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

module.exports = {
  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'core-js/stable',
    'eventsource-polyfill', // necessary for hot reloading with IE
    'webpack-hot-middleware/client?path=http://mataara-server.local:3000/__webpack_hmr',
    './src/index'
  ],
  output: {
    publicPath: 'http://mataara-server.local:3000/bundles/',
    path: path.join(__dirname, 'bundles'),
    filename: '[name]-[hash].js'
  },
  plugins: [
    /**
     * This is where the magic happens! You need this to enable Hot Module Replacement!
     */
    new webpack.HotModuleReplacementPlugin(),
    // Migrate from react-hot-loader to react-refresh
    // https://github.com/gaearon/react-hot-loader#moving-towards-next-step
    // https://github.com/pmmmwh/react-refresh-webpack-plugin/
    new ReactRefreshWebpackPlugin(),
    /**
     * NoErrorsPlugin prevents your webpack CLI from exiting with an error code if
     * there are errors during compiling - essentially, assets that include errors
     * will not be emitted. If you want your webpack to 'fail', you need to check out
     * the bail option.
     */
    new webpack.NoEmitOnErrorsPlugin(),
    new CleanWebpackPlugin(),
    /**
     * This is a webpack plugin that simplifies creation of HTML files to serve your
     * webpack bundles. This is especially useful for webpack bundles that
     * include a hash in the filename which changes every compilation.
     */
    new HtmlWebpackPlugin({
      template: 'index_frontend.html',
      filename: 'index_frontend.html',
      title: 'Mataara',
      inject: 'body'
    }),
    new BundleTracker({ filename: './webpack-stats.json' }),
  ],
  module: {
    rules: [
      {
        test: /\.js?/,
        exclude: [/node_modules/, /styles/],
        loader: 'babel-loader',
        include: path.join(__dirname, 'src'),
        options: {
          cacheDirectory: true,
          plugins: ['react-refresh/babel'],
        },
      },
      {
        test: /(\.css|\.scss)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        // Exclude `js` files to keep "css" loader working as it injects
        // its runtime that would otherwise processed through "file" loader.
        // Also exclude `html` and `json` extensions so they get processed
        // by webpacks internal loaders.
        exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/, /(\.css|\.scss)$/],
        loader: require.resolve('file-loader'),
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
    ]
  }
};
