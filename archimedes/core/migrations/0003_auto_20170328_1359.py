# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_userprofile_active_days'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='active_days',
            field=models.PositiveSmallIntegerField(default=7, validators=[django.core.validators.MaxValueValidator(64)], help_text='Sites that have reported within this many days are considered "active". A "0" indicates all sites are active.'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='user',
            field=models.OneToOneField(related_query_name='profile', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
    ]
