from django.contrib.auth.models import User
from django.test import TestCase


class UserProfileTestCase(TestCase):

    def test_create_user_profile(self):
        """Ensure that when a :User is created, a :UserProfile is also created."""
        user = User.objects.create(username='testuser')
        self.assertTrue(hasattr(user, 'profile'), 'User has UserProfile')
        profile = user.profile
        profile_query = User.objects.filter(profile__pk=profile.pk)
        self.assertTrue(profile.active_days is not None, 'UserProfile has default active_days set')
        self.assertEqual(len(profile_query), 1, 'Profile query of User has unique result')
        self.assertEqual(profile.pk, profile_query[0].profile.pk, 'Profile query of User returns correct profile key')
        self.assertEqual('Profile for testuser', profile.__str__(), 'UserProfile has __str__ in correct format')
