from django.contrib import admin
from . import models
# Register your models here.


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'active_days']


admin.site.register(models.UserProfile, UserProfileAdmin)
