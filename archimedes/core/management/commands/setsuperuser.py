from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """
    Takes an existing user in the application and give them superuser rights.

    Expected users are in development environments, for easy use by the developers,
    and for initial setting up of production deployments.

    :param username: Username of existing user to look for
    :type username: str
    """

    help = '''Assign superuser rights to an existing user'''

    def add_arguments(self, parser):
        parser.add_argument('--username',
                            action='store',
                            dest='username')

    def handle(self, *args, **options):
        if options['username']:
            try:
                user = User.objects.get(username=options['username'])
                user.is_staff = True
                user.is_superuser = True
                user.save()
                self.stdout.write('User promoted to a superuser')
            except User.DoesNotExist:
                self.stderr.write('User does not exist')
        else:
            self.stderr.write('Username must be specified')
