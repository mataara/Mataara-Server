from django.db import models
from django.core.validators import MaxValueValidator
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField


class UserProfile(models.Model):
    MAX_ACTIVE_DAYS = 64

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_query_name='profile')
    active_days = models.PositiveSmallIntegerField(
        help_text=(
            'Sites that have reported within this many days are considered "active". '
            'A "0" indicates all sites are active.'
        ),
        default=7,
        validators=[MaxValueValidator(MAX_ACTIVE_DAYS)]
    )

    usersettings = JSONField(
        default=dict(),
        help_text="JSON user setting configuration object"
    )

    def __str__(self):
        return("Profile for {}".format(self.user.__str__()))

# Map the User profile property to the head of the (object, created) tuple
# returned by get_or_create(). This will create a UserProfile on demand when a
# User is created, either within the admin interface or via external
# authentication such as LDAP.
#
# See: https://docs.djangoproject.com/en/1.10/ref/models/querysets/#get-or-create

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])  # noqa: E305
