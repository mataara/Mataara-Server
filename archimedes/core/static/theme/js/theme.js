// Sidebar menu toggle
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("span", this).toggleClass('glyphicon-menu-left').toggleClass('glyphicon-menu-right');
    $("#wrapper").toggleClass("toggled");
});
