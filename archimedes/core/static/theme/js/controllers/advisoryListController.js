'use strict';

/*
 * Angular controller for the advisory list
 */
angular.module('archimedes').controller('AdvisoryListController', [
    '$scope', 'DrupalAdvisory',
    function($scope, DrupalAdvisory) {

    $scope.page = 1;
    $scope.next = true;
    $scope.loading = false;
    $scope.advisories = [];
    $scope.search = '';
    $scope.filter = '';

    $scope.loadAdvisories = function () {
        if (!$scope.loading) {
            if ($scope.next)
            {
                $scope.loading = true;
                DrupalAdvisory.get({
                    page: $scope.page,
                    search: $scope.search,
                    source__slug: $scope.filter,
                }, function(data) {
                    // If we are still loading (i.e. haven't been cancelled by a filter)
                    if ($scope.loading)
                    {
                        $scope.next = data.next;
                        $scope.page += 1;
                        for (let advisory of data.results) {
                            $scope.advisories.push(advisory);
                        }
                        $scope.loading = false;
                        $scope.$emit('advisories:updated');
                    }
                });
            }
        }
    }

    var clearAdvisories = function () {
        $scope.page = 1;
        $scope.next = true;
        $scope.advisories = [];
        $scope.loading = false;
    }

    $scope.$watch("search", function (newValue, oldValue) {
        if (newValue !== oldValue) {
            clearAdvisories();
            $scope.loadAdvisories();
        }
    });

    $scope.$watch("filter", function (newValue, oldValue) {
        if (newValue !== oldValue) {
            clearAdvisories();
            $scope.loadAdvisories();
        }
    });

}]);
