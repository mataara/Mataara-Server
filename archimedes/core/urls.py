from django.conf.urls import include, url

from . import views

# IndexView patterns pass through to the frontend router.
#
# Since React tends to do its own special internal routing, these patterns will
# generally only be triggered when first entering the React app.
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^sites', views.IndexView.as_view(), name='index'),
    url(r'^sitegroups', views.IndexView.as_view(), name='index'),
    url(r'^modules', views.IndexView.as_view(), name='index'),
    url(r'^advisories', views.IndexView.as_view(), name='index'),
    url(r'^drupal', views.IndexView.as_view(), name='index'),
    url(r'^api/', include('archimedes.api.urls')),
    url(r'^reports/', include('archimedes.reports.urls', namespace='reports-incoming')),
]
