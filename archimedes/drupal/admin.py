from django.contrib import admin
from django.core.management import call_command
from polymorphic.admin import PolymorphicChildModelAdmin

from . import models


@admin.register(models.Advisory)
class AdvisoryAdmin(admin.ModelAdmin):
    raw_id_fields = ('releases',)
    search_fields = ('title', 'advisory_id', 'project__slug')
    readonly_fields = ('status',)
    list_display = ('date_posted', 'title', 'status', 'url', 'project', 'risk',)
    list_filter = ('status', 'risk',)
    actions = ['parse_advisories']

    def get_queryset(self, request):
        return super(AdvisoryAdmin, self).get_queryset(request).select_related('risk', 'project')  # pragma: no cover

    def parse_advisories(self, request, queryset):
        from archimedes.drupal.tasks import parse_advisory

        for obj in queryset:
            parse_advisory.delay(obj.pk, force=True)

        self.message_user(request, "Submitted advisory(ies) for processing")

    parse_advisories.short_description = "Schedule advisories for parsing"


@admin.register(models.AdvisorySource)
class AdvisorySourceAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'url')
    actions = ['import_api_advisories']

    def import_api_advisories(self, request, queryset):
        for source in queryset:
            call_command('import_drupal_advisories', source.slug)

        self.message_user(request, 'Importing advisories from the "{}" API source'.format(source.slug))

    import_api_advisories.short_description = "Import advisories"


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    search_fields = ('name', 'slug', 'type',)
    readonly_fields = ('type_name',)
    list_display = ('slug', 'name', 'url', 'type_name', 'date_last_sync', 'custom',)
    list_filter = ('custom', 'type',)
    actions = ['sync_project_releases']

    def type_name(self, obj):
        return "{} (type: {})".format(
            obj.type_name,
            obj.type or '??'
        )

    type_name.short_description = "Type"

    def sync_project_releases(self, request, queryset):
        for project in queryset:
            call_command('syncreleases', project.slug, '--now')

        self.message_user(request, 'Releases synced for {} projects.'.format(len(queryset)))

    sync_project_releases.short_description = "Sync project releases"


@admin.register(models.Release)
class ReleaseAdmin(admin.ModelAdmin):
    search_fields = ('version', 'project__slug')
    list_display = ('__str__', 'project', 'version', 'url')


@admin.register(models.DrupalSite)
class DrupalSiteAdmin(PolymorphicChildModelAdmin):
    base_model = models.DrupalSite
    raw_id_fields = ('modules', 'themes')
    list_display = ('__str__', 'core_version', 'environment', 'url', 'hostname', 'root', 'title')

    def get_queryset(self, request):
        queryset = super(DrupalSiteAdmin, self).get_queryset(request)
        return queryset.select_related('core_version', 'environment')  # pragma: no cover
