import urllib.error
import urllib.parse
import urllib.request

import json
import logging

logger = logging.getLogger(__name__)


class DOValidationException(Exception):
    """A custom exception for Drupal.org URL validation."""
    pass


class DOAPIException(Exception):
    """A custom exception from Drupal.org API responses."""
    pass


class DrupalOrgAPI:
    """
    A helper class for interacting with the API at drupal.org.

    This class assumes JSON as the transport method.

    See: https://www.drupal.org/drupalorg/docs/api
    """
    base_api_url = "https://www.drupal.org/api-d7/"

    bad_pagination_error_msg = "JSON response does not contain valid pagination keys."

    @staticmethod
    def _is_url_page_match(source_url, target_url):
        """
        Determine whether the page value in the source is the same as the page
        value in the target.

        If page value is not present, '0' is assumed.

        Note that this helper method does not do any of its own URL validation,
        so should only be used with the first, last, next, prev and self URLs
        that have been fetched and validated by the other API methods.
        """
        if source_url is None or target_url is None:
            raise DOAPIException(DrupalOrgAPI.bad_pagination_error_msg)

        # On last page, self may have no page query argument.
        source_parse_result = urllib.parse.urlparse(source_url)
        self_qs = urllib.parse.parse_qs(source_parse_result.query)
        if 'page' not in self_qs:
            # parse_qs() wraps single query values in lists.
            self_qs['page'] = ['0']

        target_parse_result = urllib.parse.urlparse(target_url)
        target_qs = urllib.parse.parse_qs(target_parse_result.query)
        if 'page' not in target_qs:
            # parse_qs() wraps single query values in lists.
            target_qs['page'] = ['0']

        # Assume 'self' and 'last' are the same if their query strings match.
        return(target_qs == self_qs)

    @staticmethod
    def validate_url(url):
        """
        Validate URL against Drupal.org API schema.

        Ensure the URL uses the correct base.

        Add the .json extension to a URL, if it is not already present.
        [ Bug: https://www.drupal.org/project/restws/issues/2253947 ]

        Drupal entity endpoints in JSON use the following schema:
        https://www.drupal.org/api-d7/{entity-type}.json
        """

        if not url.startswith(DrupalOrgAPI.base_api_url):
            raise DOValidationException("Attempted to process non Drupal.org API URL: {}".format(url))

        if DrupalOrgAPI.base_api_url == url:
            raise DOValidationException("Base Drupal.org API endpoint called with no entity")

        extra = url.split(DrupalOrgAPI.base_api_url)[1]
        parts = extra.split('?')
        entity = parts[0]
        arguments = '?' + parts[1] if len(parts) == 2 else ''

        if entity.endswith('.xml'):
            raise DOValidationException("URL is an XML request, not JSON.")

        if not entity.endswith('.json'):
            entity = entity + '.json'

        # Reassemble the parts.
        return "{}{}{}".format(DrupalOrgAPI.base_api_url, entity, arguments)

    @staticmethod
    def has_multi_pages(resp_js):
        """Does the response have more than one page of results?"""
        return resp_js['first'] != resp_js['last']

    @staticmethod
    def get_current_page_number(resp_js):
        """Return the current page number from a response."""
        self_url = DrupalOrgAPI.get_self_page_url(resp_js)
        if self_url is None:
            raise DOAPIException(DrupalOrgAPI.bad_pagination_error_msg)
        qs = urllib.parse.urlparse(self_url).query
        qs_dict = urllib.parse.parse_qs(qs)
        pages = qs_dict['page'] if 'page' in qs_dict else []  # Could be multiple vars or none.
        if len(pages) == 1:
            return(int(pages[0]))
        elif len(pages) > 1:
            # Assume last value is correct.
            logger.warning(
                "Multiple 'page' keys found while getting page number; assuming last value is the correct one."
            )
            return(int(pages[-1]))
        else:
            # Assume page 0 if no query value is present.
            return 0

    @staticmethod
    def get_first_page_url(resp_js):
        if 'first' not in resp_js:
            return None
        return DrupalOrgAPI.validate_url(resp_js['first'])

    @staticmethod
    def get_last_page_url(resp_js):
        if 'last' not in resp_js:
            return None
        return DrupalOrgAPI.validate_url(resp_js['last'])

    @staticmethod
    def get_next_page_url(resp_js):
        if 'next' not in resp_js:
            return None
        return DrupalOrgAPI.validate_url(resp_js['next'])

    @staticmethod
    def get_prev_page_url(resp_js):
        if 'prev' not in resp_js:
            return None
        return DrupalOrgAPI.validate_url(resp_js['prev'])

    @staticmethod
    def get_self_page_url(resp_js):
        if 'self' not in resp_js:
            return None
        return DrupalOrgAPI.validate_url(resp_js['self'])

    @staticmethod
    def is_first_page(resp_js):
        """
        Determine whether the page in the current response is the first page.

        String compares validated versions of the "self" and "first" keys.
        If either value is not present, raise an exception.
        """
        first_value = DrupalOrgAPI.get_first_page_url(resp_js)
        self_value = DrupalOrgAPI.get_self_page_url(resp_js)

        return DrupalOrgAPI._is_url_page_match(self_value, first_value)

    @staticmethod
    def is_last_page(resp_js):
        """
        Determine whether the page in the current response is the last page.

        String compares validated versions of the "self" and "last" keys.
        If either value is not present, raise an exception.
        """
        last_value = DrupalOrgAPI.get_last_page_url(resp_js)
        self_value = DrupalOrgAPI.get_self_page_url(resp_js)

        return DrupalOrgAPI._is_url_page_match(self_value, last_value)

    @staticmethod
    def request(url, return_raw=False, exception_on_error=False):
        """
        Fetch a Drupal.org API URL, and return a JSON response.

        If return_raw, return response as a JSON string, rather than a dict.
        If exception_on_error, raise an exception instead of logging an error.
        """
        url = DrupalOrgAPI.validate_url(url)
        response = urllib.request.urlopen(url)  # nosec : sanitised URLS only.

        if response.code != 200:
            error_msg = "Unexpected HTTP response code from: {} {} from {}".format(
                response.code,
                response.reason,
                url
            )
            logger.error(error_msg)
            if exception_on_error:
                raise DOAPIException(error_msg)
        if not return_raw:
            return json.loads(response.read().decode('UTF-8'))
        else:
            return response.read().decode('UTF-8')

    @staticmethod
    def request_nodes_page(args={}, return_raw=False, exception_on_error=False):
        """
        Request a page of node entities with arguments.

        Takes a dict of key:value pairs to turn into key=value query string components.
        """
        url = "{}{}?{}".format(
            DrupalOrgAPI.base_api_url,
            "node",
            urllib.parse.urlencode(args))
        return DrupalOrgAPI.request(url, return_raw)

    @staticmethod
    def request_node(nid, return_raw=False, exception_on_error=False):
        """
        Request a specific node ID.
        """
        url = "{}{}/{}.json".format(
            DrupalOrgAPI.base_api_url,
            "node",
            nid
        )
        return DrupalOrgAPI.request(url, return_raw)
