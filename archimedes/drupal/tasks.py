from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.management import call_command
import json
from raven.contrib.django.raven_compat.models import client as raven_client

from .exceptions import AdvisoryParserError
from .models import Advisory, Project
from archimedes.celery import app
from .parser import AdvisoryParser
from .do_advisory_parser import DOAdvisoryParser
from .utils import download_releases, get_guid

# Is Raven enabled? Assume it is if there is a RAVEN_CONFIG setting.
has_raven = hasattr(settings, 'RAVEN_CONFIG')

logger = get_task_logger(__name__)


@app.task
def import_advisories():
    """(Deprecated) Import advisories from all RSS sources."""
    logger.warning("import_advisories task is deprecated in favour of import_api_advisories")
    call_command('importadvisories')  # pragma: no cover


@app.task
def parse_advisories():
    """(Deprecated) Parse all available advisories."""
    logger.warning("parse_advisories task is deprecated in favour of import_api_advisories")
    call_command('parseadvisories')  # pragma: no cover


@app.task
def import_api_advisories():
    """
    Import advisories from all API sources.

    This task will also schedule all new and updated advisories discovered for parsing.
    """
    call_command('import_drupal_advisories')  # pragma: no cover


@app.task
def parse_advisory(advisory_pk, force=False):
    """If an advisory has been parsed, output a message on the logger and return,\
    otherwise, give the advisory to AdvisoryParser.parse() """
    # Fetch the advisory from the database
    advisory = Advisory.objects.get(pk=advisory_pk)

    # Don't re-parse advisories unless forced to.
    if advisory.date_parsed and not force:
        logger.warning('Advisory #%d already parsed.' % advisory_pk)
        return

    logger.info('Parsing advisory #%s...' % advisory_pk)
    parser = AdvisoryParser()
    parse_status = parser.parse(advisory, forcedownload=False)
    # If parsing unsuccessful, set a failed status flag.
    if parse_status is False:
        logger.error('Parsing advisory #%s failed.' % advisory_pk)
        advisory.failed = True
        advisory.save()
    else:
        logger.info("Advisory #%s parsed." % advisory_pk)

    return parse_status


@app.task
def parse_api_advisory(advisory_json):
    """If an advisory has been parsed, output a message on the logger and return,\
    otherwise, give the advisory to DOAdvisoryParser.parse() """

    advisory_dict = json.loads(advisory_json)
    nid = advisory_dict['nid']
    guid = get_guid(nid)

    logger.info('Parsing advisory #{}...'.format(nid))
    parser = DOAdvisoryParser()
    try:
        parse_status = parser.parse(advisory_json, forcedownload=False)
    except (AdvisoryParserError) as exc:
        logger.error("Couldn't get the affected Drupal cores: %s" % str(exc))
        if has_raven:
            ident = raven_client.get_ident(raven_client.captureException())
            logger.error("Sentry ident: %s" % ident)
        parse_status = False

    # If parsing unsuccessful, set a failed status flag.
    if parse_status is False:
        logger.error('Parsing advisory #{} failed.'.format(nid))
        advisory = Advisory.objects.get(guid=guid)
        advisory.failed = True
        advisory.save()
    else:
        logger.info("Advisory #{} parsed.".format(nid))

    return parse_status


@app.task
def sync_drupal_projects():
    """
    Refresh release (version) information on all known projects.

    Note that this is a network intensive process. Ensure that the task
    periods are large enough that they don't overlap. If you're running into
    throttling problems with the update server, you may wish to reduce the
    number of workers Celery uses for this task.
    """

    project_slugs = [p.slug for p in Project.objects.filter(custom=False)]
    for project in project_slugs:
        logger.debug("Spawning sync task for: {}".format(project))
        sync_project_releases.delay(project, force=True, retry=True)


@app.task
def sync_project_releases(project, force=False, core='all', retry=False):
    """Refresh release information on a single project."""
    logger.info("Release sync: {} [{}]".format(project, core))
    download_releases(project, force, core, retry)
