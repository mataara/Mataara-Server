from .models import DrupalSite


def build_report(options, qs=None):
    """
    Build a Drupal site report to aid with patching.

    Standard options:
    - projects: a list of Drupal Project slugs
    - groups: a list of Drupal SiteGroup ids to filter on

    Supports boolean options:
    - show_empty_rows: include rows where no projects are found
    - show_groups: include site groups that sites belong to
    - show_decommissioned: include sites that are decommissioned
    """
    report = {}
    data = []
    headers = [
        "_id",
        "Hostname",
        "Environment"
    ]

    for project in options['projects']:
        pass
        if project == "drupal":
            headers.extend(["Core", "Safe?"])
        else:
            headers.extend([project, "Safe?"])

    if options['show_groups']:
        headers.append("Groups")

    if qs is None:
        qs = DrupalSite.objects.all()

    for drupalsite in qs:
        if drupalsite.decommissioned and not options['show_decommissioned']:
            continue

        # Initial base headers
        row = [
            drupalsite.pk,
            drupalsite.hostname,
            drupalsite.environment.name
        ]

        # In case we need to filter out an entire row for lack of data.
        row_has_project_data = False

        for project_slug in options['projects']:
            if project_slug == "drupal":
                row.extend([
                    drupalsite.core_version.version,
                    "N" if drupalsite.is_core_insecure else "Y"
                ])
                row_has_project_data = True
            else:
                releases = drupalsite.modules.filter(project__slug=project_slug)
                if len(releases) > 0:
                    version = releases.first().version
                    is_secure = "N" if releases.first().is_marked_insecure else "Y"
                    row.extend([version, is_secure])
                    row_has_project_data = True
                else:
                    row.extend([None, None])

        if options['show_groups']:
            groups = drupalsite.get_groups()
            rowgroups = []
            for group in groups:
                rowgroups.append({
                    "_id": group.pk,
                    "name": group.name
                })
            row.append(rowgroups)

        if options['show_empty_rows'] or row_has_project_data:
            data.append(row)

    report["data"] = data
    report["headers"] = headers
    return report


def flatten_groups(group_dicts):
    """Return a row with flattened group info."""
    group_strings = []
    for group in group_dicts:
        group_strings.append("{name}({_id})".format(**group))
    return str(",".join(group_strings))


def get_max_lengths(headers, rows):
    """Return a list with the maximum string length of data in each column."""
    lengths = []

    for header in headers:
        lengths.append(len(header))

    for row in rows:
        for i, cell in enumerate(row):
            cell_length = len(str(row[i]))
            if cell_length > lengths[i]:
                lengths[i] = cell_length

    return lengths


def groups_filter(qs, group_ids):
    """
    Filter down DrupalSite QuerySet based on SiteGroup membership.
    Returns a list, not a QuerySet.
    """
    group_ids = [int(x) for x in group_ids]
    filtered_sites = []
    for drupalsite in qs:
        group_pks = {g.pk for g in drupalsite.get_groups()}
        if len(group_pks.intersection(group_ids)) > 0:
            filtered_sites.append(drupalsite)

    return filtered_sites
