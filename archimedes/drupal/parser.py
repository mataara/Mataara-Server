import logging
import operator
import re
import string
import sys
from os.path import commonprefix

from bs4 import BeautifulSoup
from dateutil.parser import parse
from django.conf import settings
from django.utils import timezone
from fuzzywuzzy import fuzz
from raven.contrib.django.raven_compat.models import client as raven_client
from werkzeug.urls import url_fix

from .exceptions import (AffectedReleasesError, AffectedReleasesRuleError,
                         AffectedVersionError, AdvisoryParserError)
from .models import CVE, Core, Project, Release, Risk, Vulnerability
from .utils import download_releases, normalize_whitespace

# Is Raven enabled? Assume it is if there is a RAVEN_CONFIG setting.
has_raven = hasattr(settings, 'RAVEN_CONFIG')

logger = logging.getLogger(__name__)


class AffectedVersion(object):
    """
    A container for a release version, including the separated
    core, major and patch strings, and an "all versions" identifier
    """

    # Define the regex to match version strings, e.g:
    #  6.x-2.x      (core + major)
    #  6.x-2.4      (core + major + patch)
    #  6.x-22.x     (core + major)
    #  6.x-22.33    (core + major + patch)
    #  6.x-2.2-dev  (core + major + patch + extra)
    VREGEX = r'((\d)\.x\-(\d{1,2}|x)\.(\d{1,4}|x))(-\S+)?'

    # Define the regex to match core strings, e.g.:
    #  6.x
    #  7.2
    #  8.2.3
    #  9.x-dev
    #  6.34
    #  10.45
    CREGEX = r'(\d{1,2}|x{1,2})\.(\d{1,2}|x{1,2})(?:\.(\d{1,2}|x{1,2}))?(-\S+)?'

    # Valid operators to use in version comparisons
    VALID_OPERATORS = ['<', '<=', '>', '>=', '==', '!=']

    def __init__(self, version=None, all=False, core=None):
        self.all = all
        self.string = ''
        self.is_core_version = False

        # All parts passed in separately
        if type(version) == tuple:
            if len(version) != 5:
                raise AffectedVersionError('Unable to create an AffectedVersion - '
                                           'version tuple too small, need 5 values')
            (self.string, self.core, self.major, self.patch, self.extra) = version

        # Only a string passed in
        elif version:
            self.all = all
            m = re.search(self.VREGEX, version)
            # Special handling for core versions
            if m is None:
                m = re.search(self.CREGEX, version)
                self.is_core_version = True

            data = m.groups()
            if self.is_core_version:
                self.string = 'drupal'
                (self.core, self.major, self.patch, self.extra) = data
                self.extra = None
            elif len(data) == 4:
                (self.string, self.core, self.major, self.patch) = data
                self.extra = None
            elif len(data) == 5:
                (self.string, self.core, self.major, self.patch, self.extra) = data
            else:
                raise AffectedVersionError('Unable to create an AffectedVersion '
                                           'because the version string parses abnormally')

        # Quick way of creating a core only version
        elif core:
            self.all = all
            self.string = '%s.x' % str(core)
            self.core = core
            self.major = None
            self.patch = None
            self.extra = None

        elif not all:
            raise AffectedVersionError('Unable to create an AffectedVersion from the given version information')

    def tuple(self):
        """Create a tuple from this version that can be sorted numerically"""
        try:
            core = int(self.core)
        except (ValueError, TypeError):
            core = float("inf")
        try:
            major = int(self.major)
        except (ValueError, TypeError):
            major = float("inf")
        try:
            patch = int(self.patch)
        except (ValueError, TypeError):
            patch = float("inf")

        return (core, major, patch)

    def compare_extra(self, other, op):
        """Compare two strings by finding their difference and trying to convert it to integer"""

        if op not in self.VALID_OPERATORS:
            raise ValueError("Invalid operator token passed to compare function.")

        a = self.extra
        b = other.extra

        if a is None and b is None:
            return True

        # LHS has no extra
        if a is None:
            if op == '==':
                # For equality, either one can be None
                return True
            else:
                # Other operators can't work with None
                if op in ['<', '<=']:
                    return False
                if op in ['>', '>=']:
                    return True

        # RHS has no extra
        if b is None:
            if op == '==':
                # For equality, either one can be None
                return True
            else:
                # Other operators can't work with None
                if op in ['<', '<=']:
                    return True
                if op in ['>', '>=']:
                    return False

        # See if a matching prefix exists (e.g. "-beta" in "-beta3" and "-beta4")
        prefix = commonprefix([a, b])
        if prefix == '' or (prefix == a and prefix == b):
            # No matching prefix, compare normally
            return eval('a %s b' % op)  # nosec : operator validated

        a_diff = a[len(prefix):]
        b_diff = b[len(prefix):]

        try:
            a_diff = int(a_diff)
            b_diff = int(b_diff)
        except ValueError:
            # Can't convert to integers, compare normally
            pass

        return eval('a_diff %s b_diff' % op)  # nosec : operator validated

    def equal(self, a, b):
        """Compare two values for equality, acounting for infinite values"""
        if a == float('inf') or b == float('inf'):
            return True
        else:
            return a == b

    def __eq__(self, other):
        if isinstance(other, AffectedVersion):

            if not self.compare_extra(other, '=='):
                return False

            for i, t in enumerate(self.tuple()):
                if not self.equal(t, other.tuple()[i]):
                    return False
            return True

    def __ne__(self, other):
        if isinstance(other, AffectedVersion):
            return not self.__eq__(other)

    def __repr__(self):
        # Step through each part of the potential representation string.
        rep = "[AffectedVersion: %s]" % str((self.string, self.core, self.major, self.patch, self.extra))
        return rep

    def __lt__(self, other):
        if isinstance(other, AffectedVersion):
            lt = self.tuple() < other.tuple() and self.compare_extra(other, '<')
            if not lt and (self.extra is not None or other.extra is not None):
                return self.__le__(other)
            else:
                return lt

    def __le__(self, other):
        if isinstance(other, AffectedVersion):
            return self.tuple() <= other.tuple() and self.compare_extra(other, '<=')

    def __gt__(self, other):
        if isinstance(other, AffectedVersion):
            gt = self.tuple() > other.tuple() and self.compare_extra(other, '>')
            if not gt and (self.extra is not None or other.extra is not None):
                return self.__ge__(other)
            else:
                return gt

    def __ge__(self, other):
        if isinstance(other, AffectedVersion):
            return self.tuple() >= other.tuple() and self.compare_extra(other, '>=')


class ReleaseMatch(object):
    RULE_OPERATORS = ['ALL', 'OR'] + AffectedVersion.VALID_OPERATORS

    def __init__(self, line, versions, name_ph, version_ph):
        """
        Create a set of matching rules based on the line format.

        Rulesets in self.rules are a dict of lists, keyed per core version.
        Rules are (operator string token, AffectedVersion) tuples.
        """
        self.rules = {}
        self.consider = True
        ph = {'n': name_ph, 'v': version_ph}

        # Fix some spelling/whitespace mistakes
        line = line.replace('to%(v)s' % ph, 'to %(v)s' % ph)
        line = line.replace('to%(n)s' % ph, 'to %(n)s' % ph)
        line = line.replace('%(n)s_comments' % ph, name_ph)
        line = line.replace('%(n)s server' % ph, name_ph)

        # Ignore!
        if (
            line == '%(n)s %(v)s versions are not affected' % ph or
            line == '%(n)s %(v)s is unaffected' % ph
        ):
            # Skip this line
            self.consider = False

        # Triple Version
        elif (
            line == '%(n)s %(v)s from %(v)s to %(v)s' % ph or
            line == '%(n)s %(v)s versions from %(v)s to %(v)s' % ph or
            line == '%(n)s %(v)s versions from and including %(v)s to %(v)s' % ph or
            line == '%(n)s %(v)s versions between %(v)s through %(v)s' % ph
        ):
            # We can discard the first version because it's implied by the range
            self.add_rule('>=', versions[1])
            self.add_rule('<=', versions[2])

        # Three specific versions
        elif (
            line == '%(n)s %(v)s versions %(v)s and %(v)s' % ph
        ):
            self.add_rule('==', versions[0])
            self.add_rule('==', versions[1])
            self.add_rule('OR', versions[1])
            self.add_rule('==', versions[2])

        # Three versions and prior
        elif (
            line == '%(n)s %(v)s and %(v)s versions %(v)s and prior' % ph
        ):
            self.add_rule('==', versions[0])
            self.add_rule('OR', versions[0])
            self.add_rule('==', versions[1])
            self.add_rule('<', versions[2])

        # Double version
        elif (
            line == 'all %(n)s %(v)s and %(v)s versions' % ph
        ):
            self.add_rule('==', versions[0])
            self.add_rule('OR', versions[0])  # version[0] only passed for it's core value
            self.add_rule('==', versions[1])

        # Specific Version
        elif (
            line == '%(n)s version %(v)s. versions prior to %(n)s %(v)s are not vulnerable' % ph
        ):
            self.add_rule('==', versions[0])

        # Version range
        elif (
            line == '%(n)s %(v)s to %(v)s' % ph or
            line == '%(n)s versions %(v)s through %(v)s' % ph
        ):
            self.add_rule('>=', versions[0])
            self.add_rule('<=', versions[1])

        # Versions prior to version
        elif (
            line == '%(v)s versions prior to %(v)s' % ph or
            line == '%(n)s %(v)s versions prior to %(v)s' % ph or
            line == '%(n)s %(v)s versions before %(v)s' % ph or
            line == '%(n)s %(v)s beta versions prior to %(v)s' % ph or
            line == '%(n)s module %(v)s versions prior to %(v)s' % ph or
            line == '%(n)s distribution %(v)s versions prior to %(v)s' % ph or
            line == '%(n)s core module %(v)s versions prior to %(v)s' % ph or
            line == '%(n)ss %(v)s versions prior to %(v)s' % ph or
            line == '%(n)s theme %(v)s versions prior to %(v)s' % ph or
            line == 'all %(n)s %(v)s versions prior to %(v)s' % ph
        ):
            self.add_rule('==', versions[0])
            self.add_rule('<', versions[1])

        # Versions prior to version (inclusive)
        elif (
            line == '%(n)s %(v)s versions up to %(v)s' % ph or
            line == '%(n)s %(v)s versions %(v)s and prior' % ph
        ):
            self.add_rule('==', versions[0])
            self.add_rule('<=', versions[1])

        # Prior to single version
        elif (
            line == 'all versions prior to %(v)s' % ph or
            line == 'all versions before %(v)s' % ph or
            line == '%(n)s %(v)s and prior versions' % ph or
            line == 'all %(n)s versions prior to %(v)s' % ph or
            line == 'all %(n)s module versions prior to %(v)s' % ph or
            line == '%(n)s versions prior to %(v)s' % ph or
            line == '%(n)s before version %(v)s' % ph or
            line == '%(n)s theme versions prior to %(v)s' % ph
        ):
            self.add_rule('<', versions[0])

        # Prior to single version (inclusive)
        elif (
            line == '%(n)s %(v)s and all earlier version' % ph
        ):
            self.add_rule('<=', versions[0])

        # Prior to single version (6.x)
        elif (
            line == '%(n)s 6.x versions prior to %(v)s' % ph or
            line == 'all versions of %(n)s for drupal 6.x prior to %(v)s' % ph or
            line == 'if you use the %(n)s module for drupal 6.x, upgrade to %(n)s %(v)s' % ph
        ):
            self.add_rule('<', versions[0])

        # Prior to single version (7.x)
        elif (
            line == '%(n)s 7.x versions prior to %(v)s' % ph or
            line == '%(n)s for 7.x prior to %(v)s' % ph or
            line == 'all 7.x versions prior to %(v)s' % ph or
            line == 'if you use the %(n)s module for drupal 7.x, upgrade to %(n)s %(v)s' % ph or
            line == 'upgrade to %(n)s %(v)s' % ph
        ):
            self.add_rule('<', versions[0])

        # Single version
        elif (
            line == '%(n)s %(v)s' % ph or
            line == '%(n)s-%(v)s' % ph or
            line == '%(n)stheme-%(v)s' % ph or
            line == '%(n)s version %(v)s only' % ph
        ):
            self.add_rule('==', versions[0])

        # All versions (single version given)
        elif (
            line == '%(n)s %(v)s versions' % ph or
            line == 'all %(n)s %(v)s versions' % ph or
            line == 'all %(v)s versions' % ph or
            line == '%(n)s %(v)s any version' % ph or
            line == '%(n)s %(v)s all versions' % ph
        ):
            self.add_rule('==', versions[0])

        # All versions (6.x)
        elif (
            line == '%(n)s 6.x - all versions' % ph
        ):
            self.add_rule('ALL', AffectedVersion(core=6, all=True))

        # All versions (no version given!)
        elif (
            line == 'all versions of %(n)s' % ph or
            line == 'all versions of %(n)s module' % ph or
            line == 'all versions of the %(n)s module' % ph or
            line == 'all versions' % ph or
            line == 'all %(n)s versions' % ph or
            line == 'all %(n)s module versions' % ph or
            line == '%(n)s all versions' % ph or
            line == '%(n)s module all versions' % ph
        ):
            for v in versions:
                # We can add multiple rules because they'll be from different core versions
                self.add_rule('ALL', v)

        else:
            raise AffectedReleasesError('Unable to match the affected release line: "%s"' % line)

    def add_rule(self, op, version=None, core=None):
        """
        Adds a rule to this match object to be parsed when a release is compared
        """

        # Get the core version this rule applies to
        if not core:
            if version is None or type(version) is not AffectedVersion:
                raise AffectedReleasesError('Unable to add a rule: no version object or core specified')
            core = version.core
            if not version.core:
                raise AffectedReleasesRuleError('Unable to add a rule, version object has no core specified')

        core = str(core)

        # Make sure this core has a ruleset
        if not self.rules.get(core, False):
            self.rules[core] = []

        # Add the rule
        self.rules[core].append(
            (op, version)
        )

    def match(self, release):
        """
        Determine if a release matches these affected version rules.
        """

        if not self.consider:
            return False

        if not release.core_number:
            return False

        rules = self.rules.get(str(release.core_number), False)
        if not rules:
            return False

        passing = True
        for op, version in rules:
            if op not in self.RULE_OPERATORS:
                raise ValueError("Invalid operator token passed to rule match function.")
            if op == 'ALL':
                return True
            if op == 'OR':
                passing = True
                continue
            if passing:
                release_version = AffectedVersion(release.version)

                # Don't ever match -dev versions
                if release_version.extra == 'dev':
                    passing = False

                else:
                    r = eval('release_version %s version' % op)  # nosec : operator validated
                    passing = r

        return passing

    def matches(self, releases):
        """
        Filters a list of releases to those that match the affected
        versions specified by this object.
        """
        return [r for r in releases if self.match(r)]


class AdvisoryParser(object):
    """
    Parses a Drupal.org security advisory, allowing Django to request various
    bits of information to be saved into the ORM.
    """

    def parse(self, advisory, forcedownload=False, api_source=False):
        """
        Parses the advisory, saving information into the ORM and setting the parsed date

        - forcedownload: force a download of new releases
        - api_source: Parse advisory from newer API source, rather than published forum post.
        """
        self.advisory = advisory
        self.forcedownload = forcedownload

        if api_source or (self.advisory.source and 'api' in self.advisory.source.slug):
            # The advisory HTML is loaded via JSON body.value
            s = BeautifulSoup(self.advisory.html, 'html.parser')
            self.soup = s
        else:
            # The advisory HTML is contained in a DIV
            s = BeautifulSoup(self.advisory.html, 'lxml')
            self.soup = s.find('div', class_="field-item")

        if self.soup is None:
            msg = "Could not initialise HTML parser for advisory #{}".format(
                advisory.pk
            )
            logger.error(msg)
            if has_raven:
                ident = raven_client.get_ident(raven_client.captureMessage(msg, level='error'))
                logger.error("Sentry ident: %s" % ident)
            return False

        # Preprocessing for malformed advisories that need special handling.
        self.soup = self.parser_fixes(self.soup)
        # Rewrite source with any fixes to avoid later confusion.
        self.advisory.html = str(self.soup)

        # The first UL should be the list of advisory info
        infolist = self.soup.find('ul')
        self.info = infolist.find_all('li') if infolist else None

        # Get the affected releases (this causes the project to be downloaded)
        try:
            self.advisory.releases.set(self.versions_affected())
        except (AffectedReleasesError, AffectedVersionError, AdvisoryParserError) as exc:
            logger.error("Couldn't get the affected releases: %s" % str(exc))
            if has_raven:
                ident = raven_client.get_ident(raven_client.captureException())
                logger.error("Sentry ident: %s" % ident)
            return False

        # Set the project
        name, url, slug = self.project()
        if not slug:
            logger.error("Couldn't find the project slug in the advisory")
            if has_raven:
                ident = raven_client.get_ident(raven_client.captureException())
                logger.error("Sentry ident: %s" % ident)
            return False
        project, created = Project.objects.get_or_create(slug=slug, defaults={
            'name': name or '',
            'url': url or '',
            'type': 'project_module',
        })
        self.advisory.project = project

        self.advisory.date_posted = timezone.make_aware(self.date(), timezone.get_default_timezone())
        self.advisory.advisory_id = self.advisory_id()

        # Set risk info
        label, score, score_max, metrics = self.risk()
        if label:
            obj, created = Risk.objects.get_or_create(name=label)
            self.advisory.risk = obj
        else:
            self.advisory.risk = None
        self.advisory.risk_score = score
        self.advisory.risk_score_total = score_max

        # Set vulnerabilities
        vulnerabilities = self.vulnerabilities()
        if isinstance(vulnerabilities, list):
            self.advisory.vulnerabilities.clear()
            for v in vulnerabilities:
                obj, created = Vulnerability.objects.get_or_create(name=v)
                self.advisory.vulnerabilities.add(
                    obj
                )
        else:
            self.advisory.vulnerabilities = []

        # Set CVE(s)
        cves = self.cve()
        if isinstance(cves, list):
            self.advisory.cves.clear()
            for identifier in cves:
                obj, created = CVE.objects.get_or_create(identifier=identifier)
                self.advisory.cves.add(
                    obj
                )
        else:
            self.advisory.cves = []

        try:
            # Set core(s)
            cores = self.cores()
            if isinstance(cores, list):
                self.advisory.cores.clear()
                for c in cores:
                    obj, created = Core.objects.get_or_create(version=c)
                    self.advisory.cores.add(
                        obj
                    )
            else:
                self.advisory.cores = []
        except (AdvisoryParserError) as exc:
            logger.error("Couldn't get the affected Drupal cores: %s" % str(exc))
            if has_raven:
                ident = raven_client.get_ident(raven_client.captureException())
                logger.error("Sentry ident: %s" % ident)
            return False

        # Mark as parsed correctly, and save.
        self.advisory.date_parsed = timezone.now()
        self.advisory.failed = False
        self.advisory.save()
        # Inform caller that parsing has successfully completed.
        return True

    def parser_fixes(self, soup):
        """
        Adds fixes to specific advisories that are known to have bad formatting.

        This should be called before the BeautifulSoup object has been used for
        parsing other fields.

        Returns the BeautifulSoup object, updated with any necessary insertions
        or modifications.
        """

        if self.advisory.title.startswith("SA-CORE-2017-001"):
            # SA-CORE-2017-001 has no "Versions affected" H2.
            if soup.find('h2', text="Versions affected") is None:
                snippet = BeautifulSoup(
                    '<h2>Versions affected</h2><ul><li>Drupal 8 prior to 8.2.7</li></ul>',
                    'html.parser'
                )
                soup.append(snippet)

        elif self.advisory.title.startswith("SA-CONTRIB-2017-042"):
            # SA-CONTRIB-2017-042 has nonstandard version signature.
            target = soup.find('h2', text='Versions affected').find_next('ul').find('li')
            if target.string.startswith("Only the 1.x branch is affected."):
                # Replace with a usable signature
                target.string.replace_with("All Media 7.x-1.x versions")
                # Ensure core value of 7.x in the first UL.
                core_snippet = BeautifulSoup("<li>Version: 7.x</li>", "html.parser")
                soup.find('ul').append(core_snippet)

        elif self.advisory.title.startswith("SA-CONTRIB-2017-041"):
            # SA-CONTRIB-2017-041 has a nonstandard version signature.
            # Replace first "Open Atrium distribution" LI
            target = soup.find('li', text='Open Atrium distribution 7.x-2.x versions prior to 7.x-2.615')
            if target is not None:
                target.string.replace_with('Open Atrium 7.x-2.x versions prior to 7.x-2.615')
                # Remove other submodule version strings, since the parser only handles one project.
                # Removing:
                # oa_core 7.x-2.x versions prior to 7.x-2.84.
                # oa_comment 7.x-2.x versions prior to 7.x-2.14.
                subtargets = soup.find_all(lambda tag: tag.text.startswith("oa_c"))
                [removal.decompose() for removal in subtargets]

                # Replace multiple "Project:"s in header for Open Atrium project.
                project_li = soup.find(lambda tag: tag.text.startswith("Project:"))
                snippet = BeautifulSoup(
                    '<li>Project: <a href="https://www.drupal.org/project/openatrium">Open Atrium</a>' +
                    '     (third-party module)</li>',
                    'html.parser'
                )
                project_li.replace_with(snippet)

        elif self.advisory.title.startswith(
            "SA-CONTRIB-2017-38"
        ) or self.advisory.title.startswith("SA-CONTRIB-2017-038"):
            # SA-CONTRIB-2017-038 has:
            # - a nonstandard title
            # - erroneous "Versions affected" H2 value of "all  versions"
            # - no "Version:" info

            target = soup.find('h2', text='Versions affected').find_next('ul').find('li')
            if target.text == 'All  versions':
                target.string.replace_with('References 7.x-2.x versions prior to 7.x-2.2.')
                core_snippet = BeautifulSoup("<li>Version: 7.x</li>", "html.parser")
                soup.find('ul').append(core_snippet)
            self.advisory.title = "SA-CONTRIB-2017-038"

        elif self.advisory.title.startswith("SA-CONTRIB-2017-005"):
            # SA-CONTRIB-2017-005 has a missing Project URL and name.
            for a in soup.find_all('a', href="https://www.drupal.org/project/"):
                a['href'] = "https://www.drupal.org/project/mailjet"
                a.string = "Mailjet"
            # Version signature fix.
            target = soup.find('li', text="Mailjet 7.x-2.x versions prior 7.x-2.10.")
            if target is not None:
                target.string.replace_with("Mailjet 7.x-2.x versions prior to 7.x-2.10.")

        elif self.advisory.title.startswith("SA-CONTRIB-2016-060"):
            # SA-CONTRIB-2016-060 blends two version constraint signatures in the same line.
            target = soup.find(
                'li',
                text='Workbench Moderation 7.x-1.x versions and 7.x-3.x versions prior to 7.x-3.0.')
            if target is not None:
                snippet = BeautifulSoup(
                    '<li>Workbench Moderation 7.x-1.x versions prior to 7.x-3.0.</li>' +
                    '<li>Workbench Moderation 7.x-3.x versions prior to 7.x-3.0.</li>',
                    'html.parser'
                )
                soup.append(snippet)
                target.replace_with(snippet)
                pass

        elif self.advisory.title.startswith("SA-CONTRIB-2017-014"):
            # SA-CONTRIB-2017-014 advisory is on submodule - convert to parent.
            target = soup.find('li', text='osf_querybuilder 7.x-3.3 versions prior to 7.x-3.3.')
            if target is not None:
                target.string.replace_with('osf 7.x-3.x versions prior to 7.x-3.3.')

        elif self.advisory.title.startswith("SA-CONTRIB-2017-069"):
            # SA-CONTRIB-2017-069 advisory cannot spell the module name.
            target = soup.find('li', text='views  fresh versions prior to 7.x-1.2.')
            if target is not None:
                target.string.replace_with('views refresh versions prior to 7.x-1.2.')

        elif self.advisory.title.startswith("SA-CONTRIB-2017-070"):
            # SA-CONTRIB-2017-070 advisory doesn't pluralise
            target = soup.find('li', text='All Commerce invoice versions prior to 7.x-1.1')
            if target is not None:
                target.string.replace_with('commerce invoices versions prior to 7.x-1.1')

        elif 'SA-CORE-2018-005' in self.advisory.title:
            # SA-CORE-2018-005: legacy forum type; 'Versions Affected' is a P-tag.
            target = soup.find('h2', text="Versions affected")
            if target.find_next_sibling('ul') is None:
                snippet = BeautifulSoup('<ul><li>Drupal 8.x versions prior to 8.5.6.</li></ul>', 'html.parser')
                target.find_next_sibling().replace_with(snippet)

        elif self.advisory.title.startswith("SA-CONTRIB-2016-020"):
            # SA-CONTRIB-2016-020 versions affected signature.
            target = soup.find('li', text='Features 7.x-1.x which is no longer supported.')
            if target is not None:
                target.string.replace_with('All Features 7.x-1.x versions.')

        # Return new BS object, so that find methods work for inserted content.
        return BeautifulSoup(str(soup), 'lxml')

    def normalize(self, s):
        """
        Normalizes a string for comparison.
        """
        if s is None:
            return ''
        for p in string.punctuation:
            s = s.replace(p, '')
        s = normalize_whitespace(s)
        return s.lower().strip()

    def soupstring(self, soup, normalized=False):
        """
        Safely gets a string of a BeautifulSoup node, using stripped strings as
        a fallback.
        """

        if soup.string is not None:
            soupstring = soup.string
        else:
            soupstring = ' '.join(soup.stripped_strings)

        return self.normalize(soupstring) if normalized else soupstring

    def get_info(self, prefix, soupstring=False):
        """
        Gets an item from the bulleted list at the top of the advisory. Lookup
        is via the prefix of the line, using fuzzy matching. Returns a soup
        object.
        """

        try:
            if not self.info:
                logger.error("Couldn't find the information list at the beginning of the advisory")
                return None

            prefix = self.normalize(prefix)

            # Rate each line of the list
            ratings = []
            for s in self.info:
                s = self.soupstring(s, normalized=True)
                ratings.append(
                    # How well the prefix fuzzy matches the string
                    fuzz.partial_ratio(prefix, s[:len(prefix)])
                )

            # Pick the best option based on the ratio
            idx, ratio = max(enumerate(ratings), key=operator.itemgetter(1))
            if ratio < 90:
                return None
            elif soupstring:
                return self.soupstring(self.info[idx])
            else:
                return self.info[idx]
        except (Exception) as exc:
            logger.error("An unexpected error has occurred for advisory #%s: %s" % (self.advisory.pk, str(exc)))
            tb = sys.exc_info()[2]
            raise AdvisoryParserError(str(exc)).with_traceback(tb)

    def project(self):
        """
        Gets the affected project's name, URL and slug (machine name).
        """

        project = self.get_info('Project')

        # Project name/title
        try:
            name = self.soupstring(project).split(':')[1]

            # Cleanup
            name = re.sub(r'\(third\-party (module|theme|distribution)\)', '', name)

            if name.startswith('- '):
                name = name[2:]
            if name.endswith(' -'):
                name = name[:-2]

            name = normalize_whitespace(name)
        except Exception:
            name = None

        # Project URL & slug
        try:
            url = project.find('a')['href']
            url = url_fix(url)
            url = (url.replace('//drupal.org', '//www.drupal.org')
                      .replace('.org//node', '.org/node')
                      .replace('http://', 'https://'))
            slug = url.replace('https://www.drupal.org/project/', '')
        except Exception:
            url = None
            slug = None

        if self.normalize(name) == 'drupal core':
            url = 'https://www.drupal.org/project/drupal'
            slug = 'drupal'

        if self.normalize(name) == 'open atrium':
            url = 'https://www.drupal.org/project/openatrium'
            slug = 'openatrium'

        return name, url, slug

    def date(self):
        """
        Gets the advisory published date, as printed (not from the RSS XML).
        """

        dateinfo = self.get_info('Date')

        try:
            date_str = dateinfo.string.split(':')[1].strip()
            date = parse(date_str)
        except Exception:
            date = None

        return date

    def advisory_id(self):
        """
        Gets the Drupal.org Advisory ID (similar to a CVE number).
        """

        idinfo = self.get_info('Advisory ID')

        try:
            advisory_id = idinfo.string.split(':')[1].strip()

            # Add DRUPAL- prefix if missing
            if advisory_id.startswith('SA-'):
                advisory_id = 'DRUPAL-' + advisory_id

        except Exception:
            advisory_id = None

        return advisory_id

    def risk(self):
        """
        Gets the risk classification, score and metric
        """

        try:
            info = self.get_info('Security risk')
            if info is None:
                msg = "Advisory #%s has no 'Security risk' label." % self.advisory.pk
                logger.warning(msg)
                if has_raven:
                    ident = raven_client.get_ident(raven_client.captureMessage(msg, level='warning'))
                    logger.warning("Sentry ident: %s" % ident)

                return None, None, None, None

            risk = self.soupstring(self.get_info('Security risk'))

            # Remove 'Security risk' from the start
            risk = ":".join(risk.split(':')[1:])

            # Use regex to parse the risk format
            #  e.g. 8/25 (Less Critical) AC:Basic/A:None/CI:Basic/II:None/E:None/TD:None
            m = re.search(r'(?P<score>\d{1,2})\/(?P<score_max>\d{1,2})\s\((?P<label>[\s\w]+)\)\s(.*)', risk)
            if not m:
                return None, None, None, None

            parts = m.groupdict()

            # Risk label
            label = normalize_whitespace(parts['label'])

            # Risk score & total
            try:
                score = int(parts['score'])
                score_max = int(parts['score_max'])
            except (ValueError, IndexError):
                # No point having one without the other
                score = None
                score_max = None

            # Risk metrics
            try:
                metrics = re.findall(r'(\w+):([\w\s]+)\/?', m.group(4))

            except IndexError:
                # Group 4 not found - the metric string wasn't provided
                metrics = None

            return label, score, score_max, metrics
        except (Exception) as exc:
            msg = "An unexpected error occurred for advisory #%s: %s" % (self.advisory.pk, str(exc))
            logger.error(msg)
            tb = sys.exc_info()[2]
            raise AdvisoryParserError(msg).with_traceback(tb)

    def vulnerabilities(self):
        """
        Gets a list of the comma-separated vulnerabilities.
        """

        v = self.get_info('Vulnerability')

        try:
            return [s.strip() for s in v.string.split(':')[1].split(',')]
        except Exception:
            return None

    def cve(self):
        """
        Gets any CVE identifiers issued for this advisory
        """

        # Get the text of the advisory and search for CVE-####-### patterns
        matches = re.findall(r'CVE-\d{4}-\d{3,}', self.soup.getText())

        # Remove duplicates (case-insensitive)
        matches = list(set([m.upper() for m in matches]))

        return matches

    def cores(self):
        """
        Gets the core versions affected by this advisory.
        """
        v = self.get_info('Version', True)
        error_msg = None

        if v is None:

            # Special handling for "All versions" unsupported advisories.
            # Look for "All versions" in Versions Affected.
            try:
                heading = self.soup.find("h2", text='Versions affected')
                if heading is not None:
                    first_li = heading.find_next_sibling('ul').find('li')
                    all_version_signatures = [
                        "All versions.",
                        "All  versions"
                    ]
                    if first_li.string in all_version_signatures:
                        # Special case found.
                        # Return all current *.x core signatures for maximal coverage.
                        return [c.version for c in Core.objects.filter(version__endswith='.x')]
                    else:
                        raise AffectedVersionError("Unexpected all-version signature: %s" % first_li.string)
                else:
                    raise AffectedVersionError("Could not find 'Versions affected' heading")

            except (AffectedVersionError) as exc:
                # Report error.
                error_msg = "An unexpected error occurred for advisory #%s: %s" % (self.advisory.pk, exc)

            # Regular error handling
            if error_msg is None:
                error_msg = "An unexpected error occurred for advisory #%s: " \
                            "no 'Version' entry found in preamble." % (self.advisory.pk)

            logger.error(error_msg)
            tb = sys.exc_info()[2]
            raise AdvisoryParserError(error_msg).with_traceback(tb)

        else:
            # Fix for SA-CORE-2016-004
            v = v.string.replace("Version:li", "Version:")

            try:
                return [s.strip() for s in v.split(':')[1].split(',')]
            except Exception:
                return None

    def versions_affected(self):
        """
        Get the string(s) listed under the 'Versions affected' heading
        """

        # Find the 'Versions affected' heading
        heading = None
        for h in self.soup.find_all('h2'):
            # Use fuzzy matching
            n = fuzz.token_sort_ratio(
                self.normalize(h.string),
                self.normalize('Versions affected')
            )
            if n > 90:
                heading = h

        if heading is None:
            raise AffectedVersionError("Could not find 'Versions affected' heading")

        # Get list items (there's no easy way of knowing this is the right list)
        lines = heading.find_next_sibling('ul').find_all('li')

        affected = set()
        for line in lines:
            line = self.soupstring(line)

            # Make sure this a line with a version string
            if '.x-' not in line and 'all ' not in line.lower() and 'versions prior to' not in line:
                continue

            # Get the releases affected by this line
            versions = self.get_versions(line)

            if versions is not None:
                affected.update(
                    self.get_versions(line)
                )
            else:
                raise AffectedVersionError("Could not get versions for project in line: %s" % line)

        return list(affected)

    def get_versions(self, line):
        """
        Parse the line to figure out which versions/releases are affected. Try
        a bunch of formats one by one until something matches.
        """

        # Placeholder values for matching signatures.
        name_ph = '#NAME#'
        version_ph = '#VERSION#'

        # Can't parse the line without knowing the module info
        name, url, slug = self.project()
        if not name or not url or not slug:
            return None

        # Local normalize function
        def normalize(s):
            s = re.sub(r'\([^)]*\)', '', s)  # Remove bracketed content
            return s.lower().strip()

        line = normalize(line)
        name = normalize(name)
        slug = normalize(slug)

        # Replace the module name in the line with a placeholder
        line = line.replace(name, name_ph)
        line = line.replace(slug, name_ph)
        line = line.replace(slug.replace('-', '_'), name_ph)
        line = line.replace(slug.replace('_', '-'), name_ph)

        slug_spaced = slug
        for p in string.punctuation:
            slug_spaced = slug_spaced.replace(p, ' ')
        line = line.replace(slug_spaced, name_ph)

        # Remove double spaces & trailing full stop
        line = re.sub(' +', ' ', line)
        if line[-1] == '.':
            line = line[:-1]

        # Remove some generic strings we don't care about
        line = line.replace('. neither earlier nor later versions are affected', '')

        # Replace some abstract names that have appeared in the past
        # - given we already have figured out which project this is from the info list
        line = line.replace('drupal commons distribution and ', '')
        line = line.replace('drupal commons and ', '')
        line = line.replace('organic groups menu ', '')
        line = line.replace('aegir hosting system ', '')
        line = line.replace('acquia cloud site factory ', '')
        line = line.replace('oa_core ', '')
        line = line.replace('mulitlink ', '')
        line = line.replace('login redirect ', '')
        line = line.replace('node.js', name_ph)
        line = line.replace('f%s' % name_ph, name_ph)  # i.e. CKEditor an FCKEditor (FCKEditor is now obsolete)
        line = line.replace('novalnet payment module drupal commerce', name_ph)
        line = line.replace('novalnet payment module ubercart module', name_ph)

        # Define a custom replacement
        # - this allows us to replace versions with placeholders, and
        #   capture what was replaced at the same time
        #   see: http://stackoverflow.com/a/9135166
        class Replacement(object):
            def __init__(self, replacement):
                self.replacement = replacement
                self.occurrences = []

            def __call__(self, match):
                matched = match.group(0)
                replaced = match.expand(self.replacement)
                self.occurrences.append(matched)
                return replaced
        repl = Replacement(version_ph)

        # Insert version placeholders
        line = re.sub(AffectedVersion.VREGEX, repl, line)

        # Make the versions into AffectedVersion objects so they can be compared a bit easier
        versions = [AffectedVersion(x) for x in repl.occurrences]

        if len(versions) == 0:
            # See if it's an "all versions of [Name]"
            if (
                line.startswith('all versions of') or
                line.endswith('all versions') or
                line == 'all %s module versions' % name_ph or
                line == 'all %s versions' % name_ph
            ):
                # Add the "core" version(s) as the affected version
                versions = []
                for core in self.cores():
                    versions.append(
                        AffectedVersion(core=core.replace('.x', ''), all=True)
                    )

            # See if it's "all xx.xx versions" (i.e. a specific core version)
            elif fuzz.ratio('all xx.x versions', line) >= 90:
                # Insert version placeholders for "xx.xx"
                line = re.sub(AffectedVersion.CREGEX, repl, line)
                if len(repl.occurrences) == 1:
                    # Create an AffectedVersion with only the core specified
                    core = repl.occurrences[0]
                    versions = [AffectedVersion(core=core, all=True)]

            # Check if it's an advisory against a core.
            elif slug == 'drupal':
                # Insert version placeholders for "xx.xx.xx"
                line = re.sub(AffectedVersion.CREGEX, repl, line)
                if len(repl.occurrences) == 2:
                    # Create an AffectedVersion with only the core specified
                    core = repl.occurrences[0].replace('.x', '')

                    if not repl.occurrences[1].startswith(core):
                        raise AffectedVersionError("Major core version does not match specific version in advisory.")

                    versions.extend([
                        AffectedVersion(core=core, all=True),
                        AffectedVersion(repl.occurrences[1])
                    ])

        # Create a ReleaseMatch object for this line
        try:
            rm = ReleaseMatch(line, versions, name_ph=name_ph, version_ph=version_ph)
        except AffectedReleasesError as e:
            raise AffectedReleasesError('%s (pk=%d, id=%s)' % (e, self.advisory.pk, self.advisory_id()))

        # Sync the release history for this module
        for core in ['all', '6.x', '7.x', '8.x']:
            download_releases(slug, self.forcedownload, core, True)

        # Get a list of the project's releases
        releases = Release.objects.filter(project__slug=slug)

        # Return the releases that match the affected versions of this advisory line
        return rm.matches(releases)
