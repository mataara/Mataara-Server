

from django.core.management.base import BaseCommand

from ...models import Advisory
from ...tasks import parse_advisory, parse_api_advisory


class Command(BaseCommand):
    help = 'Parse security advisories'

    def handle(self, *args, **options):

        # Find unparsed advisories
        advisories = Advisory.objects.filter(status='downloaded')

        # Queue the advisory for parsing
        for advisory in advisories:
            # Check whether source is API.
            if '/api-d7/' in advisory.source.url and advisory.html.startswith('{'):
                parse_api_advisory.delay(advisory.html)  # Actually JSON; will be refactored.
            else:
                # Non-API source, or has already been parsed once.
                parse_advisory.delay(advisory.pk)

        if len(advisories):
            self.stdout.write('Submitted %d advisory(ies) for processing' % len(advisories))
        else:
            self.stdout.write('No advisories are awaiting processing at the moment')
