

from django.core.management.base import BaseCommand

from ...utils import download_releases
from ...tasks import sync_project_releases


class Command(BaseCommand):
    help = 'Download releases for a Drupal project'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'project', nargs=1, type=str,
            help='project slug (machine name) to sync',
        )

        # Optional flag
        parser.add_argument(
            '--now',
            action='store_true',
            help="Process immediately rather than spawning a task",
        )

    def handle(self, *args, **options):
        project = options['project'][0]
        if options['now']:
            self.stdout.write("Syncing release information for project '{}'...".format(project))
            download_releases(project, force=True, core='all', retry=True)
            self.stdout.write(self.style.SUCCESS("Release information for '{}' project downloaded.".format(
                project
            )))
        else:
            self.stdout.write("Spawning release information sync task for project: {}".format(project))
            sync_project_releases.delay(project, force=True, core='all', retry=True)
