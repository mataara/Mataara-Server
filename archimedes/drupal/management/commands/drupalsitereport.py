import csv
import io
import json
from django.core.management.base import BaseCommand, CommandError
from ...models import DrupalSite
from ...tool_patch_report import build_report, get_max_lengths, flatten_groups, groups_filter


class Command(BaseCommand):
    """Generate a report for Drupal sites to aid with patching."""

    help = 'Generate patch report on Drupal sites.'

    def add_arguments(self, parser):

        # Named (optional) arguments
        parser.add_argument(
            '--project',
            action='append',
            dest='projects',
            default=['drupal'],
            help="Project machine name to check against (supports multiple; default: drupal)"
        )

        parser.add_argument(
            '--group',
            action='append',
            dest='groups',
            default=[],
            help="Filter sites by group id(s)"
        )

        parser.add_argument(
            '--format',
            action='store',
            nargs='?',
            dest='report_format',
            default='console',
            choices=['console', 'raw', 'json', 'csv'],
            help="Format to display patch report"
        )

        parser.add_argument(
            '--showgroups',
            action='store_true',
            dest='show_groups',
            default=False,
            help="Show site groups in report"
        )

        parser.add_argument(
            '--showempty',
            action='store_true',
            dest='show_empty_rows',
            default=False,
            help="Include sites without specific project data"
        )

        parser.add_argument(
            '--showdecommissioned',
            action='store_true',
            dest='show_decommissioned',
            default=False,
            help="Include decommissioned sites"
        )

        parser.add_argument(
            '--limit',
            nargs='?',
            dest='limit',
            default=None,
            help="Limit to first LIMIT entries"
        )

    def handle(self, *args, **options):
        drupalsites = DrupalSite.objects.all()

        # Filter on SiteGroup ids (if any)
        if len(options['groups']) > 0:
            drupalsites = groups_filter(drupalsites, options['groups'])

        if options['limit'] is not None:
            drupalsites = drupalsites[:int(options['limit'])]

        report = build_report(options, drupalsites)

        if options['report_format'] == "raw":
            print(report)

        elif options['report_format'] == 'console':

            # Special handling on Groups column.
            for row in report['data']:
                if "Groups" in report['headers']:
                    i = report['headers'].index("Groups")
                    row[i] = flatten_groups(row[i])

            col_sizes = get_max_lengths(report['headers'], report['data'])

            # Build format string to unpack lists into.
            format_row = "|" + "|".join(
                [(" {:" + str(size) + "} ") for size in col_sizes]
            ) + "|"
            print(format_row.format(*report['headers']))
            for row in report['data']:
                for i, cell in enumerate(row):
                    if cell is None:
                        row[i] = '---'
                print(format_row.format(*row))

        elif options['report_format'] == "json":
            print(json.dumps(report))

        elif options['report_format'] == "csv":
            buffer = io.StringIO()
            writer = csv.writer(buffer, quoting=csv.QUOTE_NONNUMERIC)
            writer.writerow(report['headers'])
            # Special handling on Groups column.
            for row in report['data']:
                if "Groups" in report['headers']:
                    i = report['headers'].index("Groups")
                    row[i] = flatten_groups(row[i])

                writer.writerow(row)

            print(buffer.getvalue())

        else:
            raise CommandError("Unknown format '{}' specified.".format(options['report_format']))
