import logging
import urllib.request

from django.conf import settings
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class DrupalOrgUpdatesAPI:
    """
    A helper class for interacting with the Updates API at drupal.org.
    """
    base_api_url = "https://updates.drupal.org/release-history/"
    settings.DRUPAL_UPDATE_SERVER

    @staticmethod
    def get_project_url(project_slug, core='all'):
        """Fetch data for the Drupal update server"""

        # Build URL
        url = '{base}/release-history/{project}'.format(
            base=settings.DRUPAL_UPDATE_SERVER.strip("/"),
            project=project_slug
        )

        if core:
            url += '/' + core

        return url

    @staticmethod
    def get_history(project_slug, core='all'):
        """
        Fetch parsed XML update information for project.

        Returns a BeautifulSoup parser object for the XML response.
        """
        url = DrupalOrgUpdatesAPI.get_project_url(project_slug, core)

        # Trusting that drupal.org project names are suitably sanitised.
        with urllib.request.urlopen(url) as response:  # nosec
            xml = response.read()

        return BeautifulSoup(xml, 'lxml-xml')
