from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from .models import Advisory


@receiver(m2m_changed)
def advisory_count_update_handler(sender, **kwargs):
    """Watch :Release changes and update :Advisory affected_sites_count derived value as necessary."""
    # Only watching post signals: post_add, post_remove
    if kwargs['action'] in ['post_add', 'post_remove']:
        # If a DrupalSite changes modules, determine which Advisories may be affected.
        if sender.__name__ == 'DrupalSite_modules':
            # Update counts for all advisories on the project that each release is in.
            target_advisories = Advisory.objects.filter(project__release__pk__in=kwargs['pk_set']).distinct()

            for advisory in target_advisories:
                advisory.save()
        elif sender.__name__ == 'Release_release_types':
            # Update counts for all advisories on the project that each release type is changing in.
            target_advisories = Advisory.objects.filter(
                project__release__release_types__pk__in=kwargs['pk_set'],
                project__release__release_types__label='Insecure'
            ).distinct()

            for advisory in target_advisories:
                advisory.save()
