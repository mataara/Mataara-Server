from django.test import TestCase
from ..models import Advisory, AdvisorySource, DrupalSite, Project, Release, ReleaseType


class AffectedSitesCountTestCase(TestCase):
    """
    Test suite for Drupal :Advisory affected site counts.
    """
    def setUp(self):
        # Test projects and releases.
        type_insecure = ReleaseType.objects.create(label='Insecure')

        # Three projects with three releases each.
        # - 7.x-0.8 will be insecure
        # - 7.x-0.9 will start secure, but may be marked as insecure during testing
        # - 7.x-1.0 will be secure
        self.projects = dict()  # Integer keys
        self.releases = dict()  # Tuple keys
        for i in range(1, 4):
            project_slug = 'testproject' + str(i)
            self.projects[project_slug] = Project.objects.create(slug=project_slug)
            self.releases[(project_slug, 1)] = Release.objects.create(
                project=self.projects[project_slug], version='7.x-0.8'
            )
            self.releases[(project_slug, 1)].release_types.add(type_insecure)
            self.releases[(project_slug, 2)] = Release.objects.create(
                project=self.projects[project_slug], version='7.x-0.9'
            )
            self.releases[(project_slug, 3)] = Release.objects.create(
                project=self.projects[project_slug], version='7.x-1.0'
            )

        # Prior advisories exist on testproject1 and testproject2.
        # A new advisory being processed should cause recalculation in advisories on the same project.
        self.src = AdvisorySource.objects.create(slug='testsource')
        # self.src.save()
        self.advisory1 = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-01T00:00:00.000000+12:00",
            date_updated="2020-04-04T03:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv1',
            title='test advisory 1',
            project=self.projects['testproject1'],
            reviewer=None,
            risk_score=10,
            risk_score_total=25,
            source=self.src
        )
        self.advisory2 = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-02T00:00:00.000000+12:00",
            date_updated="2020-04-04T01:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv2',
            title='test advisory 2',
            project=self.projects['testproject2'],
            reviewer=None,
            risk_score=14,
            risk_score_total=25,
            source=self.src
        )

        # site1: insecure versions of testproject1, testproject2
        self.dsite_1 = DrupalSite.objects.create(name='site1')
        self.dsite_1.modules.set([
            self.releases[('testproject1', 1)],
            self.releases[('testproject2', 1)]
        ])

        # site2: secure testproject1, insecure testproject2
        self.dsite_2 = DrupalSite.objects.create(name='site2')
        self.dsite_2.modules.set([
            self.releases[('testproject1', 2)],
            self.releases[('testproject2', 1)]
        ])

        # site3: no releases to start with
        self.dsite_3 = DrupalSite.objects.create(name='site3')
        self.advisory1.save()
        self.advisory2.save()

    def tearDown(self):
        AdvisorySource.objects.all().delete()
        Advisory.objects.all().delete()
        DrupalSite.objects.all().delete()
        Project.objects.all().delete()
        Release.objects.all().delete()
        pass

    def test_baseline(self):
        """Baseline sanity check for affected site count tests."""
        self.assertEqual(9, Release.objects.count(), 'All releases created')
        insecure_release_count = Release.objects.filter(release_types__label='Insecure').count()
        self.assertEqual(3, insecure_release_count, 'Initial insecure releases found')
        self.assertEqual(2, Advisory.objects.count(), 'Initial Advisories created')
        self.assertEqual(3, DrupalSite.objects.count(), 'All Drupal sites created')

        self.assertEqual(self.advisory1.affected_sites_count, self.advisory1.affected_sites.count())
        self.assertEqual(self.advisory2.affected_sites_count, self.advisory2.affected_sites.count())

    def test_new_advisory_save(self):
        """An :Advisory (re-)counts affected sites on save()."""
        # Create new advisory against existing project.
        advisory_new = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-03T00:00:00.000000+12:00",
            date_updated="2020-04-05T01:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv-new',
            title='test advisory new',
            project=self.projects['testproject2'],
            reviewer=None,
            risk_score=1,
            risk_score_total=25,
            source=self.src
        )
        self.assertEqual(2, advisory_new.affected_sites_count, 'Both sites with insecure testproject2 counted.')

        # Update project field
        advisory_new.project = self.projects['testproject1']
        advisory_new.save()

        # advisory_new.refresh_from_db()
        self.assertEqual(1, advisory_new.affected_sites_count, 'Only insecure testproject1 counted after update.')

    def test_site_patched_secure(self):
        """Affected sites counts are updated when a :DrupalSite updates to a new version of a :Release"""
        self.assertEqual(1, self.advisory1.affected_sites_count, 'affected site count reduced and updated.')
        self.assertTrue(self.dsite_1 in self.advisory1.affected_sites.all(), 'dsite_1 is insecure')

        # Patch dsite_1
        self.dsite_1.modules.remove(self.releases[('testproject1', 1)])
        self.dsite_1.modules.add(self.releases[('testproject1', 3)])

        self.advisory1.refresh_from_db()
        self.assertTrue(self.dsite_1 not in self.advisory1.affected_sites.all(), 'dsite_1 is now secure')
        # WIP
        self.assertEqual(0, self.advisory1.affected_sites_count, 'affected site count reduced and updated')

    def test_release_marked_insecure(self):
        """Affected sites counts are updated when a :Release is marked insecure."""
        self.assertEqual(1, self.advisory1.affected_sites_count, 'Only dsite_1 initially insecure')

        # Mark bad release as insecure
        bad_release = self.releases[('testproject1', 2)]
        bad_release.release_types.add(ReleaseType.objects.get(label='Insecure'))

        self.advisory1.refresh_from_db()

        self.assertEqual(2, self.advisory1.affected_sites.count(), 'affected site count() increased and updated')
        self.assertEqual(2, self.advisory1.affected_sites_count, 'affected site count increased and updated')

    def test_add_insecure_release(self):
        """Affected sites counts are updated when an insecure :Release is added to a new :DrupalSite"""
        self.assertEqual(1, self.advisory1.affected_sites_count, 'Only dsite_1 initially insecure')

        self.dsite_3.modules.add(self.releases[('testproject1', 1)])

        self.advisory1.refresh_from_db()
        self.assertEqual(2, self.advisory1.affected_sites_count, 'Now registers dsite_3 in insecure count')

    def test_set_insecure_release(self):
        """Affected sites counts are still updated after a set() operation."""
        self.assertEqual(1, self.advisory1.affected_sites_count, 'Only dsite_1 initially insecure')
        self.assertEqual(2, self.advisory2.affected_sites_count, 'Two sites start insecure on advisory 2')

        # Updated releases via set()
        self.dsite_3.modules.set([
            self.releases[('testproject1', 1)],
            self.releases[('testproject2', 3)]
        ])

        self.advisory1.refresh_from_db()
        self.assertEqual(2, self.advisory1.affected_sites_count, 'Now registers dsite_3 in insecure count')
        # Advisory 2 houldn't be affected.
        self.advisory2.refresh_from_db()
        self.assertEqual(2, self.advisory2.affected_sites_count, 'dsite_3 to the count for advisory 2')
