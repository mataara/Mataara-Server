from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.test import TestCase
from django.utils import timezone

from ..models import (CVE, Advisory, AdvisorySource, Core, DrupalSite, Project,
                      Release, ReleaseType, Risk, Vulnerability)


class AdvisoryTestCase(TestCase):

    def test_str(self):
        """Given an :Advisory, the string representaion is the 'title' field"""
        adv = Advisory(title='Test Title')
        self.assertEqual(str(adv), 'Test Title')

    def test_save(self):
        """Given an :Advisory, update the status on saving"""
        src = AdvisorySource.objects.create(slug='test')
        usr = User.objects.create(username='testuser')

        adv = Advisory.objects.create(
            ignored=False,
            date_parsed=None,
            reviewer=None,
            date_posted=timezone.now(),
            source=src
        )
        self.assertEqual(adv.status, 'downloaded')

        adv.date_parsed = timezone.now()
        self.assertEqual(adv.status, 'downloaded')

        adv.save()  # Status is updated upon save()
        self.assertEqual(adv.status, 'parsed')

        adv.reviewer = usr
        adv.save()
        self.assertEqual(adv.status, 'reviewed')

        adv.ignored = True
        adv.save()
        self.assertEqual(adv.status, 'ignored')

    def test_review_multiple_advisories_with_a_single_reviewer(self):
        """Given multiple :Advisories, allow reviewer field to be a single :User"""
        user = User.objects.create(username='testuser')
        src = AdvisorySource.objects.create(slug='test')
        adv1 = Advisory.objects.create(
            ignored=False,
            date_parsed=None,
            reviewer=None,
            date_posted=timezone.now(),
            guid='test-adv1',
            source_id=src.pk
        )

        adv2 = Advisory.objects.create(
            ignored=False,
            date_parsed=None,
            reviewer=None,
            date_posted=timezone.now(),
            guid='test-adv2',
            source_id=src.pk
        )

        adv1.reviewer = user
        adv1.save()
        testUser_reviewed = Advisory.objects.filter(reviewer=user)
        self.assertCountEqual(testUser_reviewed.values_list('pk', flat=True), [adv1.pk])

        adv2.reviewer = user
        adv2.save()
        testUser_reviewed = Advisory.objects.filter(reviewer=user)
        self.assertCountEqual(testUser_reviewed.values_list('pk', flat=True), [adv1.pk, adv2.pk])

    def test_affected_sites(self):
        """
        :Advisory returns a valid list of potentially affected :DrupalSite(s).

        If an :Advisory exists for a :Project, and that :Project has an
        insecure :Release that is part of a :DrupalSite, return
        any affected :DrupalSite(s)."""
        project = Project.objects.create(slug='testproject')
        release_good = Release.objects.create(project=project, version='7.x-0.8')
        release_bad = Release.objects.create(project=project, version='7.x-0.9')
        type_insecure = ReleaseType.objects.create(label='Insecure')
        release_bad.release_types.add(type_insecure)

        dsite_1 = DrupalSite.objects.create(name='site1')
        dsite_2 = DrupalSite.objects.create(name='site2')
        dsite_1.modules.add(release_good)
        dsite_2.modules.add(release_bad)

        src = AdvisorySource.objects.create(slug='testsource')
        adv = Advisory.objects.create(date_posted=timezone.now(), source_id=src.pk)
        adv.project = project

        affected = adv.affected_sites

        self.assertTrue(isinstance(affected, QuerySet))
        self.assertFalse(dsite_1 in affected)
        self.assertTrue(dsite_2 in affected)


class AdvisorySourceTestCase(TestCase):

    def test_str(self):
        """Given an :AdvisorySource, the string representaion is the 'title' field"""
        src = AdvisorySource(title='Test Title')
        self.assertEqual(str(src), 'Test Title')


class RiskTestCase(TestCase):

    def test_str(self):
        """Given a :Risk, the string representaion is the 'name' field"""
        risk = Risk(name='Test Name')
        self.assertEqual(str(risk), 'Test Name')


class VulnerabilityTestCase(TestCase):

    def test_str(self):
        """Given a :Vulnerability, the string representaion is the 'name' field"""
        vul = Vulnerability(name='Test Name')
        self.assertEqual(str(vul), 'Test Name')


class CVETestCase(TestCase):

    def test_str(self):
        """Given a :CVE the string representaion is the 'identifier' field"""
        cve = CVE(identifier='TEST-01-ABC')
        self.assertEqual(str(cve), 'TEST-01-ABC')


class ProjectTestCase(TestCase):

    def test_str(self):
        """Given a :Project the string representaion is the slug field"""
        proj = Project(slug='test-project')
        self.assertEqual(str(proj), 'test-project')

    def test_type_name(self):
        """ Given a :Project set the type_name attribute"""
        proj = Project(slug='test-project')
        self.assertEqual(proj.type_name, 'Project')
        proj.type = 'something_unknown'
        self.assertEqual(proj.type_name, 'Project')
        proj.type = 'project_module'
        self.assertEqual(proj.type_name, 'Module')
        proj.type = 'project_theme'
        self.assertEqual(proj.type_name, 'Theme')

    def test_sites_using(self):
        """Given :DrupalSites and a :Release of a :Project, return the :DrupalSite(s) using that :Release"""
        proj = Project.objects.create(slug='test-project')

        release = Release.objects.create(project=proj, version='7.x-0.1')
        dsite_1 = DrupalSite.objects.create(name='test-site-1')
        dsite_1.modules.add(release)

        dsite_2 = DrupalSite.objects.create(name='test-site-2')

        using = proj.sites_using

        self.assertTrue(isinstance(using, QuerySet))
        self.assertTrue(dsite_1 in using)
        self.assertFalse(dsite_2 in using)

    def test_sites_vulnerable(self):
        """Return :DrupalSite(s) using an insecure :Release of a :Project."""
        proj = Project.objects.create(slug='test-project')

        insecure_type = ReleaseType.objects.create(label='Insecure')
        insecure_release = Release.objects.create(project=proj, version='7.x-0.1')
        insecure_release.release_types.add(insecure_type)
        dsite_1 = DrupalSite.objects.create(name='test-site-1')
        dsite_1.modules.add(insecure_release)

        secure_release = Release.objects.create(project=proj, version='7.x-0.2')
        dsite_2 = DrupalSite.objects.create(name='test-site-2')
        dsite_2.modules.add(secure_release)

        vulnerable = proj.sites_vulnerable

        self.assertTrue(isinstance(vulnerable, list))
        self.assertTrue(dsite_1 in vulnerable)
        self.assertFalse(dsite_2 in vulnerable)


class ReleaseTestCase(TestCase):

    def test_slug(self):
        """Given a :Release of a :Project, the release.slug property is the project.slug + release.version"""
        proj = Project.objects.create(slug='testproject')
        release = Release(version='7.x-2.4', project=proj)
        self.assertEqual(release.slug, 'testproject:7.x-2.4')

    def test_str(self):
        """Given a :Release of a :Project, the string representation is the release.slug property"""
        proj = Project.objects.create(slug='testproject')
        release = Release(version='7.x-2.4', project=proj)
        self.assertEqual(str(release), 'testproject:7.x-2.4')

    def test_core_number(self):
        """Given a :Release the core_number property is the integer value of release.core before the ."""
        release = Release(core='6.x')
        self.assertEqual(release.core_number, 6)
        badrelease = Release(core='q.x')
        self.assertEqual(badrelease.core_number, None)


class CoreTestCase(TestCase):

    def test_str(self):
        """Given a :Core the string represetation is the version field"""
        core = Core(version='7.41')
        self.assertEqual(str(core), '7.41')


class DrupalSiteTestCase(TestCase):

    def test_set_core(self):
        """Given a :DrupalSite, set the core field and create and add the :Core foreign key"""
        dsite = DrupalSite()

        dsite.set_core('7.41')
        self.assertEqual(dsite.core, 7)
        self.assertEqual(Core.objects.filter(version='7.41').count(), 1)
        self.assertEqual(dsite.core_version.pk, Core.objects.get(version='7.41').pk)

        dsite.set_core('x.x')
        self.assertEqual(dsite.core, None)

    def test_vulnerable_modules(self):
        """Given a :DrupalSite that contains :Releases of a :Project of type module
         return the :Release(s) that contain an insecure :ReleaseType"""
        dsite = DrupalSite.objects.create(name='test-site')
        insecure_type = ReleaseType.objects.create(label='Insecure')
        secure_type = ReleaseType.objects.create(label='Security update')
        other_type = ReleaseType.objects.create(label='Another type')
        test_module = Project.objects.create(slug='test_module', type='project_module')
        module_release1 = Release.objects.create(project=test_module, version='1.x')
        module_release2 = Release.objects.create(project=test_module, version='2.x')

        dsite.modules.add(module_release1, module_release2)
        module_release1.release_types.add(insecure_type, other_type)
        module_release2.release_types.add(secure_type, other_type)

        self.assertTrue(isinstance(dsite.vulnerable_modules, QuerySet))
        self.assertCountEqual(dsite.vulnerable_modules.values_list('pk', flat=True), [module_release1.pk])

    def test_vulnerable_themes(self):
        """Given a :DrupalSite that contains :Releases of a :Project of type theme
         return the :Release(s) that contain an insecure :ReleaseType"""
        dsite = DrupalSite.objects.create(name='test-site')
        insecure_type = ReleaseType.objects.create(label='Insecure')
        secure_type = ReleaseType.objects.create(label='Security update')
        other_type = ReleaseType.objects.create(label='Another type')
        test_theme = Project.objects.create(slug='test_theme', type='project_theme')
        theme_release1 = Release.objects.create(project=test_theme, version='1.x')
        theme_release2 = Release.objects.create(project=test_theme, version='2.x')

        dsite.themes.add(theme_release1, theme_release2)
        theme_release1.release_types.add(insecure_type, other_type)
        theme_release2.release_types.add(secure_type, other_type)

        self.assertTrue(isinstance(dsite.vulnerable_themes, QuerySet))
        self.assertCountEqual(dsite.vulnerable_themes.values_list('pk', flat=True), [theme_release1.pk])
