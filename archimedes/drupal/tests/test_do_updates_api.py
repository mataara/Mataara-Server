import logging
import os
import vcr
from django.conf import settings
from django.test import TestCase
from ..do_updates_api import DrupalOrgUpdatesAPI as updates_api

# Suppress debug info from VCR logging.
logging.getLogger("vcr").setLevel(logging.WARNING)
logging.getLogger("archimedes.drupal.do_updates_api").setLevel(logging.WARNING)


class DrupalOrgUpdatesAPITestCase(TestCase):
    """Tests for drupal.org updates API"""

    def setUp(self):
        self.parser_vcr = vcr.VCR(
            cassette_library_dir=os.path.join(settings.SITE_ROOT, 'archimedes/drupal/tests/vcr_cassettes/updates'),
            record_mode='once'
        )

    def test_base_urls(self):
        with self.settings(DRUPAL_UPDATE_SERVER='baseurl/'):
            drupal_default = updates_api.get_project_url('drupal')
            self.assertEqual(drupal_default, 'baseurl/release-history/drupal/all', 'Core URL with defaults')
            views_default = updates_api.get_project_url('views')
            self.assertEqual(views_default, 'baseurl/release-history/views/all', 'Module URL with defaults')
            views_core = updates_api.get_project_url('views', core='7.x')
            self.assertEqual(views_core, 'baseurl/release-history/views/7.x', 'Module URL for specific core')

    def test_fetch_release_history(self):
        with self.parser_vcr.use_cassette("views-7x.yaml") as cassette:
            soup = updates_api.get_history('views', core='7.x')
            self.assertTrue(soup.is_xml, 'XML returned')
            self.assertEqual(type(soup.project.short_name).__name__, 'Tag', 'project.short_name Tag found')
            self.assertEqual(soup.project.short_name.string, 'views', 'Project is views')
            self.assertTrue(cassette.all_played)

        with self.parser_vcr.use_cassette("drupal-core-all.yaml") as cassette:
            soup = updates_api.get_history('drupal')
            self.assertTrue(soup.is_xml, 'XML returned')
            self.assertEqual(type(soup.project.short_name).__name__, 'Tag', 'project.short_name Tag found')
            self.assertEqual(soup.project.short_name.string, 'drupal', 'Project is drupal')
            self.assertTrue(cassette.all_played)
