import json
import logging
import os

import vcr
from django.conf import settings
from django.test import TestCase

from ..do_api import DrupalOrgAPI as do_api
from ..do_api import DOAPIException, DOValidationException

parser_vcr = vcr.VCR(
    cassette_library_dir=os.path.join(settings.SITE_ROOT, 'archimedes/drupal/tests/vcr_cassettes'),
    record_mode='once'
)

# Suppress debug info from VCR logging.
logging.getLogger("vcr").setLevel(logging.WARNING)


class DrupalOrgAPITestCase(TestCase):
    """Tests for Drupal.org API."""

    def test_base_api(self):
        """Given a URL, ensure that it is calling the Drupal.org API correctly."""

        base_api_url = do_api.base_api_url
        bad_api_url = base_api_url.replace('drupal', 'durpal')

        base_api_base_msg = "Attempted to process non Drupal.org API URL: {}".format(
            bad_api_url
        )
        with self.assertRaisesMessage(DOValidationException, base_api_base_msg):
            do_api.validate_url(bad_api_url)

        no_entity_msg = "Base Drupal.org API endpoint called with no entity"
        with self.assertRaisesMessage(DOValidationException, no_entity_msg):
            do_api.validate_url(base_api_url)

        with_entity_url = base_api_url + "node"
        self.assertEqual(type(do_api.validate_url(with_entity_url)), str)

    def test_url_shim(self):
        """Insert .json extensions only into Drupal.org URLs that do not have them."""

        last_url = do_api.base_api_url + "node?taxonomy_forums=44&page=11"
        last_shimmed_url = do_api.base_api_url + "node.json?taxonomy_forums=44&page=11"

        self.assertEqual(
            do_api.validate_url(last_url),
            last_shimmed_url,
            '.json inserted where not present.'
        )
        self.assertEqual(
            do_api.validate_url(last_shimmed_url),
            last_shimmed_url,
            '.json not inserted where present.'
        )

        # Again, with no arguments.
        last_url = do_api.base_api_url + "taxonomy_term"
        last_shimmed_url = do_api.base_api_url + "taxonomy_term.json"

        self.assertEqual(
            do_api.validate_url(last_url),
            last_shimmed_url,
            '.json inserted where not present (no args).'
        )
        self.assertEqual(
            do_api.validate_url(last_shimmed_url),
            last_shimmed_url,
            '.json not inserted where present (no args).'
        )

    def test_first_last_pages(self):
        """Check that it is possible to determine the first and last page values from a response."""
        # Cut down response featuring only [self, first, last] list keys.
        first_raw_js = r"""
        {"self":"https:\/\/www.drupal.org\/api-d7\/node?page=0",
         "first":"https:\/\/www.drupal.org\/api-d7\/node?page=0",
         "last":"https:\/\/www.drupal.org\/api-d7\/node?page=15508"}
        """

        resp_js = json.loads(first_raw_js)
        self.assertEqual(do_api.get_current_page_number(resp_js), 0, "Current page on page 0")
        self.assertTrue(do_api.is_first_page(resp_js), "Self matches first")
        self.assertFalse(do_api.is_last_page(resp_js), "Self does not match last")

        last_raw_js = r"""
        {"self":"https:\/\/www.drupal.org\/api-d7\/node?page=15508",
         "first":"https:\/\/www.drupal.org\/api-d7\/node?page=0",
         "last":"https:\/\/www.drupal.org\/api-d7\/node?page=15508"}
        """

        resp_js = json.loads(last_raw_js)
        self.assertEqual(do_api.get_current_page_number(resp_js), 15508, "Current page on page 15508")
        self.assertFalse(do_api.is_first_page(resp_js), "Self does not match first")
        self.assertTrue(do_api.is_last_page(resp_js), "Self matches last")

        other_raw_js = '{"error": "something horribly bad happened"}'
        bad_pagination = do_api.bad_pagination_error_msg
        resp_js = json.loads(other_raw_js)

        with self.assertRaisesMessage(DOAPIException, bad_pagination):
            do_api.get_current_page_number(resp_js)
        with self.assertRaisesMessage(DOAPIException, bad_pagination):
            do_api.is_last_page(resp_js)
        with self.assertRaisesMessage(DOAPIException, bad_pagination):
            do_api.is_last_page(resp_js)

    def test_fetch_sa_onepage(self):
        """Fetch security advisories on a single page."""
        page_url = "https://www.drupal.org/api-d7/node.json?type=sa&status=1"
        valid_url = do_api.validate_url(page_url)
        self.assertEqual(page_url, valid_url)
        with parser_vcr.use_cassette("drupal-fetch-sa-onepage-index.yaml") as cassette:
            page_dict = do_api.request_nodes_page({"type": "sa", "status": "1"})
            self.assertTrue(do_api.is_first_page(page_dict))
            self.assertTrue(do_api.is_last_page(page_dict))
            self.assertTrue(cassette.all_played)
        self.assertEqual(len(page_dict['list']), 100, "100 Advisories in test data.")
