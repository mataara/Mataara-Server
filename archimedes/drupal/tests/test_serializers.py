from collections import OrderedDict

from archimedes.drupal import models
from archimedes.reports.models import Report
from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIClient

from ..models import Advisory, AdvisorySource, DrupalSite, Project, Release, ReleaseType


def get_sample_site(pk):
    """Generate a test site based on a database public key"""

    test_site = OrderedDict([
        ('count', 1),
        ('next', None),
        ('previous', None),
        ('results', [
            OrderedDict([
                ('api_url', 'http://testserver/api/drupal/sites/{pk}/'.format(pk=pk)),
                ('pk', pk),
                ('string', 'test-site'),
                ('name', 'test-site'),
                ('hostname', ''),
                ('url', ''),
                ('root', ''),
                ('title', ''),
                ('environment', None),
                ('keys', []),
                ('type', 'Drupal'),
                ('core', '7.41'),
                ('date_last_received', None),
                ('vulnerability_count', 0),
                ('decommissioned', False),
                ('subscribed', False),
                ('report__adminrole', None),
                ('core_version', '7.41'),
                ('slogan', ''),
                ('nodes', None),
                ('revisions', None),
                ('users', None),
                ('modules', []),
                ('themes', []),
                ('core_vulnerabilities', []),
                ('vulnerable_core', []),
                ('vulnerable_modules', []),
                ('vulnerable_themes', [])
            ])
        ])
    ])

    return test_site


class APITest(TestCase):
    def setUp(self):
        self.username = "test"
        self.password = "tester:1012"
        self.user = User.objects.create_user(
            username=self.username,
            email="testuser@example.com",
            password=self.password)

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

        self.dsite = DrupalSite.objects.create(name='test-site')
        self.dsite.set_core('7.41')
        self.dsite.save()
        self.test_pk = self.dsite.pk

    def test_drupal_sites_drupal_version(self):
        """
        Given a :drupal_version return a list of Drupal :Sites with that version
        :ac: return a list of Drupal Sites with s given version
        :ac: return status 200
        """
        resp = self.client.get('/api/drupal/sites/?drupal_version=7.41')

        self.assertEqual(resp.data, get_sample_site(self.test_pk))
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_drupal_sites_drupal_version_fail(self):
        """
        :ac: return an empty list
        :ac: return status 200
        """
        resp = self.client.get('/api/drupal/sites/?drupal_version=6.38')

        empty_response = OrderedDict([
            ('count', 0),
            ('next', None),
            ('previous', None),
            ('results', [])
        ])

        self.assertEqual(resp.data, empty_response)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_drupal_statistics(self):
        """
        Return a list of statistics about all DrupalSites
        :ac: a valid data structure of Drupal statistics should be returned
        :ac: return status 200
        """
        now = timezone.now()
        src = AdvisorySource.objects.create(slug='test')
        adv = Advisory.objects.create(date_posted=now, source=src, title='Test Advisory')

        dsite0 = DrupalSite.objects.create(name='test-site')
        dsite0.set_core('6.38')
        dsite0.save()
        dsite1 = DrupalSite.objects.create(name='test-site')
        dsite1.set_core('7.3')
        dsite1.save()
        dsite2 = DrupalSite.objects.create(name='test-site')
        dsite2.set_core('7.41')
        dsite2.save()
        dsite3 = DrupalSite.objects.create(name='test-site')
        dsite3.set_core('7.41')
        dsite3.save()

        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite0,
        )
        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite1,
        )
        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite2,
        )
        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite3,
        )

        stats_response = {
            'latest_drupal_advisories': [
                {'title': 'Test Advisory', 'pk': adv.pk, 'affected': 0, 'date_posted': now}
            ],
            'core_version_data': [(7, 3), (6, 1)],
            'drupal_version_labels': ['7.41', '7.3', '6.38'],
            'drupal_version_data': [2, 1, 1]}

        resp = self.client.get('/api/drupal/stats/')
        self.assertDictEqual(resp.data, stats_response)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_advisories_sites_affected(self):
        """
        Check the number of affected sites from the API call
        :ac: API response contains correct number of affected sites
        """
        project = Project.objects.create(slug='testproject')
        release_good = Release.objects.create(project=project, version='good')
        release_bad = Release.objects.create(project=project, version='bad')
        type_insecure = ReleaseType.objects.create(label='Insecure')
        release_bad.release_types.add(type_insecure)
        release_good.save()
        release_bad.save()

        src = AdvisorySource.objects.create(slug='testsource')
        adv = Advisory.objects.create(date_posted=timezone.now(), source_id=src.pk, title='test-advisory')
        adv.project = project
        adv.releases.add(release_bad)
        adv.save()

        goodsite_1 = DrupalSite.objects.create(title='good-site-1')
        goodsite_1.modules.add(release_good)
        goodsite_1.save()
        goodsite_2 = DrupalSite.objects.create(title='good-site-2')
        goodsite_2.modules.add(release_good)
        goodsite_2.save()
        badsite_1 = DrupalSite.objects.create(title='bad-site-1')
        badsite_1.modules.add(release_bad)
        badsite_1.save()
        badsite_2 = DrupalSite.objects.create(title='bad-site-2')
        badsite_2.modules.add(release_bad)
        badsite_2.save()

        # Use same latest_advisories as calcstats.
        latest_advisories = models.Advisory.objects.all().order_by('-date_posted')[:10]
        self.assertEqual(adv, latest_advisories[0])

        # Verify affected sites are as expected before calling API.
        affected = adv.affected_sites
        self.assertTrue(badsite_1 in affected)
        self.assertTrue(badsite_2 in affected)
        self.assertFalse(goodsite_1 in affected)
        self.assertFalse(goodsite_2 in affected)

        # Check that the API call agrees.
        resp = self.client.get('/api/drupal/stats/')
        self.assertEqual(resp.data['latest_drupal_advisories'][0]['title'], 'test-advisory')
        self.assertEqual(resp.data['latest_drupal_advisories'][0]['affected'], 2)
