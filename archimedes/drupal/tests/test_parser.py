# from mock import patch, Mock
# from bs4 import BeautifulSoup
# from collections import Iterable
import os

from bs4 import BeautifulSoup
from django.test import TestCase
from django.utils import timezone

from ..exceptions import AffectedVersionError
from ..models import Advisory, AdvisorySource, Project, Release
from ..parser import AdvisoryParser, AffectedVersion


# from django.db.models.query import QuerySet
# from django.core.urlresolvers import reverse

def get_full_core_advisory_html():
    """Fetch core advisory HTML sample."""
    with open(os.path.join(os.path.dirname(__file__), 'sample/DRUPAL-SA-CORE-2016-005.html'), 'r') as myfile:
        html = myfile.read().replace('\n', '')
    return html


def get_advisory_no_versions_affected_heading_html():
    """Fetch core advisory with no versions affected HTML sample."""
    with open(os.path.join(os.path.dirname(__file__), 'sample/DRUPAL-SA-CORE-WITH-NO_HEADING.html'), 'r') as myfile:
        html = myfile.read().replace('\n', '')
    return html


class AdvisoryParserCoreTestCase(TestCase):
    core_advisory_html = None

    @classmethod
    def setUpTestData(cls):
        cls.advisory_source = AdvisorySource.objects.create(slug='testsource3')
        cls.advisory = Advisory.objects.create(date_posted=timezone.now(), source_id=cls.advisory_source.pk)
        cls.advisory.html = get_full_core_advisory_html()
        cls.bad_advisory = Advisory.objects.create(date_posted=timezone.now(),
                                                   guid='parser-test3',
                                                   source_id=AdvisoryParserCoreTestCase.advisory_source.pk,
                                                   html=get_advisory_no_versions_affected_heading_html())
        cls.parser = AdvisoryParser()

    def test_core_versions(self):
        """Core with "Version: 7.x, 8.x" line parsed."""
        self.parser.parse(self.advisory)
        cores = self.parser.cores()
        self.assertTrue('6.x' not in cores)
        self.assertTrue('7.x' in cores)
        self.assertTrue('8.x' in cores)
        self.assertTrue(len(cores) == 2, 'Only 7.x and 8.x core expected.')

    def test_cves(self):
        """CVE identifiers are being parsed."""
        self.parser.parse(self.advisory)
        cves = self.parser.cve()
        self.assertTrue('CVE-2016-9451' in cves)
        self.assertTrue('CVE-2016-9449' in cves)
        self.assertTrue('CVE-2016-9452' in cves)
        self.assertTrue('CVE-2016-9450' in cves)
        self.assertTrue(len(cves) == 4, 'Four CVEs expected.')

    def test_project(self):
        """Drupal core project is correct for core advisories."""
        self.parser.parse(self.advisory)
        name, url, slug = self.parser.project()
        self.assertEqual(name, 'Drupal core')
        self.assertEqual(url, 'https://www.drupal.org/project/drupal')
        self.assertEqual(slug, 'drupal')

    def test_get_versions_core(self):
        """get_versions with 'Drupal core N.x versions prior to...' pattern."""
        self.parser.parse(self.advisory)

        release_versions = [r.version for r in self.parser.get_versions("Drupal core 7.x versions prior to 7.1")]
        self.assertTrue('6.37' not in release_versions, 'Match on different major version found.')
        self.assertTrue('7.1' not in release_versions, 'Prior to 7.1 should not match itself.')
        self.assertTrue('7.0' in release_versions, 'Prior version not found.')
        self.assertTrue('7.0-beta3' in release_versions, 'Prior version with "-beta3" not found.')
        self.assertTrue('7.0-rc2' in release_versions, 'Prior version with "-rc2" not found.')
        self.assertTrue('7.2' not in release_versions, 'Later version found.')
        self.assertTrue('8.1.0' not in release_versions, 'Match on different major version found.')

        release_versions = [r.version for r in self.parser.get_versions("Drupal core 8.x versions prior to 8.1.3")]
        self.assertTrue('6.37' not in release_versions, 'Match on different major version found.')
        self.assertTrue('7.2' not in release_versions, 'Match on different major version found.')
        self.assertTrue('8.1.3' not in release_versions, 'Prior to 8.1.3 should not match itself.')
        self.assertTrue('8.2.0' not in release_versions, 'Later version found.')
        self.assertTrue('8.1.0' in release_versions, 'Prior version not found.')

    def test_versions_affected(self):
        """'Versions affected' rules match appropriate version releases (prior to 7.52, 8.2.3)."""
        self.parser.parse(self.advisory)
        self.assertEqual(self.parser.advisory.project.slug, 'drupal')
        project = Project.objects.get_or_create(slug='drupal')[0]
        version_list = [
            '6.4',
            '7.51',
            '7.51-dev',
            '7.52',
            '7.53',
            '8.1',
            '8.1.x-dev',
            '8.2.2',
            '8.2.3',
            '8.2.4',
        ]

        pks = []
        for version in version_list:
            version_string = version
            r = Release.objects.get_or_create(project_id=project.id, version=version_string)[0]
            pks.append(r.pk)

        affected = self.parser.versions_affected()

        def check_affected(version_string):
            return Release.objects.get(project_id=project.id, version=version_string) in affected

        self.assertFalse(check_affected('6.4'), 'Drupal core 6.x should not be affected.')
        self.assertTrue(check_affected('7.51'), 'Drupal core 7.51 is prior to 7.52.')
        # self.assertTrue(check_affected('7.51-dev'), 'Dev versions should be marked as affected.')
        self.assertFalse(check_affected('7.52'), 'Version from "prior to" clause affected.')
        self.assertFalse(check_affected('7.53'), 'Later version found.')
        self.assertTrue(check_affected('8.1.0'), 'Prior version not found.')
        self.assertTrue(check_affected('8.2.2'), 'Prior version not found.')
        # self.assertTrue(check_affected('8.2.2-dev'))
        self.assertFalse(check_affected('8.2.3'), 'Version from "prior to" clause affected.')
        self.assertFalse(check_affected('8.2.4'), 'Later version found.')
        # self.assertTrue(False, 'Breakpoint')

    def test_versions_affected_returns_none_because_incorrect_heading(self):
        """Parser.parse raises an AffectedVersionError if the advisory is
         missing the 'Versions affected' Heading (or if it is misspelled)"""

        expected_msg = "Could not find 'Versions affected' heading"
        with self.assertRaisesMessage(AffectedVersionError, expected_msg):
            self.parser.advisory = self.bad_advisory
            s = BeautifulSoup(self.bad_advisory.html, 'lxml')
            self.parser.soup = s.find('div', class_="field-item")
            self.assertEqual(self.parser.advisory.pk, self.bad_advisory.pk)
            self.parser.versions_affected()

    def test_compare_operators(self):
        """Ensure that only valid operators are accepted by the AffectedVersion
        compare_extra() method."""
        a = AffectedVersion('7.x-1.0-beta1')
        b = AffectedVersion('7.x-1.0-beta3')

        self.assertTrue(AffectedVersion.compare_extra(a, b, '<'))
        self.assertFalse(AffectedVersion.compare_extra(a, a, '<'))
        self.assertTrue(AffectedVersion.compare_extra(a, b, '<='))
        self.assertTrue(AffectedVersion.compare_extra(a, a, '<='))
        self.assertTrue(AffectedVersion.compare_extra(a, a, '=='))
        self.assertFalse(AffectedVersion.compare_extra(a, b, '=='))
        self.assertFalse(AffectedVersion.compare_extra(a, a, '!='))
        self.assertTrue(AffectedVersion.compare_extra(a, b, '!='))
        self.assertFalse(AffectedVersion.compare_extra(a, b, '>'))
        self.assertFalse(AffectedVersion.compare_extra(a, a, '>'))
        self.assertFalse(AffectedVersion.compare_extra(a, b, '>='))
        self.assertTrue(AffectedVersion.compare_extra(a, a, '>='))
        # Exceptions for invalid operators
        expected_msg = "Invalid operator token passed to compare function."
        with self.assertRaisesMessage(ValueError, expected_msg):
            AffectedVersion.compare_extra(a, b, '=')
        with self.assertRaisesMessage(ValueError, expected_msg):
            AffectedVersion.compare_extra(a, b, '===')
        with self.assertRaisesMessage(ValueError, expected_msg):
            AffectedVersion.compare_extra(a, b, "Robert') DROP TABLE auth_user;--")
