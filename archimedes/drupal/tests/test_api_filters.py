from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from ..models import Advisory, AdvisorySource, DrupalSite, User, Project, Release, ReleaseType


class APIDrupalAdvisoryFilterTestCase(TestCase):
    """
    Test suite for API filters on Drupal Advisories, for the :AdvisorySiteViewSet.
    """
    def setUp(self):
        self.username = "test"
        self.password = "tester:1012"
        self.user = User.objects.create_user(
            username=self.username,
            email="testuser@example.com",
            password=self.password)
        # Pre-authenticate user for test requests.
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

        self.src = AdvisorySource.objects.create(slug='testsource')
        self.src.save()

        # Other model fixtures
        type_insecure = ReleaseType.objects.create(label='Insecure')
        projects = {}
        releases_good = {}
        releases_bad = {}
        for i in range(1, 4):
            projects[i] = Project.objects.create(slug='testproject' + str(i))
            releases_good[i] = Release.objects.create(project=projects[i], version='7.x-0.8')
            releases_bad[i] = Release.objects.create(project=projects[i], version='7.x-0.9')
            releases_bad[i].release_types.add(type_insecure)

        self.dsite_1 = DrupalSite.objects.create(name='site1')
        self.dsite_2 = DrupalSite.objects.create(name='site2')
        self.dsite_3 = DrupalSite.objects.create(name='site3')

        # Advisory fixtures
        self.advisory1 = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-01T00:00:00.000000+12:00",
            date_updated="2020-04-04T03:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv1',
            project=projects[1],
            reviewer=None,
            risk_score=10,
            risk_score_total=25,
            source=self.src
        )
        self.advisory1.save()
        self.advisory2 = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-02T00:00:00.000000+12:00",
            date_updated="2020-04-04T01:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv2',
            project=projects[2],
            reviewer=None,
            risk_score=14,
            risk_score_total=25,
            source=self.src
        )
        self.advisory2.save()
        self.advisory3 = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-03T00:00:00.000000+12:00",
            date_updated="2020-04-04T02:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv3',
            project=projects[3],
            reviewer=None,
            risk_score=12,
            risk_score_total=15,
            source=self.src
        )
        self.advisory3.save()
        self.advisory4 = Advisory.objects.create(
            ignored=False,
            date_posted="2020-04-07T00:00:00.000000+12:00",
            date_updated="2020-04-07T02:00:00.000000+12:00",
            date_parsed=None,
            guid='test-adv4',
            project=None,
            reviewer=None,
            risk_score=None,
            risk_score_total=None,
            source=self.src
        )
        self.advisory4.save()

        # Add secure and insecure releases to sites.
        # Advisory 1: 2 Sites
        # Advisory 2: 3 Sites
        # Advisory 3: 1 Site
        # Advisory 4: 0 sites
        self.dsite_1.modules.add(releases_good[1], releases_bad[2], releases_good[3])
        self.dsite_2.modules.add(releases_bad[1],  releases_bad[2], releases_good[3])
        self.dsite_3.modules.add(releases_bad[1],  releases_bad[2], releases_bad[3])

    def tearDown(self):
        Advisory.objects.all().delete()
        AdvisorySource.objects.all().delete()

    def test_advisory_api_routes(self):
        """Ensure basic routes are working."""
        self.assertIsNotNone(reverse('advisory-list'), msg='Advisory list has route')
        self.assertIsNotNone(reverse('advisory-detail', kwargs={
            'pk': self.advisory1.id
        }), msg='Test Advisory 1 has route')
        self.assertIsNotNone(reverse('advisory-detail', kwargs={
            'pk': self.advisory2.id
        }), msg='Test Advisory 2 has route')
        self.assertIsNotNone(reverse('advisory-detail', kwargs={
            'pk': self.advisory3.id
        }), msg='Test Advisory 3 has route')
        self.assertIsNotNone(reverse('advisory-detail', kwargs={
            'pk': self.advisory4.id
        }), msg='Test Advisory 4 has route')

    def test_advisory_list(self):
        """Get basic Advisory list."""
        response = self.client.get(reverse('advisory-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response_dict = response.json()
        self.assertEqual(response_dict['count'], Advisory.objects.count(), 'Advisory count match')
        guids = [result['guid'] for result in response_dict['results']]
        self.assertIn('test-adv1', guids)
        self.assertIn('test-adv2', guids)
        self.assertIn('test-adv3', guids)
        self.assertIn('test-adv4', guids)

    def test_advisory_risk_score_ordering(self):
        """Ensure ordering by risk scores works, with null values last."""
        response = self.client.get(reverse('advisory-list'), {'ordering': 'risk_score'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_dict = response.json()
        self.assertEqual(response_dict['count'], Advisory.objects.count(), 'Advisory count match')
        # Convert to ints for comparisons.
        risks = []
        for result in response_dict['results']:
            if result['risk_score'] is not None:
                risks.append(int(result['risk_score']))
            else:
                risks.append(None)
        self.assertIsNone(risks[3], "Null risk_score is last")
        self.assertTrue(risks[0] < risks[1] < risks[2], "risk_score ordering ascends")

        response = self.client.get(reverse('advisory-list'), {'ordering': '-risk_score'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_dict = response.json()
        self.assertEqual(response_dict['count'], Advisory.objects.count(), 'Advisory count match')
        # Convert to ints for comparisons.
        risks = []
        for result in response_dict['results']:
            if result['risk_score'] is not None:
                risks.append(int(result['risk_score']))
            else:
                risks.append(None)
        self.assertIsNone(risks[3], "Null risk_score is last")
        self.assertTrue(risks[0] > risks[1] > risks[2], "-risk_score ordering descends")

    def test_advisory_affected_sites(self):
        """Ensure accurate counts for affected_sites values."""
        response = self.client.get(reverse('advisory-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_dict = response.json()
        advisories = response_dict['results']

        # Advisory 1: 2 Sites
        self.assertTrue(any(a['guid'] == 'test-adv1' and a['affected_sites_count'] == 2 for a in advisories))
        # Advisory 2: 3 Sites
        self.assertTrue(any(a['guid'] == 'test-adv2' and a['affected_sites_count'] == 3 for a in advisories))
        # Advisory 3: 1 Site
        self.assertTrue(any(a['guid'] == 'test-adv3' and a['affected_sites_count'] == 1 for a in advisories))
        # Advisory 4: 0 sites
        self.assertTrue(any(a['guid'] == 'test-adv4' and a['affected_sites_count'] == 0 for a in advisories))

    def test_advisory_affected_sites_count_ordering(self):
        """Ensure accurate ordering for affected_sites on count values."""
        response = self.client.get(reverse('advisory-list'), {'ordering': 'affected_sites_count'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_dict = response.json()
        advisories = response_dict['results']

        count_pairs = [(a['guid'], a['affected_sites_count']) for a in advisories]
        # Expecting:
        # ('test-adv4', 0)
        # ('test-adv3', 1)
        # ('test-adv1', 2)
        # ('test-adv2', 3)
        self.assertEqual(count_pairs[0][0], 'test-adv4')
        self.assertEqual(count_pairs[1][0], 'test-adv3')
        self.assertEqual(count_pairs[2][0], 'test-adv1')
        self.assertEqual(count_pairs[3][0], 'test-adv2')

        response = self.client.get(reverse('advisory-list'), {'ordering': '-affected_sites_count'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_dict = response.json()
        advisories = response_dict['results']

        # Expecting:
        # ('test-adv2', 3)
        # ('test-adv1', 2)
        # ('test-adv3', 1)
        # ('test-adv4', 0)

        count_pairs = [(a['guid'], a['affected_sites_count']) for a in advisories]
        self.assertEqual(count_pairs[0][0], 'test-adv2')
        self.assertEqual(count_pairs[1][0], 'test-adv1')
        self.assertEqual(count_pairs[2][0], 'test-adv3')
        self.assertEqual(count_pairs[3][0], 'test-adv4')


class APIDrupalSiteFilterTestCase(TestCase):
    """
    STUB. Test suite for API filters on Drupal sites, for the :DrupalSiteViewSet.

    Filters available on Drupal sites include:
    - ordering filters (as regular fields)
    - DrupalSiteFilter filterset
    - other custom filters (admin_name, admin_email)
    """
    def setUp(self):
        self.site1 = DrupalSite.objects.create()

    def tearDown(self):
        DrupalSite.objects.all().delete()
