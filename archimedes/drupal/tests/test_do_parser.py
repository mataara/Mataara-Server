import json
import logging
import os

import vcr
from django.conf import settings
from django.db import transaction
from django.test import TestCase
from django.utils import timezone
from mock import patch

from ..do_advisory_parser import DOAdvisoryParser, import_api_advisories
from ..do_api import DrupalOrgAPI as do_api
from ..exceptions import AdvisoryParserError, AffectedVersionError
from ..models import Advisory, AdvisorySource, Project, Release, Vulnerability
from ..utils import get_guid

# Suppress debug info from VCR logging.
logging.getLogger("vcr").setLevel(logging.WARNING)

# Make our tests less noisy.
logging.getLogger("archimedes.drupal.do_advisory_parser").setLevel(logging.WARNING)
# logging.getLogger("archimedes.drupal.do_advisory_parser").setLevel(logging.INFO)

logging.getLogger("django.db.backends.schema").setLevel(logging.INFO)


parser_vcr = vcr.VCR(
    cassette_library_dir=os.path.join(settings.SITE_ROOT, 'archimedes/drupal/tests/vcr_cassettes'),
    record_mode='once'
)

# The following are places where advisories are known to contain strange values
# that Mataara should be logging a warning about during processing.
#
# They are listed here so that we can check for the presence of new, unexpected
# warnings, and verify that the ones that we know about are being raised as
# expected.

expected_sa_warnings = set([
    'Advisory SA-CORE-2018-003: No release match found in solution URL https://www.drupal.org/project/ckeditor.',
    'Advisory SA-CORE-2018-003: No release match found in solution URL https://www.drupal.org/project/wysiwygw.',
    'Advisory SA-CONTRIB-2018-018: No matchable releases in solution field.',
    'Advisory SA-CONTRIB-2018-057: No matchable releases in solution field.',
    'Advisory SA-CONTRIB-2018-066: No matchable releases in solution field.'
])


def get_contrib_advisory_json_sa():
    """Fetch SA contrib advisory in JSON format."""
    json_string = do_api.request_node(2915530, return_raw=True)
    # with open(os.path.join(os.path.dirname(__file__), 'sample/DRUPAL-SA-CONTRIB-2017-077.json'), 'r') as myfile:
    #     json_string = myfile.read()
    return json_string


def get_core_advisory_json_sa():
    """Fetch SA core advisory in JSON format."""
    json_string = do_api.request_node(2946543, return_raw=True)
    # with open(os.path.join(os.path.dirname(__file__), 'sample/DRUPAL-SA-CONTRIB-2017-077.json'), 'r') as myfile:
    #     json_string = myfile.read()
    return json_string


def get_advisory_json_legacy_contrib():
    """Fetch Contrib advisory in JSON format."""
    with open(os.path.join(os.path.dirname(__file__), 'sample/DRUPAL-SA-CONTRIB-2017-069.json'), 'r') as myfile:
        json_string = myfile.read()
    return json_string


class DrupalOrgAdvisoryParserTestCase(TestCase):
    def setUp(self):
        self.api_source, created = AdvisorySource.objects.get_or_create(
            title='Drupal SA API',
            url="https://www.drupal.org/api-d7/node.json?type=sa&status=1"
        )
        if created:
            self.api_source.save()
        self.api_source, created = AdvisorySource.objects.get_or_create(
            title='Drupal Core API',
            url="https://www.drupal.org/api-d7/node.json?taxonomy_forums=1852"
        )
        if created:
            self.api_source.save()
        self.api_source, created = AdvisorySource.objects.get_or_create(
            title='Drupal Contrib API',
            url="https://www.drupal.org/api-d7/node.json?taxonomy_forums=44"
        )
        if created:
            self.api_source.save()

    def tearDown(self):
        # Remove any Advisory, Project and Release objects to normalise VCR cassettes
        Project.objects.all().delete()
        Release.objects.all().delete()
        Advisory.objects.all().delete()
        AdvisorySource.objects.all().delete()

    def test_missing_requirements(self):
        """Ensure parser fails appropriately on missing data."""
        parser = DOAdvisoryParser()

        json_good_reqs = """{
            "nid":"1234",
            "url":"http://www.example.com",
            "type":"sa",
            "changed":"1500000000",
            "field_sa_description": {
                "value": "This is a description."
            }
        }"""

        # Loads each time for deep copies.
        json_no_type = json.loads(json_good_reqs)
        del json_no_type['type']
        json_no_type = json.dumps(json_no_type)
        expected_msg = "Required key 'type' not found in input"
        with self.assertRaisesMessage(AdvisoryParserError, expected_msg):
            parser.parse(json_no_type)

        json_no_nid = json.loads(json_good_reqs)
        del json_no_nid['nid']
        json_no_nid = json.dumps(json_no_nid)
        expected_msg = "Required key 'nid' not found in input."
        with self.assertRaisesMessage(AdvisoryParserError, expected_msg):
            parser.parse(json_no_nid)

        json_no_url = json.loads(json_good_reqs)
        del json_no_url['url']
        json_no_url = json.dumps(json_no_url)
        expected_msg = "Required key 'url' not found in input"
        with self.assertRaisesMessage(AdvisoryParserError, expected_msg):
            parser.parse(json_no_url)

        json_no_desc = json.loads(json_good_reqs)
        del json_no_desc['field_sa_description']['value']
        json_no_desc = json.dumps(json_no_desc)
        expected_msg = "Required key '['field_sa_description', 'value']' not found in input"
        with self.assertRaisesMessage(AdvisoryParserError, expected_msg):
            parser.parse(json_no_desc)

        json_bad_type = json.loads(json_good_reqs)
        json_bad_type['type'] = "book"
        json_bad_type = json.dumps(json_bad_type)
        expected_msg = "Node 1234 is not a security advisory node [actual type: book]"
        with self.assertRaisesMessage(AdvisoryParserError, expected_msg):
            parser.parse(json_bad_type)

    def test_sa_api_parsing(self):
        """Ensure that new content-typed advisories can be parsed."""
        parser = DOAdvisoryParser()
        with parser_vcr.use_cassette('drupal-sa-contrib-2017-077.yaml') as cassette:
            advisory_json = get_contrib_advisory_json_sa()
            parser.parse(advisory_json)
            advisory = parser.advisory
            self.assertTrue(cassette.all_played)

        self.assertEqual(parser.advisory_dict['nid'], "2915530", "Sample JSON advisory loads okay.")
        self.assertEqual(parser.advisory_dict['type'], "sa", "Ensure security advisory type 'sa'.")

        # Check project ID and slug.
        project_nid = parser.find_key(["field_project", "id"])
        self.assertEqual(
            advisory.project,
            Project.objects.get(drupal_org_nid=project_nid),
            "Project node ID matched."
        )
        project_machine_name = parser.find_key(["field_project", "machine_name"])
        self.assertEqual(
            advisory.project,
            Project.objects.get(slug=project_machine_name),
            "Project machine name matched."
        )

        # Check expected advisory ID.
        self.assertEqual(advisory.advisory_id, "SA-CONTRIB-2017-077", "Advisory ID parsed.")

        # Check expected risk score.
        self.assertEqual(advisory.risk.name, 'Moderately critical', "Risk label parsed.")
        self.assertEqual(advisory.risk_score, 12, "Risk score parsed.")
        self.assertEqual(advisory.risk_score_total, 25, "Risk score total parsed.")

        self.assertEqual(1, advisory.vulnerabilities.count(), "One vulnerability type found.")
        self.assertEqual(
            Vulnerability.objects.get(name="Access Bypass"),
            advisory.vulnerabilities.first(), "Access Bypass vulnerability type found.")

        self.assertTrue('7.x' in [c.version for c in advisory.cores.all()], '7.x core found.')

    def test_core_sa_api_parsing(self):
        """Ensure that new content-typed advisories work for core advisories."""
        parser = DOAdvisoryParser()
        with parser_vcr.use_cassette("drupal-sa-core-2018-001.yaml") as cassette:
            advisory_json = get_core_advisory_json_sa()
            parser.parse(advisory_json)
            advisory = parser.advisory
            self.assertTrue(cassette.all_played)
        self.assertEqual(parser.advisory_dict['nid'], "2946543", "Sample JSON advisory loads okay.")
        self.assertEqual(parser.advisory_dict['type'], "sa", "Ensure security advisory type 'sa'.")

        # Check project ID and slug.
        project_nid = parser.find_key(["field_project", "id"])
        self.assertEqual(
            advisory.project,
            Project.objects.get(drupal_org_nid=project_nid),
            "Project node ID matched."
        )
        project_machine_name = parser.find_key(["field_project", "machine_name"])
        self.assertEqual(
            advisory.project,
            Project.objects.get(slug=project_machine_name),
            "Project machine name matched."
        )

        # Check expected advisory ID.
        self.assertEqual(advisory.advisory_id, "SA-CORE-2018-001", "Advisory ID parsed.")

    def test_legacy_api_parsing(self):
        """Ensure that JSON content from old forum-based advisories can be parsed."""

        with parser_vcr.use_cassette("drupal-fetch-legacy-api.yaml") as cassette:
            # Transaction needed so as not to interact with tearDown step.
            with transaction.atomic():
                advisory_json = get_advisory_json_legacy_contrib()
                parser = DOAdvisoryParser()
                parser.parse(advisory_json)
                self.assertTrue(cassette.all_played)

        self.assertEqual(parser.find_key('nid'), "2902606", "Sample JSON advisory loads okay.")
        self.assertEqual(parser.find_key('type'), "forum", "Ensure security advisory type 'forum'.")
        self.assertTrue(parser.advisory.title.startswith("SA-CONTRIB-2017-069"), "Title ID match.")
        self.assertTrue(
            parser.advisory.url.startswith("https://www.drupal.org/forum/newsletters/"),
            "URL is a forum post.")
        self.assertEqual(parser.advisory.guid, "2902606 at https://www.drupal.org", "GUID match.")

    def test_parse_sa_onepage(self):
        """
        Parse all security advisories on a single page.

        Note that this test runs through 100 security advisories of 'sa' type.
        It may take a long time to run.

        TODO: Refactor this to:
        - Mock out the advisory parsing into individual tests
        - Reduce the number of advisories being tested to a representative subset.
        """
        with parser_vcr.use_cassette("drupal-fetch-sa-onepage-index.yaml") as cassette:
            page_dict = do_api.request_nodes_page({"type": "sa", "status": "1"})
            self.assertTrue(do_api.is_first_page(page_dict))
            self.assertTrue(do_api.is_last_page(page_dict))
            self.assertTrue(cassette.all_played)
        self.assertEqual(len(page_dict['list']), 100, "100 Advisories in test data.")

        parser = DOAdvisoryParser()
        with self.assertLogs('archimedes.drupal.do_advisory_parser', level='WARNING') as cm:
            with parser_vcr.use_cassette("drupal-parse-sa-onepage-full.yaml") as cassette:
                for advisory in page_dict['list']:
                    parser.parse(do_api.request_node(advisory['nid']))
                warnings = [msg.replace('WARNING:archimedes.drupal.do_advisory_parser:', '') for msg in cm.output]
                for message in warnings:
                    # Match any messages against known and expected test data warnings.
                    if message not in expected_sa_warnings:
                        self.fail("Unexpected warning encountered in advisory parser log: {}".format(message))
            self.assertTrue(cassette.all_played)

    def test_parse_contrib_all_pages(self):
        """
        Test extraction of legacy contrib advisories across multiple pages.

        This test covers the API fetching and pagination thru to the
        DOAdvisoryParser call.
        """
        contrib_api_source = AdvisorySource.objects.get(slug='drupal-contrib-api')
        with parser_vcr.use_cassette("drupal-fetch-legacy-contrib-api-full.yaml") as cassette:
            with patch('celery.current_app.send_task') as mock_task:
                import_api_advisories(contrib_api_source, nids=None)
            mock_task.assert_called()
            # Expect 80 advisories parsed with drupal-fetch-legacy-contrib-api-full.yaml.
            # This number may change if the VCR cassette is refreshed.
            self.assertEqual(mock_task.call_count, 80)
            self.assertTrue(cassette.all_played)

    def test_parse_contrib_all_pages_ignore_one(self):
        """
        Test extraction of when one of the advisories is set to be ignored

        This uses a VCR cassette that is essentially a copy of
          drupal-fetch-legacy-contrib-api-full.yaml

        with the requests for the ignored advisories removed.
        TODO: in future cut the advisory down so that it contains no more than
        3 or 4 nids.
        """
        contrib_api_source = AdvisorySource.objects.get(slug='drupal-contrib-api')
        # create advisories to be ignored
        for nid in [2900966, 2179085]:
            ignored_advisory = Advisory(
                guid=get_guid(nid), title='Ignored advisory {}'.format(nid),
                url='https://www.drupal.org/nid/{}'.format(nid),
                date_posted=timezone.now(),
                ignored=True,
                source=contrib_api_source)
            ignored_advisory.save()
        with parser_vcr.use_cassette("drupal-fetch-legacy-contrib-api-full_ignore_advisory.yaml") as cassette:
            with patch('celery.current_app.send_task') as mock_task:
                import_api_advisories(contrib_api_source, nids=None)
            mock_task.assert_called()
            # Expect 80 advisories parsed with drupal-fetch-legacy-contrib-api-full.yaml.
            # This number may change if the VCR cassette is refreshed.
            # It is 1 less than the number of advisories as one is ignored.
            self.assertEqual(mock_task.call_count, 79)
            self.assertTrue(cassette.all_played)

    def test_solution_parsing_core(self):
        """Test extraction of core release-matching data from solution field."""
        parser = DOAdvisoryParser()
        # SA-CORE-2018-004
        with parser_vcr.use_cassette("drupal-sa-core-2018-004.yaml") as cassette:
            advisory_json = do_api.request_node(2966049, return_raw=True)
            parser.parse(advisory_json)
            self.assertEqual(
                str(parser.match_versions),
                "[['<', 'drupal', '7.59'], ['<', 'drupal', '8.5.3'], ['<', 'drupal', '8.4.8']]",
                "Expected versions on SA-CORE-2018-004"
            )
            self.assertTrue(cassette.all_played)
        # Tear down: remove project for next cassette.
        Project.objects.get(slug="drupal").delete()

        # SA-CORE-2018-001
        with parser_vcr.use_cassette("drupal-sa-core-2018-001.yaml") as cassette:
            advisory_json = do_api.request_node(2946543, return_raw=True)
            parser.parse(advisory_json)
            self.assertEqual(
                str(parser.match_versions),
                "[['<', 'drupal', '8.4.5'], ['<', 'drupal', '7.57']]",
                "Expected versions on SA-CORE-2018-001"
            )
            self.assertTrue(cassette.all_played)

    def test_solution_parsing_contrib(self):
        """Test extraction of contrib release-matching data from solution field."""
        parser = DOAdvisoryParser()
        # SA-CONTRIB-2017-077 - relative project URLs
        with parser_vcr.use_cassette("drupal-sa-contrib-2017-077.yaml") as cassette:
            advisory_json = do_api.request_node(2915530, return_raw=True)
            parser.parse(advisory_json)
            self.assertEqual(
                str(parser.match_versions),
                "[['<', 'netforum_authentication', '7.x-1.1']]",
                "Expected versions on SA-CONTRIB-2017-077"
            )
            self.assertTrue(cassette.all_played)
        # Tear down: remove project for next cassette.
        Project.objects.all().delete()
        # SA-CONTRIB-2018-020 - match on 7.x-2.x
        with parser_vcr.use_cassette("drupal-sa-contrib-2018-020.yaml") as cassette:
            advisory_json = do_api.request_node(2966067, return_raw=True)
            parser.parse(advisory_json)
            self.assertEqual(
                str(parser.match_versions),
                "[['<', 'media', '7.x-2.19']]",
                "Expected versions on SA-CONTRIB-2018-020"
            )
            self.assertTrue(cassette.all_played)
        # SA-CONTRIB-2018-022 - raw NIDs
        with parser_vcr.use_cassette("drupal-sa-contrib-2018-022.yaml") as cassette:
            advisory_json = do_api.request_node(2966073, return_raw=True)
            parser.parse(advisory_json)
            self.assertEqual(
                str(parser.match_versions),
                "[['<', 'drd', '8.x-3.14'], ['<', 'drd_agent', '8.x-3.7'], ['<', 'drd_agent', '7.x-3.5']]",
                "Expected versions on SA-CONTRIB-2018-022"
            )
            self.assertTrue(cassette.all_played)

    def test_projects_affected(self):
        """
        Test advisory parser can correctly handle advisories with single and multiple projects affected.
        """
        parser = DOAdvisoryParser()
        # SA-CORE-2018-004 - single project.
        with parser_vcr.use_cassette("drupal-sa-core-2018-004.yaml") as cassette:
            advisory_json = do_api.request_node(2966049, return_raw=True)
            parser.parse(advisory_json)
            affected = parser.get_projects_affected()
            self.assertTrue('drupal' in affected)
            self.assertEqual(len(affected), 1)
            self.assertTrue(cassette.all_played)
        # Tear down: remove project for next cassette.
        Project.objects.all().delete()
        # SA-CONTRIB-2018-022 - two projects affected.
        with parser_vcr.use_cassette("drupal-sa-contrib-2018-022.yaml") as cassette:
            advisory_json = do_api.request_node(2966073, return_raw=True)
            parser.parse(advisory_json)
            affected = parser.get_projects_affected()
            self.assertTrue('drd' in affected)
            self.assertTrue('drd_agent' in affected)
            self.assertEqual(len(affected), 2)
            self.assertTrue(cassette.all_played)

    def test_cores_affected(self):
        """
        Match core versions against advisories.
        """
        parser = DOAdvisoryParser()
        # SA-CONTRIB-2018-022 - Module core-version version signature.
        versions = [['<', 'drd', '8.x-3.14'], ['<', 'drd_agent', '8.x-3.7'], ['<', 'drd_agent', '7.x-3.5']]
        result_set = parser.cores(versions)
        self.assertTrue('8.x' in result_set)
        self.assertTrue('7.x' in result_set)
        self.assertEqual(len(result_set), 2)

        # SA-CORE-2018-004 - core versions with x.y and x.y.z signatures.
        versions = [['<', 'drupal', '7.59'], ['<', 'drupal', '8.5.3'], ['<', 'drupal', '8.4.8']]
        result_set = parser.cores(versions)
        self.assertTrue('8.5.x' in result_set)
        self.assertTrue('8.4.x' in result_set)
        self.assertTrue('7.x' in result_set)
        self.assertEqual(len(result_set), 3)

        versions = [['<', 'drupal', 'somethingbad']]
        expected_msg = "Unexpected version signature: somethingbad"
        with self.assertRaisesMessage(AffectedVersionError, expected_msg):
            result_set = parser.cores(versions)

    def test_project_releases_affected(self):
        """
        Match releases against advisories.
        """
        parser = DOAdvisoryParser()
        # Create many releases to test what does and doesn't match.
        project_versions = {
            'drd': [
                '8.x-3.6', '8.x-3.13', '8.x-3.14', '8.x-3.15'
            ],
            'drd_agent': [
                '8.x-3.5', '8.x-3.6', '8.x-3.7', '8.x-3.8',
                '7.x-3.3', '7.x-3.4', '7.x-3.5', '7.x-3.6',
                '6.x-1.0', '9.x-1.0'
            ]
        }
        for project_slug in project_versions:
            project = Project.objects.get_or_create(slug=project_slug)[0]
            for version in project_versions[project_slug]:
                core = parser.get_core_from_version(version)
                Release.objects.get_or_create(project_id=project.id, version=version, core=core)

        # Uses specialised cassette as Projects information is pre-populated.
        with parser_vcr.use_cassette("drupal-sa-contrib-2018-022-match.yaml"):  # as cassette
            advisory_json = do_api.request_node(2966073, return_raw=True)
            parser.parse(advisory_json)
            projects_affected = parser.get_projects_affected()
            self.assertTrue('drd' in projects_affected)
            self.assertTrue('drd_agent' in projects_affected)
            releases_affected = parser.get_release_versions_affected()
            drd_releases = [r.version for r in releases_affected if r.project.slug == 'drd']
            # Matches: ['<', 'drd', '8.x-3.14']
            self.assertTrue('8.x-3.6' in drd_releases)
            self.assertTrue('8.x-3.13' in drd_releases)
            self.assertFalse('8.x-3.14' in drd_releases)
            self.assertFalse('8.x-3.15' in drd_releases)
            drd_agent_releases = [r.version for r in releases_affected if r.project.slug == 'drd_agent']
            # Matches: ['<', 'drd_agent', '8.x-3.7']
            self.assertTrue('8.x-3.5' in drd_agent_releases)
            self.assertTrue('8.x-3.6' in drd_agent_releases)
            self.assertFalse('8.x-3.7' in drd_agent_releases)
            self.assertFalse('8.x-3.8' in drd_agent_releases)
            # Matches: ['<', 'drd_agent', '7.x-3.5']
            self.assertTrue('7.x-3.3' in drd_agent_releases)
            self.assertTrue('7.x-3.4' in drd_agent_releases)
            self.assertFalse('7.x-3.5' in drd_agent_releases)
            self.assertFalse('7.x-3.6' in drd_agent_releases)
            # Not found: 6.x and 9.x core versions
            self.assertFalse('6.x-1.0' in drd_agent_releases)
            self.assertFalse('9.x-1.0' in drd_agent_releases)

            self.assertEqual(len(releases_affected), parser.advisory.releases.count())
