from django.apps import AppConfig


class DrupalConfig(AppConfig):
    name = "archimedes.drupal"

    def ready(self):
        import archimedes.drupal.signals  # noqa: F401
