from django.db.models import F
from django_filters import OrderingFilter
from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.filters import BaseFilterBackend, SearchFilter
from rest_framework.settings import api_settings
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_api_key.permissions import HasAPIKey

from . import serializers
from .. import models
from archimedes.sites.models import Site
from archimedes.drupal.calcstats import get_drupal_stats
import coreapi

# Filter Backends.


class AdvisoryFilterBackend(BaseFilterBackend):
    """
    Filter backend for Advisories, handling search and sort.
    """
    # api_settings.DEFAULT_FILTER_BACKENDS
    def filter_queryset(self, request, queryset, view):
        # Check for ordering field for special handling.
        order_params = request.GET.get('ordering')
        if order_params:
            fields = [param.strip() for param in order_params.split(',')]
            # Apply ordering rules
            queryset = AdvisoryOrderingFilter().filter(queryset, fields)

        return queryset


class AdvisoryOrderingFilter(OrderingFilter):
    """
    Custom advisory ordering filter.
    Orders the queryset by:
    - the number of the affected_sites property
    - risk score descending with nulls last
    - default ordering in other situations

    Note: This may break ordering on multiple fields.
    """
    def __init__(self, *args, **kwargs):
        super(AdvisoryOrderingFilter, self).__init__(*args, **kwargs)
        self.extra['choices'] += [
            ('affected_sites_count', 'Number of sites using project'),
            ('-affected_sites_count', 'Number of sites using project (descending)'),
        ]

    def filter(self, qs, ordering_list):
        # OrderingFilter is CSV-based, so `value` is a list
        if '-risk_score' in ordering_list:
            qs = qs.order_by(F('risk_score').desc(nulls_last=True))
        else:
            qs = super(AdvisoryOrderingFilter, self).filter(qs, ordering_list)

        return qs


class DrupalSiteAdminFilterBackend(BaseFilterBackend):
    """
    Filter backend for Sites with specific admin users.

    This is likely to be quite a slow query, as it involves pulling JSON blobs
    out of reports to make comparisons.

    admin_mail: filter by email address of AdminRole user
    admin_name: filter by username of AdminRole user
    """
    def filter_queryset(self, request, queryset, view):
        filterset = {}

        admin_mail = request.GET.get('admin_mail')
        if admin_mail:
            filterset['Mail'] = admin_mail

        admin_name = request.GET.get('admin_name')
        if admin_name:
            filterset['Name'] = admin_name

        if len(filterset) > 0:
            # Determine first which records have a given admin user, then use
            # their private keys as a filter to return a QuerySet object.
            filtered_pks = [ds for ds in queryset if ds.has_admin_user(filterset)]
            filtered_queryset = queryset.filter(pk__in=filtered_pks)
        else:
            # Pass through if there are no criteria to check against.
            filtered_queryset = queryset

        return filtered_queryset

    def get_schema_fields(self, view):
        """Register latest :Report fields for Open API use."""
        return [
            coreapi.Field(
                name='admin_mail',
                location='query',
                required=False,
                type='string',
                description='Filter sites by the email address of an AdminRole user (checking latest Report)'
            ),
            coreapi.Field(
                name='admin_name',
                location='query',
                required=False,
                type='string',
                description='Filter sites by the username of an AdminRole user (checking latest Report)'
            )
        ]


class ProjectSitesUsingFilterBackend(BaseFilterBackend):
    """
    Filter backend for Project sites_using property.

    sites_using__pk: filter queryset to Projects used by a particular Site.
    [-]sites_using__count: ordering queryset by the number of Sites using a particular Project.
    """
    def filter_queryset(self, request, queryset, view):
        # Reduce queryset if keyed against a particular Site pk.
        pk = request.GET.get('sites_using__pk')
        if pk:
            filter_site = Site.objects.get(pk=pk)
            filtered_queryset = [p for p in queryset if filter_site in p.sites_using]
        else:
            filtered_queryset = queryset

        # Check for sites_using__count ordering.
        # TODO: Get this reviewed so that multiple orderings are applied in order.
        order_params = request.GET.get('ordering')
        if order_params:
            fields = [param.strip() for param in order_params.split(',')]
            filtered_queryset = ProjectSitesCountOrderingFilter().filter(filtered_queryset, fields)

        return filtered_queryset


class DrupalSiteFilter(filters.FilterSet):

    decommissioned = filters.BooleanFilter(help_text="Sites marked as decommissioned")
    using_module = filters.CharFilter(
        field_name='modules__project__slug',
        label="Module",
        help_text="Filter sites by a specific module (machine name)",
    )
    using_theme = filters.CharFilter(
        field_name='themes__project__slug',
        label="Theme",
        help_text="Filter sites by a specific theme (machine name)",
    )
    drupal_version = filters.CharFilter(
        field_name='core_version__version',
        label="Drupal Core version",
        help_text="Filter sites by specific Drupal Core version number",
    )
    major_version = filters.CharFilter(
        field_name='core_version__version',
        label="Drupal Core major version",
        lookup_expr='startswith',
        help_text="Filter sites by specific Drupal Core major version number (digits only)",
    )

    class Meta:
        model = models.DrupalSite
        fields = ('decommissioned', 'hostname',)


class DrupalSiteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal sites to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    queryset = models.DrupalSite.objects.select_related('environment',
                                                        'core_version').prefetch_related('keys', 'modules', 'themes')
    serializer_class = serializers.DrupalSiteSerializer
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS + [DrupalSiteAdminFilterBackend]
    filterset_class = DrupalSiteFilter


class ProjectSitesCountOrderingFilter(OrderingFilter):
    """
    Filter that orders the queryset by the number of sites in the sites_using property.
    """
    def __init__(self, *args, **kwargs):
        super(ProjectSitesCountOrderingFilter, self).__init__(*args, **kwargs)
        self.extra['choices'] += [
            ('sites_using__count', 'Number of sites using project'),
            ('-sites_using__count', 'Number of sites using project (descending)'),
        ]

    def filter(self, qs, value):
        # OrderingFilter is CSV-based, so `value` is a list
        if 'sites_using__count' in value:
            qs = sorted(qs, key=lambda q: q.sites_using.count())
            return qs
        elif '-sites_using__count' in value:
            qs = sorted(qs, key=lambda q: -q.sites_using.count())
            return qs
        else:
            return super(ProjectSitesCountOrderingFilter, self).filter(qs, value)


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal projects to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    queryset = models.Project.objects.all()
    serializer_class = serializers.ProjectSerializer
    search_fields = ('name',)
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS + [ProjectSitesUsingFilterBackend]
    filter_fields = ('type',)
    lookup_field = 'slug'


class ReleaseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal project releases to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    queryset = models.Release.objects.select_related('project')
    serializer_class = serializers.ReleaseSerializer
    filter_fields = ('project__slug',)


class AdvisorySourceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal advisory sources to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    queryset = models.AdvisorySource.objects.all()
    serializer_class = serializers.AdvisorySourceSerializer


class AdvisoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal advisories to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    queryset = models.Advisory.objects.select_related('project')
    search_fields = ('title', 'advisory_id', 'project__slug', )
    filterset_fields = ('source__slug', 'project__slug',)
    filter_backends = [filters.DjangoFilterBackend, SearchFilter, AdvisoryFilterBackend]

    def get_serializer_class(self):
        if self.action in ['retrieve']:
            return serializers.AdvisoryDetailSerializer
        else:
            return serializers.AdvisorySerializer


class DrupalStatsViewSet(viewsets.ViewSet):
    """
    API endpoint to allow easy visualization of Drupal statistics.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]

    def list(self, request):
        return Response(get_drupal_stats())
