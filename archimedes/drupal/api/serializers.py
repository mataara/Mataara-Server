from archimedes.sites.api.serializers import (SiteIndexSerializer,
                                              SiteSerializer)
from rest_framework import serializers

from .. import models


class ReleaseSerializer(serializers.HyperlinkedModelSerializer):
    release_types = serializers.ListField(source='release_types.all', child=serializers.CharField())
    project = serializers.HyperlinkedRelatedField(
        view_name='project-detail',
        lookup_field='slug',
        read_only=True
    )

    class Meta:
        model = models.Release
        fields = (
            'api_url',
            'project',
            'slug',
            'date',
            'date_updated',
            'version',
            'release_types'
        )


class ReleaseIndexSerializer(ReleaseSerializer):
    """
    A cut-down version of the ReleaseSerializer, for stubbing in other models.

    This provides just enough information to fetch further data, without running
    into cyclic loops.
    """
    class Meta:
        model = models.Release
        fields = (
            'api_url',
            'project',
            'slug',
            'date',
        )


class ProjectSerializer(serializers.Serializer):
    api_url = serializers.HyperlinkedIdentityField(view_name='project-detail', lookup_field='slug')
    slug = serializers.SlugField(read_only=True)
    pk = serializers.IntegerField(read_only=True)  # May eventually not need this.
    name = serializers.CharField(read_only=True)
    url = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    custom = serializers.BooleanField(read_only=True)
    type = serializers.CharField(read_only=True)
    type_name = serializers.CharField(read_only=True)
    date_updated = serializers.DateTimeField(read_only=True)
    maintenance_status = serializers.CharField(read_only=True)
    development_status = serializers.CharField(read_only=True)
    sites_using = SiteIndexSerializer(many=True, read_only=True)
    sites_vulnerable = SiteIndexSerializer(many=True, read_only=True)

    class Meta:
        model = models.Project
        fields = (
            'api_url',
            'description',
            'custom',
            'pk',
            'name',
            'slug',
            'date_updated',
            'sites_using',
            'type',
            'type_name',
            'maintenance_status',
            'development_status',
            'sites_using',
            'sites_vulnerable',
            'url',
            'date_last_sync',
        )


class ProjectIndexSerializer(ProjectSerializer):
    """
    A cut-down version of the ProjectSerializer, for stubbing in other models.

    This provides just enough information to fetch further data, without running
    into cyclic loops.
    """
    api_url = serializers.HyperlinkedIdentityField(view_name='project-detail', lookup_field='slug')

    class Meta:
        model = models.Project
        fields = (
            'api_url',
            'name',
            'slug',
            'date',
        )


class AdvisorySourceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.AdvisorySource
        fields = (
            'title',
            'slug',
            'url'
        )


class AdvisoryIndexSerializer(serializers.HyperlinkedModelSerializer):
    """Minimal serializer for referencing."""
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.Advisory
        fields = (
            'api_url',
            'pk',
            'title',
        )


class AdvisorySerializer(serializers.HyperlinkedModelSerializer):
    """Advisory data without affected site objects."""
    source = serializers.StringRelatedField()
    pk = serializers.IntegerField(read_only=True)
    status_display = serializers.CharField(source='get_status_display')
    # Expose affected sites property for site counts.
    affected_sites_count = serializers.IntegerField(read_only=True)
    cores = serializers.ListField(source='cores.all', child=serializers.CharField())
    vulnerabilities = serializers.ListField(source='vulnerabilities.all', child=serializers.CharField())
    cves = serializers.ListField(source='cves.all', child=serializers.CharField())
    risk = serializers.CharField()
    project = serializers.HyperlinkedRelatedField(
        view_name='project-detail',
        lookup_field='slug',
        read_only=True
    )

    class Meta:
        model = models.Advisory
        fields = (
            'api_url',
            'pk',
            'guid',
            'title',
            'url',
            'date_posted',
            'date_updated',
            'date_parsed',
            'advisory_id',
            'risk',
            'risk_score',
            'risk_score_total',
            'source',
            'status',
            'status_display',
            'affected_sites_count',
            'project',
            'cores',
            'vulnerabilities',
            'cves',
        )


class AdvisoryDetailSerializer(AdvisorySerializer):
    """Advisory data including affected sites / releases."""
    affected_sites = SiteIndexSerializer(many=True, read_only=True)
    releases = ReleaseIndexSerializer(many=True, read_only=True)

    class Meta:
        model = models.Advisory
        fields = (
            'api_url',
            'pk',
            'guid',
            'title',
            'url',
            'date_posted',
            'date_updated',
            'date_parsed',
            'advisory_id',
            'risk',
            'risk_score',
            'risk_score_total',
            'source',
            'status',
            'status_display',
            'affected_sites',
            'affected_sites_count',
            'project',
            'cores',
            'releases',
            'vulnerabilities',
            'cves',
        )


class DrupalSiteSerializer(SiteSerializer):
    core_version = serializers.SlugRelatedField(
        read_only=True,
        slug_field='version',
    )
    modules = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='slug'
    )
    themes = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='slug'
    )

    core_vulnerabilities = AdvisoryIndexSerializer(many=True, read_only=True)
    # Source overrides for release-based marking of insecure components.
    vulnerability_count = serializers.IntegerField(source='vulnerability_count_by_release', default=None)
    vulnerable_core = ReleaseIndexSerializer(many=True, read_only=True, source='vulnerable_core_by_release')
    vulnerable_modules = ReleaseIndexSerializer(many=True, read_only=True, source='vulnerable_modules_by_release')
    vulnerable_themes = ReleaseIndexSerializer(many=True, read_only=True, source='vulnerable_themes_by_release')

    class Meta:
        model = models.DrupalSite
        fields = SiteSerializer.Meta.fields + (
            'core',
            'core_version',
            'slogan',
            'nodes',
            'revisions',
            'users',
            'modules',
            'themes',
            'vulnerability_count',
            'core_vulnerabilities',
            'vulnerable_core',
            'vulnerable_modules',
            'vulnerable_themes',
        )
