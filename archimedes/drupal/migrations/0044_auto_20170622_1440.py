# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0043_auto_20170327_1758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drupalsite',
            name='modules',
            field=models.ManyToManyField(to='drupal.Release', related_query_name='module_site', related_name='site_modules'),
        ),
        migrations.AlterField(
            model_name='drupalsite',
            name='themes',
            field=models.ManyToManyField(to='drupal.Release', related_query_name='theme_site', related_name='site_themes'),
        ),
        migrations.AlterField(
            model_name='release',
            name='project',
            field=models.ForeignKey(related_query_name='release', related_name='releases', to='drupal.Project', on_delete=models.CASCADE),
        ),
    ]
