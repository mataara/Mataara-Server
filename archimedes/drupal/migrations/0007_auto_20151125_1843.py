# -*- coding: utf-8 -*-


import datetime

from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0006_release_version'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='date_updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 25, 5, 42, 48, 175427, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='release',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 25, 5, 42, 54, 839860, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='release',
            name='date_updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 25, 5, 43, 2, 999421, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='release',
            name='url',
            field=models.URLField(default=''),
            preserve_default=False,
        ),
    ]
