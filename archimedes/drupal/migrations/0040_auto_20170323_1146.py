# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0039_auto_20170323_1120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisorysource',
            name='slug',
            field=models.SlugField(editable=False, unique=True),
        ),
    ]
