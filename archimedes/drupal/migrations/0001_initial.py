# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Advisory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('guid', models.CharField(unique=True, max_length=200)),
                ('title', models.CharField(max_length=200)),
                ('url', models.URLField()),
                ('date_posted', models.DateTimeField()),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('date_parsed', models.DateTimeField(null=True)),
                ('html', models.TextField()),
            ],
            options={
                'ordering': ['-date_posted'],
                'verbose_name_plural': 'Advisories',
            },
        ),
        migrations.CreateModel(
            name='AdvisorySource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('slug', models.CharField(unique=True, max_length=200)),
                ('url', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='DrupalSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='advisory',
            name='source',
            field=models.ForeignKey(default=None, to='drupal.AdvisorySource', on_delete=models.CASCADE),
        ),
    ]
