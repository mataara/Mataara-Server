# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0033_auto_20160316_1304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='advisory',
            name='cves',
        ),
        migrations.RemoveField(
            model_name='advisory',
            name='vulnerabilities',
        ),
    ]
