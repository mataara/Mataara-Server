# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0008_auto_20190222_0843'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='date_last_sync',
            field=models.DateTimeField(null=True, verbose_name='Last sync timestamp', blank=True, help_text='Timestamp of last Releases sync with drupal.org'),
        ),
        migrations.AlterField(
            model_name='project',
            name='drupal_org_nid',
            field=models.PositiveIntegerField(null=True, verbose_name='Drupal.org node ID', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='slug',
            field=models.CharField(unique=True, verbose_name='Machine name (slug)', help_text='Machine readable version of project name', max_length=200),
        ),
        migrations.AlterField(
            model_name='project',
            name='url',
            field=models.URLField(blank=True, verbose_name='URL'),
        ),
    ]
