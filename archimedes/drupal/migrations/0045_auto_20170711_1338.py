# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0044_auto_20170622_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='reviewer',
            field=models.ForeignKey(null=True, related_name='django_advisory', to=settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE),
        ),
    ]
