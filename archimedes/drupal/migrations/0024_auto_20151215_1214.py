# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0023_auto_20151215_1153'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='risk_score',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='advisory',
            name='risk_score_total',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
        ),
    ]
