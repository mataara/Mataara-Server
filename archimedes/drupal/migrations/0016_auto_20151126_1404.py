# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0015_auto_20151126_1400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='core',
            field=models.CharField(max_length=50, blank=True),
        ),
    ]
