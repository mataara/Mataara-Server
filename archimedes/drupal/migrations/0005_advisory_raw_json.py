# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0004_auto_20181126_1116'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='raw_json',
            field=models.TextField(help_text='Raw unprocessed advisory data.', null=True, blank=True),
        ),
    ]
