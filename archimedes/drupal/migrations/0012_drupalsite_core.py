# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0011_remove_drupalsite_core'),
    ]

    operations = [
        migrations.AddField(
            model_name='drupalsite',
            name='core',
            field=models.ForeignKey(blank=True, to='drupal.Core', null=True, on_delete=models.CASCADE),
        ),
    ]
