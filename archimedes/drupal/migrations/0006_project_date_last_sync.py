# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0005_advisory_raw_json'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='date_last_sync',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
