# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0003_auto_20180615_1432'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReleaseType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('label', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='release',
            name='release_types',
            field=models.ManyToManyField(blank=True, related_name='releases', to='drupal.ReleaseType'),
        ),
    ]
