# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0009_auto_20151126_1221'),
    ]

    operations = [
        migrations.CreateModel(
            name='Core',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version', models.CharField(max_length=50)),
            ],
        ),
        migrations.AlterField(
            model_name='drupalsite',
            name='core',
            field=models.CharField(max_length=50, blank=True),
        ),
    ]
