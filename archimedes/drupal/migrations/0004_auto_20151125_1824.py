# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0003_auto_20151125_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='version_extra',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_patch',
            field=models.PositiveIntegerField(blank=True),
        ),
    ]
