# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2019-05-16 02:48
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0009_auto_20190228_0932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='reviewer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='drupal_advisory', to=settings.AUTH_USER_MODEL),
        ),
    ]
