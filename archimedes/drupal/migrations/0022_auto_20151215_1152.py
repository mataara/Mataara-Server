# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0021_auto_20151214_1634'),
    ]

    operations = [
        migrations.CreateModel(
            name='CVE',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Risk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Vulnerability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='advisory',
            name='cores',
            field=models.ManyToManyField(to='drupal.Core', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='advisory',
            name='project',
            field=models.ForeignKey(blank=True, to='drupal.Project', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='advisory',
            name='releases',
            field=models.ManyToManyField(to='drupal.Release', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='advisory',
            name='cves',
            field=models.ManyToManyField(to='drupal.CVE', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='advisory',
            name='risk',
            field=models.ForeignKey(blank=True, to='drupal.Risk', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='advisory',
            name='vulnerabilities',
            field=models.ManyToManyField(to='drupal.Vulnerability', null=True, blank=True),
        ),
    ]
