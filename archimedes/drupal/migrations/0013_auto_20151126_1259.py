# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0012_drupalsite_core'),
    ]

    operations = [
        migrations.RenameField(
            model_name='drupalsite',
            old_name='core',
            new_name='core_version',
        ),
    ]
