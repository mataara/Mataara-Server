# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0007_remove_release_duplicates'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='release',
            unique_together=set([('project', 'version')]),
        ),
    ]
