# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0002_auto_20151125_1637'),
    ]

    operations = [
        migrations.RenameField(
            model_name='release',
            old_name='version_tag',
            new_name='version_extra',
        ),
    ]
