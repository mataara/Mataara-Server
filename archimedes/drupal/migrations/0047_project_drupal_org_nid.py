# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0046_auto_20170713_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='drupal_org_nid',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
