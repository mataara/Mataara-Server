# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0037_auto_20170214_1117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisorysource',
            name='title',
            field=models.CharField(unique=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='advisorysource',
            name='url',
            field=models.URLField(unique=True),
        ),
    ]
