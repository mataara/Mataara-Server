# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0008_auto_20151126_1036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drupalsite',
            name='nodes',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='drupalsite',
            name='revisions',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='drupalsite',
            name='users',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]
