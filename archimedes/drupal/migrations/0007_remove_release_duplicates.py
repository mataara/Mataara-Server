# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from collections import Counter


def dedupe_releases(apps, schema_editor):
    """
    Remove all Release duplicates prior to setting unique constraint.

    This will favour the latest updated release object for any given
    project:version pair.
    """
    db_alias = schema_editor.connection.alias
    Release = apps.get_model('drupal', 'Release')
    DrupalSite = apps.get_model('drupal', 'DrupalSite')

    c = Counter([(r.project, r.version) for r in Release.objects.using(db_alias).all()])
    pvs = [(p, v) for ((p, v), n) in c.items() if n > 1]
    for (p, v) in pvs:
        dupes = Release.objects.using(db_alias).filter(project=p, version=v)
        target = dupes.latest('date_updated')
        # print("Deduping {p}:{v} objects, pk={pks} -> {target}".format(
        #     p=p,
        #     v=v,
        #     pks=[r.pk for r in dupes],
        #     target=target.pk
        # ))
        for r in dupes:
            if r != target:
                if p.type == 'project_theme':
                    sites = DrupalSite.objects.using(db_alias).filter(themes=r)
                    for s in sites:
                        # print("Fix: {} {}".format(s.title, s.pk))
                        s.themes.remove(r)
                        s.themes.add(target)
                        s.save()
                else:
                    # Assume it's a module, or being treated like one.
                    sites = DrupalSite.objects.using(db_alias).filter(modules=r)
                    for s in sites:
                        # print("Fix: {} {}".format(s, s.pk))
                        s.modules.remove(r)
                        s.modules.add(target)
                        s.save()
                r.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0006_project_date_last_sync'),
    ]

    operations = [
        migrations.RunPython(dedupe_releases)
    ]
