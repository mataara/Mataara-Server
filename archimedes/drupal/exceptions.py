class DrupalException(Exception):
    """An exception occured in the Drupal app"""
    pass


class DrupalProjectDownloadError(DrupalException):
    """Unable to download project from Drupal.org"""
    pass


class DrupalProjectNotFoundError(DrupalException):
    """Project was not found on Drupal.org"""
    pass


class AffectedVersionError(DrupalException):
    """The affected version could not be parsed"""
    pass


class AffectedReleasesError(DrupalException):
    """The affected release could not be parsed"""
    pass


class AffectedReleasesRuleError(DrupalException):
    """The affected release rule could not be created"""
    pass


class AdvisoryParserError(DrupalException):
    """The Drupal advisory parser has failed in an unexpected fashion."""
    pass
