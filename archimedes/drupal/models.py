from archimedes.sites.models import Site, SiteManager
from archimedes.reports.models import Report
from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from unidecode import unidecode


class Advisory(models.Model):

    STATUS = (
        ('downloaded', 'Downloaded'),
        ('parsed', 'Needs Review'),
        ('reviewed', 'Reviewed'),
        ('ignored', 'Ignored'),
        ('failed', 'Parsing Failed'),
    )

    guid = models.CharField(max_length=200, unique=True)
    title = models.CharField(max_length=200)
    url = models.URLField()
    source = models.ForeignKey('AdvisorySource', default=None, on_delete=models.CASCADE)

    date_posted = models.DateTimeField()
    date_updated = models.DateTimeField(auto_now=True)
    date_parsed = models.DateTimeField(null=True, blank=True)

    ignored = models.BooleanField(default=False)
    failed = models.BooleanField(default=False)
    reviewer = models.ForeignKey(User, null=True, blank=True, related_name='drupal_advisory',
                                 on_delete=models.SET_NULL)
    status = models.CharField(max_length=50, choices=STATUS, default='downloaded')

    html = models.TextField()
    # New in sa type.
    solution = models.TextField(null=True, blank=True)
    # JSON fragment as passed in from API.
    raw_json = models.TextField(null=True, blank=True, help_text="Raw unprocessed advisory data.")

    advisory_id = models.CharField(max_length=200, blank=True)

    cores = models.ManyToManyField('Core', blank=True)
    project = models.ForeignKey('Project', blank=True, null=True, related_name='advisories', on_delete=models.CASCADE)
    releases = models.ManyToManyField('Release', blank=True, related_name='advisories')

    risk = models.ForeignKey('Risk', blank=True, null=True, on_delete=models.CASCADE)
    risk_score = models.PositiveSmallIntegerField(blank=True, null=True)
    risk_score_total = models.PositiveSmallIntegerField(blank=True, null=True)

    vulnerabilities = models.ManyToManyField('Vulnerability', blank=True)
    cves = models.ManyToManyField('CVE', blank=True)

    affected_sites_count = models.PositiveSmallIntegerField('# Affected Sites', blank=True, null=True, editable=False)
    affected_sites_count.help_text = "A derived field for storing affected site counts."

    class Meta:
        ordering = ['-date_posted']
        verbose_name_plural = 'Advisories'

        indexes = [
            models.Index(fields=['affected_sites_count'])
        ]

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        """Set the status before saving"""
        if self.ignored:
            self.status = 'ignored'
        elif self.failed:
            self.status = 'failed'
        elif not self.date_parsed:
            self.status = 'downloaded'
        elif self.reviewer:
            self.status = 'reviewed'
        else:
            self.status = 'parsed'

        # Ensure affected_sites_count up to date.
        self.affected_sites_count = self.affected_sites.count()

        super(Advisory, self).save(*args, **kwargs)

    @property
    def affected_sites(self):
        """
        Sites with releases potentially affected by this advisory.

        Since advisories are now informational rather than a truth source, this
        isn't being calculated directly from the :Advisory anymore. Instead, it
        uses the :Project associated with the :Advisory, and looks for any
        :DrupalSite using a :Release of it that is marked as insecure.
        """
        if (self.project and self.project.slug == 'drupal'):
            # release_versions = [v.version for v in self.releases.all()]
            # return DrupalSite.objects.filter(core_version__version__in=release_versions)
            sites_using_project = DrupalSite.objects.all()
            ds_pks = [ds.pk for ds in sites_using_project if ds.is_core_insecure]
            return DrupalSite.objects.filter(pk__in=ds_pks)
        elif self.project:
            # return DrupalSite.objects.filter(modules__advisories__pk=self.pk)
            sites_using_project = DrupalSite.objects.filter(modules__project=self.project)
            if sites_using_project.count() == 0:
                return DrupalSite.objects.none()
            affected_sites_pks = [
                ds.pk for ds in sites_using_project if ds.modules.get(project=self.project).is_marked_insecure
            ]
            return DrupalSite.objects.filter(pk__in=affected_sites_pks)
        else:
            return DrupalSite.objects.none()


class AdvisorySource(models.Model):
    title = models.CharField(max_length=200, unique=True,
                             help_text='Enter the name for this advisory source.')
    slug = models.SlugField(unique=True, editable=False)
    # URL validation checks enforced through admin web interface
    url = models.URLField(unique=True,
                          help_text='Enter URL for the advisory source. Only http(s):// or ftp(s):// are supported.')

    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(unidecode(self.title))

        return super(AdvisorySource, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Risk(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Vulnerability(models.Model):
    # Vulnerability list item on Druapl.org/security advisory source
    # acts like a type, although there does not seem to be a restricted set of choices
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class CVE(models.Model):
    identifier = models.CharField(max_length=50)

    def __str__(self):
        return self.identifier


class Project(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(
        max_length=200,
        unique=True,
        help_text="Machine readable version of project name",
        verbose_name="Machine name (slug)"
    )
    url = models.URLField(blank=True, verbose_name="URL")
    description = models.TextField(blank=True)
    date_updated = models.DateTimeField(auto_now=True)
    custom = models.BooleanField(default=False)
    type = models.CharField(max_length=50)
    maintenance_status = models.CharField(max_length=200, blank=True)
    development_status = models.CharField(max_length=200, blank=True)
    drupal_org_nid = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name="Drupal.org node ID"
    )
    date_last_sync = models.DateTimeField(
        blank=True, null=True,
        help_text='Timestamp of last Releases sync with drupal.org',
        verbose_name='Last sync timestamp'
    )

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.slug

    @property
    def type_name(self):
        if self.type == 'project_module':
            return 'Module'
        elif self.type == 'project_theme':
            return 'Theme'
        else:
            return 'Project'

    @property
    def sites_using(self):
        """
        All sites that use a release of this project.
        """
        if self.slug == 'drupal':
            # Drupal core
            return DrupalSite.objects.distinct()
        else:
            # Modules and themes
            if self.type_name == 'Theme':
                return DrupalSite.objects.filter(themes__project__pk=self.pk).distinct()
            else:
                # Assume it's being used as a module
                return DrupalSite.objects.filter(modules__project__pk=self.pk).distinct()

    @property
    def sites_vulnerable(self):
        """
        All sites that use a release of this project that is marked insecure.
        """
        if self.slug == 'drupal':
            # Check core version against vulnerable cores list
            return [d for d in self.sites_using if d.is_core_insecure]

        else:
            if self.type_name == 'Theme':
                return [ds for ds in self.sites_using if ds.themes.get(project__pk=self.pk).is_marked_insecure]
            else:
                return [ds for ds in self.sites_using if ds.modules.get(project__pk=self.pk).is_marked_insecure]


class ReleaseManager(models.Manager):
    def get_queryset(self):
        return super(ReleaseManager, self).get_queryset().select_related('project')


class Release(models.Model):
    """A release of a project, such as a theme or model"""
    objects = ReleaseManager()

    project = models.ForeignKey('Project', related_name='releases', related_query_name='release',
                                on_delete=models.CASCADE)
    url = models.URLField()
    date = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    core = models.CharField(max_length=50, blank=True, default='')
    version = models.CharField(max_length=50)
    version_major = models.CharField(max_length=50, blank=True, default='')
    version_patch = models.CharField(max_length=50, blank=True, default='')
    version_extra = models.CharField(max_length=50, blank=True, default='')
    release_types = models.ManyToManyField('ReleaseType', blank=True, related_name='releases')

    class Meta:
        ordering = ['-version', '-date']
        unique_together = (('project', 'version'),)
        base_manager_name = 'objects'

    def save(self, *args, **kwargs):
        super(Release, self).save(*args, **kwargs)

        # Call to update affected sites counts an any Advisory that might be affected by Release changes.
        # TODO: Only check this for new Release objects or when release_types changes.
        advisories_affected = Advisory.objects.filter(project=self.project)
        for advisory in advisories_affected:
            true_count = advisory.affected_sites.count()
            if true_count != advisory.affected_sites_count:
                advisory.affected_sites_count = advisory.affected_sites.count()
                advisory.save()

    @property
    def has_bug_fixes(self):
        return self.release_types.filter(label='Bug fixes').exists()

    @property
    def has_new_features(self):
        return self.release_types.filter(label='New features').exists()

    @property
    def is_marked_insecure(self):
        return self.release_types.filter(label='Insecure').exists()

    @property
    def is_security_release(self):
        return self.release_types.filter(label='Security update').exists()

    @property
    def slug(self):
        return self.project.slug + ':' + self.version

    @property
    def core_number(self):
        try:
            val = int(self.core.split('.')[0])
        except ValueError:
            return None
        return val

    def __str__(self):
        return self.slug


class ReleaseType(models.Model):
    label = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.label


class Core(models.Model):
    version = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.version


class DrupalSite(Site):
    label = 'Drupal'
    objects = SiteManager()
    core = models.PositiveIntegerField(blank=True, null=True)
    core_version = models.ForeignKey('Core', blank=True, null=True, on_delete=models.CASCADE)
    slogan = models.CharField(max_length=200, blank=True)
    nodes = models.PositiveIntegerField(blank=True, null=True)
    revisions = models.PositiveIntegerField(blank=True, null=True)
    users = models.PositiveIntegerField(blank=True, null=True)

    modules = models.ManyToManyField(Release, related_name='site_modules', related_query_name='module_site')
    themes = models.ManyToManyField(Release, related_name='site_themes', related_query_name='theme_site')

    def set_core(self, core):
        """set the core field and create and add the :Core foreign key"""
        try:
            self.core = int(core.split('.')[0])
        except ValueError:
            self.core = None
        self.core_version, created = Core.objects.get_or_create(version=core.lower())

    def compare_json_against_latest_report(self, report_json, field_paths=None):
        """
        Check whether report has unexpected values based on last report from this site.
        Return a dict of changed fields and their old values. If no field_paths argument
        is specified, a default_fields set may be used.
        """
        default_field_paths = ['ReportType', 'SiteKey', ['Environment', 'Name'], ['Environment', 'Label']]
        if field_paths is None:
            field_paths = default_field_paths

        latest = self.reports.latest('date_received')
        return latest.compare_json(report_json, field_paths=field_paths)

    def has_admin_user(self, search_fields={}):
        """
        Tests whether Site has a particular AdminRole user.

        If there are multiple fields to search on, it will match on ANY field,
        not ALL fields.

        This query checks against the latest Report.

        search_fields: a dict of search fields and values.
        """
        latest_report = self.get_latest_report()
        if latest_report is None or type(latest_report) is not Report:
            return False

        # Fetch dictionary from JSON.
        latest_report = latest_report.dict

        if ('AdminRole' in latest_report):
            # Coerce single dict values into lists
            if type(latest_report['AdminRole']) is dict:
                latest_report['AdminRole'] = [latest_report['AdminRole']]

            for admin_role in latest_report['AdminRole']:
                # Sense check.
                if 'RoleUsers' not in admin_role:
                    continue

                # Coerce single dict values into lists
                if type(admin_role['RoleUsers']) is dict:
                    admin_role['RoleUsers'] = [admin_role['RoleUsers']]

                for role_users in admin_role['RoleUsers']:
                    for uid, userinfo in role_users.items():
                        # Check user against search fields.
                        for field, target in search_fields.items():
                            if field not in userinfo:
                                continue
                            else:
                                if userinfo[field] == target:
                                    return True

        return False

    @property
    def is_core_insecure(self):
        """Is Drupal site running on a core release marked insecure?"""
        try:
            version = self.core_version.version
        except AttributeError:
            version = None
        return Release.objects.filter(
            project__slug='drupal',
            version=version,
            release_types__label__contains='Insecure'
        ).exists()

    @property
    def vulnerability_count(self):
        """Return total count of vulnerable components across core, modules and themes."""
        count = (1 if self.is_core_insecure else 0)
        count += len(self.vulnerable_modules)
        count += len(self.vulnerable_themes)
        return count

    @property
    def core_vulnerabilities(self):
        """
        Returns any vulnerabilities for this site's core version.

        Deprecated: no longer using Advisories as a primary truth source.
        """
        return Advisory.objects.filter(releases__version=self.core_version.version).distinct()

    @property
    def vulnerable_modules(self):
        """All releases in self.modules that are marked insecure"""
        return Release.objects.filter(module_site=self, release_types__label__contains='Insecure').distinct()

    @property
    def vulnerable_themes(self):
        """All releases in self.themes that are marked insecure"""
        return Release.objects.filter(theme_site=self, release_types__label__contains='Insecure').distinct()

    @property
    def vulnerability_count_by_release(self):
        """Return total count of vulnerable releases across core, modules and themes."""
        count = len(self.vulnerable_core_by_release)
        count += len(self.vulnerable_modules_by_release)
        count += len(self.vulnerable_themes_by_release)
        return count

    @property
    def vulnerable_core_by_release(self):
        """Returns whether the core is on a vulnerable release."""
        core_release = Release.objects.filter(project__slug='drupal', version=self.core_version.version)
        return [c for c in core_release if c.is_marked_insecure]

    @property
    def vulnerable_modules_by_release(self):
        """All releases in self.modules that are marked insecure."""
        return Release.objects.filter(module_site=self, release_types__label='Insecure')

    @property
    def vulnerable_themes_by_release(self):
        """All releases in self.themes that are marked insecure."""
        return Release.objects.filter(theme_site=self, release_types__label='Insecure')
