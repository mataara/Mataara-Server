from django.test import TestCase

from archimedes.drupal.models import DrupalSite

from ..api.serializers import SiteSerializer
from ..models import Site


class ApiSiteSerializerTestCase(TestCase):

    def test_get_string(self):
        """Given a SiteSerializer, if the serializer.get_string function is\
        called with a Site object, return a string"""
        serializer = SiteSerializer()
        site = Site()
        self.assertEqual(type(serializer.get_string(site)), str)

    def test_get_type(self):
        """Given a SiteSerializer, if the serializer.get_type function is\
        called with a Site object, return the site.label"""
        serializer = SiteSerializer()
        site = Site()
        self.assertEqual(serializer.get_type(site), site.label)

    def test_get_core_drupalSite(self):
        """Given a SiteSerializer, if the serializer.get_core function is\
        called with a DrupalSite object, return the drupalSite.version"""
        # serializer = SiteSerializer()
        site = DrupalSite()
        site.set_core('7.x')
        self.assertEqual('7.x', site.core_version.version)

    def test_get_core_not_drupalSite(self):
        """Given a SiteSerializer, if the serializer.get_core function is\
        called with a DrupalSite object, return the drupalSite.version"""
        serializer = SiteSerializer()
        site = Site()
        self.assertEqual(serializer.get_core(site), 'not a drupal site')
