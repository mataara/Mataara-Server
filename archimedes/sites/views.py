from django.views.generic import DetailView, TemplateView

from archimedes.drupal.models import DrupalSite

from .models import Site

# The views that were here will need to be replaced by
# API end points for React.  They are just left here as an example
# of the information that could be needed.


class SiteListView(TemplateView):
    template_name = "sites/site_list.html"


class SiteDetailView(DetailView):

    template_name = "sites/site_detail.html"
    model = Site
    context_object_name = 'site'

    def get_context_data(self, **kwargs):
        """Add the vulnerable_modules:Release and the\
        vulnerable_Themes:Release to the context from :DrupalSite """
        context = super(SiteDetailView, self).get_context_data(**kwargs)
        if isinstance(self.object, DrupalSite):
            context['vulnerable_modules'] = self.object.vulnerable_modules.all()
            context['vulnerable_themes'] = self.object.vulnerable_themes.all()
            if self.object.core_version:
                context['vulnerable_core'] = self.object.core_vulnerabilities.all()
        return context
