from archimedes.drupal.models import DrupalSite
from rest_framework import viewsets
from rest_framework.filters import BaseFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings
from rest_framework_api_key.permissions import HasAPIKey

from .. import models
from . import serializers


class SiteTypeFilterBackend(BaseFilterBackend):
    """
    Filter that limits the queryset to a given polymorphic model instance.
    """
    def filter_queryset(self, request, queryset, view):
        if request.GET.get('type') == 'drupal':
            return queryset.instance_of(DrupalSite)
        else:
            return queryset


class SiteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sites to be viewed or edited.
    """
    permission_classes = [HasAPIKey | IsAuthenticated]
    queryset = models.Site.objects.select_related('environment').prefetch_related('keys')
    serializer_class = serializers.SiteSerializer
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS + [SiteTypeFilterBackend]
    filter_fields = ('name', 'hostname', 'title', 'decommissioned')
    search_fields = ('name', 'hostname', 'title', 'decommissioned')


class SiteGroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows site groups to be viewed or edited.

    retrieve:
    Retrieve site group details.

    list:
    Return a list of site groups.

    create:
    Add a site group.
    Note that specific keys or sites must be added via the Django admin
    interface.

    update:
    Update a site group.

    partial_update:
    Update one or more fields on a site group.

    destroy:
    Remove a site group from the system.
    """
    permission_classes = [HasAPIKey | IsAuthenticated]
    queryset = models.SiteGroup.objects.prefetch_related('keys')
    serializer_class = serializers.SiteGroupSerializer
    search_fields = ('name', 'description', 'notes',)

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        return {
            'request': self.request   # request object is passed here
        }
