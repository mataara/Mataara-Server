from rest_framework import serializers
from archimedes.subscription.models import Subscription

from .. import models


class SiteSerializer(serializers.HyperlinkedModelSerializer):
    environment = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )
    keys = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='value'
    )
    string = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    core = serializers.SerializerMethodField()
    pk = serializers.IntegerField(read_only=True)
    subscribed = serializers.SerializerMethodField()
    report__adminrole = serializers.SerializerMethodField()

    class Meta:
        model = models.Site
        fields = (
            'api_url',
            'pk',
            'string',
            'name',
            'hostname',
            'url',
            'root',
            'title',
            'environment',
            'keys',
            'type',
            'core',
            'date_last_received',
            'vulnerability_count',
            'decommissioned',
            'subscribed',
            'report__adminrole',
        )

    def get_string(self, obj):
        return '%s' % obj

    def get_type(self, obj):
        return obj.label

    def get_core(self, obj):
        if obj.label == 'Drupal':
            try:
                return obj.site_ptr.drupalsite.core_version.version
            except AttributeError:
                # May happen if core_version is not set
                return 'unknown'
        else:
            return 'not a drupal site'

    def get_subscribed(self, obj):
        request = self.context['request']
        user = request.user
        if user.is_authenticated:
            return Subscription.is_subscribed(user, obj)
        else:
            return False

    def get_report__adminrole(self, obj):
        """
        Include Admin users from latest report.
        """
        latest_report = obj.get_latest_report()
        if (latest_report is None) or ('AdminRole' not in latest_report.dict):
            return None

        return {"AdminRole": latest_report.dict['AdminRole']}


class SiteIndexSerializer(SiteSerializer):
    """
    A cut-down version of the SiteSerializer, for stubbing in other models.

    This provides just enough information to fetch further data, without running
    into cyclic loops.
    """
    class Meta:
        model = models.Site
        fields = (
            'api_url',
            'pk',
            'string',
            'decommissioned',
            'environment',
            'vulnerability_count'
        )


class SiteKeySerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for SiteKey model.
    """
    class Meta:
        model = models.SiteKey
        fields = (
            'value',
            'enabled',
            'description',
        )


class SiteGroupSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for information about a SiteGroup.
    """

    pk = serializers.IntegerField(read_only=True)
    sites = serializers.SerializerMethodField()
    keys = SiteKeySerializer(many=True, read_only=True)

    class Meta:
        model = models.SiteGroup
        fields = (
            'api_url',
            'pk',
            'name',
            'description',
            'notes',
            'sites',
            'keys',
        )

    def get_sites(self, sitegroup_instance):
        ordering = self.context['request'].query_params.get('ordering')
        sites = sitegroup_instance.get_sites(ordering=ordering)
        return SiteIndexSerializer(sites, many=True, read_only=True, context={'request': self.context['request']}).data
