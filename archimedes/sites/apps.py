from django.apps import AppConfig


class SitesConfig(AppConfig):
    name = "archimedes.sites"
