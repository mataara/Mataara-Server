# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Environment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, blank=True)),
                ('hostname', models.CharField(max_length=200, blank=True)),
                ('url', models.URLField(blank=True)),
                ('root', models.CharField(max_length=200, blank=True)),
                ('title', models.CharField(max_length=200, blank=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', blank=True, on_delete=models.CASCADE)),
                ('environment', models.ForeignKey(to='sites.Environment', blank=True, on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='SiteKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(unique=True, max_length=200)),
                ('enabled', models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name='site',
            name='keys',
            field=models.ManyToManyField(to='sites.SiteKey', blank=True),
        ),
    ]
