# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0005_auto_20151130_1610'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='site',
            name='content_type',
        ),
        migrations.RemoveField(
            model_name='site',
            name='object_id',
        ),
    ]
