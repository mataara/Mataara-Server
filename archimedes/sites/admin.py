from django.contrib import admin
from polymorphic.admin import PolymorphicParentModelAdmin

from archimedes.drupal.models import DrupalSite
from archimedes.silverstripe.models import SilverStripeSite

from . import models


@admin.register(models.SiteKey)
class SiteKeyAdmin(admin.ModelAdmin):
    list_display = ('value', 'enabled', 'description')
    search_fields = ('value', 'description')

    def get_queryset(self, request):
        return super(SiteKeyAdmin, self).get_queryset(request)  # pragma: no cover


@admin.register(models.Environment)
class EnvironmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'description')


@admin.register(models.Site)
class SiteAdmin(PolymorphicParentModelAdmin):
    base_model = models.Site
    child_models = (DrupalSite, SilverStripeSite)
    list_display = ('__str__', 'environment', 'url', 'hostname', 'root', 'title')
    list_filter = ('decommissioned', 'environment',)
    search_fields = ('environment__name', 'url', 'hostname', 'root', 'title')

    def get_queryset(self, request):
        return super(SiteAdmin, self).get_queryset(request)  # pragma: no cover


@admin.register(models.SiteGroup)
class SiteGroupAdmin(admin.ModelAdmin):
    base_model = models.SiteGroup
    list_display = ('name', 'description', 'notes')
    search_fields = ('name', 'description', 'notes', 'keys__value')
    filter_horizontal = ('keys',)

    def get_queryset(self, request):
        return super(SiteGroupAdmin, self).get_queryset(request).prefetch_related('keys')  # pragma: no cover
