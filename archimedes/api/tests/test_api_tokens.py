from datetime import timedelta
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from django.test.client import Client
from rest_framework import status
from rest_framework_api_key.models import APIKey


@override_settings(API_KEY_CUSTOM_HEADER="HTTP_X_API_KEY")
class APITokenTestCase(TestCase):

    def setUp(self):
        self.api_key, self.generated_key = APIKey.objects.create_key(name="Test API Key")
        self.api_client = Client({"X-Api-Key": self.generated_key})
        self.test_user, created = User.objects.get_or_create(username="testuser")
        self.auth_client = Client()
        self.auth_client.force_login(self.test_user)

    def tearDown(self):
        self.auth_client.logout()
        self.test_user.delete()

    def test_create_api_token(self):
        """Ensure we are getting an API key that makes sense."""
        self.assertEqual(self.api_key.name, "Test API Key", "Name match")
        self.assertFalse(self.api_key.revoked, "Key not revoked")
        self.assertTrue(self.generated_key.startswith(self.api_key.prefix), "Generated key prefix match")

    def test_clients(self):
        """
        Ensure test clients have the credentials.

        - client: normal anonymous user
        - api_client: client with API key
        - auth_client: logged in user
        """
        # _auth_user_id exists when Client user is logged in.
        self.assertFalse('_auth_user_id' in self.client.session)
        self.assertFalse('_auth_user_id' in self.api_client.session)
        self.assertTrue('_auth_user_id' in self.auth_client.session)

        # enforce_csrf_checks key exists when client has the custom HTTP header.
        self.assertFalse(self.client.handler.enforce_csrf_checks)
        self.assertTrue("X-Api-Key" in self.api_client.handler.enforce_csrf_checks)
        self.assertEqual(
            self.api_client.handler.enforce_csrf_checks["X-Api-Key"],
            self.generated_key
        )
        self.assertFalse(self.auth_client.handler.enforce_csrf_checks)

    def test_token_permissions(self):
        """Ensure tokens allow access to the right places."""
        # Everyone gets into the API root.
        self.assertEqual(self.client.get('/api/').status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.api_client.get('/api/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_200_OK
        )
        self.assertEqual(self.auth_client.get('/api/').status_code, status.HTTP_200_OK)

        # Only a logged in user gets user profiles
        self.assertEqual(self.client.get('/api/profile/').status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            self.api_client.get('/api/profile/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_403_FORBIDDEN
        )
        self.assertEqual(self.auth_client.get('/api/profile/').status_code, status.HTTP_200_OK)

        # Logged in or API key users get Group information.
        self.assertEqual(self.client.get('/api/groups/').status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            self.api_client.get('/api/groups/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_200_OK
        )
        self.assertEqual(self.auth_client.get('/api/groups/').status_code, status.HTTP_200_OK)

    def test_logout_denial(self):
        """Ensure access denied after user logout."""
        self.assertEqual(self.auth_client.get('/api/groups/').status_code, status.HTTP_200_OK)
        self.auth_client.logout()
        self.assertEqual(self.auth_client.get('/api/groups/').status_code, status.HTTP_403_FORBIDDEN)

    def test_revoke_denial(self):
        """Ensure access denied after API key is revoked."""
        self.assertEqual(
            self.api_client.get('/api/groups/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_200_OK
        )
        self.api_key.revoked = True
        self.api_key.save()
        self.assertEqual(
            self.api_client.get('/api/groups/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_403_FORBIDDEN
        )

    def test_expiry_denial(self):
        """Ensure access denied after API key has expired."""
        self.assertEqual(
            self.api_client.get('/api/groups/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_200_OK
        )
        # Grandfather paradox: expire before it was created to be sure it's gone.
        self.api_key.expiry_date = self.api_key.created - timedelta(days=1)
        self.api_key.save()
        self.assertEqual(
            self.api_client.get('/api/groups/', HTTP_X_API_KEY=self.generated_key).status_code,
            status.HTTP_403_FORBIDDEN
        )
