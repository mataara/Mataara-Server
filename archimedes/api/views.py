# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group, User
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_api_key.permissions import HasAPIKey

from archimedes.core.models import UserProfile

from . import serializers


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    ordering = ['id']
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = [IsAuthenticated | HasAPIKey]
    ordering = ['id']
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer


class UserProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint to allow current user's profile to be viewed (TODO: or edited).
    """
    # User profile information should not be available to anonymous services,
    # so this ViewSet should not be accessible with HasAPIKey.
    permission_classes = [IsAuthenticated]
    ordering = ['id']
    queryset = UserProfile.objects.all()
    serializer_class = serializers.UserProfileSerializer
