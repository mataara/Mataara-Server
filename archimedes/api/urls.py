# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from . import views

# Create a router (to be imported by other apps for registrations)
router = routers.DefaultRouter()

router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'profile', views.UserProfileViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^swagger/', get_swagger_view(title='Mataara API')),

    url(r'^', include('archimedes.sites.api.urls')),
    url(r'^', include('archimedes.drupal.api.urls', namespace='drupal')),
    url(r'^', include('archimedes.reports.api.urls')),
    url(r'^', include('archimedes.silverstripe.api.urls', namespace='silverstripe')),
    url(r'^', include('archimedes.subscription.api.urls')),

    # Must come last, so anything registering with "api.urls.router" will be included
    url(r'^', include(router.urls)),
]
