#!/usr/bin/env python3

import distutils.cmd
import os.path
import setuptools.command.build_py
import shutil
import subprocess

from setuptools import find_packages, setup

APPNAME = 'archimedes'


class CreateJavaScriptBundle(distutils.cmd.Command):
    """Custom command to invoke npm to build JavaScript bundle"""

    description = 'Invoke npm to build JavaScript bundle'
    user_options = [
        # The format is (long option, short option, description).
        ('javascript-dir=', None, 'path to JavaScript directory')
    ]

    def initialize_options(self):
        """Set default values for options."""
        self.javascript_dir = APPNAME + '/frontend'
        return

    def finalize_options(self):
        """Post-process options."""
        assert os.path.exists(self.javascript_dir), (
            'JavaScript directory %s does not exist.' % self.javascript_dir)

    def run(self):
        """Run npm build command."""
        assert shutil.which('npm'), ('Cannot find JavaScript executable "npm"')
        subprocess.check_call(['npm', 'run', 'build'], cwd=self.javascript_dir)


class BuildPyCommand(setuptools.command.build_py.build_py):
    """Custom build command that also performs JavaScript bundle creation"""

    def run(self):
        self.run_command('buildjavascript')
        setuptools.command.build_py.build_py.run(self)


# Get the dependancies out of our requirements file
# This will fail if the requirements file contains external references
with open(os.path.join(os.path.dirname(__file__), 'requirements/common.txt')) as f:
    reqs = [r for r in f.read().splitlines() if not r.startswith("#")]

# Read version and release info from the file RELEASE
with open(os.path.join(os.path.dirname(__file__), APPNAME + '/RELEASE')) as f:
    release_num = f.read().rstrip()

setup(
    name=APPNAME,
    version=release_num,
    packages=find_packages(exclude=("tests")),
    install_requires=reqs,
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'archimedes = archimedes:management_command',
        ],
    },
    cmdclass={
        'buildjavascript': CreateJavaScriptBundle,
        'build_py': BuildPyCommand
    }
)
