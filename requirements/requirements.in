Django~=1.11.0
cryptography
django_db_mutex<1.0
# django-polymorphic 2.0.0 has backwards-incompatible changes
# See: https://github.com/django-polymorphic/django-polymorphic/blob/master/docs/changelog.rst#changes-in-20-2018-01-22
django-polymorphic<2
Werkzeug
dateutils
django_extensions
django-filter

# REST API
djangorestframework~=3.9.0
django-rest-swagger
djangorestframework-api-key

# Compress CSS and precompilers
django-compressor
django-libsass

# For Sentry integration
raven

# For receiving emails
django-mailbox

# For scheduled tasks
celery<4.3.0
# appears to be bug in versions 11/04/2019 available mean jobs sometimes
# mysteriously stop running. version after 1.3.0
django-celery-beat==1.3.0
# django_celery_results 1.1+ requires celery>=4.3.
django_celery_results<1.1

# For parsing advisories
beautifulsoup4
lxml
feedparser
fuzzywuzzy

# Only used for archimedes to find the bower static file without an explicit 'collectstatic' call
# django-bower

# Used for unit tests
django-nose
mock
sqlparse

# For LDAP authentication
django-auth-ldap

# PostgreSQL support
psycopg2-binary

# Local cache
django-redis<4.9

# Jenkins integration, which we're not really using
django_coverage_plugin
django-jenkins

gunicorn

# Required to ensure that slugify is always unique for advisory sources
unidecode

# For SAML authentication
djangosaml2

# Webpack for ReactJS stuff
django-webpack-loader

# For version comparisons
packaging

# For testing against Drupal API
# Modified common.txt to use yarl==1.2.0 as 1.3.0 requires a newer python than is on the vagrant
vcrpy

# redis 3.x only works with very latest django-redis
redis<3.0
