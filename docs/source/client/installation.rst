.. _client_installation:

Installing the Client
==================================

**Note:** This guide assumes you are installing the Mataara Client on a Drupal
7 site. If you are using Drupal 6 or 8, replace any references to the '7.x'
branch with '6.x' or '8.x' accordingly.

Cloning from Git
-----------------------------------

To clone the Archimedes Client project from git, run the following::

  cd {drupal web root directory}/sites/all/modules
  git clone --branch 7.x https://gitlab.com/mataara/drupal-module.git mataara_client


Composer installation in Drupal 8
-----------------------------------

Add this to your ``composer.json`` repositories section::

  {
    "type": "vcs",
    "url":  "https://gitlab.com/mataara/drupal-module.git"
  }

And run ``composer require drupal/archimedes_client``.

Enabling the module
-----------------------------------

You will then need to enable the Mataara Client module on your Drupal site.

Using Drush
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  cd {drupal web root directory}
  drush en archimedes_client -y

Using the web interface
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Navigate to http://your-client-site.com/admin/modules
* Find `Archimedes Client` in the `Archimedes` section and check the 'Enabled' checkbox
* Click `Save configuration` at the bottom of the page

Initial configuration
-----------------------------------

Once installed, the client needs to be configured before it is able to send reports
to a Mataara server. The following must be setup before the client will run
correctly:

* The server location must be defined; an email address or URL, whichever is relevant to the reporting method
* A reporting method must be chosen (``email`` or ``http``)
* A public key must be set

These can be configured using the instructions in :ref:`Client Configuration <client_configuration>`.
Alternatively, a variation on the following could be used

1. **Set the server location**

::

  drush vset archimedes_client_server_email archimedes@your-server-site.com
  drush vset archimedes_client_server_url http://your-server-site.com/reports/endpoint

2. **Choose a reporting method** (choose one)

::

  drush vset archimedes_client_server_method email
  drush vset archimedes_client_server_method http

3. **Set the public key**

::

  drush ev "variable_set('archimedes_client_pubkey','`cat /path/to/publickey.pem`');"

The ``drush ev`` command is used rather than ``drush vset`` here so that Drush
doesn't mistake the starting ``----BEGIN PUBLIC...`` string for a CLI option.

Sending a test report
-----------------------------------

To verify that the client is setup correctly, you can trigger an immediate report to the server:

Further uses for Drush
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Refer to the documentation for the :ref:`arch-send <client_drush_arch_send>` Drush command.
