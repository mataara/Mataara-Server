.. _client_getting_started:

Getting Started
==================================

This section covers the Mataara client module for Drupal, including
installation, configuring settings and issuing console commands via the Drupal
shell utility, drush_.

.. _drush: http://www.drush.org/

Requirements
----------------------------------

The Drupal Mataara client is supported for Drupal 6, 7 and 8 sites, with support
planned for Drupal 9. You will
need sufficient administrator access on the site to install and configure
modules. Console access to run drush commands is not strictly necessary, but may
be useful while setting up and testing the configuration.

Sending reports to a Mataara server via email requires that your Drupal site
and server have been configured to work with a mail transfer agent (e.g. Postfix
or sendmail).

Choosing a Method
----------------------------------

Mataara supports sending encrypted messages via two methods: email and
HTTP/S. Each of them has advantages.

* Using HTTP or preferably HTTPS sends data to the server directly for immediate
  processing. However, it will need to be able to make a direct network
  connection which may require some extra firewall configuration on some
  networks. If the Mataara client cannot make a connection for some reason,
  the report will not be sent and it will have to be either re-sent manually or
  wait until the system's next scheduled reporting time.

* Using email is less direct, but in situations where the network is less
  reliable the mail transfer agent can queue messages and attempt to send them
  multiple times. Depending on the volume of messages being handled, it may also
  mean that reports come in a long time after they are generated (usually
  minutes, but possibly longer). Email messages can also be archived external to
  the Mataara server, for backup or auditing purposes.
