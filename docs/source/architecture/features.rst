.. _architecture_features:

Features
==================================

The following is a description of the Mataara system and some of its
features. For a better and more complete understanding of the software, it might
be best to download the source code of the client and server modules, and
experiment with the system in a test environment.

Mataara currently consists of a server built in Django_ and ReactJS_,
communicating via a `REST API`_, and a `Drupal CMS`_ client module. It is
intended to be an extensible system, and development of a client for the
`SilverStripe CMS`_ is underway.

.. _Django: https://www.djangoproject.com/
.. _Drupal CMS: https://www.drupal.org/
.. _ReactJS: https://reactjs.org/
.. _REST API: https://www.django-rest-framework.org/
.. _Silverstripe CMS: https://www.silverstripe.org/


Drupal Client Reporting
----------------------------------
Mataara's Drupal client is an installable module that periodically sends
reports to the Mataara server (daily by default, but configurable within
Drupal's cron system).

The primary payload of the report is a list of the modules and themes installed
on the host website, that can be checked against drupal.org's APIs to determine
whether they have known security vulnerabilities.

Reports also contain details pertinent to the security posture of the site,
including:

* A generated site key
* Hostname
* Core platform version
* Databases connected to the site
* The number of users, administrator roles and content nodes

Report data is :doc:`encoded in JSON <../development/report_format>` and
public-key encrypted during transport. Reports can be sent to the server via
either HTTP/S POST requests or by email attachment. The report format is
designed to be customisable, and new items can be added to the report by
extending a base ``ReportItem`` PHP class in the client module.

For use in the system console, Drush commands are available to send reports or
to generate a report in various formats. Settings can also be configured through
the standard Drupal administration interface, including the transport mechanism
(HTTP or email) and the server's public key for encryption.

The Drupal client module has legacy support for Drupal 6, is compatible with
Drupal 7 and 8, and will support Drupal 9. In order to ensure it can be used in
as many hosting environments as possible, has no dependencies on any other
Drupal modules.


Django Server Structure
----------------------------------
The Django server receives reports via either an HTTP/S endpoint or via email,
where they are decrypted and placed in a task processing queue. Queue workers
ingest the reports and match them to `Site` nodes within the system, updating
them with any new information contained in the reports. Reports are also
archived in their raw JSON format for reference, and potentially for further
processing in other tools.

Reports are matched against sites using their generated site key, and can also
be grouped into a "site group". This is intended for multiple versions of the
same site across different environments (e.g. development, staging, production),
to make them easier to keep track of and to store common information.

Note: At the time of writing this site group subsystem is quite new, and does
not have front-end functionality yet, but it is envisioned that this will allow
for integrations with other systems for issue management, bug tracking, customer
relationship management, etc.

The ReactJS frontend is primarily for viewing and querying site data, including
modules and security advisories. At present, server management commands are
primarily handled through the Django admin web interface, or by console
management commands.



Drupal Security Advisories and Project Release History
---------------------------------------------------------
Earlier versions of Mataara used Drupal.org advisories as the primary source of
truth for matching against insecure module versions. The data within the
advisories is sometimes hard to automatically parse. As a result, the system has
moved to using the release history API as its primary means of making Drupal
modules as insecure.

The Mataara server periodically checks the Drupal.org Release History API for
every project it has encountered, checking their release types for insecure
versions.

Drupal security advisories are still checked via the Drupal.org D7 APIs, and
saved for display to Mataara users. The legacy advisory parser will also
automatically correct errors in some older advisories. Advisory errors and
warnings will be logged to a Sentry server if you have one available.

In the frontend, sites can be checked for vulnerable module releases (including
Drupal core releases), and modules with vulnerable releases can list the sites
potentially affected by those vulnerabilities.
