.. _architecture_model:

Client-Server Model Design
==================================

Mataara uses a client-server model that practises inversion of control; the
server passively receives encrypted reports from client modules installed in the
other systems.

This allows Mataara to be a *write-only* repository of information about sites
over time, decoupled from any "dial in" command-and-control systems, so that it
can capture an understanding of the security posture of websites without needing
to have ongoing privileged access to each host.

This reduces the inbound attack surface on individual sites, and provides a
means for managing the release update cycle on sites hosted in managed secure
environments such as intranets, where it may not be desirable for them to call
out directly to public update servers for information.

The Mataara server will independently check for version updates and security
advisories, allowing them to be matched against incoming reports. A planned
future enhancement is to provide a subscription-notification system for users,
so that users who are subscribed to a site or group of sites can be notified
when a new insecure version is discovered.

.. TODO: This could probably benefit from a diagram.
