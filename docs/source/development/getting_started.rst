.. _development_getting_started:

Getting Started
==================================

.. _Django: https://www.djangoproject.com/
.. _Python: https://www.python.org/
.. _ReactJS: https://reactjs.org/

.. note::

  If you are familiar with the project or just want to have a go at standing up
  the default development environment check the
  `quickstart guide <quickstart.html>`_.


The Mataara server is built on Django_ using Python_ with a ReactJS_ frontend.

Coding Style
-----------------------------------
As the server is built on Django, Mataara has adopted the `Django coding style
guidelines <https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/>`_.

.. _Flake8: https://pypi.python.org/pypi/flake8

To assist in identifying potential style problems, the Flake8_ Python package is used.
It is invoked automatically in the git pre-commit hook to prevent errors from being committed into the repo.

Repositories
-----------------------------------
There are `different repositories <https://gitlab.com/mataara>`_ for the server,
and client. The server repository contains both the Django server and API, and
the ReactJS frontend.

Network Requirements
-----------------------------------

In addition to the network requirements for production servers, development/build environments
require outbound HTTP(S) to the Internet for the following:

* https://forge.puppet.com - Puppet Forge
* http://archive.ubuntu.com/ - Ubuntu update repositories or a local mirror
* https://deb.nodesource.com - Nodejs Ubuntu repository
* https://www.npmjs.com/ - NPM repository
* https://pypi.python.org - the Python Package Index, as used by pip
