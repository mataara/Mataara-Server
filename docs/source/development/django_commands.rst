.. _development_django_commands:

Useful Commands
======================================

Standard Django Commands
--------------------------------------

There are many built-in Django commands that may be useful in a development environment.
In typical documentation on the web you may see references to invoking these such as::

  python manage.py shell_plus

Inside our vagrant setup, these can be more easily invoked using the following without worrying
about paths or where files are::

  archimedes shell_plus

A full list of standard commands, those added by packages, and by Mataara can be seen by just running::

  archimedes help

Detailed help on any individual command is available by running::

  archimedes help <command>

.. csv-table:: Common useful commands in development environments
  :header: "Command", "Source", "Description"
  :widths: 20, 20, 40

  "makemigrations", "django", "Creates any new migrations required"
  "migrate", "django", "Update database schema by applying migrations"
  "squashmigrations", "django", "Squash a set of migrations into a single one"
  "shell_plus", "django_extensions", "Python interactive shell with Django models loaded"
  "test", "django_nose", "Run our defined system tests"
  "collectstatic", "staticfiles", "Collect static files in a single location"
  "runserver", "staticfiles", "Start a lightweight development web server"
  "check", "django", "Check the project for potential problems"
  "celery", "djcelery", "Manage scheduled jobs"
  "getmail", "django_mailbox", "Retrieve email from any configured mailbox"

Useful Celery Commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Initiate a manual trigger of scheduled celery tasks::

  archimedes celery beat

View details of current celery workers::

  archimedes celery worker

Custom Mataara Commands
--------------------------------------

.. csv-table:: Additional Mataara specific commands
  :header: "Command", "Source", "Description"
  :widths: 20, 20, 40

  "addadvisorysource", "core", "Add new advisory sources"
  "importadvisories", "drupal", "Import advisories from configured sources"
  "parseadvisories", "drupal", "Parse the advisories that have been imported"
  "setsuperuser", "core", "Assigned superuser rights to the specified user"

**TODO - The advisory commands are likely to move from a drupal category to a generic one**
