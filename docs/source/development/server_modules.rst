.. _development_server_modules:


Report Processing
==================================
Reports are processed by calling ``<your_module>.utils.process_report(report)``.
You must provide this function even if you don't plan to add aditional
processing logic. You module name must match the 'ReportType' field in the
report sent by your client module.
