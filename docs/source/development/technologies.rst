.. _development_technologies:

Key Development Technologies
==================================

We're using the following technologies:

`bandit <https://pypi.python.org/pypi/bandit/1.4.0>`_
  Security static code analyser for Python code

`celery <http://www.celeryproject.org/>`_
  Asyncronous task queue/job queue used internally within Mataara and for scheduled jobs

`fabric <http://www.fabfile.org/>`_
  Provides developers with shortcuts to standard tasks, such as running the server

`flake8 <https://pypi.python.org/pypi/flake8>`_
  Perform Python checks for linting errors

`Gunicorn <http://gunicorn.org/>`_ 'Green Unicorn'
  A Python WSGI HTTP server. Designed to be lightweight and speedy.

`pip <https://pypi.python.org/pypi/pip/>`_
  Perform Python package management

`puppet <https://puppet.com/>`_
  Used by Vagrant to configure Mataara inside the virtual machine

`redis <https://redis.io/>`_
  In-memory data store used by celery

`Sphinx <http://www.sphinx-doc.org/en/stable/>`_
  Used to generate this documentation

`supervisor <http://supervisord.org/>`_
  Used to control system services outside of the main Django application, such as celery

`Vagrant <https://www.vagrantup.com/>`_
  Used for managing and initialising the development environment inside VirtualBox

`VirtualBox <https://www.virtualbox.org/>`_
  Provides virtualisation of a development environment containing the server

`wheel <https://pypi.python.org/pypi/wheel>`_
  Used for packing Python modules and applications for easy installation
