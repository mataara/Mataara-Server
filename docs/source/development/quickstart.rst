.. _development_quickstart:

Quick Start guide
=================

This quick start guide is aimed at people that have stood up the development
environment before or people that want to have a go at just diving in.

Initial assumption: you're using a Debian-based \*nix.

This should be copy/pastable into a terminal. Just start in the directory you
want your development environment to start in.::

  # Get the recommended tools
  sudo apt-get install virtualbox vagrant fabric python3-pip python3-setuptools python3-bandit
  # Get the repo
  git clone git@gitlab.com:mataara/Mataara-Server.git mataara
  cd mataara
  # Install git pre-commit hook etc
  fab setup_dev_env
  # Put the default config in place
  cp conf/hiera/config.yaml.example conf/hiera/config.yaml
  # Get vagrant extension for hostnames
  vagrant plugin install vagrant-hostsupdater
  # Start the VM
  vagrant up
  # Set up the default advisory sources
  fab setup_feeds
  # Start the ReactJS server.
  fab runjs

``fab runjs`` will remain running but at this point you should be able to point
your browser at http://mataara-server.local:8008 and login with ``vagrant`` and
the password ``vagrant``.

If you edited ``conf/hiera/config.yaml`` above port and/or login details may
have changed.  If you've edited that file though, we assume that you know enough
already to work out how to access and log in to your development environment.
Similarly, the hostname can be edited in ``Vagrantfile``.

References
----------

- |get_tools|
- |repo_link|
- |install_git_bits|
- |config_file|
- |vagrant_up|
- |setup_feeds|
- |start_reactjs|


.. |get_tools| replace:: :ref:`Get the recommended tools <install_dependencies>`
.. |repo_link| replace:: :ref:`Get the repo <cloning_from_git>`
.. |install_git_bits| replace:: :ref:`Install git pre-commit hook etc <fab_setup_dev_env>`
.. |config_file| replace:: :ref:`Put the default config in place <config_mataara_server>`
.. |vagrant_up| replace:: :ref:`Start the VM <stand_up_the_vm>`
.. |setup_feeds| replace:: :ref:`Set up the default advisory sources <post_install_config>`
.. |start_reactjs| replace:: :ref:`Start the ReactJS server <development_reactjs>`
