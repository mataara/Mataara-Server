.. _development_repo_structure:

Source code repository structure
======================================

Directories in version control
--------------------------------------

archimedes
  Django application source code

bin
  Scripts used in the vagrant virtual machine

conf
  Sample and live configs used in vagrant

manifests
  Puppet manifest used in the vagrant virtual machine

puppet
  Puppet template used in the vagrant virtual machine

requirements
  Python dependencies required in different environments

Directories not in version control
--------------------------------------

.vagrant
  Internal settings managed by Vagrant

archimedes.egg-info
  Created if the wheelhouse has been built locally

build
  Temporary directory that may be used as part of a build process

dist
  Temporary directory that may be used as part of a build process

reportkey
  Contains the encryption key configured in the virtual machine for receiving reports.

venv
  Contains the 'venv' that Mataara is installed into when running in Vagrant.

Key files
--------------------------------------

.gitignore
  Defines what git should ignore and not check into the repository

.gitlab-ci.yml
  Definitions file for Gitlab CI

CHANGELOG.md
  Project changelog file

fabfile.py
  Fabric tasks used by developers

hiera_config.yaml
  Tells Hiera where to pull our config.yaml from.
LICENSE
  Project open source license
MANIFEST.in
  Used as part of the wheelbuilding process
README.md
  Project README
requirements.txt
  Shortcut to ``requirements/common.txt``
setup.cfg
  Configuration file for flake8
setup.py
  Python management file for Mataara
Vagrantfile
  Configuration file for Vagrant

archimedes/RELEASE
  Internal version and release number
