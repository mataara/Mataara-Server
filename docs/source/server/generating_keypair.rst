.. _server_generating_keypair:

Generating an OpenSSL key-pair
==================================

Mataara uses an OpenSSL RSA key pair to encrypt reports, and supports encrypting
the private key with a passphrase. If you don't already have a key pair, you can
generate one using the OpenSSL tools.

The key pair can also be used for SAML communications during login. You can optionally
create a second key pair just for SAML, or re-use the first key pair. Note that SAML
requires the public key to be in an X509 certificate. A passphrase can be used to protect
the private key used for reports, but not for SAML authentication.

To generate the private key without protecting it with a passphrase::

  openssl genrsa -out private.pem 4096

To generate a random passphrase and then create a private key protected by a passphrase::

  pwgen -1s 20
  openssl genrsa -des3 -out private.pem 4096

For encryption reports, you need to export the public key::

  openssl rsa -in private.pem -outform PEM -pubout -out public.pem

For SAML, you need to generate a certificate containing the public key::

  openssl req -new -x509 -key private.pem -out publickey.cer

Further information
----------------------------------

Still confused? Checkout this blog for a more in-depth guide on how OpenSSL keys work:
https://rietta.com/blog/2012/01/27/openssl-generating-rsa-key-from-command/
