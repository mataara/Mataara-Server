Mataara Documentation
======================================

Mataara is a security reporting tool that collects information on websites and
their dependencies, uploading reports to a server that monitors module
vulnerabilities and site configuration changes from a central location.

The primary intent of the Mataara project is to provide a system that helps site
hosting and development organisations to manage and secure their software
configurations across large numbers of sites.

The documentation for Mataara is organised into a number of sections:

* :ref:`System Architecture <architecture_features>`
* :ref:`Drupal Client <client_getting_started>`
* :ref:`Server <server_getting_started>`
* :ref:`Development <development_getting_started>`
* :ref:`Development Quickstart Guide <development_quickstart>`

The `Mataara <https://gitlab.com/mataara>`_ project was created at `Catalyst IT <https://catalyst.net.nz>`_.

Name
---------
An earlier version of this project was known as *Archimedes*, and a significant
proportion of the code still refers to itself as such. Because naming things is
hard, and there were already several other projects called Archimedes out there,
it was decided to change the name to something more distinctive.

**Mataara** (pronounced by syllable: *ma-ta-a-ra*) is a word from the Māori
language that `means <https://maoridictionary.co.nz/word/3754>`_ vigilance,
alertness, watchfulness. (It is unrelated to the `Kenyan tea factory
<https://en.wikipedia.org/wiki/Mataara>`_ of the same name.)


.. _architecture-docs:
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Architecture

   architecture/features
   architecture/model
   architecture/frontend

.. _client-docs:
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Mataara Client

   client/getting_started
   client/installation
   client/configuration
   client/drush

.. _server-docs:
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Server

  server/getting_started
  server/installation
  server/generating_keypair
  server/configuration
  server/saml

.. _development:
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Development

  development/getting_started
  development/quickstart
  development/setup
  development/fabric
  development/django_commands
  development/database
  development/reactjs
  development/server_modules
  development/report_format
  development/report_format_legacy
  development/technologies
  development/repo_structure

.. _server-api:
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Server API

  server/api/modules
