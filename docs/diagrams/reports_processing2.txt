@startuml

cloud {
  [Website agents] as agents
}

node "Django Components" {
package "reports" {
  package "utils" {
 	  [decode_and_save()]
	  [process_report()]
  }
  package "tasks" {
	  [process(pk)]
    [process_email()]
  }
  [reports/endpoint] <<reports API>>
  [getmail CMD] <<reports API>>
  [models.Report]
  storage "queues" {
	storage "mail_queue"
	storage "web_queue"
  }
}

package "sites" {
  [sites.utils.process_report()]
}

package "drupalSites" {
  [drupalSites.utils.process_report()]
}
}

database "Postgres" {
  [DrupalSite]
  [Report]
  [Site]
}

[agents] --> [reports/endpoint]: HTTP(S)
[agents] --> Mailbox: SMTP
Mailbox <-- [getmail CMD]: IMAP
[reports/endpoint] --> [web_queue]

[getmail CMD] --> [mail_queue]
[mail_queue] <-- [process_email()]
[process_email()] --> [decode_and_save()]
[decode_and_save()] --> [models.Report]
[decode_and_save()] --> [process(pk)]
[process(pk)] --> [process_report()]
[process_report()] --> [models.Report]
[models.Report] --> [Report]
[process_report()] --> [drupalSites.utils.process_report()]
[drupalSites.utils.process_report()] --> [DrupalSite]
@enduml
