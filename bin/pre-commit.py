#!/usr/bin/env python

# Based on the version at

# This pre-commit git hook prevents you from committing syntax errors
# or debug code.
#
# It performs sanity checks on the code you are trying to commit (and ONLY
# the code you are going to commit; or rather, the versions of the files
# resulting from the commit -- so unstaged debug statements will NOT trigger
# validation errors, while syntax errors with unstaged fixes WILL do).
#
# To force a commit that breaks the rules (e.g. when debug code is 100%
# required) you can use 'git commit --no-verify' to bypass the pre-commit
# hook entirely.
#
# To enable this hook, create a symlink to this script from the standard
# pre-commit file path. Run this command from the root of the repository:
#
# ln -s ../../bin/pre-commit.sh .git/hooks/pre-commit.py
#
# Note: Using a symlink is recommended we this script can be kept under
# source control.
#
# Helpful git aliases are:
#
# git config --global alias.gc commit
# git config --global alias.gcv commit --no-verify
#
# Original version: https://gist.github.com/spulec/1364640
#
# Changelog
#
# - comment out call to Django validate as our manage.py is in the vagrant image
# - Replace pyflakes and pep8 with flake8
# - Remove Yipit specific sections

import os
import re
import subprocess
import sys

modified = re.compile(b'^[MA]\s+(?P<name>.*)$')

CHECKS = [
    {
        'output': 'Checking for pdbs...',
        'command': 'grep -n "import pdb" %s',
        'ignore_files': ['.*pre-commit'],
        'print_filename': True,
    },
    {
        'output': 'Checking for ipdbs...',
        'command': 'grep -n "import ipdb" %s',
        'ignore_files': ['.*pre-commit'],
        'print_filename': True,
    },
    {
        'output': 'Checking for debugger...',
        'command': 'grep -n debugger %s',
        'match_files': ['.*\.js$'],
        'print_filename': True,
    },
    {
        'output': 'Running flake8...',
        'command': 'flake8 %s',
        'match_files': ['.*\.py$'],
        'ignore_files': ['venv', 'build', 'wheels', 'dist', '.*migrations.*', 'docs', 'frontend'],
        'print_filename': False,
    },
    # We're going to use flake8 so don't need to run pyflakes or pep8 directly
    #    {
    #        'output': 'Running Pyflakes...',
    #        'command': 'pyflakes %s',
    #        'match_files': ['.*\.py$'],
    #        'ignore_files': ['.*settings/.*', '.*manage.py', '.*migrations.*', '.*/terrain/.*'],
    #        'print_filename': False,
    #    },
    #    {
    #        'output': 'Running pep8...',
    #        'command': 'pep8 -r --ignore=E501,W293 %s',
    #        'match_files': ['.*\.py$'],
    #        'ignore_files': ['.*migrations.*'],
    #        'print_filename': False,
    #    },
    {
        'output': 'Checking for Sass changes...',
        'command': 'sass --quiet --update %s',
        'match_files': ['.*\.scss$'],
        'print_filename': True,
    },
]


def matches_file(file_name, match_files):
    return any(re.compile(match_file).match(file_name) for match_file in match_files)


def check_files(files, check):
    result = 0
    print(check['output'])
    for file_name in files:
        # Python 2/3 compatibility: coerce into string
        if type(file_name) is bytes:
            file_name = file_name.decode()
        if 'match_files' not in check or matches_file(file_name, check['match_files']):
            if 'ignore_files' not in check or not matches_file(file_name, check['ignore_files']):
                process = subprocess.Popen(check['command'] % file_name,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE,
                                           shell=True)
                out, err = process.communicate()
                if out or err:
                    if check['print_filename']:
                        prefix = '\t%s:' % file_name
                    else:
                        prefix = '\t'
                    output_lines = ['%s%s' % (prefix, line) for line in out.splitlines()]
                    print('\n'.join(output_lines))
                    if err:
                        print(err)
                    result = 1
    return result


def main(all_files):
    # Stash any changes to the working tree that are not going to be committed
    subprocess.call(['git', 'stash', '-u', '--keep-index'], stdout=subprocess.PIPE)

    files = []
    if all_files:
        for root, dirs, file_names in os.walk('.'):
            for file_name in file_names:
                files.append(os.path.join(root, file_name))
    else:
        p = subprocess.Popen(['git', 'status', '--porcelain'], stdout=subprocess.PIPE)
        out, err = p.communicate()
        for line in out.splitlines():
            match = modified.match(line)
            if match:
                files.append(match.group('name'))

    result = 0

    # We're not going to run the validator because it requires vagrant to be up
    #
    # print('Running Django Code Validator...')
    # return_code = subprocess.call('$VIRTUAL_ENV/bin/python manage.py validate', shell=True)
    # result = return_code or result

    for check in CHECKS:
        result = check_files(files, check) or result

    # Unstash changes to the working tree that we had stashed
    subprocess.call(['git', 'reset', '--hard'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call(['git', 'stash', 'pop', '--quiet', '--index'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sys.exit(result)


if __name__ == '__main__':
    all_files = False
    if len(sys.argv) > 1 and sys.argv[1] == '--all-files':
        all_files = True
    main(all_files)
