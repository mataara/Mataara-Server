#!/bin/bash

## This script runs as 'vagrant' and sets up the venv on vagrant instances.
## It assumes that $VENV is already created and owned by 'vagrant'

set -e
set -x

VENV=/vagrant/venv

cd $(dirname $0)/..

./bin/mk-venv.sh ${VENV}

${VENV}/bin/pip install -r /vagrant/requirements/common.txt
${VENV}/bin/pip install -r /vagrant/requirements/dev.txt
${VENV}/bin/python ./setup.py develop
